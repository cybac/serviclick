<?php

return [
    'characters' => ['2', '3', '4', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'm', 'n', 'p', 'q', 'r', 't', 'u', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'M', 'N', 'P', 'Q', 'R', 'T', 'U', 'X', 'Y', 'Z'],
    'default' => [
        'length' => 9,
        'width' => 120,
        'height' => 36,
        'quality' => 90,
        'math' => false,
        'expire' => 60,
        'encrypt' => false,
    ],
    'math' => [
        'length' => 9,
        'width' => 120,
        'height' => 36,
        'quality' => 90,
        'math' => true,
    ],

    'flat' => [
        'length'        => 8,
        'width'         => 270,
        'height'        => 70,
        'quality'       => 90,
        'lines'         => 6,
        'bgImage'       => false,
        'bgColor'       => '#27343e',
        'fontColors'    => ['#FFFFFF', '#FFB3B3', '#59FF00', '#33D3FF', '#C0C0C0', '#EEB2AA', '#22D8B4', '#EEB5AF', '#D2BAE3', '#BBC1E7', '#FFB266', '#DAC7BE', '#FFB3F7'],
        'contrast'      => 0,
        'expire'        => 60,
    ],
    'mini' => [
        'length' => 3,
        'width' => 60,
        'height' => 32,
    ],
    'inverse' => [
        'length' => 5,
        'width' => 120,
        'height' => 36,
        'quality' => 90,
        'sensitive' => true,
        'angle' => 12,
        'sharpen' => 10,
        'blur' => 2,
        'invert' => true,
        'contrast' => -5,
    ]
];
