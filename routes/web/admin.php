<?php
//Ruta para los administradores
Route::group(['middleware' => ['auth', 'role:administrador'], 'prefix' => 'control_sc'], function () {
    Route::get('/', 'HomeController@administrador')->name('control_sc');
    Route::post('status_equipo', 'EquipoController@status')->name('status_equipo');
    Route::post('status_criterio', 'CriteriosController@status')->name('status_criterio');
    Route::post('status_slider', 'SliderFortalezasController@status')->name('status_slider');
    Route::post('status_comentario', 'ComentariosController@status')->name('status_comentario');
    Route::post('status_negocio', 'AdministradorController@status_negocio')->name('status_negocio');
    Route::post('status_testimonial', 'TestimonialesController@status')->name('status_testimonial');
    Route::post('status_documentacion', 'TiposDocumentosController@status')->name('status_documentacion');
    Route::post('status_redes', 'RedesSocialesController@status')->name('status_redes');
    Route::post('accion_comentario', 'ComentariosController@aprobar_denegar')->name('accion_comentario');
    Route::post('status_anuncio', 'AnunciosController@status')->name('status_anuncio');
    Route::post('status_negocio_pago', 'AdministradorController@status_negocio_pago')->name('status_negocio_pago');

    Route::prefix('password_reset')->group(function () {
        Route::get('/', 'AdministradorController@password_reset')->name('control_sc_password');
        Route::post('actualizar', 'AdministradorController@actualizar_password')->name('actualizar_password');
    });

    Route::prefix('negocios')->group(function () {
        Route::get('/', 'AdministradorController@negocios')->name('control_sc_negocios');
        Route::post('datatable', 'AdministradorController@datatable_negocios')->name('dt_negocios');
        Route::get('editar/{id_negocio}', 'AdministradorController@editar_negocio')->name('editar_negocio');
        Route::post('guardar', 'AdministradorController@guardar_negocio')->name('guardar_negocio');
        Route::post('actualizar/{id_negocio}', 'AdministradorController@actualizar_negocio')->name('actualizar_negocio');
        Route::post('eliminar/{id_negocio}', 'AdministradorController@eliminar_negocio')->name('eliminar_negocio');

        Route::prefix('tramites')->group(function () {
            Route::get('/', 'TramitesController@index')->name('control_sc_negocios_tramites');
            Route::post('datatable', 'TramitesController@datatable')->name('dt_negocios_tramites');
            Route::post('seguimiento', 'TramitesController@seguimiento')->name('seguimiento_negocio');
            Route::post('eliminar/{id_negocio}', 'TramitesController@eliminar')->name('eliminar_tramite_negocio');
            Route::post('renovar_ficha', 'TramitesController@ficha')->name('renovar_ficha_admin');

            Route::prefix('documentos')->group(function () {
                Route::get('/', 'TramitesController@volver')->name('tramites_documentos');
                Route::get('/{id_negocio}', 'TramitesController@documentos')->name('documentos_tramite');
                Route::post('datatable/{id_negocio}', 'TramitesController@datatable_documentos')->name('dt_documentos_tramite');
                Route::post('rechazar', 'TramitesController@rechazar_documentacion')->name('rechazar_documento');
            });

            Route::prefix('informacion')->group(function () {
                Route::get('/', 'TramitesController@volver')->name('tramites_informacion');
                Route::get('/{id_negocio}', 'TramitesController@informacion')->name('informacion_negocio');
                Route::post('aprobar', 'TramitesController@aprobar_informacion')->name('aprobar_informacion');
                Route::post('rechazar', 'TramitesController@rechazar_informacion')->name('rechazar_informacion');
            });
        });

        Route::prefix('suspendidos')->group(function () {
            Route::get('/', 'SuspendidosController@index')->name('control_sc_negocios_suspendidos');
            Route::post('datatable', 'SuspendidosController@datatable')->name('dt_negocios_suspendidos');
        });

        Route::prefix('recordatorios')->group(function () {
            Route::prefix('{id_negocio}')->group(function () {
                Route::get('/', 'AdministradorController@recordatorios')->name('control_sc_recordatorios_negocios');
                Route::post('datatable', 'AdministradorController@datatable_recordatorios')->name('dt_recordatorios_negocios');
                Route::get('enviar', 'AdministradorController@crear_recordatorio')->name('control_sc_crear_recordatorio');
                Route::post('enviar_recordatorio', 'AdministradorController@enviar_recordatorio')->name('control_sc_enviar_recordatorio');
            });
        });

        Route::prefix('keysword')->group(function () {
            Route::get('/', 'AdministradorController@volver')->name('keysword');
            Route::get('/{id_negocio}', 'AdministradorController@editar_keysword')->name('keysword_negocio');
            Route::post('guardar_keysword/{id_negocio}', 'AdministradorController@guardar_keysword')->name('guardar_keysword_negocio');
        });

        Route::prefix('comentarios')->group(function () {
            Route::get('/', 'AdministradorController@all_comentarios')->name('comentarios');
            Route::get('/{id_negocio}', 'AdministradorController@comentarios')->name('comentarios_negocio');
            Route::post('datatable', 'ComentariosController@datatable_all')->name('dt_all_comentarios');
            Route::post('datatable/{id_negocio}', 'ComentariosController@datatable')->name('dt_comentarios');
            Route::post('eliminar/{id_negocio}', 'ComentariosController@eliminar')->name('eliminar_comentario');
        });

        Route::prefix('documentos')->group(function () {
            Route::get('/', 'AdministradorController@volver')->name('documentos');
            Route::get('/{id_negocio}', 'DocumentosController@index')->name('documentos_negocio');
            Route::get('subir/{id_negocio}', 'DocumentosController@crear')->name('crear_documento_negocio');
            Route::get('editar/{id_documento}', 'DocumentosController@editar')->name('editar_documento_negocio');
            Route::post('datatable/{id_negocio}', 'DocumentosController@datatable')->name('dt_documentos');
            Route::post('guardar/{id_negocio}', 'DocumentosController@guardar')->name('guardar_documento_negocio');
            Route::post('actualizar/{id_documento}', 'DocumentosController@actualizar')->name('actualizar_documento_negocio');
            Route::post('eliminar/{id_documento}', 'DocumentosController@eliminar')->name('eliminar_documento_negocio');
        });

        Route::prefix('documentacion')->group(function () {
            Route::get('/', 'TiposDocumentosController@index')->name('control_sc_documentacion');
            Route::post('datatable', 'TiposDocumentosController@datatable')->name('dt_documentacion');
            Route::post('guardar', 'TiposDocumentosController@guardar')->name('guardar_documentacion');
            Route::post('actualizar/{id_documentacion}', 'TiposDocumentosController@actualizar')->name('actualizar_documentacion');
            Route::post('aprobar', 'TramitesController@aprobar_documentacion')->name('aprobar_documentacion');
        });

        Route::prefix('logo')->group(function () {
            Route::get('/', 'AdministradorController@volver')->name('logo');
            Route::get('/{id_negocio}', 'AdministradorController@logo_negocio')->name('logo_negocio');
            Route::get('/{id_negocio}/editar', 'AdministradorController@editar_logo_negocio')->name('editar_logo_negocio');
            Route::post('guardar/{id_negocio}', 'AdministradorController@guardar_logo_negocio')->name('guardar_logo_negocio');
            Route::post('actualizar/{id_negocio}', 'AdministradorController@actualizar_logo_negocio')->name('actualizar_logo_negocio');
        });

        Route::prefix('galeria')->group(function () {
            Route::get('/', 'AdministradorController@volver')->name('galeria');
            Route::get('/{id_negocio}', 'GaleriaTrabajosController@editar')->name('editar_galeria_negocio');
            Route::post('guardar', 'GaleriaTrabajosController@guardar')->name('guardar_galeria_negocio');
            Route::post('ordenar', 'GaleriaTrabajosController@ordenar')->name('ordenar_galeria_negocio');
            Route::post('eliminar', 'GaleriaTrabajosController@eliminar')->name('eliminar_imagen_galeria_negocio');
        });
    });

    Route::prefix('historial')->group(function () {
        Route::get('/', 'AdministradorController@historial')->name('control_sc_pagos');
        Route::get('/{id_negocio}', 'PagosController@historial_negocio')->name('pagos_negocio');
        Route::post('datatable', 'PagosController@datatable_admin')->name('dt_historial_pagos');
        Route::post('datatable_negocio', 'PagosController@datatable_negocio')->name('dt_historial_pagos_negocio');
        Route::post('exonerar', 'PagosController@exonerar')->name('exonerar_pago');
    });

    Route::prefix('correos')->group(function () {
        Route::get('/', 'AdministradorController@correos')->name('control_sc_emails');
        Route::post('datatable', 'CorreosController@datatable')->name('dt_correos');
        Route::get('editar/{id_categoria}', 'CorreosController@editar')->name('editar_correo');
        Route::post('actualizar/{id_correo}', 'CorreosController@actualizar')->name('actualizar_correo');
    });

    Route::prefix('notificaciones')->group(function () {
        Route::get('/', 'AdministradorController@notificaciones')->name('control_sc_notificaciones');
        Route::post('datatable', 'NotificacionesController@datatable')->name('dt_notificaciones');
        Route::post('actualizar/{id_notificacion}', 'NotificacionesController@actualizar')->name('actualizar_notificacion');
    });

    Route::prefix('precios')->group(function () {
        Route::get('/', 'AdministradorController@precio')->name('control_sc_precios');
        Route::post('actualizar', 'AdministradorController@actualizar_precio')->name('actualizar_precio');
    });

    Route::prefix('anuncios')->group(function () {
        Route::get('/', 'AdministradorController@anuncios')->name('control_sc_anuncios');
        Route::post('actualizar', 'AnunciosController@actualizar')->name('actualizar_anuncio');
    });

    Route::prefix('categorias')->group(function () {
        Route::get('/', 'AdministradorController@categorias')->name('control_sc_categorias');
        Route::post('datatable', 'CategoriasController@datatable')->name('dt_categorias');
        Route::get('crear', 'CategoriasController@crear')->name('crear_categoria');
        Route::get('editar/{id_categoria}', 'CategoriasController@editar')->name('editar_categoria');
        Route::post('guardar', 'CategoriasController@guardar')->name('guardar_categoria');
        Route::post('actualizar/{id_categoria}', 'CategoriasController@actualizar')->name('actualizar_categoria');
    });

    Route::prefix('ubicaciones')->group(function () {
        Route::get('/', 'AdministradorController@ubicaciones')->name('control_sc_ubicaciones');
        Route::post('datatable', 'UbicacionesController@datatable')->name('dt_ubicaciones');
        Route::get('crear', 'UbicacionesController@crear')->name('crear_ubicacion');
        Route::get('editar/{id_negocio}', 'UbicacionesController@editar')->name('editar_ubicacion');
        Route::post('guardar', 'UbicacionesController@guardar')->name('guardar_ubicacion');
        Route::post('actualizar/{id_negocio}', 'UbicacionesController@actualizar')->name('actualizar_ubicacion');
    });

    Route::prefix('criterios')->group(function () {
        Route::get('/', 'AdministradorController@criterios')->name('control_sc_criterios');
        Route::post('datatable', 'CriteriosController@datatable')->name('dt_criterios');
        Route::post('guardar', 'CriteriosController@guardar')->name('guardar_criterio');
        Route::post('actualizar/{id_criterio}', 'CriteriosController@actualizar')->name('actualizar_criterio');
        Route::post('eliminar/{id_criterio}', 'CriteriosController@eliminar')->name('eliminar_criterio');
    });

    Route::prefix('web')->group(function () {
        Route::get('/', 'AdministradorController@regresar')->name('control_sc_web');
        Route::prefix('slider_text')->group(function () {
            Route::get('/', 'AdministradorController@slider_text')->name('control_sc_web_slider_text');
            Route::post('datatable', 'SliderFortalezasController@datatable')->name('dt_slider_text');
            Route::get('crear', 'SliderFortalezasController@crear')->name('crear_slider_text');
            Route::get('editar/{id_slider}', 'SliderFortalezasController@editar')->name('editar_slider_text');
            Route::post('guardar', 'SliderFortalezasController@guardar')->name('guardar_slider_text');
            Route::post('actualizar/{id_slider}', 'SliderFortalezasController@actualizar')->name('actualizar_slider_text');
            Route::post('eliminar/{id_slider}', 'SliderFortalezasController@eliminar')->name('eliminar_slider_text');
        });

        Route::prefix('beneficios')->group(function () {
            Route::get('/', 'AdministradorController@beneficios')->name('control_sc_web_beneficios');
            Route::post('datatable', 'FortalezasController@datatable')->name('dt_beneficios');
            Route::get('editar/{id_beneficio}', 'FortalezasController@editar')->name('editar_beneficio');
            Route::post('actualizar/{id_beneficio}', 'FortalezasController@actualizar')->name('actualizar_beneficio');
        });

        Route::prefix('testimoniales')->group(function () {
            Route::get('/', 'AdministradorController@testimoniales')->name('control_sc_web_testimoniales');
            Route::post('datatable', 'TestimonialesController@datatable')->name('dt_testimoniales');
            Route::get('crear', 'TestimonialesController@crear')->name('crear_testimonial');
            Route::get('editar/{id_testimoniales}', 'TestimonialesController@editar')->name('editar_testimonial');
            Route::post('guardar', 'TestimonialesController@guardar')->name('guardar_testimonial');
            Route::post('actualizar/{id_testimoniales}', 'TestimonialesController@actualizar')->name('actualizar_testimonial');
            Route::post('eliminar/{id_testimoniales}', 'TestimonialesController@eliminar')->name('eliminar_testimonial');
        });

        Route::prefix('sucursales')->group(function () {
            Route::get('/', 'AdministradorController@sucursales')->name('control_sc_web_sucursales');
            Route::post('datatable', 'SucursalesController@datatable')->name('dt_sucursales');
            Route::get('crear', 'SucursalesController@crear')->name('crear_sucursal');
            Route::get('editar/{id_sucursal}', 'SucursalesController@editar')->name('editar_sucursal');
            Route::post('guardar', 'SucursalesController@guardar')->name('guardar_sucursal');
            Route::post('actualizar/{id_sucursal}', 'SucursalesController@actualizar')->name('actualizar_sucursal');
            Route::post('eliminar/{id_sucursal}', 'SucursalesController@eliminar')->name('eliminar_sucursal');
        });

        Route::prefix('equipo')->group(function () {
            Route::get('/', 'AdministradorController@equipo')->name('control_sc_web_equipo');
            Route::post('datatable', 'EquipoController@datatable')->name('dt_equipo');
            Route::get('crear', 'EquipoController@crear')->name('crear_equipo');
            Route::get('editar/{id_equipo}', 'EquipoController@editar')->name('editar_equipo');
            Route::post('guardar', 'EquipoController@guardar')->name('guardar_equipo');
            Route::post('actualizar/{id_equipo}', 'EquipoController@actualizar')->name('actualizar_equipo');
            Route::post('eliminar/{id_equipo}', 'EquipoController@eliminar')->name('eliminar_equipo');
        });

        Route::prefix('experiencia')->group(function () {
            Route::get('/', 'AdministradorController@experiencia')->name('control_sc_web_experiencia');
            Route::post('actualizar', 'ExperienciaController@actualizar')->name('actualizar_experiencia');
        });

        Route::prefix('habilidades')->group(function () {
            Route::get('/', 'AdministradorController@habilidades')->name('control_sc_web_habilidades');
            Route::post('datatable', 'HabilidadesController@datatable')->name('dt_habilidades');
            Route::get('crear', 'HabilidadesController@crear')->name('crear_habilidad');
            Route::get('editar/{id_habilidad}', 'HabilidadesController@editar')->name('editar_habilidad');
            Route::post('guardar', 'HabilidadesController@guardar')->name('guardar_habilidad');
            Route::post('actualizar/{id_habilidad}', 'HabilidadesController@actualizar')->name('actualizar_habilidad');
            Route::post('eliminar/{id_habilidad}', 'HabilidadesController@eliminar')->name('eliminar_habilidad');
        });

        Route::prefix('informacion')->group(function () {
            Route::get('/', 'AdministradorController@informacion')->name('control_sc_web_informacion');
            Route::post('actualizar', 'InformacionController@actualizar')->name('actualizar_informacion');
        });

        Route::prefix('redes')->group(function () {
            Route::get('/', 'AdministradorController@redes_sociales')->name('control_sc_web_redes');
            Route::post('ordenar', 'RedesSocialesController@ordenar')->name('ordenar_redes_sociales');
            Route::post('actualizar', 'RedesSocialesController@actualizar')->name('actualizar_redes');
        });

        Route::prefix('about')->group(function () {
            Route::get('/', 'AdministradorController@about')->name('control_sc_web_about');
            Route::post('actualizar', 'AboutController@actualizar')->name('actualizar_about');
        });

        Route::prefix('legales')->group(function () {
            Route::get('privacidad', 'AdministradorController@privacy')->name('control_sc_web_privacy');
            Route::get('terminos', 'AdministradorController@terms')->name('control_sc_web_terms');
            Route::post('actualizar', 'LegalesController@actualizar')->name('actualizar_legales');
        });
    });

    Route::prefix('alertas')->group(function () {
        Route::get('/', 'AlertasController@index')->name('control_sc_alertas');
        Route::get('{id_alerta}', 'AlertasController@alertas')->name('control_sc_ver_alerta');
    });
});
