<?php

Route::get('/', 'HomeController@index')->name('home');
//Route::get('test', 'PagosController@Generar')->name('generar');       //Solo para pruebas en local

Route::get('categorias', 'NegociosController@categorias')->name('categorias');
Route::get('registrarse', 'HomeController@registrarse')->name('registrarse');
Route::get('reload_captcha', 'CaptchaController@reloadCaptcha')->name('reload-captcha');

//Rutas de Reseteo de Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('acerca', 'HomeController@about')->name('about');
Route::get('contactanos', 'HomeController@contact')->name('contact');
Route::get('terminos', 'HomeController@terms')->name('terms');
Route::get('privacidad', 'HomeController@privacy')->name('privacy');

Route::post('ubicacion', 'NegociosController@ubicacion')->name('ubicacion');
Route::post('ubicaciones', 'UbicacionesController@coordenadas')->name('ubicaciones');
Route::get('redireccion/{key}', 'NegociosController@autoacceso')->name('login_automatico'); //Solo se usa una vez para loguear al usuario

Route::prefix('directorio')->group(function () {
    Route::get('/', 'NegociosController@index')->name('directory');
    Route::post('/', 'NegociosController@busqueda')->name('directory_busqueda');

    Route::prefix('categoria')->group(function () {
        Route::get('/', 'NegociosController@volver')->name('categoria');
        Route::get('/{id_categoria}', 'NegociosController@categoria')->name('directory_categoria');
        Route::get('/{id_categoria}/keysword', 'NegociosController@categoria_y_keysword')->name('directory_categoria_y_keysword');
        Route::get('/{id_categoria}/ubicacion/', 'NegociosController@volver')->name('directory_categoria_y_ubicacion_volver');
        Route::get('/{id_categoria}/ubicacion/{ubicacion}', 'NegociosController@categoria_y_ubicacion')->name('directory_categoria_y_ubicacion');
        Route::get('/{id_categoria}/ubicacion/{ubicacion}/keysword', 'NegociosController@all_criterios')->name('directory_all_criterios');
    });

    Route::prefix('ubicacion')->group(function () {
        Route::get('/', 'NegociosController@volver')->name('ubicacion');
        Route::get('/{ubicacion}', 'NegociosController@location')->name('directory_ubicacion');
        Route::get('/{ubicacion}/keysword', 'NegociosController@loc_key')->name('directory_ubicacion_y_keysword');
    });

    Route::get('keysword', 'NegociosController@keysword')->name('directory_keysword');
});

Route::prefix('detalles')->group(function () {
    Route::get('/', 'NegociosController@volver')->name('details');
    Route::get('/{id}', 'NegociosController@detalles')->name('details_id');
});

/*
//Ruta para generar las fichas de pago (Procesos cron)
Route::get('new/payment', 'PagosController@generar')->name('new-payment');
 */
