<?php
//Ruta para los negocios
Route::group(['middleware' => ['auth', 'role:negocio'], 'prefix' => 'panel'], function () {
    Route::get('/', 'HomeController@negocio')->name('panel_control');
    Route::post('siguiente', 'SeguimientoController@index')->name('etapa_proceso_negocio');
    Route::post('omitir', 'SeguimientoController@omitir')->name('omitir_etapa_proceso_negocio');

    /*
    Route::prefix('logo')->group(function () {
    Route::get('/', 'SeguimientoController@logo')->name('panel_logo');
    Route::post('guardar','SeguimientoController@guardar_logo')->name('panel_guardar_logo_negocio');
    Route::post('actualizar','SeguimientoController@actualizar_logo')->name('panel_actualizar_logo_negocio');
    });

    Route::prefix('documentos')->group(function () {
    Route::get('/', 'SeguimientoController@documentos')->name('panel_documentos');
    Route::post('subir','SeguimientoController@subir_documentacion')->name('subir_documentacion');
    });

    Route::prefix('informacion')->group(function () {
    Route::get('/', 'SeguimientoController@informacion')->name('panel_informacion');
    Route::post('actualizar','SeguimientoController@actualizar_negocio')->name('actualizar_mi_info');
    });

    Route::prefix('pago')->group(function () {
    Route::get('/', 'SeguimientoController@pago')->name('panel_pago');
    });
     */

    Route::prefix('negocio')->group(function () {
        Route::get('/', 'MiNegocioController@index')->name('panel_negocio');
        Route::post('actualizar', 'MiNegocioController@actualizar_negocio')->name('actualizar_mi_negocio');
    });

    Route::prefix('galeria')->group(function () {
        Route::get('/', 'MiNegocioController@galeria')->name('panel_galeria');
        Route::post('guardar', 'MiNegocioController@guardar_galeria')->name('guardar_galeria');
        Route::post('ordenar', 'MiNegocioController@ordenar_galeria')->name('ordenar_galeria');
        Route::post('eliminar', 'MiNegocioController@eliminar_galeria')->name('eliminar_imagen_galeria');
    });

    Route::prefix('redes')->group(function () {
        Route::get('/', 'MiNegocioController@redes')->name('panel_redes');
        Route::post('datatable', 'MiNegocioController@datatable_redes')->name('dt_redes_sociales');
        Route::post('guardar', 'MiNegocioController@guardar_redes')->name('guardar_redes');
        Route::post('eliminar/{id_red}', 'MiNegocioController@eliminar_redes')->name('eliminar_redes');
    });

    Route::prefix('keywords')->group(function () {
        Route::get('/', 'MiNegocioController@keywords')->name('panel_keywords');
        Route::post('guarda', 'MiNegocioController@guardar_keysword')->name('guardar_keysword');
    });

    Route::prefix('historial')->group(function () {
        Route::get('/', 'PagosController@historial')->name('panel_historial');
        Route::post('datatable', 'PagosController@datatable')->name('dt_historial');
        Route::post('ficha', 'PagosController@pago')->name('panel_ficha');
        Route::post('renovar_fichas', 'TramitesController@renovacion_ficha_negocio')->name('renovar_ficha_negocio');
    });
});
