<?php

Route::get('mantenimiento', function () {
    return Artisan::call("down --secret='grupocybac' ");
});

Route::get('no-mantenimiento', function () {
    return Artisan::call("up");
});
