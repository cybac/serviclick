<?php

Route::post('transacciones', 'TramitesController@transacciones');               //Para el webhook de conekta

Route::namespace('Api')->group(function(){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('send-firebase', 'NotificacionesController@notificar');
    Route::post('ubicacion_cercana', 'NegociosController@ubicacion_cercana');
    Route::post('anuncio', 'IndexController@anuncios');
    Route::post('buscar', 'NegociosController@buscar');
    Route::post('buscar_cercanos', 'NegociosController@buscar_cercanos');
    Route::post('negocios', 'NegociosController@index');
    Route::post('categorias', 'CategoriasController@lista');
    Route::post('ubicaciones', 'UbicacionesController@lista');
    Route::post('frases', 'IndexController@slider_fortalezas');
    Route::post('redes_negocio', 'NegociosController@redes_negocio');
    Route::post('galeria_negocio', 'NegociosController@galeria_negocio');
    Route::post('comentarios_negocio', 'NegociosController@comentarios_negocio');
    Route::post('recuperacion','AuthController@recuperacion');                      //Enviamos un correo con un código de recuperación de cuenta
    Route::post('recuperar','AuthController@recuperar');                            //Restauramos la contraseña de una cuenta
    Route::post('comentar-negocio', 'NegociosController@comentar');                 //Comentar y valorar un negocio
    Route::post('enviar_notificacion', 'NegociosController@enviar_notificacion');   //Enviar desde el móvil una notificación Anonima
    Route::post('obtener-criterios', 'CriteriosController@obtener');
    Route::post('buscar_temp', 'NegociosController@buscar_temp');                   //Borrar despues
});