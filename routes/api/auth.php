<?php

Route::namespace('Api')->middleware(['auth:api'])->group(function() {
    Route::post('mostrar', 'NegociosController@mis_datos');
    Route::post('actualizar_informacion', 'NegociosController@actualizar');
    Route::post('actualizar_logo', 'NegociosController@actualizar_logo');
    Route::post('listar_galeria', 'NegociosController@listar_galeria');                         //Listar Fotos de la galeria de trabajos de un negocio
    Route::post('borrar_foto_galeria', 'NegociosController@borrar_foto_galeria');               //Borrar una foto de la galeria de trabajos de un negocio
    Route::post('subir_galeria', 'NegociosController@subir_galeria');                           //Subir 1 o más fotos a la galeria de trabajos de un negocio
    Route::post('ficha', 'NegociosController@ficha');
    Route::post('historial_pagos', 'NegociosController@historial_pagos');
    Route::post('tramite_informacion', 'NegociosController@tramite_informacion');
    Route::post('obtener_notificaciones', 'NegociosController@leer_notificaciones');            //Recibir Notificaciones en el móvil
    Route::post('leer_notificacion', 'NegociosController@notificacion_leida');                  //Marcar como Leida una notificación
    Route::post('listar_notificaciones', 'NegociosController@listar_notificaciones');           //Listar todas las notificaciones del negocio
    Route::post('existe_token', 'NotificacionesController@existe_token');
    Route::post('guardar_token', 'NotificacionesController@guardar_token');
    Route::post('link-acceso', 'AuthController@obtener_link');                                  //Link de autoacceso para los usuarios
});