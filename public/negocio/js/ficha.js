$(document).ready(function () {
    $(".ficha").click(function(){
        $this = $(this);
        var Boton = $(".ficha");
        Boton.prop('disabled', true);
        $.ajax({
            url: $this.data('url'),
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        }).done(function (response) {
            M.toast({html: response});
            setTimeout(function () {
                Boton.remove();
                M.toast({html: "Sera redireccionado en unos segundos"});
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }, 2000);
            
        }).fail(function (response) {
            Boton.prop('disabled', false);
            console.log(response);
            M.toast({html: 'Error en la Ficha'});
        });
    });
});
