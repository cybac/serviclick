Dropzone.autoDiscover = false;
$(function () {
    var myDropzone = new Dropzone("#upload_image", {
        acceptedFiles: 'image/*'
    });

    myDropzone.on("success", function (file, response) {
        this.removeFile(file);
        $("#sortable_list").append(response.view);
        M.toast({html: "Imagen subida a la galeria"});
    });

    myDropzone.on('error', function (file, response) {
        console.log(response);
        M.toast({html: "Tipo de archivo no soportado"});
        $(file.previewElement).remove();
    });

    // Ordenamiento de Imagenes
    var sortable_list = document.getElementById('sortable_list');
    var sortable = Sortable.create(sortable_list, {
        onEnd: function (evt) {
            var order = sortable.toArray();
            var url = $('#sortable_list').attr('sort_url');
            var token = $('#sortable_list').attr('token');
            $.post(url, {
                order: order,
                _token: token
            }, function (response) {
                M.toast({html: response.text});
            });
        }
    });
});
