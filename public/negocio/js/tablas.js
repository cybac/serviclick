$(document).ready(function () {
    $('#dt_negocios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_negocios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "imagen",
            "orderable": false
        }, {
            data: "nombre"
        }, {
            data: "precio_min"
        }, {
            data: "direccion"
        }, {
            data: "categoria"
        }, {
            data: "ubicacion"
        }, {
            data: "telefono"
        }, {
            data: "status",
        }, {
            data: "btn_img",
            "orderable": false
        }, {
            data: "btn_otro",
            "orderable": false
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_historial').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_historial").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "total"
        }, {
            data: "pagado"
        }, {
            data: "proximo"
        }, {
            data: "status"
        }],
        "order": [[1, "desc"],[3, "asc"]]
    });

    $('#dt_redes_sociales').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_redes_sociales").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-200px",
            data: "red_social"
        }, {
            data: "url"
        }, {
            className:"w-200px",
            data: "btn",
            "orderable": false
        }],
        order: [[0, "asc"]]
    });
});