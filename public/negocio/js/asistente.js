$(document).ready(function () {
    $("body").on("click", ".continuar", function () {
        $this = $(this);
        $this.prop('disabled', true);
        Data =  new FormData($('.asistente')[0]);

        if($('#descripcion').length == 1)
        {
            Data.append('descripcion', tinyMCE.get('descripcion').getContent());
        }
        if($('#servicios').length == 1)
        {
            Data.append('servicios', tinyMCE.get('servicios').getContent());
        }

        $.ajax({
            url: $this.data('url'),
            type: "POST",
            data: Data,
            processData: false,
            contentType: false
        }).done(function (response) {
            if(response.status)
            {
                if(response.contenido)
                {
                    $(".asistente").addClass("animacion-salida");
                    setTimeout(function () {
                        $('.asistente').html(response.contenido);
                        $(".asistente").removeClass("animacion-salida");
                        $(".asistente").addClass("animacion-entrada");
                        setTimeout(function () {
                            $(".asistente").removeClass("animacion-entrada");
                        }, 800);
                    }, 500);
                }
            }
        }).fail(function (response) {
            //console.log(response);
            if (response.status == 422) {
                $('.asistente').find('.is-invalid').removeClass('is-invalid');
                M.toast({html: "Faltan datos por rellenar o existe un detalle con éstos"});
                $.each(response.responseJSON.errors, function (index, value) {
                    $("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                });
            } else {
                M.toast({html: response.responseJSON.message});
            }
            $this.prop('disabled', false);
        });



    });


    $("body").on("click", ".omitir", function () {
        $this = $(this);
        Data =  new FormData($('.asistente')[0]);
        $.ajax({
            url: $this.data('url'),
            type: "POST",
            data: Data,
            processData: false,
            contentType: false
        }).done(function (response) {
            if(response.status)
            {
                if(response.contenido)
                {
                    $(".asistente").addClass("animacion-salida");
                    setTimeout(function () {
                        $('.asistente').html(response.contenido);
                        $(".asistente").removeClass("animacion-salida");
                        $(".asistente").addClass("animacion-entrada");
                        setTimeout(function () {
                            $(".asistente").removeClass("animacion-entrada");
                        }, 800);
                    }, 500);
                }
            }
        }).fail(function (response) {
            M.toast({html: "Ocurrio un error inesperado"});
            setTimeout(function () {
                M.toast({html: "Sera redireccionado en unos segundos"});
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }, 2000);
        });
    });


    $("body").on("submit", ".asistente", function (event) {
        event.preventDefault();
    });

});
