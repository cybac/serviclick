$(document).ready(function () {
    $("body").on("submit", ".form", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        var btnsubmit = $(".submit");
        Swal.fire({
            title: $this.data("title"),
            text: $this.data("text"),
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: $this.data("btn")
        }).then(function (result) {
            if (result.value) {
                Enviar($this, btnsubmit);
            }
        });
    });

    $("#ficha").on("click", function (ev) {
        var $this = $(this);
        $.ajax({
            url: $this.data('url'),
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        }).done(function (response) {
            Swal.fire({
                html: response,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Cerrar'
            });
        }).fail(function (response) {
            console.log("Error");
            console.log(response);
        });
    });

    function Enviar(Datos, Boton)
    {
        Boton.prop('disabled', true);
        $.ajax({
            url: Datos.attr('action'),
            type: "POST",
            data: Datos.serialize()
        }).done(function (response) {
            Boton.prop('disabled', false);
            if (response.status) {
                M.toast({html: response.text});
                $(".img-galeria[data-id=" + Datos[0][1].value + "]").remove();
            }
        }).fail(function (response) {
            console.log(response);
            Boton.prop('disabled', false);
            if (response.status == 422) {
                M.toast({html: "Faltan datos por rellenar o existe un detalle con éstos"});
                Datos.find('.is-invalid').removeClass('is-invalid');
                $.each(response.responseJSON.errors, function (index, value) {
                    $("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                });
            } else {
                M.toast({html: response.responseJSON.message});
            }
        });
    }

    $("body").on("submit", ".documentacion", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        var btnsubmit = $(".submit");
        var formData = new FormData(document.getElementById($this.attr('id')));
        btnsubmit.prop('disabled', true);
        $this.find('.is-invalid').removeClass('is-invalid');
        $.ajax({
            url: $this.attr('action'),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false
        }).done(function (response) {
            if(response.status)
            {
                btnsubmit.remove();
                M.toast({html: response.text});
                setTimeout(function () {
                    M.toast({html: "Sera redireccionado en unos segundos"});
                    setTimeout(function () {
                        window.location.href = response.url;
                    }, 3000);
                }, 2000);
            }
        }).fail(function (response) {
            if (response.status == 422) {
                $this.find('.is-invalid').removeClass('is-invalid');
                M.toast({html: "Faltan datos por rellenar o existe un detalle con éstos"});
                $.each(response.responseJSON.errors, function (index, value) {
                    $("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                });
            } else {
                console.log(response);
                M.toast({html: response.responseJSON.message});
            }
            btnsubmit.prop('disabled', false);
        });
    });

    $("body").on('click', '.delete_file', function () {
        var $this = $(this);
        var nameinput = $this.data('name');
        var inputfile = "<input type='file' name=" + nameinput + " class='form-control form-control-lg' id='" + nameinput + "'><span class='invalid-feedback'></span>";
        Swal.fire({
            title: '¿Está seguro de querer reemplazar este elemento?',
            text: "Se visualizaran los cambios hasta guardar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, reemplazar!'
        }).then(function (result) {
            if (result.value) {
                $this.parent().html(inputfile);
            }
        });
    });

    $("body").on('click', '.delete', function () {
        var $this = $(this);
        Swal.fire({
            title: '¿Está seguro de querer eliminar este elemento?',
            text: "No se podrá revertir esta acción",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, borrar!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }

                }).done(function (response) {
                    if (response.status) {
                        Swal.fire({
                            title: response.title,
                            text: response.text,
                            type: response.type
                        }).then(function (result) {
                            if (response.load) {
                                window.location.href = response.url;
                            }
                        });

                        if (response.reload) //Evalua si se debe recargar la página
                            location.reload(); //Recarga la página
                        if (response.input) //Regresa un html
                            $this.parent().html(response.input); //Se incrusta el html en el padre

                        if (response.dttable) //Regresa el selector del DataTable
                            $(response.dttable).DataTable().ajax.reload(); //Recarga la tabla
                        if (response.dttable2) //Regresa el selector del DataTable
                            $(response.dttable2).DataTable().ajax.reload(); //Recarga otra tabla
                    }
                }).fail(function (response) {
                    Swal.fire({
                        title: "Ocurrió un error",
                        text: response.responseJSON.message,
                        type: 'error'
                    });
                });
            }
        });
    });

    $("body").on("submit", ".form_files", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        var btnsubmit = $(".submit");
        var formData = new FormData(document.getElementById($this.attr('id')));
        btnsubmit.prop('disabled', true);
        $.ajax({
            beforeSend: function beforeSend() {
                $(".cargando").removeClass('d-none');
            },
            url: $this.attr('action'),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false
        }).done(function (response) {
            btnsubmit.prop('disabled', false);
            $(".cargando").addClass('d-none');

            if (response.status) {
                Swal.fire({
                    type: response.type,
                    title: response.title,
                    text: response.text
                }).then(function (result) {
                    if (result.value) {
                        if (response.reload) {
                            location.reload();
                        }

                        if (response.dttable) {
                            $(response.dttable).DataTable().ajax.reload(); //Recarga la tabla
                        }
                        if (response.social) {
                            $("#red").prop('selectedIndex',0);
                            $("#url").val("");
                        }
                    }
                });
            }
        }).fail(function (response) {
            console.log(response);

            if (response.status == 422) {
                $this.find('.is-invalid').removeClass('is-invalid');
                Swal.fire({
                    type: "error",
                    title: "Error",
                    text: "Faltan datos por rellenar o existe un detalle con éstos"
                });
                $.each(response.responseJSON.errors, function (index, value) {
                    $("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                });
            } else {
                Swal.fire({
                    type: "error",
                    title: "Error",
                    text: response.responseJSON.message
                });
            }

            $(".cargando").addClass('d-none');
            btnsubmit.prop('disabled', false);
        });
    });
});