var minimo = document.getElementById('minimo');
var maximo = document.getElementById('maximo');
var slider = document.getElementById('slider_precio');

minimo = minimo.value.substring(1, minimo.value.length);
maximo = maximo.value.substring(1, maximo.value.length);

var rangos = {
	'min': [100, 10],
    '30%': [1000, 100],
    '60%': [10000, 100],
    '80%': [20000, 1000],
    'max': [50000]
};

noUiSlider.create(slider, {
    start: [minimo, maximo],
    connect: true,
    range: rangos,
    format: wNumb({
        decimals: 0,
        prefix: '$'
    }),
    pips: {
        mode: 'range',
        density: 5,
        format: wNumb({
            decimals: 0,
            prefix: '$'
        })
    },
    tooltips: false
});


var moneyValues = [
    document.getElementById('minimo'),
    document.getElementById('maximo')
];

slider.noUiSlider.on('update', function (values, handle) {
    moneyValues[handle].value = values[handle];
});