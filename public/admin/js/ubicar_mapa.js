var Urldestino = window.location.origin + "/ubicaciones";

$("select[name=ubicacion]").change(function(){
    goto_ubicacion = $('select[name=ubicacion]').val();
    if(goto_ubicacion == "")
    {
        viajar(1);
    }else{
        viajar(goto_ubicacion);
    }
});


function viajar(id) {
    $.ajax({
        url: Urldestino,
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            "ubicacion":id,
        }
    }).done(function (response) {
        coords = response.split(",");
        map.flyTo(coords, 13);
        //marker.setLatLng([e.latlng.lat, e.latlng.lng]);
        marker.setLatLng(coords);
    }).fail(function (response) {
        console.log("Fallo al enviar");
        console.log(response);
    });
}