var valor_edicion = "";
var boton = "";

$(document).ready(function () {
    $("body").on('click', '.input-text', function () {
        var $this = $(this);
        if ($this.data('value')) {
            valor_edicion = $this.data('value');
            boton = "Actualizar";
        } else {
            valor_edicion = "";
            boton = "Guardar";
        }
        Swal.fire({
            title: $this.data('title'),
            input: 'text',
            inputLabel: $this.data('subtitle'),
            inputValue: valor_edicion,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: boton,

            inputValidator: (value) => {
                if (!value) {
                    return 'El campo no puede quedar vacio'
                }
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        nombre: result.value
                    }
                }).done(function (response) {
                    if (response.status) {
                        Swal.fire({
                            title: response.title,
                            text: response.text,
                            type: response.type
                        }).then(function (result) {
                            if (response.load) {
                                window.location.href = response.url;
                            }
                        });

                        if (response.reload) //Evalua si se debe recargar la página
                            location.reload(); //Recarga la página
                        if (response.input) //Regresa un html
                            $this.parent().html(response.input); //Se incrusta el html en el padre

                        if (response.dttable) //Regresa el selector del DataTable
                            $(response.dttable).DataTable().ajax.reload(); //Recarga la tabla
                    }
                }).fail(function (response) {
                    Swal.fire({
                        title: "Ocurrió un error",
                        text: response.responseJSON.message,
                        type: 'error'
                    });
                });
            }
        });
    });

    $("body").on('click', '.redes_sociales', function () {
        var $this = $(this);
        if ($this.data('value')) {
            valor_edicion = $this.data('value');
            boton = "Actualizar";
        } else {
            valor_edicion = "";
            boton = "Guardar";
        }
        Swal.fire({
            title: $this.data('title'),
            input: 'text',
            inputLabel: $this.data('subtitle'),
            inputValue: valor_edicion,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: boton,

            inputValidator: (value) => {
                if (!value) {
                    return 'El campo no puede quedar vacio'
                }
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id:$this.data('id'),
                        dato: result.value
                    }
                }).done(function (response) {
                    M.toast({html: response.text});
                    $this.data('value', result.value);
                }).fail(function (response) {
                    console.log(response);
                    M.toast({html: "Ocurrió un error"});
                });
            }
        });
    });

    $("body").on('click', '.aprobar-archivos', function () {
        var btnsubmit = $(".aprobar-archivos");
        var btnrechazar = $(".rechazar-archivo");
        btnsubmit.prop('disabled', true);
        btnrechazar.prop('disabled', true);
        var $this = $(this);
        Swal.fire({
            title: "Aprobar la documentación del negocio",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar y aprobar',
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id: $this.data('id')
                    }
                }).done(function (response) {
                    if(response.status)
                    {
                        M.toast({html: response.text});
                        if (response.url) {
                            setTimeout(function () {
                                btnrechazar.remove();
                                M.toast({html: "Sera redireccionado en unos segundos"});
                                setTimeout(function () {
                                    window.location.href = response.url;
                                }, 3000);
                            }, 2000);
                        }else{
                            btnsubmit.prop('disabled', true);
                            btnrechazar.prop('disabled', true);
                        }
                    }
                }).fail(function (response) {
                    btnsubmit.prop('disabled', false);
                    btnrechazar.prop('disabled', false);
                    M.toast({html: "Error al realizar la petición"});
                    console.log(response);
                });
            }else{
                btnsubmit.prop('disabled', false);
                btnrechazar.prop('disabled', false);
            }
        });
    });

    $("body").on('click', '.rechazar-archivo', function () {
        var $this = $(this);
        Swal.fire({
            title: $this.data('title'),
            input: 'text',
            inputLabel: $this.data('subtitle'),
            inputValue: '',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            inputValidator: (value) => {
                if (!value) {
                    return 'El campo no puede quedar vacio'
                }
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id: $this.data('id'),
                        dato: result.value
                    }
                }).done(function (response) {
                    if(response.status)
                    {
                        M.toast({html: response.text});
                        if (response.dttable) {
                            $(response.dttable).DataTable().ajax.reload();
                        }
                    }
                }).fail(function (response) {
                    M.toast({html: "Error al realizar la petición"});
                    console.log(response);
                });
            }
        });
    });

    $("body").on('click', '.accion-info', function () {
        var btnsubmit = $(".aprobar-info");
        var btnrechazar = $(".rechazar-info");
        btnsubmit.prop('disabled', true);
        btnrechazar.prop('disabled', true);
        var $this = $(this);
        Swal.fire({
            title: $this.data('title'),
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: $this.data('btn'),
        }).then(function (result) {
            if (result.value) {
                if($this.data('motivo')) {
                    Swal.fire({
                        title: "Solicitar Corrección",
                        input: 'text',
                        inputLabel: "indique el motivo de la corrección",
                        inputValue: '',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Confirmar y enviar',
                        inputValidator: (value) => {
                            if (!value) {
                                return 'El campo no puede quedar vacio'
                            }
                        }
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: $this.data('url'),
                                type: 'POST',
                                data: {
                                    _token: $('meta[name="csrf-token"]').attr('content'),
                                    id: $this.data('id'),
                                    dato: result.value
                                }
                            }).done(function (response) {
                                if(response.status)
                                {
                                    M.toast({html: response.text});
                                    if (response.url) {
                                        setTimeout(function () {
                                            M.toast({html: "Sera redireccionado en unos segundos"});
                                            setTimeout(function () {
                                                window.location.href = response.url;
                                            }, 3000);
                                        }, 2000);
                                    }else{
                                        btnsubmit.prop('disabled', true);
                                        btnrechazar.prop('disabled', true);
                                    }
                                }
                            }).fail(function (response) {
                                M.toast({html: "Error al realizar la petición"});
                                console.log(response);
                            });
                        }else{
                            btnsubmit.prop('disabled', false);
                            btnrechazar.prop('disabled', false);
                        }
                    });
                }else{
                    $.ajax({
                        url: $this.data('url'),
                        type: 'POST',
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                            id: $this.data('id')
                        }
                    }).done(function (response) {
                        if(response.status)
                        {
                            M.toast({html: response.text});
                            if (response.url) {
                                setTimeout(function () {
                                    btnrechazar.remove();
                                    M.toast({html: "Sera redireccionado en unos segundos"});
                                    setTimeout(function () {
                                        window.location.href = response.url;
                                    }, 3000);
                                }, 2000);
                            }
                        }
                    }).fail(function (response) {
                        btnsubmit.prop('disabled', false);
                        btnrechazar.prop('disabled', false);
                        M.toast({html: "Error al realizar la petición"});
                        console.log(response);
                    });
                    
                }
            }else{
                btnsubmit.prop('disabled', false);
                btnrechazar.prop('disabled', false);
            }
        });
    });

    $("body").on('click', '.exoneracion', function () {
        var $this = $(this);
        Swal.fire({
            title: 'Meses a exonerar el pago',
            input: 'select',
            inputPlaceholder: 'Seleccione la cantidad de meses a exonerar',
            inputOptions: {
                '1': '1 Mes',
                '2': '2 Meses',
                '3': '3 Meses',
                '4': '4 Meses',
                '5': '5 Meses',
                '6': '6 Meses',
                '7': '7 Meses',
                '8': '8 Meses',
                '9': '9 Meses',
                '10': '10 Meses',
                '11': '11 Meses',
                '12': '1 Año',
            },
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',

            inputValidator: (value) => {
                if (!value) {
                    return 'Seleccione la cantidad de meses'
                }
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id:$this.data('id'),
                        meses: result.value
                    }
                }).done(function (response) {
                    if(response.status)
                    {
                        M.toast({html: response.text});
                        if(response.load)
                        {
                            setTimeout(function () {
                                M.toast({html: "Sera redireccionado en unos segundos"});
                                setTimeout(function () {
                                    window.location.href = response.url;
                                }, 3000);
                            }, 2000);
                        }
                    }
                }).fail(function (response) {
                    console.log(response);
                    M.toast({html: "Ocurrió un error"});
                });
            }
        });
    });
});
