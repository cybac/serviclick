var IVA = 0.00624;          //0.624%
var Comision = 0.039;       //3.9%
var Cobro_IVA, Cobro_Comision, Cobro_Comision_Total, Deposito;

$(document).ready(function () {
    $("#moneda").on("keyup", function(){
        $("#moneda").val($("#moneda").val().toUpperCase());
    });
    
    $("#monto").on("keypress", function(event){
        var key = window.event ? event.which : event.keyCode;
        if (key < 48 || key > 57 ) {
            event.preventDefault();
        }
    });
    
    $("#monto").on("keyup", function(event){
        if($("#monto").val().length == 0)
        {
            $("#total").val("0.00");
            $("#iva").val("0.00");
            $("#comision").val("0.00");
            $("#comision_total").val("0.00");
        }else{
            Cobro_IVA = ($("#monto").val() * IVA).toFixed(2);
            Cobro_Comision = ($("#monto").val() * Comision).toFixed(2);
            Cobro_Comision_Total = (Number(Cobro_IVA) + Number(Cobro_Comision)).toFixed(2);
            Deposito = ($("#monto").val() - Cobro_Comision_Total).toFixed(2);
            $("#iva").val(Cobro_IVA);
            $("#comision").val(Cobro_Comision);
            $("#comision_total").val(Cobro_Comision_Total);
            $("#total").val(Deposito);
        }
    });

    $("#monto").on("copy", function(event){
        event.preventDefault();
        M.toast({html: "Esta acción está prohibida"});
    });

    $("#monto").on("paste", function(event){
        event.preventDefault();
        M.toast({html: "Esta acción está prohibida"});
    });
});
