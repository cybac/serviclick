$(document).ready(function () {
    var sortable_list = document.getElementById('sortable_list');
    var sortable = Sortable.create(sortable_list, {
        onEnd: function (evt) {
            var order = sortable.toArray();
            var url = $('#sortable_list').data('url');
            var token = $('meta[name="csrf-token"]').attr('content');
            $.post(url, {
                order: order,
                _token: token
            }, function (response) {
                M.toast({html: response.text});
            });
        }
    });
});