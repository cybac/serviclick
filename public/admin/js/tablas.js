$(document).ready(function () { 

    $('#dt_negocios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_negocios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "imagen",
            "orderable": false
        }, {
            data: "nombre"
        }, {
            data: "precio_min"
        }, {
            data: "direccion"
        }, {
            data: "categoria"
        }, {
            data: "ubicacion"
        }, {
            data: "telefono"
        }, {
            data: "pagando",
        }, {
            data: "status",
        },{
            className:"va-t w-150px",
            data: "btn_img",
            "orderable": false
        }, {
            className:"va-t w-150px",
            data: "btn_otro",
            "orderable": false
        }, {
            className:"va-t w-150px",
            data: "btn",
            "orderable": false
        }],
        order: [[1, "desc"]]
    });

    $('#dt_negocios_tramites').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_negocios_tramites").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-200px",
            data: "imagen",
            "orderable": false
        }, {
            data: "nombre"
        }, {
            className: "text-left",
            data: "contacto"
        }, {
            className:"w-150px",
            data: "registro"
        }, {
            className:"va-t w-150px",
            data: "seguimiento",
            "orderable": false
        },{
            className:"va-t w-150px",
            data: "btn",
            "orderable": false
        }],
        order: [[2, "desc"], [1, "asc"]]
    });

    $('#dt_documentos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_documentos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "tipo",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_documentos_tramite').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_documentos_tramite").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "tipo",
        }, {
            className:"w-200px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_documentacion').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_documentacion").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        }, {
            className:"w-100px",
            data: "status",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_categorias').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_categorias").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-200px",
            data: "imagen",
            "orderable": false
        },{
            data: "nombre",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_ubicaciones').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_ubicaciones").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            data: "ubicacion",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_slider_text').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_slider_text").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "frase",
        },{
            className:"w-100px",
            data: "status",
        }, {
            className:"w-200px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_beneficios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_beneficios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-100px",
            data: "imagen",
            "orderable": false
        },{
            className:"w-200px",
            data: "titulo",
        },{
            data: "descripcion",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_testimoniales').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_testimoniales").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-100px",
            data: "imagen",
            "orderable": false
        },{
            className:"w-75px",
            data: "nombre",
        },{
            className:"w-100px",
            data: "trabajo",
        },{
            data: "contenido",
        },{
            className:"w-100px",
            data: "status",
        }, {
            className:"w-100px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_sucursales').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_sucursales").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            data: "direccion",
        },{
            data: "telefono",
        },{
            data: "correo",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_equipo').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_equipo").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "imagen",
            "orderable": false
        },{
            data: "nombre",
        },{
            data: "puesto",
        },{
            data: "status",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_habilidades').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_habilidades").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            className:"w-100px",
            data: "cantidad",
        }, {
            className:"w-200px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_criterios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_criterios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            className:"w-100px",
            data: "status",
        }, {
            className:"w-200px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_comentarios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_comentarios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            data: "correo",
        },{
            data: "puntuacion",
        },{
            data: "contenido",
        },{
            data: "status",
        }, {
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_all_comentarios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_all_comentarios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            data: "correo",
        },{
            data: "puntuacion",
        },{
            data: "negocio",
        },{
            data: "contenido",
        },{
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_historial_pagos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_historial_pagos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            data: "total"
        }, {
            data: "emitido"
        },{
            data: "pagado"
        }, {
            data: "proximo"
        }, {
            data: "status"
        }],
        "order": [[2, "desc"]]
    });

    $('#dt_historial_pagos_negocio').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_historial_pagos_negocio").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "total"
        }, {
            data: "emitido"
        }, {
            data: "pagado"
        }, {
            data: "proximo"
        },{
            data: "status"
        }],
        "order": [[1, "desc"]]
    });

    $('#dt_negocios_suspendidos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_negocios_suspendidos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        },{
            className:"w-150px",
            data: "completados"
        },{
            className:"w-150px",
            data: "expirados"
        },{
            className:"w-175px",
            data: "ultima",
            "orderable": false
        }, {
            className:"w-175px",
            data: "fecha_ultima",
            "orderable": false
        },{
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_correos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_correos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-200px",
            data: "tipo"
        },{
            className:"w-200px",
            data: "titulo",
        },{
            data: "mensaje",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_notificaciones').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_notificaciones").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            className:"w-200px",
            data: "tipo"
        },{
            data: "mensaje",
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_recordatorios_negocios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_recordatorios_negocios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "contenido"
        }, {
            className:"w-200px",
            data: "fecha"
        }],
        order: [[1, "desc"]]
    });


});