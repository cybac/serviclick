var URLBase = window.location.origin + "/control_sc/";
var URLDestino;
var Mensaje = "";

function Cambio_anuncio(id)
{
    URLDestino = URLBase + "status_anuncio";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El anuncio ahora es visible en la APP</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> Anuncio oculto en la APP</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_redes(id)
{
    URLDestino = URLBase + "status_redes";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> La red social ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> La red social ahora esta oculta</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_documentacion(id)
{
    URLDestino = URLBase + "status_documentacion";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El documento sera solicitado a los nuevos negocios</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El documento no sera solicitado a los nuevo negocios</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_negocio(id)
{
    URLDestino = URLBase + "status_negocio";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El negocio ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El negocio ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_negocio_pago(id)
{
    URLDestino = URLBase + "status_negocio_pago";
    valor = document.getElementById('status_pago_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El negocio empezara a recibir fichas de pago</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> Negocio omitido del pago</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_slider(id)
{
    URLDestino = URLBase + "status_slider";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El slider ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El slider ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_testimonial(id)
{
    URLDestino = URLBase + "status_testimonial";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El testimonial ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El testimonial ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_equipo(id)
{
    URLDestino = URLBase + "status_equipo";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El integrante del equipo ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El integrante del equipo ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_criterio(id)
{
    URLDestino = URLBase + "status_criterio";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El criterio de evaluación ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El criterio de evaluación ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function Cambio_comentario(id)
{
    URLDestino = URLBase + "status_comentario";
    valor = document.getElementById('status_'+id).checked
    if(valor){
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye'></i> El comentario ahora es visible</p>";
        enviar_cambio_status(id, 1);
    }else{
        Mensaje = "<p class='mensaje-toast'><i class='mdi mdi-eye-off'></i> El comentario ahora esta oculto</p>";
        enviar_cambio_status(id, 0);
    }
}

function enviar_cambio_status(id, status)
{
    $.ajax({
        url: URLDestino,
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            "id":id,
            "status":status,
        }
    }).done(function (response) {
        M.toast({html: Mensaje});
    }).fail(function (response) {
        console.log("Fallo al enviar");
        console.log(response);
    });
}