Dropzone.autoDiscover = false;
$(function () {
    var myDropzone = new Dropzone("#upload_image", {
        acceptedFiles: 'image/*'
    });

    myDropzone.on("success", function (file, response) {
        this.removeFile(file);
        $("#sortable_list").append(response.view);
        M.toast({html: "Imagen subida a la galeria"});
    });

    myDropzone.on('error', function (file, response) {
        console.log(response);
        M.toast({html: "Tipo de archivo no soportado"});
        $(file.previewElement).remove();
    });

    $(document).on('click', '.btn_delete', function (ev) {
        var btn = $(this);
        btn.attr('disabled', true);
        swal({
            title: "¿Realmente deseas eliminar la imagen?",
            type: "warning",
            buttons: ["Cancelar", "Confirmar"],
        }).then((value) => {
            if (value) {
                $.ajax({
                    url: btn.attr('delete_url'),
                    type: 'POST',
                    data: {
                        _method: 'DELETE',
                        _token: $('#sort-card').attr('token')
                    },
                    success: function (result) {
                        $(".card[data-id=" + result.image_delete
                                .id + "]")
                            .remove();
                        swal(
                            'Eliminado',
                            'La imagen ha sido eliminada correctamente',
                            'success'
                        );
                    }
                });
                btn.removeAttr('disabled');
            } else {
                swal("Cancelado", "La imagen no se eliminó.", "error");
            }
        });
    });

    // Ordenamiento de Imagenes
    var sortable_list = document.getElementById('sortable_list');
    var sortable = Sortable.create(sortable_list, {
        onEnd: function (evt) {
            var order = sortable.toArray();
            var url = $('#sortable_list').attr('sort_url');
            var token = $('#sortable_list').attr('token');
            $.post(url, {
                order: order,
                _token: token
            }, function (response) {
                console.log(response);
                M.toast({html: response.text});
            });
        }
    });
});
