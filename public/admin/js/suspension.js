$(document).ready(function () { 
    $("body").on('click', '.quitar_suspension', function () {
        $this = $(this);

        $.ajax({
            url: $this.data("url"),
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                "id":$this.data("id"),
                "status":1,
            }
        }).done(function (response) {
            console.log(response);
            M.toast({html: "Suspensión Revocada"});
            $($this.data("dttable")).DataTable().ajax.reload();
        }).fail(function (response) {
            console.log("Fallo al enviar");
            console.log(response);
        });
    });
});
