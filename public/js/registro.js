$(document).ready(function () {
    $(".tipo_registro").click(function(){
        $this = $(this);
        if($this.val() == 2)
        {
            $("#empresa").attr("hidden",false);
            $("#empresa").attr("required",true);
        }else{
            $("#empresa").attr("hidden",true);
            $("#empresa").attr("required",false);
        }
    });
    
    $("#ayuda").click(function(){
        Swal.fire({
            title: 'Registrarse',
            text: 'El registro puedes hacerlo de dos maneras distintas. Si seleccionas NO, Serviclick te registrará como un usuario normal, si seleccionas SI, Serviclick te registrará como un negocio o persona que ofrece algún tipo de servicio.',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok'
        });
    });
});

