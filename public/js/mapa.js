var map;
var volar = true;
var nueva_ubicacion = 0;
let coords;
var Urldestino = window.location.origin + "/ubicacion";

$(window).on('load', function () {
    setTimeout(function () {
        var mbUrl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
        map = L.map('map', {
            center: ubicacion,
            zoom: 13,
            minZoom: 4,
            maxZoom: 17,
        });

        L.tileLayer(mbUrl, {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);


        var trabajos = document.getElementsByClassName("goto-map");
        for (var i = 0; i < trabajos.length; i++) {
            trabajos[i].addEventListener("click", function () {
                corte_inicial = this.innerHTML.indexOf('<div class="hidden" id="map') + 27;
                recorte = this.innerHTML.substr(corte_inicial);
                corte_final = recorte.indexOf('"');
                recorte = recorte.substr(0, corte_final);
                if (volar) {
                    volar = false;
                    nueva_ubicacion = recorte;
                    viajar(recorte);
                } else {
                    if (nueva_ubicacion != recorte) {
                        nueva_ubicacion = recorte;
                        viajar(recorte);
                    } else {
                        volar = true;
                        map.flyTo(ubicacion, 13);
                    }
                }

            });
        }

        function viajar(id) {
            $.ajax({
                url: Urldestino,
                type: "POST",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    "negocio": id,
                }
            }).done(function (response) {
                coords = response.split(",");
                map.flyTo(coords, 18);
            }).fail(function (response) {
                console.log("Fallo al enviar");
                console.log(response);
            });
        }
    }, 500);
})
