$(document).ready(function(){
    $("#phone").keydown(function(e){
        var key = window.event ? e.which : e.keyCode;
        if(key == 190 || key == 69){
            e.preventDefault();
        }
    }).keydown(function(e){
        var key = window.event ? e.which : e.keyCode;
        if(key == 38 || key == 40){
            e.preventDefault();
        }
    }).keypress(function(e){
        $this = $(this);
        var key = window.event ? e.which : e.keyCode;
        if($this.val().length > 9){
            e.preventDefault();
        }
    });
});