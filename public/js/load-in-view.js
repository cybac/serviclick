inView('.gl-feat-areas').on('enter', function(picture) {
    var img = picture.querySelector('img');
    if ('undefined' !== typeof img.dataset.src ) {
        //picture.classList.add('is-loading');
        picture.children[0].classList.add('is-loading');
        newImg = new Image();
        newImg.src = img.dataset.src;
        newImg.addEventListener('load', function() {
            //picture.innerHTML = '';
            //picture.appendChild(this);
            picture.children[0].innerHTML = '';
            picture.children[0].appendChild(this);
            setTimeout(function() {
                //picture.classList.remove('is-loading');
                //picture.classList.add('is-loaded');
                picture.children[0].classList.remove('is-loading');
                picture.children[0].classList.add('is-loaded');
            }, 300);
        });
    }
});
