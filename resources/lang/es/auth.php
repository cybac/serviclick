<?php

return [
    'failed' => 'Error: Verifique su correo y contraseña.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
];
