@extends('layouts.admin')
@section('titulo', "Administrador - Keysword")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">"Editar Keysword"</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('guardar_keysword_negocio', $id) }}"
                    method="POST" class="form_files row" id="form_keysword" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12">
                        <label>Keysword</label>
                        <input style="margin-bottom: 20px;" type="text" class="form-control form-control-lg" id="keysword" name="keysword" value="">
                        <div class="keys" id="keys">
                            @php $i=1; @endphp
                            @foreach ($keys as $item)
                            <span class="key" id="key_{{$i}}">{{$item->key}} 
                                <label for="keysword_{{$i}}" style="display: none"></label>
                                <input type="text" name="keysword_{{$i}}" id="keysword_{{$i}}" value="{{$item->key}}" style="display: none">
                                <i class="mdi mdi-close-box cerrar" onclick="borrar({{$i}});"></i>
                            </span>
                            @php
                                $i++;
                            @endphp
                            @endforeach
                        </div>
                        <script> edi_tag_id={{$i}};</script>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                        <a href="{{ route('control_sc_negocios') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{asset('admin/css/tags.css?n='.uniqid())}}">
<script src="{{asset('admin/js/tags.js?n='.uniqid())}}"></script>
@endsection
