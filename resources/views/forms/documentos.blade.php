@extends('layouts.admin')
@section('titulo', "Administrador - Subir Documento")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Edición del documento" :"Nuevo Documento" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_documento_negocio', $documento->id) : route('guardar_documento_negocio', $negocio) }}"
                    method="POST" class="form_files row" id="form_negocio" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-6" style="display: none;">
                        <label for="negocio"></label>
                        <input type="text" class="form-control form-control-lg" name="negocio" id="negocio" value="{{ $form_edit ? $documento->negocio : $negocio}}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label for="alias">Nombre del documento <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre"
                            value="{{ $form_edit ? $documento->tipo : "" }}" {{ $form_edit ? "readonly" : "" }}>
                        <span class="invalid-feedback"></span>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label for="documento">Documento <span class="text-danger">*</span></label>
                        <input type="file" name="documento" id="documento" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                        <a href="{{ route('documentos_negocio', $form_edit ? $documento->negocio : $negocio) }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
