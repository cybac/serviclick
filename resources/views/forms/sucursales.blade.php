@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Sucursal" : "Administrador - Nueva Sucursal")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Sucursal" :"Nueva Sucursal" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                <form action="{{ $form_edit ? route('actualizar_sucursal', $sucursal->id) : route('guardar_sucursal') }}"
                    method="POST" class="form_files row" id="form_sucursal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $sucursal->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-8">
                        <label for="direccion">Dirección <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="direccion" id="direccion" value="{{ $form_edit ? $sucursal->direccion : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="telefono" id="telefono" value="{{ $form_edit ? str_replace(' ','',$sucursal->telefono) : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-8">
                        <label for="correo">Correo <span class="text-danger">*</span></label>
                        <input type="email" class="form-control form-control-lg" name="correo" id="correo" value="{{ $form_edit ? $sucursal->correo : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('control_sc_web_beneficios') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/numeros.js?n='.uniqid())}}"></script>
@endsection
