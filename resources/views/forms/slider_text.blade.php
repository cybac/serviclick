@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Slider Text" : "Administrador - Nueva Slider Text")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Slider Text" :"Nueva Slider Text" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_slider_text', $slider->id) : route('guardar_slider_text') }}"
                    method="POST" class="form_files row" id="form_slider_text" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="inicio">Inicio <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="inicio" id="inicio" value="{{ $form_edit ? $slider->inicio : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="remarcado">Texto Remarcado <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="remarcado" id="remarcado" value="{{ $form_edit ? $slider->remarcado : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="fin">Fin <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="fin" id="fin" value="{{ $form_edit ? $slider->fin : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label>Previo </label><br><span class="typed"></span>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('control_sc_web_slider_text') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/typed.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/escritura/previo.js?n='.uniqid())}}"></script>
@endsection
