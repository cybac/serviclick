@extends('layouts.admin')
@section('titulo',  "Administrador - Editar Pago")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">Editar Pago</h3>
</div>
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('actualizar_precio') }}"
                    method="POST" class="form_files row" id="form_pago" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-12">
                        <label for="nombre">Concepto</label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $precio->nombre }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="costo">Monto</label>
                        <input type="number" class="form-control form-control-lg" name="costo" id="costo" min="1" value="{{ ($precio->costo / 100) }}" maxlength="12">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="moneda">Moneda</label>
                        <input type="text" class="form-control form-control-lg" name="moneda" id="moneda" value="{{ $precio->moneda }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="meses">Meses Gratis</label>
                        <input type="number" class="form-control form-control-lg" name="meses" id="meses" min="0" max="12" value="{{$precio->gratis}}" maxlength="2">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <p>Calculadora</p>
                <div class="row">
                    <div class="form-group col-12 col-md-6">
                        <label>Monto</label>
                        <input type="text" class="form-control form-control-lg" name="monto" id="monto" value="0" maxlength="12">
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>Depósito total a tu cuenta</label>
                        <input type="text" class="form-control form-control-lg" name="total" id="total" value="0.00" readonly>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label>IVA</label>
                        <input type="text" class="form-control form-control-lg" name="iva" id="iva" value="0.00" readonly>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label>Comisión</label>
                        <input type="text" class="form-control form-control-lg" name="comision" id="comision" value="0.00" readonly>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label>Comisión Total</label>
                        <input type="text" class="form-control form-control-lg" name="comision_total" id="comision_total" value="0.00" readonly>
                    </div>
                    <div class="form-group col-12">
                        <p>Comisión por transacción exitosa: 3.9% + IVA</p>
                        <p>Siempre puedes usar la calculadora de <a href="https://conekta.com/pricing" target="_blank">CONEKTA</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/calculadora.js?n='.uniqid())}}"></script>
@endsection
