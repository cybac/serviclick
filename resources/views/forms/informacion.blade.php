@extends('layouts.admin')
@section('titulo',  "Administrador - Editar Información de contacto")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">Editar Información de contacto</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                <form action="{{ route('actualizar_informacion') }}"
                    method="POST" class="form_files row" id="form_informacion" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-8">
                        <label for="correo">Correo <span class="text-danger">*</span></label>
                        <input type="email" class="form-control form-control-lg" name="correo" id="correo" value="{{ $informacion->correo }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="telefono" id="telefono" value="{{ str_replace(' ','',$informacion->telefono) }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="direccion">Dirección <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="direccion" id="direccion" value="{{$informacion->direccion }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar Información</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/numeros.js?n='.uniqid())}}"></script>
@endsection
