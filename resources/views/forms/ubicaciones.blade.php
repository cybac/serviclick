@extends('layouts.admin')
@section('titulo', $form_edit ? 'Administrador - Editar Ubicación' : 'Administrador - Nueva Ubicación')
@section('contenido')
    <div class="page-header">
        <h3 class="page-title">{{ $form_edit ? 'Editar Ubicación' : 'Nueva Ubicación' }}</h3>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ $form_edit ? route('actualizar_ubicacion', $ubicacion->id) : route('guardar_ubicacion') }}" method="POST" class="form_files row" id="form_categoria" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12 col-md-6">
                            <label for="alias">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $ubicacion->nombre : '' }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="mapa">Coordenadas <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="mapa" id="mapa" value="{{ $form_edit ? $ubicacion->ubicacion : '16.75360420439424, -93.11623418107793' }}" readonly>
                            <span class="invalid-feedback"></span>
                            <div style="width: 100%; height:400px; margin-top:20px" id="map"></div>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                            <a href="{{ route('control_sc_ubicaciones') }}" class="btn btn-danger btn-lg">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        var mbUrl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
        @if ($form_edit)
            var localizar = [{{ $ubicacion->ubicacion }}];
            var nivel_zoom = 13;
        @else
            var localizar = [16.615137799987075, -92.5274673199478];
            var nivel_zoom = 7;
        @endif
        var map = L.map('map', {
            center: localizar,
            zoom: nivel_zoom,
            minZoom: 6,
            maxZoom: 18,
        });
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        map.doubleClickZoom.disable();
        var marker = L.marker(localizar, {
            draggable: 'true',
            autoPan: 'true',
            autoPanPadding: L.point(50, 50)
        }).addTo(map);

        marker.on('dragend', function(e) {
            var position = marker.getLatLng();
            var lacosa = document.getElementById('mapa');
            lacosa.value = `${position.lat}, ${position.lng}`;
            setTimeout(function() {
                map.panTo(new L.LatLng(`${position.lat}`, `${position.lng}`));
            }, 500);
        });

        marker.on('move', function(e) {
            var position = marker.getLatLng();
            var lacosa = document.getElementById('mapa');
            lacosa.value = `${position.lat}, ${position.lng}`;
        });

        map.on('dblclick', function(e) {
            marker.setLatLng([e.latlng.lat, e.latlng.lng]);
            setTimeout(function() {
                map.panTo(new L.LatLng(e.latlng.lat, e.latlng.lng));
            }, 500);
        });
    </script>
@endsection
