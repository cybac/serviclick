@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Habilidad" : "Administrador - Nueva Habilidad")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Habilidad" :"Nueva Habilidad" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                <form action="{{ $form_edit ? route('actualizar_habilidad', $habilidad->id) : route('guardar_habilidad') }}"
                    method="POST" class="form_files row" id="form_habilidad" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-8">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $habilidad->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-6 col-md-4">
                        <label for="cantidad">Cantidad: <span id="ponderacion">{{ $form_edit ? $habilidad->cantidad : "50" }}</span></label>
                        <input type="range" class="form-control form-control-lg ponderacion" name="cantidad" id="cantidad" value="{{ $form_edit ? $habilidad->cantidad : "50" }}" min="1" max="100">
                        <span class="invalid-feedback"></span>
                    </div>

                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('control_sc_web_habilidades') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/valoracion.js?n='.uniqid())}}"></script>
@endsection
