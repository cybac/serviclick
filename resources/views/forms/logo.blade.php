@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Logo" : "Administrador - Nuevo Logo")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Edición del logo" :"Nueva Logo" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_logo_negocio', $id) : route('guardar_logo_negocio', $id) }}"
                    method="POST" class="form_files row" id="form_negocio" enctype="multipart/form-data">
                    @csrf
                    @if ($form_edit)
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">logo <span class="text-danger">*</span></label>
                        <div class="text-center">
                            <img src="{{ asset('images/negocios/'.$negocio->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">Logo <span class="text-danger">*</span></label>
                        <input type="file" name="imagen" id="imagen" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                        <a href="{{ route('control_sc_negocios') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
