@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Categoria" : "Administrador - Nueva Categoria")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Categoria" :"Nueva Categoria" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_categoria', $categoria->id) : route('guardar_categoria') }}" method="POST" class="form_files row" id="form_categoria" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-6">
                        <label for="alias">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $categoria->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>

                    @if ($form_edit)
                    <div class="form-group col-12 col-md-6">
                        <label for="imagen">Imagen <span class="text-danger">*</span></label>
                        <div class="text-center">
                            <img src="{{ asset('images/areas/'.$categoria->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-6">
                        <label for="imagen">Imagen (800 x 600) <span class="text-danger">*</span></label>
                        <input type="file" name="imagen" id="imagen" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                        <a href="{{ route('control_sc_categorias') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
