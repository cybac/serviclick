@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Correo" : "Administrador - Nueva Correo")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Correo" :"Nueva Correo" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_correo', $correo->id) : route('guardar_catcorreo') }}"
                    method="POST" class="form_files row" id="form_correo" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-6">
                        <label for="tipo">Tipo</label>
                        <input type="text" class="form-control form-control-lg" name="tipo" id="tipo" value="{{ $form_edit ? $correo->tipo : "" }}" readonly>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control form-control-lg" name="titulo" id="titulo" value="{{ $form_edit ? $correo->titulo : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="mensaje">Mensaje <span class="text-danger">*</span></label>
                        <script src="https://cdn.tiny.cloud/1/vwzpp78mw7q9z5bpxjzyxrrwf7e5ojz10ey7qcy9jkx5ygfk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                        <textarea cols="30" rows="4" name="mensaje" id="mensaje" class="form-control my-editor">{!! $correo->mensaje !!}</textarea>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar</button>
                        <a href="{{ route('control_sc_emails') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/editor.js?n='.uniqid())}}"></script>
@endsection
