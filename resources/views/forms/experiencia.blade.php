@extends('layouts.admin')
@section('titulo',  "Administrador - Editar Información de nuestra experiencia")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">Editar Información de Nuestra experiencia</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('actualizar_experiencia') }}" method="POST" class="form_files row" id="form_informacion" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-12">
                        <label for="contenido">Contenido </label>
                        <script src="https://cdn.tiny.cloud/1/vwzpp78mw7q9z5bpxjzyxrrwf7e5ojz10ey7qcy9jkx5ygfk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                        <textarea cols="30" rows="4" name="contenido" id="contenido" class="form-control my-editor">{!! $experiencia->contenido !!}</textarea>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar Información</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/editor.js?n='.uniqid())}}"></script>
@endsection
