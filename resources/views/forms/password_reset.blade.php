@extends('layouts.admin')
@section('titulo',  "Administrador - Cambiar Contraseña")
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('actualizar_password') }}" method="POST" class="form_files row" id="form_informacion" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-12">
                        <h3 class="page-title text-center">Cambiar Contraseña</h3>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="current_password">Contraseña Actual <span class="text-danger">*</span></label>
                        <input type="password" class="form-control form-control-lg" name="current_password" id="current_password" value="" required>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="password">Nueva Contraseña <span class="text-danger">*</span></label>
                        <input type="password" class="form-control form-control-lg" name="password" id="password" value="" required>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="password_confirmation">Confirmar Nueva Contraseña <span class="text-danger">*</span></label>
                        <input type="password" class="form-control form-control-lg" name="password_confirmation" id="password_confirmation" value="" required>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar Contraseña</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
