@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Testimonial" : "Administrador - Nuevo Testimonial")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Testimonial" :"Nuevo Testimonial" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_testimonial', $testimonial->id) : route('guardar_testimonial') }}"
                    method="POST" class="form_files row" id="form_testimonial" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-8">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $testimonial->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="trabajo">Trabajo <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="trabajo" id="trabajo" value="{{ $form_edit ? $testimonial->trabajo : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <label for="contenido">Contenido <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="contenido" id="contenido" value="{{ $form_edit ? $testimonial->contenido : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    
                    @if ($form_edit)
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">Foto (375 * 520)<span class="text-danger">*</span></label>
                        <div class="text-center">
                            <img src="{{ asset('images/testimoniales/'.$testimonial->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">Foto (375 * 520)<span class="text-danger">*</span></label>
                        <input type="file" name="imagen" id="imagen" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('control_sc_web_testimoniales') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
