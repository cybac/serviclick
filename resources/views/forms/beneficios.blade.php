@extends('layouts.admin')
@section('titulo',  $form_edit ? "Administrador - Editar Beneficio" : "Administrador - Nueva Beneficio")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">{{ $form_edit ?"Editar Beneficio" :"Nuevo Beneficio" }}</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('actualizar_beneficio', $beneficio->id) : route('guardar_beneficio') }}"
                    method="POST" class="form_files row" id="form_beneficio" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="titulo">Titulo <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="titulo" id="titulo" value="{{ $form_edit ? $beneficio->titulo : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-8">
                        <label for="descripcion">Contenido <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="descripcion" id="descripcion" value="{{ $form_edit ? $beneficio->descripcion : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    @if ($form_edit)
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">Icono <span class="text-danger">*</span></label>
                        <div class="text-center">
                            <img src="{{ asset('images/fortalezas/'.$beneficio->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-12">
                        <label for="imagen">Icono <span class="text-danger">*</span></label>
                        <input type="file" name="imagen" id="imagen" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('control_sc_web_beneficios') }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
