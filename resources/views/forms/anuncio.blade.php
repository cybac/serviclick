@extends('layouts.admin')
@section('titulo',  "Administrador - Editar Anuncio App")
@section('contenido')
<div class="page-header">
    <h3 class="page-title">Editar Anuncio App</h3>
</div>
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('actualizar_anuncio') }}" method="POST" class="form_files row" id="form_anuncio" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-10">
                        <label for="imagen">Imagen (720 x 1280)<span class="text-danger">*</span></label>
                        <div class="text-center">
                            <img src="{{ asset('images/anuncio/'.$anuncio->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-2">
                        <label for="imagen" class="w-100">Visible </label>
                        <span class="switch">
                        <input type="checkbox" {{$anuncio->status ? "checked" : ""}} onchange="Cambio_anuncio(1);" class="switch" id="status_1">
                        <label for="status_1"></label>
                        </span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Actualizar Anuncio</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection
