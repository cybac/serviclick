@extends('layouts.admin')
@section('titulo',  "Administrador - Enviar recordatorio a ".$negocio->nombre)
@section('area', 'Enviar recordatorio a '.$negocio->nombre)
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('control_sc_enviar_recordatorio', $id) }}"
                    method="POST" class="form_files row" id="form_recordatorios" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-12 col-md-12">
                        <label for="mensaje">Recordatorio para {{$negocio->nombre}}</label>
                        <script src="https://cdn.tiny.cloud/1/vwzpp78mw7q9z5bpxjzyxrrwf7e5ojz10ey7qcy9jkx5ygfk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                        <textarea cols="30" rows="4" name="mensaje" id="mensaje" class="form-control my-editor"></textarea>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Enviar</button>
                        <a href="{{ route('control_sc_recordatorios_negocios', $id) }}" class="btn btn-danger btn-lg">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/js/editor.js?n='.uniqid())}}"></script>
@endsection
