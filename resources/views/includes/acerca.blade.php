<section class="gl-about-page-header-wrapper">
    <div class="container">
        <div class="row">
            <h1 class="gl-page-title">Acerca De</h1>
        </div>
    </div>
</section>
<section class="gl-page-content-section">
    <div class="gl-about-compnay-section gl-inner-section">
        <div class="container">
            <div class="row">
                <div class="gl-about-company-image gl-left-side col-md-6 col-sm-6 col-xs-12">
                    <div class="gl-page-img-wrapper">
                        <img src="{{asset('images/'.$about->imagen)}}" alt="About Us" class="gl-lazy">
                    </div>
                </div>
                <div class="gl-about-company-text gl-right-side col-md-6 col-sm-6 col-xs-12">
                    <h3 class="gl-single-title">Acerca de la empresa</h3>
                    {!!$about->contenido !!}
                </div>
            </div>
        </div>
    </div>
    @if (count($equipo) > 0)
    <div class="gl-expert-team-section gl-inner-section">
        <div class="container">
            <div class="row">
                <div class="gl-section-headings">
                    <h1>Nuestro equipo de expertos</h1>
                    <p>El corazon de la empresa</p>
                </div>
                <div class="gl-team-wrapper">
                    @foreach ($equipo as $item)
                    <div class="gl-agents-box col-md-3 col-sm-6 col-xs-12 appear fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
                        <div class="gl-agents-img-wrapper">
                            <img src="{{asset('images/about/'.$item->imagen)}}" alt="" class="gl-lazy">
                        </div>
                        <div class="gl-feat-item-details">
                            <h3>
                                <a>{{$item->nombre}}</a>
                            </h3>
                            <p>{{$item->puesto}}</p>
                        </div>
                    </div>
                    @endforeach                    
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="gl-our-expertise-section gl-inner-section">
        <div class="container">
            <div class="row">
                <div class="gl-expertise-text-wrapper gl-left-side col-md-6 col-sm-6 col-xs-12">
                    <h3 class="gl-single-title">Nuestra experiencia</h3>
                        {!! $experiencia->contenido !!}
                </div>
                <div class="gl-progress-wrapper gl-right-side col-md-6 col-sm-6 col-xs-12">
                    <h3 class="gl-single-title">Nuestras habilidades</h3>
                    @foreach ($habilidades as $item)
                    <div class="gl-skillbar clearfix" data-percent="{{$item->cantidad}}">
                        <p class="gl-skillbar-title">{{$item->nombre}}</p>
                        <div class="gl-skillbar-bar-wrapper">
                            <div class="gl-skillbar-bar"></div>
                        </div>
                        <span class="skill-bar-percent"></span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
