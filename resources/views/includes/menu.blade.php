<header class="gl-header">
    <div class="gl-header-bottombar">
        <nav class="navbar gl-header-main-menu" role="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{route('home')}}"><img class="logo" src="{{asset('images/logo-header.png')}}" alt="GLIMPSE"></a>
                    </div>
                    <div id="nav-menu" class="navbar-collapse gl-menu-wrapper collapse" role="navigation">
                        <ul class="nav navbar-nav gl-menus">
                            @if ($_SERVER["REQUEST_URI"] == "/")
                                <li class="active"><a href="{{route('home')}}">Inicio</a></li>
                            @else
                                <li><a href="{{route('home')}}">Inicio</a></li>
                            @endif
                            @if ($_SERVER["REQUEST_URI"] == "/directorio")
                                <li class="active"><a href="{{route('directory')}}">Directorio</a></li>
                            @else
                                <li><a href="{{route('directory')}}">Directorio</a></li>
                            @endif
                            @if ($_SERVER["REQUEST_URI"] == "/acerca")
                                <li class="active"><a href="{{route('about')}}">Acerca de</a></li>
                            @else
                                <li><a href="{{route('about')}}">Acerca de</a></li>
                            @endif
                            @if ($_SERVER["REQUEST_URI"] == "/contactanos")
                                <li class="active"><a href="{{route('contact')}}">Contactanos</a></li>
                            @else
                                <li><a href="{{route('contact')}}">Contactanos</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="gl-extra-btns-wrapper">
                        @guest
                        <button class="gl-login-btn mt-0 mb-0" id="gl-side-menu-btn"><span class="d-none-movil">Iniciar Sesión</span></button>
                        <a class="gl-login-btn gl-register-btn mt-0 mb-0" href="{{route('registrarse')}}"><span class="d-none-movil">Registrarse</span></a>
                        @else
                        @hasrole('administrador')
                        <i class="fas fa-tools exit" onclick="event.preventDefault(); window.location='{{route('control_sc')}}'"></i><button onclick="event.preventDefault(); window.location='{{route('control_sc')}}'" class="btn-login">Control Administrador</button>
                        @endhasrole
                        @hasrole('negocio')
                        <i class="fas fa-tools exit" onclick="event.preventDefault(); window.location='{{route('panel_control')}}'"></i><button onclick="event.preventDefault(); window.location='{{route('panel_control')}}'" class="btn-login">Panel de Control</button>
                        @endhasrole
                        <i class="fas fa-sign-out-alt exit" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"></i><button class="dropdown-item btn-login" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesión</button>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
                        </div>
                        @endguest
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>