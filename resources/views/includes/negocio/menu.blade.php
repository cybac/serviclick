<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">
        <ul>
            <li class="menu-title">Bienvenido</li>
            <li>
                <a href="{{route('panel_control')}}" style="background-color: transparent">
                    <i class="mdi mdi-home"></i> 
                    <span class="mi-menu"> Inicio</span>
                </a>
            </li>
            <li>
                <a href="{{route('panel_negocio')}}" style="background-color: transparent">
                    <i class="mdi mdi-store"></i> 
                    <span class="mi-menu"> Mi Negocio</span>
                </a>
            </li>
            <li>
                <a href="{{route('panel_galeria')}}" style="background-color: transparent">
                    <i class="mdi mdi-folder-multiple-image"></i> 
                    <span class="mi-menu"> Mi Galeria</span>
                </a>
            </li>
            <li>
                <a href="{{route('panel_redes')}}" style="background-color: transparent">
                    <i class="mdi mdi-facebook"></i> 
                    <span class="mi-menu"> Mis redes</span>
                </a>
            </li>
            <li>
                <a href="{{route('panel_keywords')}}" style="background-color: transparent">
                    <i class="mdi mdi-key"></i> 
                    <span class="mi-menu"> Keywords</span>
                </a>
            </li>
            <li>
                <a href="{{route('panel_historial')}}" style="background-color: transparent">
                    <i class="mdi mdi-currency-usd"></i> 
                    <span class="mi-menu"> Historial de pagos</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>