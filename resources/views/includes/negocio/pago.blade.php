<link rel="stylesheet" href="{{asset('css/oxxo.css?n='.uniqid())}}">
<div class="row">
    <div class="col-12">
        @if ($expiro)
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3 style="color: #ff0000; margin-bottom: 0.8rem; font-size: 20px;">Su ficha ha expirado</h3>
                <p style="margin-top: 0; margin-bottom: 0.7rem;">La ficha de pago del negocio ha caducado.</p>
                <p style="margin-top: 0; margin-bottom: 0.7rem;">Si requiere una nueva ficha, solicitela dando click al boton de abajo.</p>
                <p style="margin-top: 0; margin-bottom: 0.7rem;">Se le generara una nueva ficha de pago, podra consultarla aqui mismo o en su correo.</p>
                <p style="margin-top: 0; margin-bottom: 0.7rem;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>
                <div class="row">
                    <div class="col-12 col-md-4"><p style="margin-top: 0; margin-bottom: 0.7rem;">&nbsp;</p></div>
                    <div class="col-12 col-md-4"><button class="btn btn-success w-100 ficha" data-url="{{route('renovar_ficha_negocio')}}">Ficha nueva</button></div>
                    <div class="col-12 col-md-4"><p style="margin-top: 0; margin-bottom: 0.7rem;">&nbsp;</p></div>
                </div>
            </div>
        </div>
        <script src="{{asset('negocio/js/ficha.js?n='.uniqid())}}"></script>
        @else
        <div class="opps">
            <div class="opps-header">
                <div class="opps-reminder">Ficha digital. No es necesario imprimir.</div>
                <div class="opps-info">
                    <div class="opps-brand"><img src="{{asset('images/oxxopay_brand.png')}}" alt="OXXOPay"></div>
                    <div class="opps-ammount">
                        <h3>Monto a pagar</h3>
                        <h2>$ <span class="money">{{$monto/100}}</span> <sup>{{$moneda}}</sup></h2>
                        <p>OXXO cobrar&aacute; una comisi&oacute;n adicional al momento de realizar el pago.</p>
                    </div>
                </div>
                <div class="opps-reference">
                    <h3>Referencia</h3>
                    <h1>{{$referencia}}</h1>
                </div>
            </div>
            <div class="opps-instructions">
                <h3>Instrucciones</h3>
                <ol>
                    <li>Acude a la tienda OXXO m&aacute;s cercana. <a href="https://www.google.com.mx/maps/search/oxxo/" target="_blank">Encu&eacute;ntrala aqu&iacute;</a>.</li>
                    <li>Indica en caja que quieres realizar un pago de <strong>OXXOPay</strong>.</li>
                    <li>Dicta al cajero el n&uacute;mero de referencia en esta ficha para que tecle&eacute; directamete en la pantalla de venta.</li>
                    <li>Realiza el pago correspondiente con dinero en efectivo.</li>
                    <li>Al confirmar tu pago, el cajero te entregar&aacute; un comprobante impreso. <strong>En el podr&aacute;s verificar que se haya realizado correctamente.</strong> Conserva este comprobante de pago.</li>
                </ol>
                <div class="opps-footnote">
                    <p class="mr-1 ml-1 mb-0">Al completar estos pasos recibir&aacute;s un correo de <strong>ServiClick</strong> confirmando tu pago.</p>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>