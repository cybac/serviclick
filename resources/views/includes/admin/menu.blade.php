<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">
        <ul>
            <li class="menu-title">Bienvenido</li>
            <li>
                <a href="{{route('control_sc')}}" style="background-color: transparent">
                    <i class="mdi mdi-home"></i> 
                    <span class="mi-menu"> Inicio</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent"><i class="mdi mdi-store"></i><span class="mi-menu"> Negocios <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('control_sc_negocios')}}"><i class="mdi mdi-check-all"></i>Aprobados</a></li>
                    <li><a href="{{route('control_sc_negocios_tramites')}}"><i class="mdi mdi-timer-sand"></i>Tramitando</a></li>
                    <li><a href="{{route('control_sc_negocios_suspendidos')}}"><i class="mdi mdi-alert"></i>Suspendidos</a></li>
                    <li><a href="{{route('control_sc_documentacion')}}"><i class="mdi mdi-file-outline"></i>Documentación</a></li>
                    <li><a href="{{route('comentarios')}}"><i class="mdi mdi-star-circle"></i>Reseñas</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('control_sc_pagos')}}" style="background-color: transparent">
                    <i class="mdi mdi-currency-usd"></i> 
                    <span class="mi-menu"> Historial de pagos</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_categorias')}}" style="background-color: transparent">
                    <i class="mdi mdi-tag"></i> 
                    <span class="mi-menu"> Categorias</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_ubicaciones')}}" style="background-color: transparent">
                    <i class="mdi mdi-map-marker-radius"></i> 
                    <span class="mi-menu"> Ubicaciones</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_criterios')}}" style="background-color: transparent">
                    <i class="mdi mdi-star-half"></i> 
                    <span class="mi-menu"> Criterios de evaluación</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_emails')}}" style="background-color: transparent">
                    <i class="mdi mdi-email"></i> 
                    <span class="mi-menu"> Correos</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_notificaciones')}}" style="background-color: transparent">
                    <i class="mdi mdi-cellphone"></i> 
                    <span class="mi-menu"> Notificaciones</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_precios')}}" style="background-color: transparent">
                    <i class="mdi mdi-cash"></i> 
                    <span class="mi-menu"> Pago</span>
                </a>
            </li>
            <li>
                <a href="{{route('control_sc_anuncios')}}" style="background-color: transparent">
                    <i class="mdi mdi-file-image"></i> 
                    <span class="mi-menu"> Anuncio App</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent"><i class="mdi mdi-web"></i><span class="mi-menu"> La Web <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('control_sc_web_slider_text')}}"><i class="mdi mdi-alphabetical"></i>Slider de textos</a></li>
                    <li><a href="{{route('control_sc_web_beneficios')}}"><i class="mdi mdi-chart-bar"></i>Beneficios</a></li>
                    <li><a href="{{route('control_sc_web_testimoniales')}}"><i class="mdi mdi-camera"></i>Testimoniales</a></li>
                    <li><a href="{{route('control_sc_web_sucursales')}}"><i class="mdi mdi-store"></i>Sucursal</a></li>
                    <li><a href="{{route('control_sc_web_equipo')}}"><i class="mdi mdi-briefcase"></i>Nuestro Equipo</a></li>
                    <li><a href="{{route('control_sc_web_experiencia')}}"><i class="mdi mdi-worker"></i>Nuestra Experiencia</a></li>
                    <li><a href="{{route('control_sc_web_habilidades')}}"><i class="mdi mdi-chart-pie"></i>Nuestras Habilidades</a></li>
                    <li><a href="{{route('control_sc_web_informacion')}}"><i class="mdi mdi-contact-mail"></i>Info. de contacto</a></li>
                    <li><a href="{{route('control_sc_web_redes')}}"><i class="mdi mdi-facebook"></i>Redes Sociales</a></li>
                    <li><a href="{{route('control_sc_web_privacy')}}"><i class="mdi mdi-alert"></i>Aviso de privacidad</a></li>
                    <li><a href="{{route('control_sc_web_terms')}}"><i class="mdi mdi-account-multiple"></i>Términos del servicio</a></li>
                    <li><a href="{{route('control_sc_web_about')}}"><i class="mdi mdi-information"></i>Acerca de</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>