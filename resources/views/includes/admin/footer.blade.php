<footer class="footer">
    © <?php echo date("Y",strtotime("-1 year")); ?> - <?php echo date("Y"); ?> ServiClick 
    <span class="text-muted d-none d-sm-inline-block float-right">Todos los derechos reservados</span>
</footer>