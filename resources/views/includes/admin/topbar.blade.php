<div class="topbar">
    <nav class="navbar-custom">
        <div class="search-wrap" id="search-wrap">
            <div class="search-bar">
                <input class="search-input" type="search" placeholder="Search" />
                <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                    <i class="mdi mdi-close-circle"></i>
                </a>
            </div>
        </div>

        <ul class="list-inline float-right mb-0">
            @hasrole('administrador')
            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <i class="ion-ios7-bell noti-icon"></i>
                    @if (count($alertas) > 0)
                    <span class="badge badge-danger noti-icon-badge">{{count($alertas)}}</span>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                    <div class="noti-title">
                        <h5> {{(count($alertas) > 0) ? "Notificaciones (".count($alertas).")" : "Sin Notificaciones"}} </h5>
                    </div>

                    @foreach ($alertas as $item)
                    <a href="{{route('control_sc_ver_alerta', $item->id)}}" class="dropdown-item notify-item">
                        <div class="notify-icon {{$item->importancia}}"><i class="{{$item->icono}}"></i></div>
                        <p class="notify-details"><b>{{$item->titulo}}</b><small class="text-muted">{{$item->contenido}}</small></p>
                    </a>

                    @endforeach
                    <a href="{{route('control_sc_alertas')}}" class="dropdown-item notify-item">
                        Ver todas
                    </a>
                </div>
            </li>
            @endhasrole

            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none nav-user" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="false" aria-expanded="false">
                    <img src="{{asset('admin/img/user.png')}}" alt="user" class="rounded-circle">
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                @hasrole('administrador')
                <a class="dropdown-item" href="{{route('control_sc_password')}}"><i class="dripicons-toggles text-muted"></i> Cambiar Contraseña</a>
                @endhasrole
                <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="dripicons-exit text-muted"></i> Cerrar Sesión</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
                </div>
            </li>
        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="list-inline-item hide-movil-asistente">
                <button type="button" class="button-menu-mobile open-left">
                    <i class="ion-navicon"></i>
                </button>
            </li>
            <li class="hide-phone list-inline-item app-search">
                <h3 class="page-title">@yield('area')</h3>
            </li>
        </ul>

        <div class="clearfix"></div>
    </nav>

</div>