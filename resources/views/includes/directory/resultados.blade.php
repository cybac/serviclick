<!-- SEARCH META -->
<div class="gl-search-meta-wrapper">
    @if (isset($_GET['page']))
        @if($negocios->count() == 6)
            <p class="gl-search-number">Mostrando  {{($_GET['page'] * 6) - 5}} de {{$_GET['page'] * 6}} de {{$total->count()}} resultados encontrados</p>
        @else
            <p class="gl-search-number">Mostrando  {{($_GET['page'] * 6) - 5}} de {{$total->count()}} de {{$total->count()}} resultados encontrados</p>
        @endif
    @else
        @if($negocios->count() > 0)
        <p class="gl-search-number">Mostrando 1 de {{$negocios->count()}} de {{$total->count()}} resultados encontrados</p>
        @else
        <p class="gl-search-number">Sin resultados para la busqueda</p>
        @endif
    @endif
    <a href="{{route('directory')}}" class="gl-reset-btn">Reiniciar Busqueda</a>
</div>
<!-- SEARCH META END -->

<div class="gl-search-result-wrapper">
    <div class="panel-group" id="featured-job" role="tablist" aria-multiselectable="true">
        @foreach ($negocios as $item)
            <!-- Job item -->
            <div class="panel panel-default gl-job-list-item">
                <!-- Job Excerpt -->
                <div class="gl-job-list-item-wrapper panel-heading" role="tab" id="job{{$item->id}}">
                    <a role="button" data-toggle="collapse" data-parent="#featured-job" href="#{{$item->id}}" aria-expanded="true"
                        aria-controls="{{$item->id}}" class="goto-map">
                        <div class="hidden" id="map{{$item->id}}"></div>
                        <!-- LOGO -->
                        <div class="gl-job-company-logo gl-job-item-part">
                            @if($item->imagen == "0")
                            <img src="{{asset('images/negocios/0.png')}}" class="gl-lazy">
                            @else
                            <img src="{{asset('images/negocios/'.$item->imagen)}}" class="gl-lazy">
                            @endif
                        </div>
                        <!-- END -->

                        <!-- JOB POSTION & COMPANY NAME -->
                        <div class="gl-job-position-company gl-job-item-part">
                            <h3>{{$item->nombre}}</h3><br>

                            <p class="gl-company-name">{{$item->categoria}}</p>
                        </div>
                        <!-- END -->

                        <!-- JOB AVAILABILITY -->
                        <div class="gl-job-availability gl-job-item-part">
                            @if (number_format($item->puntuacion, 1) == 0)
                                <span class="gl-item-status-label part-time-job">Sin Calificar</span>
                            @else
                                <span class="gl-item-status-label part-time-job">{{number_format($item->puntuacion, 1)}} / 5</span>
                            @endif
                        </div>
                        <!-- END -->

                        <!-- SALLERY -->
                        <div class="gl-job-sallery gl-job-item-part"></div>
                        <!-- END -->

                        <!-- AREA -->
                        <div class="gl-job-location gl-job-item-part">
                            <i class="ion-ios-location-outline"></i>
                            <span>{{$item->direccion}}</span>
                        </div>
                        <!-- END -->
                    </a>
                </div>
                <!-- END -->

                <!-- Job Details -->
                <div id="{{$item->id}}" class="panel-collapse collapse gl-job-list-details" role="tabpanel"
                    aria-labelledby="job{{$item->id}}">
                    <div class="panel-body">
                        <div class="gl-jobdetails-sec">
                            <h3>Acerca de Mí</h3>
                            {!! $item->descripcion !!}
                        </div>
                        <div class="gl-jobdetails-sec">
                            <a href="{{route('details_id', $item->id)}}" class="gl-btn gl-details-job">Detalles</a>
                        </div>
                    </div>
                </div>
                <!-- END -->
            </div>
        @endforeach
    </div>
</div>
{{ $negocios->onEachSide(0)->links() }}

<script>
    paginacion = document.getElementsByClassName("flex justify-between flex-1 sm:hidden")[0];   //Contiene el paginador
    if(paginacion != undefined)
    {
        numeros_paginacion = document.getElementsByClassName("relative z-0 inline-flex shadow-sm rounded-md")[0];   //Contiene los numeros del paginador
        flechapre = document.getElementsByClassName("relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-l-md leading-5")[0];  //Contiene la flecha previous (esto se borra mas abajo)
        flechanet = document.getElementsByClassName("relative inline-flex items-center px-2 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-r-md leading-5")[0];  //Contiene la flecha next (esto se borra mas abajo)
        nextlink = document.getElementsByClassName("relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300")[0];
        nextlink_fin = document.getElementsByClassName("relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md")[0];

        paginacion.appendChild(numeros_paginacion);
        if(flechapre != undefined)
        {
            flechapre.remove();
        }
        if(flechanet != undefined)
        {
            flechanet.remove();
        }
        if(nextlink != undefined)
        {
            paginacion.appendChild(nextlink);
        }else{
            paginacion.appendChild(nextlink_fin);
        }
    }
</script>
