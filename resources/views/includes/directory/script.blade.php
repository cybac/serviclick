<script>
    $(window).on('load', function() {
        ubicacion = [{{ $inicial->ubicacion }}];
        setTimeout(function() {
            @foreach ($negocios as $item)
                L.marker([{{ $item->mapa }}]).addTo(map).bindPopup('{{ $item->direccion }}').bindTooltip("{{ $item->nombre }}");
                //.openPopup();
            @endforeach
        }, 1000);
    });
</script>
