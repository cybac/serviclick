<!-- PAGE CONTETNT -->
<section class="gl-page-content-section">
    <div class="container-fluid">
        <div class="row">
            @include('includes.directory.mapa')
            <!-- SEARCH -->
            <div id="gl-result-section" class="gl-search-filter-result-wrapper">
                <!-- FILTER -->
                <div class="gl-search-filter-wrapper">
                    <form method="POST" action="{{route('directory_busqueda')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="gl-filter-options gl-search-keyword">
                            <input type="text" name="keysword" id="keysword" placeholder="Palabras claves">
                        </div>
                        <div class="gl-filter-options gl-search-location">
                            <input type="text" name="ubicacion" id="ubicacion" placeholder="Ubicación" list="ubicaciones">
                            <datalist id="ubicaciones">
                                @foreach ($ubicaciones as $item)
                                    <option value="{{$item->nombre}}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="gl-search-category gl-filter-options">
                            <select class="gl--search-category-selection" name="categoria" id="categoria">
                                <option>&nbsp;</option>
                                @foreach ($categorias as $item)
                                    <option value="{{$item->id}}">{{$item->nombre}}</option>    
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Filtrar" class="gl-btn">
                    </form>
                </div>
                <!-- FILTER END -->
                @include('includes.directory.resultados')
            </div>
            <!-- SEARCH END-->
        </div>
    </div>
</section>
<!-- PAGE CONTETNT END -->