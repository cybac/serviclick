<div class="gl-side-menu-overlay"></div>
<div class="gl-side-menu-wrap">
    <div class="gl-side-menu">
        <div class="gl-side-menu-widget-wrap">
            <div class="gl-login-form-wrapper">
                <h3>Bienvenido</h3>
                <p>Inicia sesión para acceder al servicio</p>
                <div class="gl-login-form">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                    @endif
                    <form method="POST" action="{{ route("login") }}">
                        @csrf
                        <input type="text" name="user" id="user" placeholder="Correo o Número Telefónico" value="{{ old('user') }}" autocomplete="username" required autofocus>
                        <input type="password" name="password" id="password" placeholder="Contraseña" autocomplete="none" required>
                        <button type="submit" id="submit">Iniciar Sesión</button>
                    </form>
                    <a href="{{route('password.request')}}" class="text-center w-100 m-1 ml-0 mr-0">¿Olvidaste tu contraseña?</a>
                </div>
                {{--
                <div class="gl-social-login-opt">
                    <a href="#" class="gl-social-login-btn gl-facebook-login">Login with Facebook</a>
                    <a href="#" class="gl-social-login-btn gl-twitter-login">Login with Twitter</a>
                </div>
                <div class="gl-other-options">
                    <a href="#" class="gl-forgot-pass">Forget Password ?</a>
                    <a href="#" class="gl-signup">Sign up</a>
                </div>
                --}}
            </div>
        </div>
    </div>

    <button class="gl-side-menu-close-button" id="gl-side-menu-close-button">Close Menu</button>
</div>