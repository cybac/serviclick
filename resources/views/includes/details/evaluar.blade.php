<!-- REVIEWS / COMMENT DETAILS -->
@if(count($comentarios) == 0)
<div class="gl-review-details">
    <h3 class="gl-content-title">0 Reseñas</h3>
    <!-- Reviews -->
    <div class="gl-reviews">
        <!-- TEXT -->
        <div class="gl-review-text">
            <h3>Se el primero en dejar una reseña del negocio</h3>
        </div>
        <!-- END -->
    </div>
    <!-- END -->
</div>
<!-- END -->


@else


<div class="gl-review-details">
    <h3 class="gl-content-title">{{count($comentarios)}} Reseñas</h3>
    <!-- Reviews -->
    @foreach ($comentarios as $item)
    <div class="gl-reviews">
        <!-- USER IMG -->
        <div class="gl-user-img">
            <img src="{{asset('images/author-img.png')}}" class="gl-lazy">
        </div>
        <!-- END -->

        <!-- TEXT -->
        <div class="gl-review-text">
            <h3>{{$item->nombre}}</h3>
            <p>{{$item->contenido}}</p>
            <span class="gl-item-rating"><i class="ion-ios-star"></i>{{$item->puntuacion}}</span>
        </div>
        <!-- END -->
    </div>
    @endforeach
    <!-- END -->
</div>
<!-- END -->
@endif

<!-- SUBMIT REVIEW -->
@if (Auth::check())
<div class="gl-review-submission appear fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
    <h3 class="gl-content-title">Escribenos una reseña sobre el negocio</h3>
    @if (session('resultado'))
    <div class="alert alert-success" role="alert" id="resena">
        {{ session('resultado') }}
    </div>
    @section('post-js')
    <script>
        $(document).ready(function () {
            $('html, body').animate({
                scrollTop: $('#resena').offset().top - 140
            }, 'slow');
        });
    </script>
    @endsection
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Upsssss!</strong> Hubo algunos problemas con tu entrada.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{route('comentar')}}" class="gl-review-submission-form" method="POST">
        @csrf
        <fieldset class="gl-form-fields">
            <div class="gl-input-area">
                <input type="hidden" name="negocio" id="negocio" value="{{$negocio->id}}">
                <textarea name="comentario" id="comentario" cols="30" rows="13" placeholder="Comentario u observación" required></textarea>
            </div>

            <div class="gl-rating-wrapper">
                <h4>Deja tu calificación</h4>
                @foreach ($criterios as $item)
                <div class="gl-rating-inputs">
                    <span class="gl-rating-title">{{$item->nombre}}</span>
                    <div class="gl-rating-wrap">
                        <span id="gl-quality-rating"></span>
                    </div>
                </div>
                @endforeach
            </div>
        </fieldset>

        <fieldset class="gl-submit-wrapper">
            <input type="submit" value="Evaluar" class="gl-btn">
        </fieldset>
    </form>
</div>
@else
<div class="gl-review-submission appear fadeIn" data-wow-duration=".5s" data-wow-delay=".5s">
    <h3 class="gl-content-title">Escribenos una reseña sobre el  negocio</h3>
    <p>Necesitar iniciar sesión para dejar una reseña del negocio</p>
</div>
@endif
<!-- END -->