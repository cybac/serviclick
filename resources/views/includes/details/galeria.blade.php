@if($galeria->count() > 0)
<div class="gl-profile-gallery">
    <h3 class="gl-content-title">Mis Trabajos</h3>
    <ul class="gl-gallery">
        @foreach ($galeria as $item)
            <li>
                <a href="{{asset('images/trabajos/'.$item->imagen)}}" class="gl-lightbox-img">
                    <picture>
                        <source media="(min-width: 768px)" srcset={{asset('images/trabajos/miniaturas/web/'.$item->imagen)}}>
                        <img alt="Category Image" srcset={{asset('images/trabajos/miniaturas/movil/'.$item->imagen)}}>
                    </picture>
                </a>
            </li>
        @endforeach

    </ul>
</div>

@endif
