@foreach ($relacionados as $item)
<div class="panel-group" id="featured-job-{{$item->id}}" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default gl-job-list-item">
        <!-- Job Excerpt -->
        <div class="gl-job-list-item-wrapper panel-heading" role="tab" id="jobTwo">
            <a role="button" data-toggle="collapse" data-parent="#featured-{{$item->id}}" href="#job{{$item->id}}Details" aria-expanded="true" aria-controls="job{{$item->id}}Details">
                <!-- LOGO -->
                <div class="gl-job-company-logo gl-job-item-part">
                    <img src="{{asset('images/negocios/'.$item->imagen)}}" class="gl-lazy">
                </div>
                <!-- END -->
    
                <!-- JOB POSTION & COMPANY NAME -->
                <div class="gl-job-position-company gl-job-item-part">
                    <h3>{{$item->nombre}}</h3>
                    <p class="gl-company-name">{{$item->categoria}}</p>
                </div>
                <!-- END -->
    
                <!-- JOB AVAILABILITY -->
                <div class="gl-job-availability gl-job-item-part">
                    @if (number_format($item->puntuacion, 1) == 0)
                        <span class="gl-item-status-label part-time-job">Sin Calificar</span>                        
                    @else
                        <span class="gl-item-status-label part-time-job">{{number_format($item->puntuacion, 1)}} / 5</span>
                    @endif
                </div>
                <!-- END -->
    
                <!-- SALLERY -->
                <div class="gl-job-sallery gl-job-item-part">
                </div>
                <!-- END -->
    
                <!-- AREA -->
                <div class="gl-job-location gl-job-item-part">
                    <i class="ion-ios-location-outline"></i>
                    <span>{{$item->direccion}}</span>
                </div>
                <!-- END -->
            </a>
        </div>
        <!-- END -->
    
        <!-- Job Details -->
        <div id="job{{$item->id}}Details" class="panel-collapse collapse gl-job-list-details" role="tabpanel" aria-labelledby="job{{$item->id}}">
            <div class="panel-body">
                <div class="gl-jobdetails-sec">
                    <h3>Acerca de Mí</h3>
                    {!! $item->descripcion !!}
                </div>
                <div class="gl-jobdetails-sec">
                    <a href="{{route('details_id', $item->id)}}" class="gl-btn gl-details-job">Detalles</a>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>
</div>
@endforeach