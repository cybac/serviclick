<!-- PAGE HEADER -->
<section class="gl-page-header-wrapper">
    <div class="container">
        <div class="row">
            @if (number_format($valoracion, 1) == 0)
                <h1>{{ $negocio->nombre }} <span class="gl-item-status-label part-time-job">Sin Calificar</span></h1>
            @else
                <h1>{{ $negocio->nombre }} <span class="gl-item-status-label part-time-job">{{ number_format($valoracion, 1) }}</span></h1>
            @endif
            <p><i class="ion-ios-location-outline"></i>{{ $negocio->direccion }}</p>
            <!--
            <div class="gl-page-head-btn-wrapper">
                <a href="#" class="gl-btn gl-icon-btn"><i class="far fa-paper-plane"></i></i>Contratar</a>
            </div>
            -->
        </div>
    </div>
</section>
<!-- PAGE HEADER -->

<!-- TAB NAV & META -->
<section class="gl-tab-profile-meta-section">
    <div class="container">
        <div class="row">
            <div class="gl-tab-nav-wrapper col-md-8 col-sm-8 col-xs-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Información</a></li>
                    <li role="presentation"><a href="#openjob" aria-controls="openjob" role="tab" data-toggle="tab">Negocios Similares ({{ $relacionados->count() }})</a></li>
                </ul>
            </div>

            <div class="gl-profile-meta-wrapper col-md-4 co-sm-4 col-xs-12">
                <ul>
                    <li><i class="fa fa-link"></i> <a target="_blank" href="{{ $negocio->web }}" style="color: #888888;">{{ $negocio->web }}</a> </li>
                    <li><i class="fa fa-phone"></i>{{ $negocio->telefono }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- TAB NAV & META END -->

<!-- PAGE CONTETNT -->
<section class="gl-page-content-section">
    <div class="container">
        <div class="row">
            <!-- PAGE CONTENT DETAILS -->
            <div class="gl-page-content tab-content col-md-8 col-sm-8 col-xs-12">

                <!-- GALLERY TAB -->
                <div role="tabpanel" class="tab-pane fade in active" id="overview">
                    @include('includes.details.galeria')
                    <!-- ABOUT JOB -->
                    <div class="gl-page-contents-wrapper gl-about-job" style="margin-bottom: 60px">
                        <h3 class="gl-content-title">Acerca de Mí</h3>
                        {!! $negocio->descripcion !!}
                        <h3 class="gl-content-title">Servicios</h3>
                        {!! $negocio->servicios !!}
                    </div>
                    @include('includes.details.evaluar')
                    <!-- END -->
                </div>
                <!-- GALLERY TAB END -->
                <!-- PROTFOLIO TAB -->
                <div role="tabpanel" class="tab-pane fade gl-open-job-tab" id="openjob">
                    <h3 class="gl-content-title">Negocios Relacionados <span class="gl-job-num">({{ $relacionados->count() }})</span></h3>
                    @include('includes.details.relacionados')
                </div>
                <!-- PROTFOLIO TAB END -->

            </div>
            <!-- PAGE CONTENT DETAILS END -->
            <!-- SIDEBAR -->
            <div class="gl-sidebar gl-page-sidebar col-md-4 col-sm-4 col-xs-12">
                <!-- SIDEBAR WIDGET -->
                <div class="gl-sidebar-widget gl-sidebar-job-meta">
                    <ul>
                        <li class="gl-job-meta-location">
                            <span class="gl-job-meta-title">Localización :</span>
                            <span class="gl-job-meta-text">{{ $negocio->direccion }}</span>
                        </li>
                        <li class="gl-job-meta-joindate">
                            <span class="gl-job-meta-title">Fecha de inicio :</span>
                            <span class="gl-job-meta-text">{{ date_format(new DateTime($negocio->created_at), 'd/m/Y') }}</span>
                        </li>
                        <li class="gl-job-meta-jobtype">
                            <span class="gl-job-meta-title">Categoría :</span>
                            <span class="gl-item-status-label part-time-job">{{ $categoria->nombre }}</span>
                        </li>
                        <li class="gl-job-meta-salary">
                            <span class="gl-job-meta-title">Costo aproximado :</span>
                            <span class="gl-job-meta-text">{{ $negocio->precio_min }} - {{ $negocio->precio_max }}</span>
                        </li>
                    </ul>
                </div>
                <!-- END -->
                <div class="gl-sidebar-widget gl-sidebar-map-widget">
                    <div id="gl-map-small" class="gl-map-small">
                        <div style="width: 100%; height:100%" id="map"></div>
                    </div>
                </div>
            </div>
            <!-- SIDEBAR END -->
        </div>
    </div>
</section>
<!-- PAGE CONTETNT END -->

<!-- APPLY MODAL -->
<div class="remodal-bg">
    <div class="gl-modal-wrapper" data-remodal-id="modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h3 class="gl-modal-title">Personal Info</h1>

            <div class="gl-modal-form-wrapper">
                <form action="#">
                    <fieldset>
                        <p class="gl-input-label">Name</p>
                        <input type="text" name="gl-applicant-name" id="gl-applicant-name" placeholder="Full Name">
                    </fieldset>

                    <fieldset>
                        <p class="gl-input-label">Email</p>
                        <input type="text" name="gl-applicant-email" id="gl-applicant-email" placeholder="Email">
                    </fieldset>

                    <fieldset>
                        <p class="gl-input-label">Address</p>
                        <input type="text" name="gl-applicant-address" id="gl-applicant-address" placeholder="Address">
                    </fieldset>

                    <fieldset>
                        <p class="gl-input-label">Cover Letter</p>
                        <textarea name="gl-applicant-letter" id="gl-applicant-letter" cols="30" rows="7" placeholder="Cover Letter"></textarea>
                    </fieldset>

                    <fieldset>
                        <p class="gl-input-label">Upload</p>

                        <input type="file" name="gl-applicant-resume" id="gl-applicant-resume">
                        <label for="gl-applicant-resume">upload</label>

                        <span class="gl-extra-info">we allow only .doc and .pdf file</span>
                    </fieldset>

                    <fieldset>
                        <p class="gl-input-label">Your Name</p>
                        <input type="text" name="gl-applicant-yname" id="gl-applicant-yname" placeholder="Your Name">
                    </fieldset>

                    <fieldset class="gl-apply-btns-wrapper">
                        <button class="gl-apply-btn gl-btn" type="submit">Apply</button>
                        <button class="gl-apply-linkedin-btn gl-btn">Apply from Linkedin</button>
                    </fieldset>
                </form>
            </div>
    </div>
</div>
<!-- APPLY MODAL END -->

<!-- SHARE MODAL -->
<div class="remodal-bg">
    <div class="gl-share-modal-wrapper" data-remodal-id="modal-share">
        <button data-remodal-action="close" class="remodal-close"></button>

        <div class="gl-share-opt-wrapper">
            <ul>
                <li class="gl-fb-share">
                    <a href="#">
                        <i class="fa fa-facebook"></i>
                        <span>Facebook</span>
                    </a>
                </li>

                <li class="gl-twitter-share">
                    <a href="#">
                        <i class="fa fa-twitter"></i>
                        <span>Twitter</span>
                    </a>
                </li>

                <li class="gl-gplus-share">
                    <a href="#">
                        <i class="fa fa-google-plus"></i>
                        <span>Google+</span>
                    </a>
                </li>

                <li class="gl-linkedin-share">
                    <a href="#">
                        <i class="fa fa-linkedin"></i>
                        <span>Linkedin</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@php
    $ubicacion = explode(',', $negocio->mapa);
    echo '<script>
        var ubicacion_x = "'.$ubicacion[0].'",
            ubicacion_y = "'.$ubicacion[1].'";
    </script>';
@endphp

<script>
    var mbUrl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';

    var map = L.map('map', {
        center: [ubicacion_x, ubicacion_y],
        zoom: 14,
        minZoom: 4,
        maxZoom: 17,
    });

    L.tileLayer(mbUrl, {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    L.marker([ubicacion_x, ubicacion_y]).addTo(map);
    ubicacion_x = undefined;
    ubicacion_y = undefined;
</script>
