<!-- FOOTER -->
<footer>
    <!-- FOOTER TOP -->
    <div class="gl-footer-top-wrapper">
        <div class="container">
            <div class="row">
                <div class="gl-footer-widget gl-useful-links">
                    <h3 class="gl-footer-widget-title">Mapa de sitio</h3>
                    <div class="gl-footer-widget-content">
                        <ul>
                            <li><a href="{{route('about')}}">Acerca de</a></li>
                            <li><a href="{{route('privacy')}}">Privacidad</a></li>
                            <li><a href="{{route('terms')}}">Condiciones</a></li>
                        </ul>
                    </div>
                </div>

                <div class="gl-footer-widget gl-contact-info">
                    <h3 class="gl-footer-widget-title">Información de Contacto</h3>

                    <div class="gl-footer-widget-content">
                        <ul>
                            @foreach ($contacto as $item)
                            <li><i class="fa fa-map-marker"></i>{{$item->direccion}}</li>
                            <li><i class="fa fa-phone"></i>{{$item->telefono}}</li>
                            <li><i class="fa fa-envelope-o"></i>{{$item->correo}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="gl-footer-widget gl-subscribe-widget" style="border-left: none;">
                    <div class="gl-footer-widget-content"></div>
                </div>

                <div class="gl-footer-widget gl-footer-logo gl-footer-fixed-widget pt-0" style="border-left: none;">
                    <a href="{{route('home')}}">
                        <img src="{{asset('images/logo-footer.png')}}">
                    </a>
                </div>


                {{-- <div class="gl-footer-widget gl-subscribe-widget">
                    <h3 class="gl-footer-widget-title">Suscribete</h3>
                    <div class="gl-footer-widget-content">
                        <p>Recibir ofertas del sitio</p>

                        <form action="#">
                            <input type="email" name="gl-subscription" id="gl-subscription"
                                placeholder="Tu Correo Electrónico">
                            <button type="submit" class="gl-btn">Suscribirse</button>
                        </form>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    <div class="gl-footer-bottom-wrapper">
        <div class="container">
            <div class="row">
                <div class="gl-copyright-info-wrapper">
                    <p>Copyright &copy; 2021 ServiClick. Todos los derechos reservados</p>
                    <a target="_blank" href="https://www.grupocybac.com/">Hosting y Diseño Web CYBAC TI.</a>
                </div>

                <div class="gl-social-info-wrapper">
                    <ul>
                        @foreach ($sociales as $item)
                        <li>
                            <a href="{{$item->url}}" target="_blank">
                                <i class="{{$item->icono}}"></i>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
