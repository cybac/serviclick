<section class="gl-page-content-section">
    <div class="gl-inner-section">
        <div class="container">
            <div class="row">
                <div class="gl-contact-form-wrapper gl-left-side col-md-6 col-sm-6 col-xs-12">
                    <h3 class="gl-single-title">Contactanos</h3>

                    <form action="#">
                        <input type="text" name="gl-contact-name" id="gl-contact-name" placeholder="Nombre">
                        <input type="email" name="gl-contact-email" id="gl-contact-email" placeholder="Correo">
                        <textarea name="gl-contact-message" id="gl-contact-message" cols="30" rows="10"
                            placeholder="Mensaje"></textarea>

                        <input type="submit" value="Enviar" class="gl-btn">
                    </form>
                </div>

                <div class="gl-company-addresses-wrapper gl-right-side col-md-6 col-sm-6 col-xs-12">
                    @php $i=0; @endphp
                    @foreach ($sucursales as $item)
                    @if ($i%2==0)
                    <div class="gl-address-col">
                        <div class="gl-address">
                            <h3 class="gl-single-title">{{$item->nombre}}</h3>
                            <ul>
                                <li class="gl-address-location">{{$item->direccion}}</li>
                                <li class="gl-address-phone">{{$item->telefono}}</li>
                                <li class="gl-address-email">{{$item->correo}}</li>
                            </ul>
                        </div>
                    @else
                        <div class="gl-address">
                            <h3 class="gl-single-title">{{$item->nombre}}</h3>
                            <ul>
                                <li class="gl-address-location">{{$item->direccion}}</li>
                                <li class="gl-address-phone">{{$item->telefono}}</li>
                                <li class="gl-address-email">{{$item->correo}}</li>
                            </ul>
                        </div>
                    </div>
                    @endif
                    @php $i++; @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>