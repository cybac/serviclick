<div class="gl-directory-searchbar gl-bz-directory-searchbar">
    <form method="POST" action="{{route('directory_busqueda')}}" enctype="multipart/form-data">
        @csrf
        <fieldset>
        <input type="text" name="keysword" id="keysword" class="gl-directory-input" placeholder="Palabras claves">
        <input type="text" name="ubicacion" id="ubicacion" placeholder="Ubicación" list="ubicaciones">
        <datalist id="ubicaciones">
            @foreach ($ubicaciones as $item)
                <option value="{{$item->nombre}}">
            @endforeach
        </datalist>
        <div class="gl-jobs-category gl-category-dropdown">
            <select class="gl-category-dropdown-selection" name="categoria" id="categoria" placeholder="Localización">
                <option>&nbsp;</option>
                @foreach ($categorias as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                @endforeach
            </select>
        </div>
        </fieldset>
        <button type="submit" class="gl-icon-btn"><i class="fa fa-search"></i> Buscar</button>
    </form>
</div>