<section class="gl-service-section-wrapper gl-section-wrapper" id="gl-next-section">
    <div class="container">
        <div class="row">
            <div class="gl-service-we-offer">
                @foreach ($fortalezas as $item)
                <div class="gl-icon-top-with-text col-md-4 col-sm-4 col-xs-12 appear fadeIn" data-wow-duration=".5s"
                    data-wow-delay=".3s">
                    <div class="gl-icon-wrapper">
                        <img src="{{asset('images/fortalezas/'.$item->imagen)}}">
                    </div>
                    <h3>{{$item->titulo}}</h3>
                    <p>{{$item->descripcion}}</p>
                </div>
                @endforeach
            </div>
        </div>
</section>
