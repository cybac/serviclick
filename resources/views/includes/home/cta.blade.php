<section class="gl-cta-section gl-cta-parallax">
    <div class="container">
        <div class="row">
            <div class="gl-cta-content-wrapper">
                <p> Descarga nuestra APP desde la tienda de <span class="gl-bold-font">aplicaciones</span></p>
                <ul>
                    <li><a href="https://play.google.com/store/apps/details?id=javier.com.serviclick" target="_blank" class="gl-google-play-btn"></a></li>
                    <li><a href="https://apps.apple.com/mx/app/serviclick-mx/id1566926956" target="_blank" class="gl-app-store-btn"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
