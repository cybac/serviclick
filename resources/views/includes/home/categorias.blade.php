<section class="gl-featured-areas-section gl-section-wrapper" {{Route::is("categorias") ? "id=gl-next-section" : "" }}>
    <div class="container-fluid">
        <div class="row">
            <div class="gl-section-headings">
                <h1>Categorias</h1>
                <p>Encuentra el Servicio o Negocio que necesitas con tan solo un click.</p>
            </div>
            <div class="gl-feat-areas-wrapper">
                @foreach ($look_categorias as $item)
                    <div class="gl-feat-areas">
                        <picture>
                            {{-- <source media="(min-width: 768px)" srcset={{asset('images/areas/'.$item->imagen)}}> --}}
                            {{-- <img alt="Category Image" class="w-100" srcset={{asset('images/areas/'.$item->imagen)}}> --}}
                            <img class="w-100" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="{{asset('images/areas/'.$item->imagen)}}" alt="">
                        </picture>
                        <a href="{{route('directory_categoria', $item->id)}}" class="gl-img-overlay-effect">
                            <span class="gl-cat-link">{{$item->nombre}}</span>
                        </a>
                    </div>
                @endforeach
                @if (count($look_categorias) == 7)
                <div class="gl-feat-areas">
                    <picture>
                        <source media="(min-width: 768px)" srcset={{asset('images/areas/all.jpg')}}>
                        <img alt="Category Image" class="w-100" srcset={{asset('images/areas/all.jpg')}}>
                    </picture> 
                    <a href="{{route('categorias')}}" class="gl-img-overlay-effect">
                        <span class="gl-cat-link">Todas las categorias</span>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>