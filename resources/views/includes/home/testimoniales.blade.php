<section class="gl-custom-section gl-testimonial-section">
    <div class="container">
        <div class="row">
            <div class="gl-testimonial-wrapper">
                <h3 class="gl-testimonial-header">Experiencias con <span class="gl-header-bold">el servicio</span></h3>
                <div class="swiper-container gl-jobs-testimonial">
                    <div class="swiper-wrapper">
                        @foreach ($testimoniales as $item)
                        <div class="swiper-slide gl-testimonial-slide">
                            <div class="col-md-5 col-sm-6 col-xs-12 gl-testimonial-img">
                                <div class="gl-testimonial-img-wrapper">
                                    <img src="{{asset('images/testimoniales/'.$item->imagen)}}" alt="Testimonial Img"
                                        class="gl-lazy">
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-12 gl-testimonial-contents">
                                <div class="gl-testimonial-text">
                                    <p>{{$item->contenido}}</p>
                                </div>
                                <p class="gl-testimonial-name-comp">{{$item->nombre}} <span
                                        class="gl-comp-name">{{$item->trabajo}}</span></p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
