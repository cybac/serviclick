<section class="gl-hero-img-wrapper">
    <div class="container">
        <div class="row">
            <div class="gl-elements-content-wrapper">
                <div id="typed-strings">
                    @foreach ($frases as $item)
                    <p>{{$item->inicio}} <span class="gl-color-text">{{$item->remarcado}}</span> {{$item->fin}}</p>
                    @endforeach
                </div>
                <h2 id="gl-slogan" class="gl-hero-text-heading"></h2>
                <p class="gl-hero-text-paragraph">Contamos con un <span class="gl-color-text">extenso catálogo</span> de
                    negocios, servicios y profesionistas</p>
                @include('includes.home.buscador')

                <div class="gl-scroll-down-wrapper">
                    <a href="#gl-next-section" class="gl-scroll-down"><i class="ion-chevron-down"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
