@extends('layouts.register')
@section('title', 'Registrarse')
@section('formulario')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="mt-4" method="post" action="{{ route('register') }}">
        @csrf
        <p class="mb-4 text-center">Regístrate</p>
        <input type="hidden" name="alta" id="alta" value="2">
        {{-- 
        <p class="mb-2">¿Regístrarse como negocio? <i class="fas fa-question-circle" style="margin-left: .5rem; color: white; cursor: pointer;" id="ayuda"></i></p>
        <div class="mb-4">
            <input type="radio" class="tipo_registro" id="no" name="alta" value="1" {{ (old('alta') != 2) ? "checked" : "checked"}}><label for="no">No</label>
            <input type="radio" class="tipo_registro" id="si" name="alta" value="2" {{ (old('alta') == 2) ? "checked" : ""}}><label for="si" style="margin-left: 2rem;">Si</label>
        </div>
        <input type="text" class="form-control"  name="empresa" id="empresa" placeholder="Nombre de la empresa" value="{{old('empresa')}}" autocomplete="organization" {{ old('empresa') ? "" : "hidden"}}>
        --}}
        <input type="text" class="form-control"  name="empresa" id="empresa" placeholder="Nombre de la empresa" value="{{old('empresa')}}" autocomplete="organization" required>
        <input type="text" class="form-control"  name="nombre" placeholder="Nombre Completo" value="{{old('nombre')}}" autocomplete="name" required>
        <input type="email" class="form-control" name="email" placeholder="Correo" value="{{old('email')}}" autocomplete="email" required>
        <input type="number" class="form-control" name="phone" id="phone" placeholder="Número Telefónico" value="{{old('phone')}}" autocomplete="phone" required>
        <input type="password" class="form-control" name="password" placeholder="Nueva Contraseña" autocomplete="current-password" required>
        <input type="text" class="form-control"  name="captcha" id="captcha" placeholder="Captcha" minlength="8"  maxlength="8" required>
        
        <div class="form-group mt-4 mb-4">
            <div class="row" style="min-height: 70px;">
                <div class="col-9 col-md-10 d-flex">
                    <span class="captcha m-auto">{!! captcha_img('flat') !!}</span>
                </div>
                <div class="col-3 col-md-2">
                    <button type="button" class="btn btn-danger refresh" class="reload" style="padding: 1rem; margin: 0; border-radius: 50%; font-size: 1rem; width: 50px; height: 50px; position: relative; margin-top: 10px; margin-left: -6px;" id="reload">
                        <i class="fas fa-redo-alt" style="position: absolute; top: 18px; left: 18px;"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="form-button">
            <p class="terminos">Al registrarte aceptas nuestras <a href="{{route('privacy')}}" target="_blank" >Políticas de privacidad</a>, <a href="{{route('terms')}}" target="_blank">Términos y Condiciones</a></p>
            <button id="submit" type="submit" class="btn btn-success">Registrarse</button>
        </div>
    </form>
@endsection