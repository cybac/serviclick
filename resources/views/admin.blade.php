@extends('layouts.admin')
@section('titulo', 'Administrador')
@section('contenido')
<div class="row">
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_negocios')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-store text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Negocios Aprobados</h3>
                    <p class="text-secondary">Verifique los negocios que terminaron el proceso de alta en el sistema.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_negocios_tramites')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-timer-sand text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Negocios en tramites</h3>
                    <p class="text-secondary">Verifique los tramites de los negocios y el status de los mismos.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_documentacion')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-file-outline text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Documentación</h3>
                    <p class="text-secondary">Administre los documentos que se solicitan a los nuevos negocios.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_web_slider_text')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-alphabetical text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Sliders de textos</h3>
                    <p class="text-secondary">Administre los mensajes que aparecen en la pantalla de inicio.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_web_beneficios')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-chart-bar text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Beneficios</h3>
                    <p class="text-secondary">Indique los 3 beneficios principales de usar Serviclick.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('control_sc_negocios')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-camera text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Testimoniales</h3>
                    <p class="text-secondary">Administre los testimoniales sobre el servicio.</p>
                </div>
            </div>
        </a>
    </div>
</div>
@endsection
