@extends('layouts.home')
@section('titulo', 'ServiClick')
@section('pre-js')
<link rel="stylesheet" href="{{asset('css/in-view.css?n='.uniqid())}}">
<script src="{{asset('js/in-view.min.js')}}"></script>
@endsection
@section('content')
    @include('includes.home.busqueda')
    @include('includes.home.fortalezas')
    @if (count($testimoniales) > 0)
        @include('includes.home.testimoniales')
    @endif
    @include('includes.home.categorias')
    @include('includes.home.cta')
@endsection
@section('pos-js')
<script src="{{asset('js/load-in-view.js')}}"></script>
@endsection