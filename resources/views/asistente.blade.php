@extends('layouts.asistente')
@section('titulo', 'Panel de control')
@section('contenido')
<link rel="stylesheet" href="{{asset('negocio/css/asistente.css')}}">
<script src="https://cdn.tiny.cloud/1/vwzpp78mw7q9z5bpxjzyxrrwf7e5ojz10ey7qcy9jkx5ygfk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form class="col-12 asistente" action="#" method="POST" enctype="multipart/form-data">
                    {!! $contenido !!}
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('negocio/js/asistente.js?n='.uniqid())}}"></script>
@endsection
