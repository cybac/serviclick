@extends('layouts.details')
@section('titulo', 'ServiClick - '.$legal->titulo)
@section('content')
    <section class="gl-page-content-section" style="margin-left: 3rem; margin-right: 3rem">
        <div class="container">
          <div class="row">
            <div class="gl-blog-details-wrapper">
              <div class="gl-blog-heading-metas" style="margin-bottom: 1rem;">
                <h1 class="gl-blog-titles">{{$legal->titulo}}</h1>
              </div>
              <div class="gl-blog-post-details">
                  {!! $legal->contenido !!}
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection