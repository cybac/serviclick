@extends('layouts.negocio')
@section('titulo', 'Panel de control')
@section('contenido')
<div class="row">
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('panel_negocio')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-store text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Mi negocio</h3>
                    <p class="text-secondary">Actualice la información correspondiente a su negocio.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('panel_galeria')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-folder-multiple-image text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Mi Galeria</h3>
                    <p class="text-secondary">Suba o reemplace las imagenes de sus trabajos realizados.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('panel_redes')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-facebook text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Mis redes sociales</h3>
                    <p class="text-secondary">Agregue los enlaces a sus redes sociales.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('panel_keywords')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-key text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Palabras Clave</h3>
                    <p class="text-secondary">Agregue palabras clave que ayuden a identificar su negocio entre los demas.</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4 mb-2">
        <a href="{{route('panel_historial')}}">
            <div class="card h-100">
                <div class="card-body text-center">
                    <i class="mdi mdi-currency-usd text-primary" style="font-size: 115px;"></i>
                    <h3 class="ml-2 mr-2 mb-5 text-primary">Historial de pagos</h3>
                    <p class="text-secondary">Verifique la aplicación de los pagos realizados.</p>
                </div>
            </div>
        </a>
    </div>
</div>
@endsection
