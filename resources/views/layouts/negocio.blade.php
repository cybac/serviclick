<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('titulo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link  rel="icon" href="{{asset('images/favicon.png')}}" type="image/png" />
    <link href="{{ asset('admin/css/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/style.css?n='.uniqid()) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/extra.css?n='.uniqid()) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('negocio/css/extra.css?n='.uniqid()) }}" rel="stylesheet" type="text/css" />
    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
    
    <script src="{{asset('admin/js/nouislider.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/css/nouislider.css')}}">
    <script src="{{asset('admin/js/wNumb.js')}}"></script>

    <link rel="stylesheet" href="{{asset('js/maps/leaflet.css')}}" />
    <script src="{{asset('js/maps/leaflet.js')}}"></script>
</head>
<body class="fixed-left">
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <div id="wrapper">
        <div class="left side-menu">
            <div class="topbar-left">
                <div class="">
                    <a href="{{route('home')}}" class="logo"><img src="{{asset('admin/img/logo.png')}}" height="36" alt="logo"></a>
                </div>
            </div>
            @include('includes.negocio.menu')
        </div>
        <div class="content-page">
            <div class="content">
                @include('includes.admin.topbar')
                <div class="page-content-wrapper">
                    <div class="container-fluid">
                        @yield('contenido')
                    </div>
        
                </div>
            </div>
            @include('includes.admin.footer')
        </div>
    </div>
    <script src="{{ asset('admin/js/jquery.min.js')}}"></script>
    <script src="{{ asset('admin/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('admin/js/modernizr.min.js')}}"></script>
    <script src="{{ asset('admin/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ asset('admin/js/waves.js')}}"></script>
    <script src="{{ asset('admin/js/jquery.nicescroll.js')}}"></script>
    <script src="{{ asset('admin/js/jquery.scrollTo.min.js')}}"></script>

    <!-- Peity chart JS -->
    <script src="{{ asset('admin/js/peity-chart/jquery.peity.min.js')}}"></script>

    <!--C3 Chart-->
    <script src="{{ asset('admin/js/d3/d3.min.js')}}"></script>
    <script src="{{ asset('admin/js/c3/c3.min.js')}}"></script>

    <!-- KNOB JS -->
    <script src="{{ asset('admin/js/jquery-knob/excanvas.js')}}"></script>
    <script src="{{ asset('admin/js/jquery-knob/jquery.knob.js')}}"></script>

    <!-- App js -->
    <script src="{{ asset('admin/js/app.js')}}"></script>

    <script src="{{asset('negocio/js/tablas.js?n='.uniqid())}}"></script>
    <script src="{{asset('negocio/js/modal.js?n='.uniqid())}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>