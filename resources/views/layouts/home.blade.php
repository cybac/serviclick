<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>@yield('titulo')</title>
    <link  rel="icon" href="{{asset('images/favicon.png')}}" type="image/png" />
    <link rel="stylesheet" href="{{asset('css/style.css?n='.uniqid())}}">
    <script src="https://kit.fontawesome.com/459fd11905.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{asset('css/extra.css?n='.uniqid())}}">
    <style>
        input::-webkit-calendar-picker-indicator {
            color: transparent;
            cursor: pointer;
        }
    </style>
    @yield('pre-js')
</head>
@if ($errors->any())
<body class="gl-jobs-template gl-home-template gl-show-menu">
@else
<body class="gl-jobs-template gl-home-template">
@endif
    @include('includes.preloader')
    @include('includes.login')
    @include('includes.menu')
    @yield('content')
    @include('includes.footer')
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    @if ($errors->any())
    <script src="{{asset('js/scripts-open.js')}}"></script>
    @else
    <script src="{{asset('js/scripts.js')}}"></script>
    @endif
    @yield('pos-js')
</body>
</html>
