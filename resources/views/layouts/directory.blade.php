<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>@yield('titulo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link  rel="icon" href="{{asset('images/favicon.png')}}" type="image/png" />
    <link rel="stylesheet" href="{{asset('css/style.css?n='.uniqid())}}">
    <script src="https://kit.fontawesome.com/459fd11905.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{asset('css/extra.css?n='.uniqid())}}">
    <link rel="stylesheet" href="{{asset('js/maps/leaflet.css')}}" />
    <script src="{{asset('js/maps/leaflet.js')}}"></script>
    <style>
        input::-webkit-calendar-picker-indicator {
            color: transparent;
            cursor: pointer;
        }
    </style>
</head>
<body class="gl-search-template gl-search-style-map-left">
    @include('includes.preloader')
    @include('includes.login')
    @include('includes.menu')

    @yield('content')
    
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/mapa.js?n='.uniqid())}}"></script>
    @include('includes.directory.script')
</body>
</html>
