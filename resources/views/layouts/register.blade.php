<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link  rel="icon" href="{{asset('images/favicon.png')}}" type="image/png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/iofrm-theme2.css')}}">
    <script src="https://kit.fontawesome.com/459fd11905.js" crossorigin="anonymous"></script>

    <style>
        .captcha img{
            max-width: 100%;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
</head>
<body>
    <div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <a href="{{route('home')}}">
                            <img class="mb-3" src="{{ asset('images/logo-header.png')}}">
                        </a>
                        @yield('formulario')
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('js/captcha-reload.js?n='.uniqid())}}"></script>
<script src="{{ asset('js/telefono_validator.js?n='.uniqid())}}"></script>
{{-- 
<script src="{{ asset('js/registro.js?n='.uniqid())}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
--}}
</body>
</html>
