@extends('layouts.admin')
@section('titulo', "Administrador - Comentarios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Comentarios</h3>
        <table class="table" id="{{isset($id_negocio) ? 'dt_comentarios' : 'dt_all_comentarios'}}" data-url="{{ isset($id_negocio) ? route('dt_comentarios', $id_negocio) : route('dt_all_comentarios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Puntuación</th>
                    @if (isset($id_negocio))
                    <th>Comentario</th>
                    <th>Status</th>
                    @else
                    <th>Negocio</th>
                    <th>Comentario</th>
                    @endif
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@if (isset($id_negocio))
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endif
@endsection