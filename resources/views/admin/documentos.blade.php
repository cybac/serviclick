@extends('layouts.admin')
@section('titulo', "Administrador - Documentos")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Documentos</h3>
        @if ($add)
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_documento_negocio',$negocio) }}">Subir</a>
        </div>
        @else
            @if (!$status)
            <div class="w-100 text-right form-group">
                <button class="btn btn-success col-md-3 aprobar-archivos" data-url="{{ route('aprobar_documentacion') }}" data-id="{{$negocio}}">Aprobar Documentos</button>
            </div>                
            @endif
        @endif
        <table class="table" id="{{$dttable}}" data-url="{{ route($dttable, $negocio) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Documento</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@if (!$add)
    @if (!$status)
    <script src="{{asset('admin/js/modal_text.js?n='.uniqid())}}"></script>
    @endif
@endif
@endsection