@extends('layouts.admin')
@section('titulo', "Administrador - Negocios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Negocios</h3>
        <table class="table" id="dt_negocios" data-url="{{ route('dt_negocios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Logo</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Direccion</th>
                    <th>Categoria</th>
                    <th>Ubicacion</th>
                    <th>Teléfono</th>
                    <th>Pagando</th>
                    <th>Status</th>
                    <th>Imagenes</th>
                    <th>Otros</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection