@extends('layouts.admin')
@section('titulo', "Administrador - Usuarios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Historial de pagos de {{$nombre}}</h3>
        <div class="w-100 text-right form-group">
        @if ($exonerar)
            <button class="btn btn-indigo exoneracion col-md-2" data-id="{{$id}}" data-url="{{route('exonerar_pago')}}">Exonerar Pago</button>
        @else
        {!! $mensaje !!}
        @endif
        </div>
        <table class="table" id="dt_historial_pagos_negocio" data-url="{{ route('dt_historial_pagos_negocio', ['negocio' => $id]) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Monto</th>
                    <th>Fecha de emisión</th>
                    <th>Fecha de pago</th>
                    <th>Fecha proximo pago</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/modal_text.js')}}"></script>
@endsection