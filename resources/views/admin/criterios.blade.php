@extends('layouts.admin')
@section('titulo', "Administrador - Criterios de evaluación")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Criterios de evaluación</h3>
        <div class="w-100 text-right form-group">
            <button class="btn btn-success col-md-2 input-text" data-url="{{route('guardar_criterio')}}" data-title="Nuevo Criterio de evaluación" data-subtitle="Nombre para el criterio de evaluación">Nuevo Criterio</button>
        </div>
        <table class="table" id="dt_criterios" data-url="{{ route('dt_criterios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/modal_text.js?n='.uniqid())}}"></script>
@endsection