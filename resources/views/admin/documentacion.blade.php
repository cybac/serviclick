@extends('layouts.admin')
@section('titulo', "Administrador - Documentación")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Documentación</h3>
        <div class="w-100 text-right form-group">
            <button class="btn btn-success col-md-2 input-text" data-url="{{route('guardar_documentacion')}}" data-title="Nuevo Documentación" data-subtitle="Nombre para el documento">Nuevo</button>
        </div>
        <table class="table" id="dt_documentacion" data-url="{{ route('dt_documentacion') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Documento</th>
                    <th>Requerido</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/modal_text.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection