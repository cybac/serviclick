@extends('layouts.admin')
@section('titulo', "Administrador - Notificaciones")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Notificaciones App</h3>
        <table class="table" id="dt_notificaciones" data-url="{{ route('dt_notificaciones') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Mensaje</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/modal_text.js?n='.uniqid())}}"></script>-
@endsection