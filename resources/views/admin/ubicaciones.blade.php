@extends('layouts.admin')
@section('titulo', "Administrador - ubicaciones")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Ubicaciones</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_ubicacion') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_ubicaciones" data-url="{{ route('dt_ubicaciones') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Coordenadas</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection