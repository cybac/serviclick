@extends('layouts.admin')
@section('titulo', "Administrador - Recordatorios enviados a ".$negocio->nombre)
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Recordatorios enviados a {{$negocio->nombre}}</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('control_sc_crear_recordatorio', $id) }}">Enviar recordatorio</a>
        </div>
        <table class="table" id="dt_recordatorios_negocios" data-url="{{ route('dt_recordatorios_negocios', $id) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Contenido</th>
                    <th>Fecha de envio</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection