@extends('layouts.admin')
@section('titulo', "Administrador - Testimoniales")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Testimoniales</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_testimonial') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_testimoniales" data-url="{{ route('dt_testimoniales') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Trabajo</th>
                    <th>Contenido</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection