@extends('layouts.admin')
@section('titulo', "Administrador - Redes Sociales")
@section('contenido')
<script src="https://kit.fontawesome.com/459fd11905.js" crossorigin="anonymous"></script>
<div>
    <div class="card w-100 p-4">
        <h3 class="text-center">Redes Sociales</h3>
        <div class="w-100 text-right form-group"></div>
        
        <div id="sortable_list" class="list-group" data-url="{{route('ordenar_redes_sociales')}}">
            @foreach ($redes as $item)
            <div class="list-group-item d-flex" data-id="{{$item->id}}">
                @if ($item->url == "#")
                <p class="btn mb-0 redes_sociales" data-id="{{$item->id}}" data-url="{{route("actualizar_redes")}}" data-value="" data-title="URL de {{$item->nombre}}" data-subtitle="Proporcione el link">
                    <i class="mr-2 {{$item->icono}}"></i>{{$item->nombre}}
                </p>
                @else
                <p class="btn mb-0 redes_sociales" data-id="{{$item->id}}" data-url="{{route("actualizar_redes")}}" data-value="{{$item->url}}" data-title="URL de {{$item->nombre}}" data-subtitle="Proporcione el link">
                    <i class="mr-2 {{$item->icono}}"></i>{{$item->nombre}}
                </p>
                @endif
                <span class="ml-auto switch">
                    <input type="checkbox" {{ $item->status ? "checked":"" }} onchange="Cambio_redes({{$item->id}});" class="switch" id="status_{{$item->id}}">
                    <label for="status_{{$item->id}}"></label>
                </span>
            </div>
            @endforeach
        </div>
        <p class="mt-4 pb-2">Nota: para editar la URL de una red social, da click sobre el nombre.</p>
    </div>
</div>
<script src="{{asset('admin/js/sortable.min.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/ordenamiento.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/modal_text.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection