@extends('layouts.admin')
@section('titulo', "Administrador - Usuarios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Informacion de contacto</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_usuarios') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_usuarios" data-url="{{ route('dt_usuarios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Tipo</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection