@extends('layouts.admin')
@section('titulo', 'Administrador - Información del negocio')
@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <h3 class="page-title col-12">Datos del Negocio</h3>
                        <div class="border col-12 col-md-5">
                            <h6>Nombre</h6>
                            <p>{{ $negocio->nombre }}</p>
                        </div>
                        <div class="border col-12 col-md-5">
                            <h6>Dirección</h6>
                            <p>{{ $negocio->direccion }}</p>
                        </div>
                        <div class="border col-12 col-md-2">
                            <h6>Teléfono</h6>
                            <p>{{ $negocio->telefono }}</p>
                        </div>
                        <div class="border col-12 col-md-4">
                            <h6>Ubicación</h6>
                            <p>{{ $ubicacion }}</p>
                        </div>
                        <div class="border col-12 col-md-2">
                            <h6>Categoria:</h6>
                            <p>{{ $categoria }}</p>
                        </div>
                        <div class="border col-12 col-md-2">
                            <h6>Precios:</h6>
                            <p>{{ $negocio->precio_min }} - {{ $negocio->precio_max }}</p>
                        </div>
                        <div class="border col-12 col-md-4">
                            <h6>Web</h6>
                            <p>{{ is_null($negocio->web) ? 'No disponible' : $negocio->web }}</p>
                        </div>
                        <div class="border col-12 col-md-6">
                            <h6>Descripción:</h6>
                            {!! $negocio->descripcion !!}
                        </div>
                        <div class="border col-12 col-md-6">
                            <h6>Servicios:</h6>
                            {!! $negocio->servicios !!}
                        </div>
                        <div class="border col-12 col-md-4">
                            <h6>Ubicación en el Mapa</h6>
                            <div style="width: 100%; height:400px" id="map"></div>
                        </div>
                        @if (!$tramite)
                            <div class="form-group text-center col-12 mt-2">
                                <button class="btn btn-success btn-lg aprobar-info accion-info" data-url="{{ route('aprobar_informacion') }}" data-title="Aprobar información del negocio" data-btn="Aprobar y continuar" data-motivo="false" data-id="{{ $negocio->id }}">Aprobar Información</button>
                                <button class="btn btn-danger btn-lg rechazar-info accion-info" data-url="{{ route('rechazar_informacion') }}" data-title="Solicitar correcion de la información del negocio" data-btn="Solicitar" data-motivo="true" data-id="{{ $negocio->id }}">Solicitar Corrección</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var mbUrl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
        var localizar = [{{ $negocio->mapa }}];
        var map = L.map('map', {
            center: localizar,
            zoom: 13,
            minZoom: 10,
            maxZoom: 18,
        });
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker(localizar).addTo(map);
    </script>
    @if (!$tramite)
        <script src="{{ asset('admin/js/modal_text.js?n=' . uniqid()) }}"></script>
    @endif
@endsection
