@extends('layouts.admin')
@section('titulo', "Administrador - Sucursales")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Sucursales</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_sucursal') }}">Nueva</a>
        </div>
        <table class="table" id="dt_sucursales" data-url="{{ route('dt_sucursales') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Teléfono</th>
                    <th>Correo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection