@extends('layouts.admin')
@section('titulo', "Administrador - Slider de texto")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Slider de texto</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_slider_text') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_slider_text" data-url="{{ route('dt_slider_text') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Frase</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection