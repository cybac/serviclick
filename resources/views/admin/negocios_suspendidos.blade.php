@extends('layouts.admin')
@section('titulo', "Administrador - Negocios suspendidos")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Negocios suspendidos</h3>
        <table class="table" id="dt_negocios_suspendidos" data-url="{{ route('dt_negocios_suspendidos') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Pagos realizados</th>
                    <th>Fichas expiradas</th>
                    <th>Última ficha</th>
                    <th>Fecha del movimiento</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/suspension.js?n='.uniqid())}}"></script>
@endsection