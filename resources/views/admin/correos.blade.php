@extends('layouts.admin')
@section('titulo', "Administrador - Correos")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Correos</h3>
        <table class="table" id="dt_correos" data-url="{{ route('dt_correos') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Titulo</th>
                    <th>Mensaje</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection