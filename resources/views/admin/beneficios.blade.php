@extends('layouts.admin')
@section('titulo', "Administrador - Beneficios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Beneficios</h3>
        <div class="w-100 text-right form-group"></div>
        <table class="table" id="dt_beneficios" data-url="{{ route('dt_beneficios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Titulo</th>
                    <th>Contenido</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection