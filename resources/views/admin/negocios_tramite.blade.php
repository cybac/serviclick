@extends('layouts.admin')
@section('titulo', "Administrador - Negocios en tramites")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Negocios en tramites</h3>
        <table class="table" id="dt_negocios_tramites" data-url="{{ route('dt_negocios_tramites') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Logo</th>
                    <th>Nombre</th>
                    <th>Datos de contacto</th>
                    <th>Fecha de registro</th>
                    <th>Seguimiento</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection