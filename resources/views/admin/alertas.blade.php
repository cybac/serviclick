@extends('layouts.admin')
@section('titulo', "Administrador - Alertas")
@section('area', "Alertas")
@section('contenido')
<style>
    .alertas{
        height: 75vh; 
        overflow-y: auto;
    }
</style>
<div class="alertas">
    <div class="card w-100 p-5">
        @if ( count($alertas) > 0)
        <div class="noti-title mb-3">
            <h5>Nuevas </h5>
        </div>
        @endif

        @foreach ($alertas as $item)
        <a href="{{route('control_sc_ver_alerta', $item->id)}}" class="notify-item media p-3 mb-2" style="color: #212529; border: 2px solid #e0e0e0; border-radius: 10px;">
            <i class="mr-3 {{$item->icono}}" style="font-size: 3rem;"></i>
            <div class="media-body">
                <h5 class="mt-0">{{$item->titulo}}</h5>
                <p>{{$item->contenido}}</p>
            </div>
        </a>
        @endforeach 

        @if (count($anteriores_alertas) > 0)
        @if (count($alertas) > 0)
        <hr class="mt-5 mb-5">
        @endif
        <div class="noti-title mb-3">
            <h5>Anteriores </h5>
        </div>
        @endif
        @foreach ($anteriores_alertas as $item)
        <a href="{{route('control_sc_ver_alerta', $item->id)}}" class="notify-item media p-3 mb-2" style="color: #212529; border: 2px solid #e0e0e0; border-radius: 10px;">
            <i class="mr-3 {{$item->icono}}" style="font-size: 3rem;"></i>
            <div class="media-body">
                <h5 class="mt-0">{{$item->titulo}}</h5>
                <p>{{$item->contenido}}</p>
            </div>
        </a>
        @endforeach 
    </div>
</div>
@endsection