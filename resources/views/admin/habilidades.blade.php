@extends('layouts.admin')
@section('titulo', "Administrador - Nuestras Habilidades")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Nuestras Habilidades</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_habilidad') }}">Nueva Habilidad</a>
        </div>
        <div class="w-100 text-right form-group"></div>
        <table class="table" id="dt_habilidades" data-url="{{ route('dt_habilidades') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection