@extends('layouts.admin')
@section('titulo', "Administrador - Usuarios")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Historial de pagos</h3>
        <table class="table" id="dt_historial_pagos" data-url="{{ route('dt_historial_pagos') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Monto</th>
                    <th>Fecha de emisión</th>
                    <th>Fecha de pago</th>
                    <th>Fecha proximo pago</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection