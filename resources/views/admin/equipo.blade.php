@extends('layouts.admin')
@section('titulo', "Administrador - Nuestro Equipo")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Nuestro Equipo</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_equipo') }}">Nuevo Integrante</a>
        </div>
        <div class="w-100 text-right form-group"></div>
        <table class="table" id="dt_equipo" data-url="{{ route('dt_equipo') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Status</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{asset('admin/js/cambio_status.js?n='.uniqid())}}"></script>
@endsection