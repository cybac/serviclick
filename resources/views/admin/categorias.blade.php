@extends('layouts.admin')
@section('titulo', "Administrador - Categorias")
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center">Categorias</h3>
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('crear_categoria') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_categorias" data-url="{{ route('dt_categorias') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection