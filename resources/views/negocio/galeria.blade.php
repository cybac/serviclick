@extends('layouts.negocio')
@section('titulo', "Panel de control - Subir galeria de fotos")
@section('contenido')
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h3 class="h4">Galeria de imagenes</h3>
                    </div>
                    <div class="card-body">
                        <div class="card text-center">
                            <div class="card-header bg-primary text-white">
                                Cargar Imágenes
                            </div>
                            <div class="card-body">
                                <form action="{{ route('guardar_galeria') }}" class="dropzone dropzone-area" id="upload_image">
                                    @csrf
                                    <div class="dz-message">
                                        Arrastra tus imágenes o de click para seleccionarlas<br>
                                        Medida Recomendada: [Ancho 800px] [Alto 600px]
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card text-center">
                            <div class="card-header bg-success text-white">Imágenes Cargadas</div>
                            <div class="card-body">
                                <div id="sortable_list" class="row" token="{{ csrf_token() }}" sort_url="{{ route('ordenar_galeria') }}">
                                    @foreach ($galeria as $item)
                                    <div class="col-2 pt-3 img-galeria" data-id="{{ $item->id }}">
                                        <img src="{{ asset("images/trabajos/".$item->imagen) }}" alt="{{ $item->imagen }}" class="img-fluid">
                                        <form action="{{ route('eliminar_imagen_galeria') }}" method="post" class="form" data-title="¿Desea eliminar esta imagen?" data-text="Esta accion no se puede revertir" data-btn="Continuar y Borrar">
                                            @csrf
                                            <input type="hidden" name="id_imagen" value="{{ $item->id }}">
                                            <button type="submit" class="btn btn-danger w-100" style="padding-left: 25px; padding-right: 25px;">Eliminar</button>
                                        </form>
                                    </div>    
                                    @endforeach 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{asset('admin/js/dropzone.js?n='.uniqid())}}"></script>
<script src="{{asset('admin/js/sortable.min.js?n='.uniqid())}}"></script>
<script src="{{asset('negocio/js/multiples_subidas.js?n='.uniqid())}}"></script>
@endsection
