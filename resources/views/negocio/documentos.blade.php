@extends('layouts.negocio')
@section('titulo', 'Panel de control - Documentación')
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('subir_documentacion')  }}" method="POST" class="documentacion row" id="form_negocio" enctype="multipart/form-data">
                    @csrf
                    @foreach ($documentos as $item)
                    @if(!$item->status)
                    <div class="form-group col-12 col-md-12">
                        @if ($item->rechazado)
                        <div class="alert alert-warning" role="alert">
                            <h6 class="alert-heading">Se requiere correccion del documento {{$item->tipo}}</h6>
                            <p>Motivos del rechazo</p>
                            <hr>
                            <p class="mb-0">{!! $item->motivo !!}</p>
                        </div>                            
                        @endif
                        <label for="documento_{{$loop->iteration}}">{{$item->tipo}} </label>
                        <input type="file" name="documento_{{$loop->iteration}}" id="documento_{{$loop->iteration}}" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    @endforeach
                    @if ($pendientes != 0)
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Enviar Documentación</button>
                        </div>
                    @else
                        @if ($tramite)
                        <div class="alert alert-success col-12" role="alert">La documentación del negocio ha sido aprobada</div>
                        @else
                        <div class="alert alert-primary col-12" role="alert">Todo la documentación solicitada ha sido enviada</div>
                        @endif
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
