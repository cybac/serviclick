@extends('layouts.negocio')
@section('titulo', 'Panel de control - Mi Negocio')
@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('actualizar_mi_negocio') }}" method="POST" class="form row" id="form_negocio" enctype="multipart/form-data" data-title="¿Desea actualizar los datos del negocio?" data-text="Los cambios seran visibles al momento" data-btn="Continuar">
                        @csrf
                        <div class="form-group col-12 col-md-4">
                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $negocio->nombre }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="ubicacion">Ubicación <span class="text-danger">*</span></label>
                            <select name="ubicacion" id="ubicacion" class="form-control form-control-lg">
                                <option value="">Selecciona una ubicación</option>
                                @foreach ($ubicaciones as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $negocio->ubicacion ? 'selected' : '' }}>{{ $item->nombre }}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group col-12 col-md-4">
                            <label for="categoria">Categoria <span class="text-danger">*</span></label>
                            <select name="categoria" id="categoria" class="form-control form-control-lg">
                                <option value="">Selecciona una categoría</option>
                                @foreach ($categorias as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $negocio->categoria ? 'selected' : '' }}>{{ $item->nombre }}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-8">
                            <label for="nombre">Direccion <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="direccion" id="direccion" value="{{ $negocio->direccion }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="telefono">Telefono <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="telefono" id="telefono" value="{{ $negocio->telefono }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12">
                            <label for="web">WEB </label>
                            <input type="text" class="form-control form-control-lg" id="web" name="web" value="{{ $negocio->web }}">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="descripcion">Descripción</label>
                            <script src="https://cdn.tiny.cloud/1/vwzpp78mw7q9z5bpxjzyxrrwf7e5ojz10ey7qcy9jkx5ygfk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                            <textarea cols="30" rows="4" name="descripcion" id="descripcion" class="form-control my-editor">{!! $negocio->descripcion !!}</textarea>
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="servicios">Servicios</label>
                            <textarea cols="30" rows="4" name="servicios" id="servicios" class="form-control my-editor">{!! $negocio->servicios !!}</textarea>
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="precio">Precio <span class="text-danger">*</span></label>
                            <div id="slider_precio" style="margin-left: 30px; margin-right: 30px;"></div>
                            <div class="dinero">
                                <label class="hidden" for="minimo">Minimo </label>
                                <label class="hidden"for="maximo">Maximo </label>
                                <input type="text" class="form-control form-control-lg" id="minimo" name="minimo" value="{{ $negocio->precio_min }}" readonly>
                                <input type="text" class="form-control form-control-lg" id="maximo" name="maximo" value="{{ $negocio->precio_max }}" readonly>
                                <span class="invalid-feedback"></span>
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <div style="width: 100%; height:400px" id="map"></div>
                            <label for="mapa">Coordenadas <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-lg" name="mapa" id="mapa" value="{{ $negocio->mapa }}" readonly>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Actualizar información</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('admin/js/precios.js?n=' . uniqid()) }}"></script>
    <script src="{{ asset('admin/js/editor.js?n=' . uniqid()) }}"></script>
    <script>
        var mbUrl = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
        var localizar = [{{ $negocio->mapa }}];
        var map = L.map('map', {
            center: localizar,
            zoom: 13,
            minZoom: 6,
            maxZoom: 18,
        });
        L.tileLayer(mbUrl, {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        map.doubleClickZoom.disable();
        var marker = L.marker(localizar, {
            draggable: 'true',
            autoPan: 'true',
            autoPanPadding: L.point(50, 50)
        }).addTo(map);
        marker.on('dragend', function(e) {
            var position = marker.getLatLng();
            var lacosa = document.getElementById('mapa');
            lacosa.value = `${position.lat}, ${position.lng}`;
            setTimeout(function() {
                map.panTo(new L.LatLng(`${position.lat}`, `${position.lng}`));
            }, 500);
        });
        marker.on('move', function(e) {
            var position = marker.getLatLng();
            var lacosa = document.getElementById('mapa');
            lacosa.value = `${position.lat}, ${position.lng}`;
        });
        map.on('dblclick', function(e) {
            marker.setLatLng([e.latlng.lat, e.latlng.lng]);
            setTimeout(function() {
                map.panTo(new L.LatLng(e.latlng.lat, e.latlng.lng));
            }, 500);
        });
    </script>
    <script src="{{ asset('admin/js/numeros.js?n=' . uniqid()) }}"></script>
    <script src="{{ asset('admin/js/ubicar_mapa.js?n=' . uniqid()) }}"></script>
@endsection
