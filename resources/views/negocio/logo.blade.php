@extends('layouts.negocio')
@section('titulo',  $form_edit ? "Panel de control - Editar Logo" : "Panel de control - Subir Logo")
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form_edit ? route('panel_actualizar_logo_negocio') : route('panel_guardar_logo_negocio') }}" method="POST" class="documentacion row" id="form_negocio" enctype="multipart/form-data">
                    @csrf
                    @if ($form_edit)
                    <div class="form-group col-12 col-md-12">
                        <div class="text-center">
                            <img src="{{ asset('images/negocios/'.$negocio->imagen) }}" class="img-fluid"><br>
                            <button type="button" class="btn btn-danger btn-lg delete_file mt-2" data-name="imagen"><i class="mdi mdi-delete"></i>Eliminar</button>
                        </div>
                    </div>
                    @else
                    <div class="form-group col-12 col-md-12">
                        <input type="file" name="imagen" id="imagen" class="form-control form-control-lg">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">{{$form_edit ? "Actualizar Logo" : "Guardar Logo"}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
