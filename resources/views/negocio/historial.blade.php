@extends('layouts.negocio')
@section('titulo', 'Panel de control')
@section('contenido')
@if ($ficha)
<link rel="stylesheet" href="{{asset('css/oxxo.css?n='.uniqid())}}">
@endif
<div>
    <div class="card w-100 p-5">
        <h3 class="text-center" style="font-size: 1.75rem !important; font-weight: 500 !important;">Historial de pagos</h3>
        @if ($ficha)
        <div class="w-100 text-right form-group">
            <button class="btn btn-success col-md-2 input-text" id="ficha" data-url="{{route('panel_ficha')}}">Ficha de pago</button>
        </div>
        @endif
        @if ($renovar)
        <div class="w-100 text-right form-group">
            <button class="btn btn-primary col-md-2 input-text ficha" data-url="{{route('renovar_ficha_negocio')}}">Renovar Ficha</button>
            <script src="{{asset('negocio/js/ficha.js?n='.uniqid())}}"></script>
        </div>
        @endif
        <table class="table" id="dt_historial" data-url="{{ route('dt_historial') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Monto</th>
                    <th>Fecha de pago</th>
                    <th>Proximo pago</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
