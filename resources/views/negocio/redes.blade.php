@extends('layouts.negocio')
@section('titulo', "Panel de control - Redes Sociales")
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h3 class="text-center">Redes Sociales</h3>
                    <form action="{{ route('guardar_redes') }}" method="POST" class="form_files row" id="form_redes" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-12 col-md-4">
                            <label for="red">Redes Sociales </label>
                            <select name="red" id="red" class="form-control form-control-lg">
                                <option value="">Seleccione una Red Social</option>
                                @foreach ($redes as $item)
                                    <option value="{{ $item->id }}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group col-8">
                            <label for="url">URL </label>
                            <input type="url" class="form-control form-control-lg" id="url" name="url" value="">
                            <span class="invalid-feedback"></span>
                        </div>
                        <div class="form-group text-center col-12">
                            <button class="btn btn-success btn-lg submit" type="submit">Guardar</button>
                        </div>
                    </form>

                    <div class="w-100 p-3">
                        <table class="table" id="dt_redes_sociales" data-url="{{ route('dt_redes_sociales') }}" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Red Social</th>
                                    <th>URL</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
