@extends('layouts.negocio')
@section('titulo', "Panel de control - Keyswords")
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h3 class="h4">Mis Palabras Clave</h3>
            </div>
            <div class="card-body">
                <form action="{{ route('guardar_keysword') }}"
                    method="POST" class="form row" id="form_keysword" enctype="multipart/form-data" data-title="¿Desea actualizar las keywords?" data-text="EL cambio surtira efecto al momento" data-btn="Continuar">
                    @csrf
                    <div class="form-group col-12">
                        <label>Keysword</label>
                        <input style="margin-bottom: 20px;" type="text" class="form-control form-control-lg" id="keysword" name="keysword" value="">
                        <div class="keys" id="keys">
                            @php $i=1; @endphp
                            @foreach ($keys as $item)
                            <span class="key" id="key_{{$i}}">{{$item->key}} 
                                <label for="keysword_{{$i}}" style="display: none"></label>
                                <input type="text" name="keysword_{{$i}}" id="keysword_{{$i}}" value="{{$item->key}}" style="display: none">
                                <i class="mdi mdi-close-box cerrar" onclick="borrar({{$i}});"></i>
                            </span>
                            @php
                                $i++;
                            @endphp
                            @endforeach
                        </div>
                        <script> edi_tag_id={{$i}};</script>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg submit" type="submit">Guardar Cambios</button>
                    </div>
                </form>
                <p>Notas:</p>
                <ul>
                    <li>Recomendamos el uso de palabras cortas y consisas</li>
                    <li>Puedes usar tantas palabras clave como requieras, aunque no recomendamos el uso exagerado de las mismas</li>
                    <li>Las palabras clave ayudan al buscador del directorio a identificarte entre otros negocios</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{asset('admin/css/tags.css?n='.uniqid())}}">
<script src="{{asset('admin/js/tags.js?n='.uniqid())}}"></script>
@endsection
