@csrf
<p class="text-justify">Hola <b> {{ Auth::user()->name}}</b></p>
<p class="text-justify">Gracias por registrarte y formar parte de <b>Serviclick</b></p>
<p class="text-justify">Te pedimos nos apoyes con el registro de tu negocio para poder darlo de alta satisfactoriamente y poder estar ya al alcance de miles de usuarios</p>
<p class="text-justify mb-5"><b>¡Bienvenido!</b></p>
<button class="btn btn-success continuar" data-url="{{route('etapa_proceso_negocio')}}">Siguiente</button>