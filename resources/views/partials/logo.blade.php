@csrf
<h5 class="text-center mb-4">Logotipo</h5>
<div class="form-group col-12">
    <input type="file" name="imagen" id="imagen" class="form-control form-control-lg" accept=".jpg, .jpeg, .png">
    <span class="invalid-feedback"></span>
</div>
<p class="text-justify"> Puedes subirlo despues al termino de tu proceso.</p>
<button class="btn btn-success text-white continuar mb-2" data-url="{{route('etapa_proceso_negocio')}}">Siguiente</button>
<button class="btn btn-warning text-white omitir mr-2 mb-2" data-url="{{route('omitir_etapa_proceso_negocio')}}">Omitir por ahora</button>