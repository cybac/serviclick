@csrf
<h5 class="text-center">Documentación</h5>

@if (!isset($boton_omision))
@php
    $boton_omision = true;
@endphp
@endif


@foreach ($documentos as $item)
    @if(!$item->status)
    <div class="form-group col-12 col-md-12">
        @if ($item->rechazado)
        @php
            $boton_omision = false;
        @endphp
        <div class="alert alert-warning" role="alert">
            <h6 class="alert-heading">Se requiere correccion del documento {{$item->tipo}}</h6>
            <p>Motivos del rechazo</p>
            <hr>
            <p class="mb-0">{!! $item->motivo !!}</p>
        </div>                            
        @endif
        <label for="documento_{{$loop->iteration}}">{{$item->tipo}} </label>
        <input type="file" name="documento_{{$loop->iteration}}" id="documento_{{$loop->iteration}}" class="form-control form-control-lg"
        accept=".jpg, .jpeg, .png, .pdf">
        <span class="invalid-feedback"></span>
    </div>
    @endif
@endforeach
@if ($boton_omision)
<p class="text-justify"> Puedes omitirlo por ahora sino cuenta con algun documento en este momento.</p>
<p class="text-justify"> Recuerda volver aquí mas tarde para subir estos documentos, en caso de omitirlos.</p>
@endif
<button class="btn btn-success continuar mb-2" data-url="{{route('etapa_proceso_negocio')}}">Siguiente</button>
@if ($boton_omision)
<button class="btn btn-warning text-white omitir mr-2 mb-2" data-url="{{route('omitir_etapa_proceso_negocio')}}">Omitir por ahora</button>
@endif