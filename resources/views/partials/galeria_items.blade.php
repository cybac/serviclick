<div class="col-2 pt-3" data-id="{{ $item->id }}">
    <img src="{{ asset($path.$item->imagen) }}" alt="{{ $item->imagen }}" class="img-fluid">
    <form action="{{ route('eliminar_imagen_galeria_negocio',['id_imagen'=>$item->id]) }}" method="post" class="form">
    @csrf
    <input type="hidden" name="id_imagen" value="{{ $item->id }}">
    <button type="submit" class="btn btn-danger btn-lg" style="padding-left: 25px; padding-right: 25px;">Eliminar</button>
    </form>
</div>