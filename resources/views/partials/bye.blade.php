<p>Gracias <b> {{ Auth::user()->name}}</b>. </p>
<p>Si has concluido el registro de los datos solicitados, serás notificado de que tu negocio ha sido  aprobado y podrás comenzar a disfrutar de esta magnifica herramienta comercial, con la cual miles de usuarios podrán buscar y localizar tus servicios muy fácilmente.</p>
<p>También, una vez transcurrido el periodo de cortesía, te haremos llegar a tu correo la ficha de pago para que sigas disfrutando de los beneficios de pertenecer al mejor directorio de negocios. <b>¡Serviclick!</b> </p>
<button class="btn btn-success salir-asistente text-white mr-2 mb-2" data-url="#">Volver al inicio</button>

<script>
$(".salir-asistente").click(function(){
    window.location.href = "/";
});
</script>