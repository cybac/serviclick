<div class="col-2 pt-3 img-galeria" data-id="{{ $item->id }}">
    <img src="{{ asset($path.$item->imagen) }}" alt="{{ $item->imagen }}" class="img-fluid">
    <form action="{{ route('eliminar_imagen_galeria') }}" method="post" class="form" data-title="¿Desea eliminar esta imagen?" data-text="Esta accion no se puede revertir" data-btn="Continuar y Borrar">
    @csrf
    <input type="hidden" name="id_imagen" value="{{ $item->id }}">
    <button type="submit" class="btn btn-danger w-100" style="padding-left: 25px; padding-right: 25px;">Eliminar</button>
    </form>
</div>