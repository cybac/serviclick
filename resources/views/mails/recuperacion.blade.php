<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo de Recuperación de cuenta - ServiClick</title>
</head>
<body>
    <h1>¿Ha olvidado su contraseña?</h1>
    <p>¡Hubo una solicitud para cambiar su contraseña!</p>
    <ul>
        <li>Código de recuperación: {{ $recuperacion->codigo }}</li>
        <li>Vigencia: {{ date("d/m/Y", strtotime($recuperacion->vigencia)) }} a las {{date("h:i:s A", strtotime($recuperacion->vigencia))}}</li>
    </ul>
    <p>Si no realizó esta solicitud, simplemente ignore este correo.</p>
</body>
</html>