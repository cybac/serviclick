@extends('layouts.register')
@section('title', 'Recuperar contraseña')
@section('formulario')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<form class="mt-4" method="POST" action="{{ route('password.email') }}">
    @csrf
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Correo" value="{{ old('email') }}" required autocomplete="email" autofocus>
    @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    
    <div class="form-button">
        <button id="submit" type="submit" class="btn btn-success">Enviar correo de restablecimiento</button>
    </div>
</form>
@endsection

