@extends('layouts.register')
@section('title', 'Confirmar contraseña')
@section('formulario')
<form class="mt-4" method="POST" action="{{ route('password.confirm') }}">
    @csrf
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">

    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="form-button">
        <button id="submit" type="submit" class="btn btn-success">Confirmar contraseña</button>
    </div>
    <div class="form-button">
        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
        @endif
    </div>
</form>
@endsection
