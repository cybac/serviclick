<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecuperacionesTable extends Migration
{
    public function up()
    {
        Schema::create('recuperaciones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('usuario')->references('id')->on('users')->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('codigo');
            $table->datetime('vigencia');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('recuperaciones');
    }
}
