<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosTable extends Migration
{
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('tipo')->references('id')->on('tipos_documentos')->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('nombre')->nullable();
            $table->string('motivo')->nullable();
            $table->boolean('rechazado')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('documentos');
    }
}
