<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyswordsTable extends Migration
{
    public function up()
    {
        Schema::create('keyswords', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('key');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keyswords');
    }
}
