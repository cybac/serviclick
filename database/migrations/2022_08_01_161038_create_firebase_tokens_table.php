<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFirebaseTokensTable extends Migration
{
    public function up()
    {
        Schema::create('firebase_tokens', function (Blueprint $table) {
            $table->id();
            $table->foreignId("usuario")->references("id")->on("users")->cascadeOnUpdate()->cascadeOnDelete();
            $table->string("token");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('firebase_tokens');
    }
}
