<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderFortalezasTable extends Migration
{
    public function up()
    {
        Schema::create('slider_fortalezas', function (Blueprint $table) {
            $table->id();
            $table->string('inicio');
            $table->string('remarcado');
            $table->string('fin');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('slider_fortalezas');
    }
}
