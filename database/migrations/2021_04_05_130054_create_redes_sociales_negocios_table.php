<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedesSocialesNegociosTable extends Migration
{
    public function up()
    {
        Schema::create('redes_sociales_negocios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('red_social')->references('id')->on('redes_sociales')->cascadeOnUpdate()->cascadeOnDelete();
            $table->text('url');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('redes_sociales_negocios');
    }
}
