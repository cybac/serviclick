<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNegociosTable extends Migration
{
    public function up()
    {
        Schema::create('negocios', function (Blueprint $table) {
            $table->id();
            $table->string('imagen')->default('0');
            $table->string('nombre');
            $table->text('direccion');
            $table->string('telefono')->default('0000000000');
            $table->text('web')->nullable();
            $table->text('descripcion');
            $table->text('servicios');
            $table->string('precio_min')->default('$100');
            $table->string('precio_max')->default('$1000');
            $table->foreignID('categoria')->default(1)->references('id')->on('categorias')->cascadeOnUpdate()->cascadeOnDelete();       //ID de la tabla categorias
            $table->foreignID('ubicacion')->default(1)->references('id')->on('ubicaciones')->cascadeOnUpdate()->cascadeOnDelete();      //ID de la tabla ubicaciones
            $table->string('mapa')->default("16.753598395778866, -93.11612627365052");                                                  //Ubicacion de google maps
            $table->date('omision')->nullable()->comment("Fecha para generar la ficha del primer pago");
            $table->boolean('pagos')->default(true)->comment("1.- True Paga 2.- False No paga");
            $table->boolean('tramites')->default(true)->comment("Indica si el negocio esta en tramites o no");
            $table->boolean('status')->default(false)->comment("visibilidad del negocio en el directorio");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('negocios');
    }
}
