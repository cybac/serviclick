<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTable extends Migration
{
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->bigIncrements('OrderID');
            $table->date('OrderDate');
            $table->date('OrderUpdateDate')->nullable();
            $table->string('OrderPaymentMethod');
            $table->string('OrderPaymentMethodID');
            $table->string('OrderTotalItems');
            $table->string('OrderTotal');
            $table->string('OrderCurrency');
            $table->string('OrderStatus');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
