<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegalesTable extends Migration
{
    public function up()
    {
        Schema::create('legales', function (Blueprint $table) {
            $table->id();
            $table->string('titulo');
            $table->longtext('contenido');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('legales');
    }
}
