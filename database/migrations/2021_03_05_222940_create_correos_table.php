<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorreosTable extends Migration
{
    public function up()
    {
        Schema::create('correos', function (Blueprint $table) {
            $table->id();
            $table->string("tipo");
            $table->string("titulo");
            $table->text("mensaje");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('correos');
    }
}
