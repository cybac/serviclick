<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeguimientosTable extends Migration
{
    public function up()
    {
        Schema::create('seguimientos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('tramite')->references('id')->on('tramites')->cascadeOnUpdate()->cascadeOnDelete();
            $table->text('comentarios')->nullable();
            $table->boolean('asistente')->default(false);
            $table->boolean('aprobado')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('seguimientos');
    }
}
