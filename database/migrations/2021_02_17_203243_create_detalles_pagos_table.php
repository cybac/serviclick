<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesPagosTable extends Migration
{
    public function up()
    {
        Schema::create('detalles_pagos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pago')->references('OrderID')->on('pagos')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('negocio')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->date('proximo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('detalles_pagos');
    }
}
