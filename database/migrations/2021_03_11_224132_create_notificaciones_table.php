<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionesTable extends Migration
{
    public function up()
    {
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->id();
            $table->foreignID('destinatario')->references('id')->on('negocios')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignID('notificacion')->references('id')->on('tipos_notificaciones')->cascadeOnUpdate()->cascadeOnDelete();
            $table->boolean('nuevo')->default(true);
            $table->boolean('leido')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
