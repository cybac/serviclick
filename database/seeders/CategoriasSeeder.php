<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasSeeder extends Seeder
{
    public function run()
    {
        DB::table('categorias')->insert([ 
            [
                'nombre'    =>'Sin Categoria',
                'imagen'    =>'img-1.jpg'
            ],[
                'nombre'    =>'Todas',
                'imagen'    =>'img-1.jpg'
            ],[
                'nombre'    =>'Plomeria',
                'imagen'    =>'plomeria.jpg'
            ],
            [
                'nombre'    =>'Carpinteria',
                'imagen'    =>'carpinteria.jpg'
            ],
            [
                'nombre'    =>'Estilista',
                'imagen'    =>'estilista.jpg'
            ],
            [
                'nombre'    =>'Modista',
                'imagen'    =>'modista.jpg'
            ],
            [
                'nombre'    =>'Electricista',
                'imagen'    =>'electricista.jpg'
            ],
            [
                'nombre'    =>'Mecanico',
                'imagen'    =>'mecanico.jpg'
            ],
            [
                'nombre'    =>'Fontanero',
                'imagen'    =>'fontaneria.jpg'
            ],
            [
                'nombre'    =>'Programador',
                'imagen'    =>'programador.jpg'
            ]
        ]);
    }
}
