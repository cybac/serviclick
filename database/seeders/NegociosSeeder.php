<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NegociosSeeder extends Seeder
{
    public function run()
    {
        DB::table('negocios')->insert([
            [
                'nombre'        =>'Dave Smith',
                'direccion'     =>'5 Pte Sur',
                'telefono'      =>'9661452148',
                'web'           =>'https://ejemplo.com',
                'descripcion'   =>'<p>En el ramo de la carpinteria por mas de 30 años</p>',
                'servicios'     =>'<ul class="gl-list-item"><li>Armado de Mesas</li><li>Armado de Sillas</li><li>Cualquier producto hecho de madera es posible</li></ul>',
                'precio_min'    =>'$500',
                'precio_max'    =>'$1500',
                'categoria'     =>'3',
                'ubicacion'     =>'1',
                'mapa'          =>'16.755222988742048, -93.14635103673312',
                'tramites'      =>false,
                'status'        =>true
            ],[
                'nombre'        =>'Kate Smith',
                'direccion'     =>'13 Nte Ote',
                'telefono'      =>'9661452148',
                'web'           =>'https://www.youtube.com',
                'descripcion'   =>'<p>Especialista en cortes de cabellos para toda ocasion, mas de 20 años me respaldan</p>',
                'servicios'     =>'<ul class="gl-list-item"><li>cortes para bodas</li><li>peinados para 15 años</li><li>afeitado</li><li>delineado de cejas</li><li>ETC...</li></ul>',
                'precio_min'    =>'$1000',
                'precio_max'    =>'$5000',
                'categoria'     =>'4',
                'ubicacion'     =>'1',
                'mapa'          =>'16.75817300940483, -93.08864375182964',
                'tramites'      =>false,
                'status'        =>true
            ],[
                'nombre'        =>'Luis Fernando Suasnavar Solís',
                'direccion'     =>'Colonia Centro',
                'telefono'      =>'9615874201',
                'web'           =>'https://www.wikipedia.org',
                'descripcion'   =>'Programador en multiples lenguiajes de programacion, amplia experienci en el mercado de desarrollo web y desktop',
                'servicios'     =>'<ul class="gl-list-item"><li>Desarrollo de páginas web</li><li>mantenimiento a servidores</li><li>mantenimiento preventivo y correctivo a equipo de computo en general</li><li>ETC...</li></ul>',
                'precio_min'    =>'$4000',
                'precio_max'    =>'$15000',
                'categoria'     =>'9',
                'ubicacion'     =>'1',
                'mapa'          =>'16.77414191780339, -93.131633888381',
                'tramites'      =>false,
                'status'        =>true
            ],[
                'nombre'        =>'Negocio #1',
                'direccion'     =>'Colonia Centro',
                'telefono'      =>'9661245781',
                'web'           =>'https://www.xd.org',
                'descripcion'   =>'Descripcion de ejemplo',
                'servicios'     =>'<ul class="gl-list-item"><li>Lista de servicios</li></ul>',
                'precio_min'    =>'$5000',
                'precio_max'    =>'$8000',
                'categoria'     =>'9',
                'ubicacion'     =>'2',
                'mapa'          =>'14.907935727209953, -92.26496685133633',
                'tramites'      =>false,
                'status'        =>true
            ],[
                'nombre'        =>'Negocio #2',
                'direccion'     =>'Colonia Centro',
                'telefono'      =>'9661245782',
                'web'           =>'https://www.elbromas.com',
                'descripcion'   =>'Descripcion de ejemplo #2',
                'servicios'     =>'<ul class="gl-list-item"><li>Lista de servicios</li></ul>',
                'precio_min'    =>'$1500',
                'precio_max'    =>'$10000',
                'categoria'     =>'4',
                'ubicacion'     =>'2',
                'mapa'          =>'14.89240822283205, -92.29410716240412',
                'tramites'      =>false,
                'status'        =>true
            ]
        ]);
    }
}
