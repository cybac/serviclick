<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CriteriosSeeder extends Seeder
{
    public function run()
    {
        DB::table('criterios')->insert([ 
            [
                'nombre'        =>'Calidad',
                'status'        =>true
            ],[
                'nombre'        =>'Servicio',
                'status'        =>true
            ],[
                'nombre'        =>'Responsabilidad',
                'status'        =>true
            ]
        ]);
    }
}
