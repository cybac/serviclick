<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CorreosSeeder extends Seeder
{
    public function run()
    {
        DB::table('correos')->insert([ 
            [
                'tipo'          =>'Bienvenido Usuario',
                'titulo'        =>'Registro de usuario',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Registro en ServiClick</h3><p style="margin-top: 0; margin-bottom: 0.5rem;">Se ha creado una cuenta en ServiClick</p><p style="margin-top: 0; margin-bottom: 0.7rem;">De ahora en adelante podra:</p><ul style="margin-top: 0; margin-bottom: 2rem;"><li>Dejar rese&ntilde;as de los negocios</li><li>Agregar a favoritos los negocios que mejor servicio le brinden</li><li>&iexcl;y mucho m&aacute;s!</li></ul><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Bienvenido Negocio',
                'titulo'        =>'Alta en el directorio',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Alta en el directorio ServiClick</h3><p style="margin-top: 0; margin-bottom: 0.5rem;">Se ha creado una cuenta de negocios en ServiClick</p><p style="margin-top: 0; margin-bottom: 0.7rem;">De ahora en adelante debera seguir los siguiente pasos:</p><ul style="margin-top: 0; margin-bottom: 2rem;"><li>Inicia sesi&oacute;n en tu cuenta creada.</li><li>Sube el logotipo de tu negocio o marca que representas.</li><li>Proporciona la informaci&oacute;n requisitada de tu negocio.</li><ul><li>Nombre del negocio</li><li>Tel&eacute;fono</li><li>Direcci&oacute;n</li><li>Etc.</li></ul><li>Sube la documentaci&oacute;n solicitada del negocio o personal (seg&uacute;n sea el caso).</li></ul><p style="margin-top: 0; margin-bottom: 0;">Una vez validado su negocio,</p><p style="margin-top: 0; margin-bottom: 0;">se liberara el primer pago que debera de realizar.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Una vez reflejado el pago, se dara por terminado el proceso de registro.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene algún comentario, por favor envíe un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Corrección de información',
                'titulo'        =>'Se solicito una corrección en la información del negocio',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Se solicito una correci&oacute;n en tu informaci&oacute;n</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">Se rechazo la informaci&oacute;n proporcionada del negocio, entre en el panel de control para obtener mas detalles del motivo del rechazo y reenviar su informaci&oacute;n.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Corrección de documentación',
                'titulo'        =>'Se solicito una corrección en la documentación del negocio',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Se solicito una correci&oacute;n en la documentaci&oacute;n del negocio</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">Se rechazo uno o mas documentos del negocio, ingrese al panel de control para obtener mas detalles del motivo del rechazo y reenviar su documentaci&oacute;n.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Aprobación de información',
                'titulo'        =>'Se aprobo la información del negocio',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Se aprobo la informaci&oacute;n del negocio</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">La informaci&oacute;n proporcionada del negocio ha sido revisada, verificada y aprobada por nuestro equipo de expertos.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Aprobación de la documentación',
                'titulo'        =>'Se aprobo la documentación del negocio',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Se aprobo la documentaci&oacute;n del negocio</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">La documentaci&oacute;n proporcionada del negocio ha sido revisada, verificada y aprobada por nuestro equipo de expertos.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Ficha liberada',
                'titulo'        =>'EL negocio paso a la fase final',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">Casi terminamos, se libero la ficha de pago del negocio</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">La ficha de pago del negocio fue liberada, tiene 5 dias a partir de la fecha de recepci&oacute;n de este correo para pagar.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Recibira la ficha de pago un instante despues de este correo o puede consultarla desde el panel de control en ServiClick.</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Ficha pagada',
                'titulo'        =>'La ficha fue pagada',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">ServiClick Informa</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">Su pago fue aplicado correctamente.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Si su pago fue por alta en el servicio se han liberado las opciones del panel del negocio.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Si su pago fue por reactivaci&oacute;n, el negocio se encontrara nuevamente visible.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">&nbsp;</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ],[
                'tipo'          =>'Ficha expirada',
                'titulo'        =>'La ficha ha expirado',
                'mensaje'       =>'<h3 style="color: #055086; margin-bottom: 0.8rem;">ServiClick Informa</h3><p style="margin-top: 0; margin-bottom: 0.7rem;">Su ficha ha expirado.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Si requiere de una nueva ficha, solicitela desde el panel de control en la secci&oacute;n <strong>historial de pagos</strong>.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">Se le generara una nueva ficha de pago con 5 dias de validez, adicionalmente se le enviara un correo con la ficha de pago.</p><p style="margin-top: 0; margin-bottom: 0.7rem;">&nbsp;</p><p style="margin-top: 0; margin-bottom: 0;"><strong>Atentamente</strong></p><p style="margin-top: 0; margin-bottom: 1rem;">Presidente ServiClick</p><p style="margin-top: 0; margin-bottom: 0;"><strong>P.D.</strong> Si requiere ayuda o tiene alg&uacute;n comentario, por favor env&iacute;e un correo a <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a></p>'
            ]
        ]);
    }
}
