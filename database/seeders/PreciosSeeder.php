<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreciosSeeder extends Seeder
{
    public function run()
    {
        DB::table('precios')->insert([
            [
                'nombre'    => 'Pago a ServiClick',
                'costo'     => '15000',
                'moneda'    => 'MXN',
            ]
        ]);
    }
}
