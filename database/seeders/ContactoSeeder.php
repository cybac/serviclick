<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactoSeeder extends Seeder
{
    public function run()
    {
        DB::table('contactos')->insert([ 
            [
                'direccion'     =>'9 sur pte',
                'telefono'      =>'965 412 45 78',
                'correo'        =>'soporte@serviclick.com',
            ]
        ]);
    }
}
