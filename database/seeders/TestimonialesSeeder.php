<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestimonialesSeeder extends Seeder
{
    public function run()
    {
        DB::table('testimoniales')->insert([ 
            [
                'imagen'        =>'1.png',
                'nombre'        =>'Dave Smith',
                'contenido'     =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam magni aliquam nemo harum, consectetur a illum explicabo doloremque tenetur fugit nam, commodi animi ratione repellat, accusantium amet? Deserunt, eum earum.',
                'trabajo'       =>'Carpintero',
                'status'        =>true
            ],
            [
                'imagen'        =>'2.png',
                'nombre'        =>'Kate Smith',
                'contenido'     =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam magni aliquam nemo harum, consectetur a illum explicabo doloremque tenetur fugit nam, commodi animi ratione repellat, accusantium amet? Deserunt, eum earum.',
                'trabajo'       =>'Estilista',
                'status'        =>true
            ],
            [
                'imagen'        =>'3.png',
                'nombre'        =>'Mario Lopez',
                'contenido'     =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam magni aliquam nemo harum, consectetur a illum explicabo doloremque tenetur fugit nam, commodi animi ratione repellat, accusantium amet? Deserunt, eum earum.',
                'trabajo'       =>'Plomero',
                'status'        =>true
            ],
            [
                'imagen'        =>'4.png',
                'nombre'        =>'Laura Gutiérrez',
                'contenido'     =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam magni aliquam nemo harum, consectetur a illum explicabo doloremque tenetur fugit nam, commodi animi ratione repellat, accusantium amet? Deserunt, eum earum.',
                'trabajo'       =>'Modista',
                'status'        =>true
            ]
        ]);
    }
}
