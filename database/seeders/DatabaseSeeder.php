<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            CorreosSeeder::class,
            Tipos_NotificacionesSeeder::class,
            AboutSeeder::class,
            ContactoSeeder::class,
            EquipoSeeder::class,
            ExperienciaSeeder::class,
            HabilidadesSeeder::class,
            SucursalesSeeder::class,
            CriteriosSeeder::class,
            PreciosSeeder::class,
            UsersSeeder::class,
            RedesSocialesSeeder::class,
            
            CategoriasSeeder::class,
            Slider_FortalezasSeeder::class,
            FortalezasSeeder::class,
            TestimonialesSeeder::class,
            UbicacionesSeeder::class,
            TramitesSeeder::class,
            Tipos_DocumentosSeeder::class,
            AnunciosSeeder::class,
            LegalesSeeder::class,
            
            /*
            NegociosSeeder::class,
            DocumentosSeeder::class,
            KeyswordSeeder::class,
            ComentariosSeeder::class,
            CalificacionesSeeder::class,
            GaleriaSeeder::class,
            */
        ]);
    }
}
