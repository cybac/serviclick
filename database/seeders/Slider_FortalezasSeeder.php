<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Slider_FortalezasSeeder extends Seeder
{
    public function run()
    {
        DB::table('slider_fortalezas')->insert([ 
            [
                'inicio'    =>'Tu empleado',
                'remarcado' => 'ideal',
                'fin'       => 'a tan solo un click',
                'status'    =>true
            ],
            [
                'inicio'    =>'Encuentra',
                'remarcado' => 'el mejor',
                'fin'       => 'trabajador para ti',
                'status'    =>true
            ],
            [
                'inicio'    =>'Necesitas',
                'remarcado' => 'ayuda,',
                'fin'       => 'encuentrala aqui',
                'status'    =>true
            ]
        ]);
    }
}
