<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentosSeeder extends Seeder
{
    public function run()
    {
        DB::table('documentos')->insert([
            [
                'negocio'   => '1',
                'tipo'      => '1',
            ],
            [
                'negocio'   => '1',
                'tipo'      => '2',
            ],[
                'negocio'   => '2',
                'tipo'      => '1',
            ],
            [
                'negocio'   => '2',
                'tipo'      => '2',
            ],[
                'negocio'   => '3',
                'tipo'      => '1',
            ],
            [
                'negocio'   => '3',
                'tipo'      => '2',
            ],[
                'negocio'   => '4',
                'tipo'      => '1',
            ],
            [
                'negocio'   => '4',
                'tipo'      => '2',
            ],[
                'negocio'   => '5',
                'tipo'      => '1',
            ],
            [
                'negocio'   => '5',
                'tipo'      => '2',
            ]
        ]);
    }
}
