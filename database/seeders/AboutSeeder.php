<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutSeeder extends Seeder
{
    public function run()
    {
        DB::table('abouts')->insert([ 
            [
                'imagen'        =>'about-us.jpg',
                'contenido'     =>' <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, accusantium delectus enim vel quis assumenda ea perferendis possimus at, minus et fugit nisi voluptatibus, impedit pariatur consequatur laborum blanditiis. Neque!</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo suscipit at, similique voluptatum tempore quis iure aliquid recusandae. Ipsam perspiciatis nesciunt obcaecati qui cumque expedita sint temporibus, labore sequi architecto?</p> '
            ]
        ]);
    }
}
