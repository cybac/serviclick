<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tipos_DocumentosSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_documentos')->insert([
            [
                'nombre'    => 'INE',
                'status'    => true
            ],
            [
                'nombre'    => 'Comprobante de domicilio',
                'status'    => true
            ]
        ]);
    }
}
