<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LegalesSeeder extends Seeder
{
    public function run()
    {
        DB::table('legales')->insert([
            [
                'titulo' => 'Aviso de privacidad para clientes y/o usuarios',
                'contenido' => '
                    <p style="text-align: justify; margin-bottom: 1rem;">De conformidad con lo dispuesto en la Ley
                        Federal de Protección de Datos Personales en Posesión de los Particulares, se emite el presente
                        Aviso de Privacidad en los siguientes términos:</p>
                    <ol>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">IDENTIDAD Y DOMICILIO DEL RESPONSABLE</li>
                        <p style="text-align: justify;">Para los efectos del presente Aviso de Privacidad ServiClick, es
                            el responsable del uso y manejo
                            de sus datos personales, teniendo como domicilio el ubicado en Fraccionamiento villas santa
                            maría C.P. 29054 Tuxtla Gutiérrez, Chiapas. De la misma forma, hacemos de su conocimiento
                            que, para cualquier duda, comentario, notificación y/o queja respecto al manejo de sus datos
                            personales, puede dirigirlos a dicho domicilio, o bien al correo electrónico
                            soporte@serviclick.com.mx.
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">FINALIDADES</li>
                        <p style="text-align: justify;">Sus datos personales serán utilizados para: (i) identificación y
                            registro de usuarios y/o clientes,
                            (ii) validación de sus datos, (iii) creación y recuperación de contraseña, (iv)
                            actualización de
                            datos, (v) recibir notificaciones de la aplicación a su dispositivo, (vi) localización de
                            negocios,
                            (vii) contactar al cliente por medio de notificaciones a su dispositivo.
                        </p>
                        <ul style="list-style: disc;">
                            <li style="margin-left: 5rem; margin-bottom: 1rem; margin-top: 1rem;">FINALIDADES
                                SECUNDARIAS </li>
                            <p style="text-align: justify;">De manera secundaria, utilizaremos sus datos para: </p>
                            <li style="margin-left: 5rem;">Notificarle sobre nuevos productos o servicios </li>
                            <li style="margin-left: 5rem;">Para envío de invitaciones y avisos de promociones.</li>
                        </ul>
                        <p style="text-align: justify;">Es importante señalar que ServiClick se compromete a tratar los
                            datos personales única y
                            exclusivamente para las finalidades aprobadas por el titular, así como establecer y mantener
                            los
                            controles de seguridad para la protección de los mismos. </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">DATOS PERSONALES QUE RECABAMOS</li>
                        <p style="text-align: justify;">Los datos personales que recabaremos serán de identificación y
                            de contacto. </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">TRANSFERENCIA DE DATOS</li>
                        <p style="text-align: justify;">Sus datos personales solicitados podrán ser transferidos a
                            entidades del mismo grupo de interés
                            de ServiClick, así como a dependencias gubernamentales, con el objetivo de cumplir con las
                            finalidades para las cuales ha proporcionado sus datos. ServiClick tomará medidas para que
                            el
                            tercero que recibe los datos personales se apegue al cumplimiento del presente aviso de
                            privacidad. Es importante señalar que ServiClick se compromete a transferir los datos
                            personales única y exclusivamente para las finalidades aprobadas por el titular, así como
                            establecer y mantener los controles de seguridad necesarios para la protección de los
                            mismos.
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">OPCIONES, MEDIOS Y MECANISMOS PARA LIMITAR EL
                            USO O DIVULGACION DE SUS DATOS</li>
                        <p style="text-align: justify;">Para limitar el uso o divulgación de sus datos, ServiClick
                            cuenta con listados de exclusión. Para
                            solicitar su registro en dicho listado, lo puede realizar a través de los mecanismos de
                            comunicación definidos en el punto 1, asimismo, usted se podrá inscribir a los siguientes
                            registros, en caso de que no desee obtener publicidad de nuestra parte: </p>
                        <p style="text-align: justify;">Registro Público para Evitar Publicidad, para mayor información
                            consulte el portal de internet de la PROFECO.</p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">DERECHOS ARCO Y REVOCACION DEL CONSENTIMIENTO
                        </li>
                        <p style="text-align: justify; margin-bottom: 1rem;">
                            Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos
                            y las condiciones de uso que les damos (Acceso). Asimismo, es su derecho solicitar la
                            corrección
                            de su información personal en caso de que esté desactualizada, sea inexacta o incompleta
                            (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere
                            que
                            la misma no está siendo utilizada conforme a los principios, deberes y obligaciones
                            previstas en
                            la normativa (Cancelación); así como oponerse al uso de sus datos personales para fines
                            específicos (Oposición). Estos derechos se conocen como derechos ARCO. Asimismo, usted
                            podrá revocar el consentimiento que nos haya otorgado. Para el ejercicio de dichos derechos,
                            así como la revocación de consentimiento deberá presentar el FORMATO PARA EJERCER LOS
                            DERECHOS ARCO Y/O REVOCACION DE CONSENTIMIENTO que se encuentra disponible en
                            nuestra página web <a href="https://serviclick.com.mx/">https://serviclick.com.mx/</a> el
                            formato se debe llenar, firmar y enviar a través
                            de los mecanismos de comunicación definidos en el punto 1.
                        </p>
                        <ol style="list-style: none;">
                            <li><strong>A. Información y/o documentos necesarios para el ejercicio de derechos
                                    ARCO</strong></li>
                            <li><strong>A.1</strong> Solicitud para ejercicio de derechos ARCO a través del Formato para
                                ejercer los derechos
                                ARCO y/o Revocación de Consentimiento disponible en nuestra página web
                                <a href="https://serviclick.com.mx/">https://serviclick.com.mx/</a>.</li>
                            <li><strong>A.2</strong> Copia simple de los documentos de acreditación del titular o del
                                representante, y carta
                                poder del representante.
                            </li>
                            <li><strong>A.3</strong> Datos de contacto (Nombre, domicilio, correo electrónico y/o
                                teléfono) del titular</li>

                        </ol>
                        <p style="text-align: justify; margin-top: 1rem;">Para cualquier duda o aclaración con respecto
                            al procedimiento de ejercicio de derechos ARCO
                            y/o revocación del consentimiento se puede poner en contacto al correo electrónico
                            <a href="mailto:soporte@serviclick.com.mx">soporte@serviclick.com.mx</a>.
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">USO DE "COOKIES" Y "WEB BEACONS"</li>
                        <p style="text-align: justify;">
                            Con el objetivo de mejorar la experiencia de sus usuarios en cualquiera de los sitios de
                            Internet,
                            ServiClick podrá utilizar "cookies" y/o "web beacons". Las "cookies" son archivos de texto
                            que
                            son descargados automáticamente y almacenados en el dispositivo del cliente para permitir a
                            nuestros sistemas reconocer el dispositivo y otras características, entre ellas, nombre de
                            usuario
                            y contraseña. Por su parte, las "web beacons" son imágenes insertadas en un página de
                            Internet
                            o correo electrónico, que puede ser utilizado para monitorear el comportamiento de un
                            visitante, entre funciones como almacenar información sobre la dirección IP del usuario,
                            duración del tiempo de interacción en dicha página y el tipo de navegador utilizado, entre
                            otros.
                            Le informamos que utilizamos "cookies" y/o "web beacons" para obtener información personal
                            de Usted, como la siguiente:
                        </p>
                        <ul style="list-style: disc;">
                            <li style="margin-left: 5rem; margin-top: 1rem;">Las páginas de Internet que visita </li>
                            <li style="margin-left: 5rem;">Los vínculos que sigue</li>
                            <li style="margin-left: 5rem;">La dirección IP</li>
                            <li style="margin-left: 5rem;">Idioma preferido por el usuario </li>
                            <li style="margin-left: 5rem;">Región en la que se encuentra el usuario </li>
                            <li style="margin-left: 5rem;">Tipo de navegador del usuario </li>
                            <li style="margin-left: 5rem;">Tipo de sistema operativo del usuario </li>
                            <li style="margin-left: 5rem;">Páginas web visitadas por un usuario </li>
                            <li style="margin-left: 5rem;">Búsquedas realizadas por un usuario </li>
                            <li style="margin-left: 5rem;">Publicidad revisada por un usuario </li>
                            <li style="margin-left: 5rem;">Ubicación del usuario </li>
                        </ul>
                        <p style="text-align: justify; margin-top: 1rem;">Estas “cookies” y otras tecnologías pueden ser
                            deshabilitadas. Para conocer cómo hacerlo,
                            consulte el menú de ayuda de su navegador. Tenga en cuenta que, en caso de desactivar las
                            "cookies" es posible que no pueda acceder a ciertas funciones personalizadas en nuestro
                            sitio
                            de Internet
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">MODIFICACIONES AL AVISO DE PRIVACIDAD</li>
                        <p style="text-align: justify;">Nos reservamos el derecho de efectuar en cualquier momento
                            modificaciones o actualizaciones
                            al presente Aviso de Privacidad, para la atención de novedades legislativas, políticas
                            internas o
                            nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios. Estas
                            modificaciones estarán disponibles a través de nuestra página web, <a
                                href="https://serviclick.com.mx/">https://serviclick.com.mx/</a>,
                            sección Aviso de Privacidad, por lo que recomendamos visitar periódicamente nuestra página
                            de Internet y/o estar atento a posibles modificaciones al presente Aviso de Privacidad.
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;"> SEGURIDAD Y MEDIOS PARA EVITAR EL USO O
                            DIVULGACIÓN DE LOS DATOS PERSONALES.<br>ALMACENAMIENTO Y TRANSFERENCIA DE LOS DATOS
                            PERSONALES.</li>
                        <p style="text-align: justify;">
                            ServiClick está obligado a cumplir con toda la normativa aplicable en materia de medidas de
                            seguridad aplicables a los Datos Personales. Adicionalmente, ServiClick usa los estándares
                            de la
                            industria entre materia de protección de la confidencialidad de sus Datos Personales,
                            incluyendo, en otras medidas, cortafuegos ("firewalls"), ServiClick considera a los datos de
                            sus
                            clientes y usuarios como un activo que debe ser protegido de cualquier pérdida o acceso no
                            autorizado. Empleamos diversas técnicas de seguridad para proteger tales datos de accesos no
                            autorizados por usuarios de dentro o fuera de nuestra compañía. Sin embargo, es necesario
                            tener muy en cuenta que la seguridad perfecta no existe en internet.
                        </p>
                        <li style="margin-bottom: 1rem; margin-top: 1rem;">TRATAMIENTO INDEBIDO DE SUS DATOS PERSONALES
                        </li>
                        <p style="text-align: justify;">Si Usted considera que su derecho de protección de datos
                            personales ha sido lesionado con
                            alguna conducta por parte de ServiClick, o en sus actuaciones o respuestas, presume que en
                            el
                            tratamiento de sus datos personales existe alguna violación a las disposiciones previstas en
                            la
                            Ley Federal de Protección de Datos Personales en Posesión de los Particulares, podrá
                            interponer
                            la queja o denuncia correspondiente ante el Instituto Nacional de Transparencia, Acceso a la
                            Información y Protección de Datos Personales (INAI).
                        </p>
                    </ol>'
            ],[
                'titulo' => 'Términos y Condiciones',
                'contenido' => '
                    <p style="text-align: justify; margin-bottom: 1rem;">Bienvenido a la plataforma en línea de
                        ServiClick. Estos términos y condiciones
                        describen las reglas y regulaciones para el uso del sitio web
                        <a href="https://serviclick.com.mx/">https://serviclick.com.mx/</a> y aplicación móvil
                        ServiClick en sus dos versiones iOS y
                        Android.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">
                        <strong>ServiClick se encuentra en</strong>
                        <a href="https://serviclick.com.mx/">https://serviclick.com.mx/</a>
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">Al acceder a este sitio web, asumimos que
                        aceptas estos términos y condiciones en
                        su totalidad. No continúes usando el sitio web de <strong>ServiClick</strong> si no aceptas
                        todos los
                        términos y condiciones establecidos en esta página.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">La siguiente terminología se aplica a estos
                        Términos y Condiciones, Declaración de
                        Privacidad y Aviso legal y cualquiera o todos los Acuerdos: el <strong>Cliente</strong>,
                        <strong>Usted</strong> y <strong>Su</strong> se
                        refieren a usted, la persona que accede a este sitio web y a la instalación de nuestra
                        aplicación en sus dispositivos móviles aceptando de manera implícita los términos y
                        condiciones de ServiClick. La <strong>Compañía</strong>, <strong>Nosotros mismos</strong>,
                        <strong>Nosotros y Nuestro</strong>, se
                        refiere a nuestra Compañía. <strong>Parte, Partes o Nosotros</strong>, se refiere en conjunto al
                        Cliente y a nosotros mismos, o al Cliente o a nosotros mismos.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">Todos los términos se refieren a la oferta,
                        aceptación y consideración del pago
                        necesario para efectuar el proceso de difusión de contenidos para el Cliente de la
                        manera más adecuada, ya sea mediante reuniones formales de una duración fija, o
                        por cualquier otro medio, con el propósito expreso de conocer las necesidades del
                        Cliente con respecto a la provisión de los servicios/productos declarados de la
                        Compañía, de acuerdo con y sujeto a la ley vigente del estado de Chiapas, México.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">Cualquier uso de la terminología anterior u
                        otras palabras en singular, plural,
                        mayúsculas y/o, él/ella o ellos, se consideran intercambiables y, por lo tanto, se refieren a lo
                        mismo.
                    </p>
                    <h3 style="text-align: justify; margin-bottom: 1rem;">Cookies y Herramientas de Analítica</h3>
                    <p style="text-align: justify; margin-bottom: 1rem;">Empleamos el uso de cookies. Al utilizar el
                        sitio web y aplicación móvil de ServiClick,
                        usted acepta el uso de cookies y herramientas de analítica de acuerdo con la política
                        de privacidad de ServiClick. La mayoría de los modernos sitios web interactivos de
                        hoy en día usan cookies y otras herramientas de analítica para permitirnos recuperar
                        los detalles del usuario para cada visita y con ello mejorar la experiencia del usuario
                        en futuras visitas o uso.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">Las cookies se utilizan en algunas áreas de
                        nuestro sitio para habilitar la funcionalidad
                        de esta área y la facilidad de uso para las personas que lo visitan. Algunos de nuestros
                        socios afiliados/publicitarios también pueden usar cookies.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">
                        Las herramientas de analítica pueden incorporar etiquetas, scripts o pixeles de rastreo
                        para recopilar información de uso y navegación. Algunos de nuestros socios afiliados
                        pueden también usar estas herramientas de analítica.
                    </p>
                    <h3 style="text-align: justify; margin-bottom: 1rem;">Licencia</h3>
                    <p style="text-align: justify; margin-bottom: 1rem;">A menos que se indique lo contrario, ServiClick
                        y/o sus licenciatarios les pertenecen
                        los derechos de propiedad intelectual de todo el material publicado en ServiClick.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">No debes:</p>
                    <ul style="list-style: disc;">
                        <li style="margin-left: 5rem;">Volver a publicar material desde <a
                                href="https://serviclick.com.mx/">https://serviclick.com.mx/</a> </li>
                        <li style="margin-left: 5rem;">Vender, alquilar u otorgar una sub-licencia de material desde <a
                                href="https://serviclick.com.mx/">https://serviclick.com.mx/</a> y/o su aplicación móvil
                            ServiClick.</li>
                        <li style="margin-left: 5rem;">Reproducir, duplicar o copiar material <a
                                href="https://serviclick.com.mx/">https://serviclick.com.mx/</a></li>
                        <li style="margin-left: 5rem;">Redistribuir contenido de ServiClick, a menos de que el contenido
                            se haga específicamente para la redistribución.</li>
                    </ul>
                    <h3 style="text-align: justify; margin-bottom: 1rem;">Aviso legal</h3>
                    <p style="text-align: justify; margin-bottom: 1rem;">En la medida máxima permitida por la ley
                        aplicable, excluimos todas las
                        representaciones, garantías y condiciones relacionadas con nuestro sitio web y el uso
                        de este sitio web (incluyendo, sin limitación, cualquier garantía implícita por la ley con
                        respecto a la calidad satisfactoria, idoneidad para el propósito y/o el uso de cuidado
                        y habilidad razonables).
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">Nada en este aviso legal:</p>
                    <ul style="list-style: disc;">
                        <li style="margin-left: 5rem;">Limita o excluye nuestra o su responsabilidad por muerte o
                            lesiones personales resultantes de negligencia.</li>
                        <li style="margin-left: 5rem;">Limita o excluye nuestra o su responsabilidad por fraude o
                            tergiversación fraudulenta.</li>
                        <li style="margin-left: 5rem;">Limita cualquiera de nuestras o sus responsabilidades de
                            cualquier manera que no esté permitida por la ley aplicable.</li>
                        <li style="margin-left: 5rem;">Excluye cualquiera de nuestras o sus responsabilidades que no
                            pueden ser excluidas bajo la ley aplicable.</li>
                    </ul>
                    <p style="text-align: justify; margin-bottom: 1rem; margin-top:1rem;">Las limitaciones y exclusiones
                        de responsabilidad establecidas en esta Sección y en otras partes de este aviso legal:</p>
                    <p style="text-align: justify; margin-bottom: 1rem;">1. están sujetas al párrafo anterior; y</p>
                    <p style="text-align: justify; margin-bottom: 1rem;">2. rigen todas las responsabilidades que surjan
                        bajo la exención de responsabilidad o
                        en relación con el objeto de esta exención de responsabilidad, incluidas las
                        responsabilidades que surjan en contrato, agravio (incluyendo negligencia) y por
                        incumplimiento del deber legal.
                    </p>
                    <p style="text-align: justify; margin-bottom: 1rem;">En la medida en que el sitio web y la
                        información y los servicios en el sitio web se
                        proporcionen de forma gratuita, no seremos responsables de ninguna pérdida o daño
                        de ningún tipo.
                    </p>'
            ]
        ]);
    }
}
