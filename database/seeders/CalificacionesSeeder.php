<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CalificacionesSeeder extends Seeder
{
    public function run()
    {
        DB::table('calificaciones')->insert([ 
            [
                'negocio'       =>'1',
                'tipo'          =>'Calidad',
                'puntuacion'    =>'3',
                'comentario'    =>'1'
            ],
            [
                'negocio'       =>'1',
                'tipo'          =>'Responsabilidad',
                'puntuacion'    =>'4.5',
                'comentario'    =>'1'
            ],
            [
                'negocio'       =>'1',
                'tipo'          =>'Servicio',
                'puntuacion'    =>'4',
                'comentario'    =>'1'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Calidad',
                'puntuacion'    =>'5',
                'comentario'    =>'2'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Responsabilidad',
                'puntuacion'    =>'5',
                'comentario'    =>'2'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Servicio',
                'puntuacion'    =>'4.5',
                'comentario'    =>'2'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Calidad',
                'puntuacion'    =>'4',
                'comentario'    =>'3'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Responsabilidad',
                'puntuacion'    =>'4.5',
                'comentario'    =>'3'
            ],
            [
                'negocio'       =>'3',
                'tipo'          =>'Servicio',
                'puntuacion'    =>'5',
                'comentario'    =>'3'
            ]
        ]);
    }
}
