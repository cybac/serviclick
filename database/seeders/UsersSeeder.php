<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([ 
            [
                'name'      =>'Soporte CYBAC',
                'email'     =>'soporte@grupocybac.com',
                'password'  =>bcrypt('SoporteSC_CYBAC')
            ],[
                'name'      =>'Administrador',
                'email'     =>'control_sc@serviclick.com.mx',
                'password'  =>bcrypt('SupSC_#210511')
            ]
        ]);
        
        Role::create(['name' => 'administrador']);
        Role::create(['name' => 'negocio']);
        Role::create(['name' => 'cliente']);
        User::find(1)->assignRole('administrador');
        User::find(2)->assignRole('administrador');
    }
}
