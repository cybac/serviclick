<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class GaleriaSeeder extends Seeder
{
    public function run()
    {
        DB::table('galeria_trabajos')->insert([ 
            [
                'negocio'   =>'3',
                'imagen'    =>'1.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'2.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'3.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'5.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'8.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'2.jpg'
            ],
            [
                'negocio'   =>'3',
                'imagen'    =>'5.jpg'
            ],[
                'negocio'   =>'1',
                'imagen'    =>'8.jpg'
            ],
            [
                'negocio'   =>'1',
                'imagen'    =>'5.jpg'
            ],
            [
                'negocio'   =>'1',
                'imagen'    =>'3.jpg'
            ]
        ]);
    }
}
