<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExperienciaSeeder extends Seeder
{
    public function run()
    {
        DB::table('experiencias')->insert([ 
            [
                'contenido'     =>'<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, expedita? Beatae corrupti quidem et officia officiis eum. Saepe facilis eum quae natus eius optio? Tempore fugit soluta veniam est odit!</p>
                                   <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Blanditiis doloribus corrupti laborum, itaque assumenda tempora alias minima numquam non placeat autem voluptates officiis nam ipsa, repellendus dolorem odio at nobis.</p>'
            ]
        ]);
    }
}
