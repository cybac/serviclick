<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HabilidadesSeeder extends Seeder
{
    public function run()
    {
        DB::table('habilidades')->insert([ 
            [
                'nombre'     =>'Atención',
                'cantidad'     =>'90'
            ],[
                'nombre'     =>'Soporte',
                'cantidad'     =>'90'
            ],[
                'nombre'     =>'Marketing',
                'cantidad'     =>'80'
            ],[
                'nombre'     =>'Finanzas',
                'cantidad'     =>'70'
            ],[
                'nombre'     =>'Cordialidad',
                'cantidad'     =>'90'
            ]
        ]);
    }
}
