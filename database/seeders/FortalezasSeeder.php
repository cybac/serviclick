<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FortalezasSeeder extends Seeder
{

    public function run()
    {
        DB::table('fortalezas')->insert([ 
            [
                'imagen'        =>'1.png',
                'titulo'        =>'Trabajadores calificados',
                'descripcion'   =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam magni aliquam nemo harum, consectetur a illum explicabo doloremque tenetur fugit nam, commodi animi ratione repellat, accusantium amet? Deserunt, eum earum.'
            ],
            [
                'imagen'        =>'2.png',
                'titulo'        =>'Titulo de prueba 2',
                'descripcion'   =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita rerum repellendus facere autem itaque, quidem veniam necessitatibus minus enim, dicta sapiente ex voluptatum aliquid, consequuntur ipsa reprehenderit aut! Eveniet, perspiciatis.'
            ],
            [
                'imagen'        =>'3.png',
                'titulo'        =>'Titulo de prueba 3',
                'descripcion'   =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio sunt doloribus iusto quidem perferendis neque ipsam facilis corrupti nesciunt, aliquid ipsa voluptatum illum! Labore, quae sit totam illum neque hic.'
            ]
        ]);
    }
}
