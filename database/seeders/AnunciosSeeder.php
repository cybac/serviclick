<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnunciosSeeder extends Seeder
{
    public function run()
    {
        DB::table('anuncios')->insert([ 
            [
                'imagen'        =>'anuncio.jpg'
            ]
        ]);
    }
}
