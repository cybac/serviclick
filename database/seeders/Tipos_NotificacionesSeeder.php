<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tipos_NotificacionesSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_notificaciones')->insert([
            [
                'tipo' => 'Corrección de información',
                'mensaje' => 'Se solicito una corrección en la información del negocio',
            ], [
                'tipo' => 'Corrección de documentación',
                'mensaje' => 'Se solicito una corrección en la documentación del negocio',
            ], [
                'tipo' => 'Aprobación de información',
                'mensaje' => 'Se aprobo la información del negocio',
            ], [
                'tipo' => 'Aprobación de la documentación',
                'mensaje' => 'Se aprobo la documentación del negocio',
            ], [
                'tipo' => 'Ficha liberada',
                'mensaje' => 'EL negocio paso a la fase final',
            ], [
                'tipo' => 'Ficha pagada',
                'mensaje' => 'La ficha fue pagada',
            ], [
                'tipo' => 'Ficha expirada',
                'mensaje' => 'La ficha ha expirado',
            ], [
                'tipo' => 'Solicitud de servicio',
                'mensaje' => 'Un usuario requiere de sus servicios',
            ], [
                'tipo' => 'Ficha de pago',
                'mensaje' => 'Su ficha ha sido expedida',
            ],
        ]);
    }
}
