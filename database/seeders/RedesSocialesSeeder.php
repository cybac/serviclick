<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RedesSocialesSeeder extends Seeder
{
    public function run()
    {
        DB::table('redes_sociales')->insert([
            [
                'nombre'    => 'Facebook',
                'icono'     => 'fa fa-facebook',
                'url'       => 'https://www.facebook.com',
                'status'    => true,
            ],[
                'nombre'    => 'Twitter',
                'icono'     => 'fa fa-twitter',
                'url'       => 'https://twitter.com/?lang=es',
                'status'    => true,
            ],[
                'nombre'    => 'Instagram',
                'icono'     => 'fa fa-instagram',
                'url'       => 'https://www.instagram.com/?hl=es-la',
                'status'    => true,
            ],[
                'nombre'    => 'Youtube',
                'icono'     => 'fa fa-youtube-play',
                'url'       => 'https://www.youtube.com/',
                'status'    => false,
            ],[
                'nombre'    => 'Linkedin',
                'icono'     => 'fa fa-linkedin',
                'url'       => 'https://mx.linkedin.com/',
                'status'    => false,
            ],[
                'nombre'    => 'Whatsapp',
                'icono'     => 'fa fa-whatsapp',
                'url'       => '#',
                'status'    => false,
            ],[
                'nombre'    => 'Telegram',
                'icono'     => 'fa fa-telegram',
                'url'       => '#',
                'status'    => false,
            ],[
                'nombre'    => 'Twitch',
                'icono'     => 'fa fa-twitch',
                'url'       => 'https://www.twitch.tv/',
                'status'    => false,
            ]
        ]);
    }
}
