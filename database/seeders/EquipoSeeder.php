<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquipoSeeder extends Seeder
{
    public function run()
    {
        DB::table('equipos')->insert([ 
            [
                'imagen'        =>'agents-1.jpg',
                'nombre'        =>'Roberto Miranda',
                'puesto'        =>'Agente de ventas',
                'status'        =>true
            ],[
                'imagen'        =>'agents-2.jpg',
                'nombre'        =>'Mario Perez',
                'puesto'        =>'CEO',
                'status'        =>true
            ],[
                'imagen'        =>'agents-3.jpg',
                'nombre'        =>'Maria Soledad',
                'puesto'        =>'Relaciones publicas',
                'status'        =>true
            ],[
                'imagen'        =>'agents-4.jpg',
                'nombre'        =>'Daniela Solis',
                'puesto'        =>'Presidenta',
                'status'        =>true
            ]
        ]);
    }
}
