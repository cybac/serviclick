<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeyswordSeeder extends Seeder
{
    public function run()
    {
        DB::table('keyswords')->insert([
            [
                'negocio' => '1',
                'key' => 'puntual'
            ],[
                'negocio' => '1',
                'key' => 'serio'
            ],[
                'negocio' => '1',
                'key' => 'amabilidad'
            ],[
                'negocio' => '2',
                'key' => 'precio_justo'
            ],[
                'negocio' => '2',
                'key' => 'servicio_cordial'
            ],[
                'negocio' => '3',
                'key' => 'a_domicilio'
            ],[
                'negocio' => '3',
                'key' => '24/7'
            ],[
                'negocio' => '5',
                'key' => '24/7'
            ]
        ]);
    }
}
