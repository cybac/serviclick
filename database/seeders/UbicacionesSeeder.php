<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UbicacionesSeeder extends Seeder
{
    public function run()
    {
        DB::table('ubicaciones')->insert([ 
            [
                'nombre'      =>'Tuxtla Gutiérrez, Chiapas',
                'ubicacion'   =>'16.75360420439424, -93.11623418107793'
            ],[
                'nombre'      =>'Tapachula, Chiapas',
                'ubicacion'     =>'14.90651354321853, -92.26341932533684'
            ],[
                'nombre'      =>'Tonalá, Chiapas',
                'ubicacion'     =>'16.092900007150774, -93.75742218942328'
            ],[
                'nombre'      =>'Terán, Tuxtla Gutiérrez, Chiapas',
                'ubicacion'     =>'16.752665476132567, -93.17194845715565'
            ],[
                'nombre'      =>'Cintalapa, Chiapas',
                'ubicacion'     =>'16.686034825297032, -93.72855626814683'
            ]
        ]);
    }
}
