<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SucursalesSeeder extends Seeder
{
    public function run()
    {
        DB::table('sucursales')->insert([ 
            [
                'nombre'        =>'Sucursal #1',
                'direccion'     =>'Tuxtla Gutierrez, Chiapas 13 Nte Ote',
                'telefono'      =>'961 000 00 00',
                'correo'        =>'ejemplo@gmail.com',
            ],[
                'nombre'        =>'Sucursal #2',
                'direccion'     =>'Tapachula, Chiapas 11 Sur 12 Ote',
                'telefono'      =>'962 000 00 01',
                'correo'        =>'chis@gmail.com',
            ],[
                'nombre'        =>'Sucursal #3',
                'direccion'     =>'Tuxtla Gutierrez, Chiapas 1 Sur 1 Pte',
                'telefono'      =>'961 000 00 03',
                'correo'        =>'test@gmail.com',
            ],[
                'nombre'        =>'Sucursal #4',
                'direccion'     =>'Tuxtla Gutierrez, Chiapas 5 Sur Pte',
                'telefono'      =>'961 000 00 04',
                'correo'        =>'mx@gmail.com',
            ],
        ]);
    }
}
