<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComentariosSeeder extends Seeder
{
    public function run()
    {
        DB::table('comentarios')->insert([ 
            [
                'nombre'        =>'Pepe Perez',
                'correo'        =>'pepe@gmail.com',
                'contenido'     =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt repudiandae cumque reprehenderit? Inventore natus temporibus nam ad distinctio earum, laborum necessitatibus molestias officia voluptatum impedit consequatur quisquam? Eos, distinctio amet.',
                'puntuacion'    =>'3.8'
            ],
            [
                'nombre'       =>'Juan Solis',
                'correo'       =>'juan@gmail.com',
                'contenido'    =>'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae, dolorem aliquam! Nesciunt incidunt doloribus saepe iusto placeat minima earum autem neque voluptatem cupiditate nulla magni, non debitis quibusdam tenetur veniam?',
                'puntuacion'    =>'4.8'
            ],
            [
                'nombre'       =>'Maria del sagrado corazon',
                'correo'       =>'masaco@gmail.com',
                'contenido'    =>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid ea, eaque eveniet assumenda voluptatibus a perferendis impedit! At tempora voluptatibus quidem itaque, temporibus numquam eius nobis, dolorum quo ipsam laborum?',
                'puntuacion'    =>'4.5'
            ]
        ]);
    }
}
