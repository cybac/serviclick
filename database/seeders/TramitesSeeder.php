<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TramitesSeeder extends Seeder
{
    public function run()
    {
        DB::table('tramites')->insert([
            ['tramite'  => 'Alta en el sistema'],
            ['tramite'  => 'Información del negocio'],
            ['tramite'  => 'Documentación del negocio'],
            ['tramite'  => 'Primer pago al sistema'],
        ]);
    }
}
