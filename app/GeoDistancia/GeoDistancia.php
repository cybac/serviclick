<?php

namespace App\GeoDistancia;


use Conekta\Conekta;
use App\Models\User;

class GeoDistancia
{
    private const RadioTierraKm = 6378.0;
    protected float $latitud_origen, $longitud_origen;

    function __construct(float $latitud, float $longitud)
    {
        $this->latitud_origen = $latitud;
        $this->longitud_origen = $longitud;
    }

    public function DistanciaKm(float $latitud, float $longitud)
    {
        $difLatitud = $this->EnRadianes($latitud - $this->latitud_origen);
        $difLongitud = $this->EnRadianes($longitud - $this->longitud_origen);
        $a = $this->AlCuadrado(sin($difLatitud / 2)) + cos($this->EnRadianes($this->latitud_origen)) * cos($this->EnRadianes($latitud)) * $this->AlCuadrado(sin($difLongitud / 2));
        $c = (2 * atan2(sqrt($a), sqrt(1 - $a)));
        return self::RadioTierraKm * floatval($c);
    }

    private function AlCuadrado(float $valor)
    {
        return pow($valor, 2);
    }

    private function EnRadianes(float $valor)
    {
        return floatval(pi() / 180) * $valor;
    }
}
