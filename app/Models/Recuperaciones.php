<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recuperaciones extends Model
{
    protected $table = 'recuperaciones';
    protected $fillable = ['usuario','codigo', 'vigencia'];
}
