<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fortalezas extends Model
{
    protected $table = 'fortalezas';

    protected $fillable = [
        'imagen', 'titulo', 'descripcion',
    ];
}
