<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    protected $table = 'comentarios';

    protected $fillable = [
        'negocio', 'nombre', 'correo', 'contenido', 
        'puntuacion', 'nuevo', 'status'
    ];
}
