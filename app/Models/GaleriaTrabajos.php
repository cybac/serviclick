<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaTrabajos extends Model
{
    protected $table = 'galeria_trabajos';

    protected $fillable = ['negocio', 'imagen', 'orden'];
}
