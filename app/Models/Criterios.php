<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Criterios extends Model
{
    protected $table = 'criterios';

    protected $fillable = ['nombre', 'status'];
}
