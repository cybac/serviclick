<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Legales extends Model
{
    protected $table = 'legales';
    protected $fillable = ['titulo','contenido'];
}
