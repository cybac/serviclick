<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipos_Documentos extends Model
{
    protected $table = 'tipos_documentos';
    protected $fillable = ['nombre', 'status'];
}
