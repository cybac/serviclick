<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedesSocialesNegocios extends Model
{
    protected $table = 'redes_sociales_negocios';

    protected $fillable = [
        'negocio','red_social','url'
   ];
}
