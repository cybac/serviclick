<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Seguimiento extends Model
{
    protected $table = 'seguimientos';

    protected $fillable = [
        'negocio', 'tramite', 'comentarios', 'asistente','aprobado', 'status'
    ];
}
