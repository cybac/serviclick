<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SliderFortalezas extends Model
{
    protected $table = 'slider_fortalezas';

    protected $fillable = [
        'inicio', 'remarcado', 'fin', 'status'
    ];

}
