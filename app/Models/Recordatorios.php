<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recordatorios extends Model
{
    protected $table = 'recordatorios';

    protected $fillable = [
        'negocio', 'contenido'
    ];
}
