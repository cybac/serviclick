<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    protected $table = 'notificaciones';
    protected $fillable = ['destinatario', 'notificacion', 'nuevo', 'leido'];
}
