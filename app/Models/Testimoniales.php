<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimoniales extends Model
{
    protected $table = 'testimoniales';

    protected $fillable = [
        'imagen', 'nombre', 'contenido', 'trabajo',
    ];
}
