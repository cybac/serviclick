<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tramites extends Model
{
    protected $table = 'tramites';

    protected $fillable = ['tramite'];
}
