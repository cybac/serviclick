<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alertas extends Model
{
    protected $table = 'alertas';
    protected $fillable = ['titulo', 'contenido', 'importancia', 'icono', 'url','leido'];
}
