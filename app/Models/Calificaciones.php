<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calificaciones extends Model
{
    protected $table = 'calificaciones';

    protected $fillable = ['negocio', 'tipo', 'puntuacion', 'comentario'];
}
