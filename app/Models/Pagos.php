<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    protected $table = 'pagos';

    protected $fillable = [
         'OrderID','OrderDate','OrderUpdateDate','OrderPaymentMethod',
         'OrderPaymentMethodID','OrderTotalItems','OrderTotal', 
         'OrderCurrency','OrderStatus'
    ];
}
