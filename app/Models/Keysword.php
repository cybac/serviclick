<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keysword extends Model
{
    protected $table = 'keyswords';

    protected $fillable = ['negocio', 'key'];
}
