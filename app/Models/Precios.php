<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Precios extends Model
{
    protected $table = 'precios';

    protected $fillable = [
        'nombre', 'costo', 'moneda', 'gratis'
    ];
}
