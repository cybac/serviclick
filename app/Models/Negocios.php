<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Negocios extends Model
{
    protected $table = 'negocios';

    protected $fillable = [
        'imagen', 'nombre', 'direccion', 'telefono', 'web',
        'descripcion', 'servicios', 'precio_min', 'precio_max',
        'categoria', 'ubicacion', 'mapa', 'tramites', 'status',
    ];

    public function Usuario()
    {
        return $this->hasOne(User::class, "negocio");
    }
}
