<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirebaseTokens extends Model
{
    protected $table = "firebase_tokens";
    protected $fillable = ["usuario", "token"];
}
