<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallesPagos extends Model
{
    protected $table = 'detalles_pagos';

    protected $fillable = [
        'pago', 'negocio', 'proximo'
    ];
}
