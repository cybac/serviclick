<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if(Auth::user()->hasRole('administrador'))
                {
                    return redirect(RouteServiceProvider::Admin);
                }else if(Auth::user()->hasRole('negocio')){
                    return redirect(RouteServiceProvider::Negocio);
                }else{
                    return redirect(RouteServiceProvider::Cliente);
                }
            }
        }

        return $next($request);
    }
}
