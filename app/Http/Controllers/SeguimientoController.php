<?php

namespace App\Http\Controllers;

use File;
use App\Models\Pagos;
use App\Models\Alertas;
use App\Models\Negocios;
use App\Models\Categorias;
use App\Models\Documentos;
use App\Models\Seguimiento;
use App\Models\Ubicaciones;
use Illuminate\Http\Request;
use App\Models\DetallesPagos;
use App\Conekta\ConektaPayment;
use Illuminate\Support\Facades\Auth;

class SeguimientoController extends Controller
{
    private $path = "images/negocios/";
    private $path_documentos = "documentos/";
    protected $ConektaPayment;

    function __construct()
    {
        $this->ConektaPayment = new ConektaPayment();
    }

    public function index(Request $request)
    {
        $progreso = Seguimiento::selectRaw("count(*) as total")->where('negocio', Auth::user()->negocio)->where('asistente', true)->get();
        if($progreso[0]->total == 0) {
            //Aun no se ha hecho nada, mostrar logo
            $contenido = view('partials.logo')->render();
            Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 1)->update(['asistente' =>  true]);
        }else{
            $progreso = Seguimiento::where('negocio', Auth::user()->negocio)
                ->where('asistente', true)
                ->get();
            if($progreso[0]->tramite == 1) {
                //guardar logo
                $this->validate($request,[
                    'imagen'        =>'required|mimes:png,jpg,jpeg|max:10240'
                ], [
                    'imagen.required'   =>'Proporcione el logo del negocio',
                    'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
                    'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
                ]);
                if (!file_exists($this->path)) {
                    File::makeDirectory($this->path, $mode = 0777, true, true);
                }
                $archivo = $request->file('imagen');
                $file_name = uniqid().".".$archivo->extension();
                $archivo->move($this->path, $file_name);

                $negocio = Negocios::find(Auth::user()->negocio);
                $negocio->imagen = $file_name;
                $negocio->save();
                //Fin de guardado de logo
                
                $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                    ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                    ->where('negocio', Auth::user()->negocio)
                    ->where('documentos.status', false)
                    ->get();
                $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
                Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 1)->update(['asistente' =>  false]);
                Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);
            }

            if($progreso[0]->tramite == 2)
            {
                //Subir mi información
                $this->validate($request,[
                    'nombre'        =>'required',
                    'ubicacion'     =>'required',
                    'categoria'     =>'required',
                    'direccion'     =>'required',
                    'telefono'      =>'required|digits:10',
                    'descripcion'   =>'required',
                    'servicios'     =>'required',
                    'minimo'        =>'required',
                    'maximo'        =>'required',
                    'mapa'          =>'required'
                ],
                [
                    'nombre.required'           =>'Proporcione el nombre del negocio',
                    'ubicacion.required'        =>'Seleccione una ubicación',
                    'categoria.required'        =>'Seleccione una categoria',
                    'direccion.required'        =>'Proporcione la dirección del negocio',
                    'telefono.required'         =>'Proporcione un número telefonico de contacto',
                    'telefono.digits'           =>'Número telefonico a 10 digitos',
                    'descripcion.required'      =>'Proporcione una descripción del negocio',
                    'servicios.required'        =>'Indique los servicios que proporciona su negocio',
                    'minimo.required'           =>'Indique el precio minimo aproximado',
                    'maximo.required'           =>'Indique el precio minimo aproximado',
                    'mapa.required'             =>'Indique su negocio en el mapa'
                ]);
        
                $negocio = Negocios::find(Auth::user()->negocio);
                $negocio->nombre = $request->nombre;
                $negocio->direccion = $request->direccion;
                $negocio->telefono = $request->telefono;
                $negocio->descripcion = $request->descripcion;
                $negocio->servicios = $request->servicios;
                $negocio->precio_min = $request->minimo;
                $negocio->precio_max = $request->maximo;
                $negocio->categoria = $request->categoria;
                $negocio->ubicacion = $request->ubicacion;
                $negocio->mapa = $request->mapa;
        
                if($request->has('web')){
                    $negocio->web = $request->web;
                }else{
                    $negocio->web = NULL;
                }
                $negocio->save();
        
                $seguimiento = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->first();
                $seguimiento->comentarios = NULL;
                $seguimiento->status = true;
                $seguimiento->save();
                //Fin de subida de información
                $negocio = Negocios::findorfail(Auth::user()->negocio);

                Alertas::create([
                    'titulo'        =>'Captura de información',
                    'contenido'     =>'El negocio '.$negocio->nombre.' capturo la información de su negocio.',
                    'importancia'   =>'bg-info',
                    'icono'         =>'mdi mdi-lead-pencil',
                    'url'           => route('informacion_negocio', Auth::user()->negocio),
                ]);

                $contenido = view('partials.bye')->render();
                Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->update(['asistente' =>  false]);
                Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  true]);
            }

            if($progreso[0]->tramite == 3)
            {
                //Subir documentos
                $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                    ->join('tipos_documentos', 'documentos.tipo', 'tipos_documentos.id')
                    ->where('negocio', Auth::user()->negocio)
                    ->where('documentos.status', false)
                    ->get();

                $i=1;
                $rules = NULL;
                $messages = NULL;
                foreach($documentacion as $item)
                {
                    $rules['documento_'.$i] = 'required|mimes:png,jpg,jpeg,pdf|max:10240';
                    $messages['documento_'.$i.'.required'] = 'El documento '.$item->tipo.' es requerido';
                    $messages['documento_'.$i.'.mimes'] = 'Solo se permiten imagenes PNG o (JPG/JPEG) o documentos PDF';
                    $messages['documento_'.$i.'.max'] = 'El tamaño máximo es de 10MB';
                    $i++;
                }

                $this->validate($request, $rules, $messages);
                $i=1;
                if (!file_exists($this->path_documentos)) {
                    File::makeDirectory($this->path_documentos, $mode = 0777, true, true);
                }
                foreach($documentacion as $item)
                {
                    $archivo = $request->file('documento_'.$i);
                    $file_name = uniqid().".".$archivo->extension();
                    $archivo->move($this->path_documentos, $file_name);
                    //Actualizar el registro de la BD
                    $documento_actual = Documentos::find($documentacion[$i -1]->id);
                    $documento_actual->nombre = $file_name;
                    $documento_actual->status = true;
                    $documento_actual->save();
                    $i++;
                }

                $mis_documentos = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->first();;
                $mis_documentos->status = true;
                $mis_documentos->comentarios = NULL;
                $mis_documentos->save();
                //Fin de subida de documentos

                $negocio = Negocios::findorfail(Auth::user()->negocio);
                Alertas::create([
                    'titulo'        =>'Subida de documentos',
                    'contenido'     =>'El negocio '.$negocio->nombre.' subio su documentación.',
                    'importancia'   =>'bg-success',
                    'icono'         =>'mdi mdi-file-document',
                    'url'           => route('documentos_tramite', Auth::user()->negocio),
                ]);

                //Verificar si mandar a informacion o a fin
                $siguiente = Seguimiento::where('negocio', Auth::user()->negocio)
                    ->where('tramite', 2)
                    ->get();

                if($siguiente[0]->status)
                {
                    $contenido = view('partials.bye')->render();
                    Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  false]);
                    Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  true]);
                }else{
                    $negocio = Negocios::find(Auth::user()->negocio);
                    $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                    $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
                    $contenido = view('partials.informacion',['negocio' => $negocio, 'ubicaciones' => $ubicaciones, 'categorias' => $categorias])->render();
                    Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  false]);
                    Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->update(['asistente' =>  true]);
                }
            }

            if($progreso[0]->tramite == 4)
            {
                //Pendiente de subir
                $validar = Seguimiento::where('negocio', Auth::user()->negocio)
                    ->where('tramite', 3)
                    ->get();

                if(!$validar[0]->status)
                {
                    //Subir documentos
                    $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                        ->join('tipos_documentos', 'documentos.tipo', 'tipos_documentos.id')
                        ->where('negocio', Auth::user()->negocio)
                        ->where('documentos.status', false)
                        ->get();

                    $i=1;
                    foreach($documentacion as $item)
                    {
                        $rules['documento_'.$i] = 'required|mimes:png,jpg,jpeg,pdf|max:10240';
                        $messages['documento_'.$i.'.required'] = 'El documento '.$item->tipo.' es requerido';
                        $messages['documento_'.$i.'.mimes'] = 'Solo se permiten imagenes PNG o (JPG/JPEG) o documentos PDF';
                        $messages['documento_'.$i.'.max'] = 'El tamaño máximo es de 10MB';
                        $i++;
                    }

                    $this->validate($request, $rules, $messages);
                    $i=1;
                    if (!file_exists($this->path_documentos)) {
                        File::makeDirectory($this->path_documentos, $mode = 0777, true, true);
                    }
                    foreach($documentacion as $item)
                    {
                        $archivo = $request->file('documento_'.$i);
                        $file_name = uniqid().".".$archivo->extension();
                        $archivo->move($this->path_documentos, $file_name);
                        //Actualizar el registro de la BD
                        $documento_actual = Documentos::find($documentacion[$i -1]->id);
                        $documento_actual->nombre = $file_name;
                        $documento_actual->status = true;
                        $documento_actual->save();
                        $i++;
                    }

                    $mis_documentos = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->first();;
                    $mis_documentos->status = true;
                    $mis_documentos->comentarios = NULL;
                    $mis_documentos->save();
                    //Fin de subida de documentos
                    $negocio = Negocios::findorfail(Auth::user()->negocio);
                    Alertas::create([
                        'titulo'        =>'Subida de documentos',
                        'contenido'     =>'El negocio '.$negocio->nombre.' subio su documentación pendiente o correciones.',
                        'importancia'   =>'bg-warning',
                        'icono'         =>'mdi mdi-file-document',
                        'url'           => route('documentos_tramite', Auth::user()->negocio),
                    ]);
                    //Verificar si mandar a informacion o a fin
                    $siguiente = Seguimiento::where('negocio', Auth::user()->negocio)
                        ->where('tramite', 2)
                        ->get();

                    if($siguiente[0]->status)
                    {
                        $contenido = view('partials.bye')->render();
                    }else{
                        $negocio = Negocios::find(Auth::user()->negocio);
                        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                        $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
                        $contenido = view('partials.informacion',['negocio' => $negocio, 'ubicaciones' => $ubicaciones, 'categorias' => $categorias])->render();
                    }
                }else{
                    $validar = Seguimiento::where('negocio', Auth::user()->negocio)
                        ->where('tramite', 2)
                        ->get();
                    if(!$validar[0]->status)
                    {
                        //Subir mi información
                        $this->validate($request,[
                            'nombre'        =>'required',
                            'ubicacion'     =>'required',
                            'categoria'     =>'required',
                            'direccion'     =>'required',
                            'telefono'      =>'required|digits:10',
                            'descripcion'   =>'required',
                            'servicios'     =>'required',
                            'minimo'        =>'required',
                            'maximo'        =>'required',
                            'mapa'          =>'required'
                        ],
                        [
                            'nombre.required'       =>'Proporcione el nombre del negocio',
                            'ubicacion.required'    =>'Seleccione una ubicación',
                            'categoria.required'    =>'Seleccione una categoria',
                            'direccion.required'    =>'Proporcione la dirección del negocio',
                            'telefono.required'     =>'Proporcione un número telefonico de contacto',
                            'telefono.digits'       =>'Número telefonico a 10 digitos',
                            'descripcion.required'  =>'Proporcione una descripción del negocio',
                            'servicios.required'    =>'Indique los servicios que proporciona su negocio',
                            'minimo.required'       =>'Indique el precio minimo aproximado',
                            'maximo.required'       =>'Indique el precio minimo aproximado',
                            'mapa.required'         =>'Indique su negocio en el mapa'
                        ]);
                
                        $negocio = Negocios::find(Auth::user()->negocio);
                        $negocio->nombre = $request->nombre;
                        $negocio->direccion = $request->direccion;
                        $negocio->telefono = $request->telefono;
                        $negocio->descripcion = $request->descripcion;
                        $negocio->servicios = $request->servicios;
                        $negocio->precio_min = $request->minimo;
                        $negocio->precio_max = $request->maximo;
                        $negocio->categoria = $request->categoria;
                        $negocio->ubicacion = $request->ubicacion;
                        $negocio->mapa = $request->mapa;
                
                        if($request->has('web')){
                            $negocio->web = $request->web;
                        }else{
                            $negocio->web = NULL;
                        }
                        $negocio->save();
                
                        $seguimiento = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->first();
                        $seguimiento->comentarios = NULL;
                        $seguimiento->status = true;
                        $seguimiento->save();
                        //Fin de subida de información
                        $negocio = Negocios::findorfail(Auth::user()->negocio);

                        Alertas::create([
                            'titulo'        =>'Correción de información',
                            'contenido'     =>'El negocio '.$negocio->nombre.' capturo la información de su negocio.',
                            'importancia'   =>'bg-warning',
                            'icono'         =>'mdi mdi-lead-pencil',
                            'url'           => route('informacion_negocio', Auth::user()->negocio),
                        ]);

                        $contenido = view('partials.bye')->render();
                    }else{
                        $contenido = view('partials.bye')->render();
                    }
                }
            }
        }
        
        return response()->json([
            'status'        => true,
            'contenido'     => $contenido
        ], 200);
    }

    public function omitir()
    {
        $progreso = Seguimiento::where('negocio', Auth::user()->negocio)
            ->where('asistente', true)
            ->get();
            
        if($progreso[0]->tramite == 1)
        {
            $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                ->where('negocio', Auth::user()->negocio)
                ->where('documentos.status', false)
                ->get();
            $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
            Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 1)->update(['asistente' =>  false]);
            Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);
        }

        if($progreso[0]->tramite == 3)
        {
            $negocio = Negocios::find(Auth::user()->negocio);
            $ubicaciones = Ubicaciones::orderBy('nombre')->get();
            $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
            $contenido = view('partials.informacion',['negocio' => $negocio, 'ubicaciones' => $ubicaciones, 'categorias' => $categorias])->render();
            Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  false]);
            Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->update(['asistente' =>  true]);
        }

        return response()->json([
            'status'        => true,
            'contenido'     => $contenido,
        ], 200);
    }
    

    public function informacion()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        if($negocio->tramites)
        {
            $progreso = Seguimiento::where('negocio', Auth::user()->negocio)->get();
            $ubicaciones = Ubicaciones::orderBy('nombre')->get();
            $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
            $info = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->first();
            return view('negocio.informacion', [
                'progreso'      => $progreso,
                'negocio'       => $negocio,
                'ubicaciones'   => $ubicaciones,
                'categorias'    => $categorias,
                'info_progreso' => $info,
                'tramitando'    => $negocio
            ]);
        }else{
            return redirect()->route('panel_control');
        }
    }

    public function actualizar_negocio(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'ubicacion'     =>'required',
            'categoria'     =>'required',
            'direccion'     =>'required',
            'telefono'      =>'required',
            'descripcion'   =>'required',
            'servicios'     =>'required',
            'minimo'        =>'required',
            'maximo'        =>'required',
            'mapa'          =>'required'
        ],
        [
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'ubicacion.required'     =>'Seleccione una ubicación',
            'categoria.required'     =>'Seleccione una categoria',
            'direccion.required'     =>'Proporcione la dirección del negocio',
            'telefono.required'      =>'Proporcione un número telefonico de contacto',
            'descripcion.required'   =>'Proporcione una descripción del negocio',
            'servicios.required'     =>'Indique los servicios que proporciona su negocio',
            'minimo.required'        =>'Indique el precio minimo aproximado',
            'maximo.required'        =>'Indique el precio minimo aproximado',
            'mapa.required'          =>'Indique su negocio en el mapa'
        ]);

        $negocio = Negocios::find(Auth::user()->negocio);
        $negocio->nombre = $request->nombre;
        $negocio->direccion = $request->direccion;
        $negocio->telefono = $request->telefono;
        $negocio->descripcion = $request->descripcion;
        $negocio->servicios = $request->servicios;
        $negocio->precio_min = $request->minimo;
        $negocio->precio_max = $request->maximo;
        $negocio->categoria = $request->categoria;
        $negocio->ubicacion = $request->ubicacion;
        $negocio->mapa = $request->mapa;

        if($request->has('web')){
            $negocio->web = $request->web;
        }else{
            $negocio->web = NULL;
        }
        $negocio->save();

        $seguimiento = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->first();
        $seguimiento->comentarios = NULL;
        $seguimiento->status = true;
        $seguimiento->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han enviado los datos del negocio a verificación',
            'load'  =>  true,
            'url'   =>  route('panel_control')
        ], 200);
    }

    public function logo()
    {
        $progreso = Seguimiento::where('negocio', Auth::user()->negocio)->get();
        $negocio = Negocios::find(Auth::user()->negocio);
        if($negocio->tramites)
        {
            if($negocio->imagen == "0")
            {
                return view('negocio.logo',[
                    'progreso'  => $progreso,
                    'form_edit'     =>false,
                    'tramitando'    => $negocio->tramites
                ]);
            }else{
                return view('negocio.logo',[
                    'progreso'      => $progreso,
                    'form_edit'     => true,
                    'negocio'       => $negocio,
                    'tramitando'    => $negocio->tramites
                ]);
            }
        }else{
            return redirect()->route("panel_control");
        }
    }

    public function guardar_logo(Request $request)
    {
        $this->validate($request,[
            'imagen'        =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'imagen.required'   =>'Proporcione el logo del negocio',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $archivo = $request->file('imagen');
        $file_name = uniqid().".".$archivo->extension();
        $archivo->move($this->path, $file_name);

        $negocio = Negocios::find(Auth::user()->negocio);
        $negocio->imagen = $file_name;
        $negocio->save();
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha subido el logo del negocio correctamente',
            'load'  =>true,
            'url'   =>route('panel_control')
        ], 200);
    }

    public function actualizar_logo(Request $request)
    {
        $this->validate($request,[
            'imagen'        =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'imagen.required'   =>'Proporcione el logo del negocio',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $archivo = $request->file('imagen');
        $file_name = uniqid().".".$archivo->extension();
        $archivo->move($this->path, $file_name);

        $negocio = Negocios::find(Auth::user()->negocio);
        File::delete($this->path.$negocio->imagen);
        $negocio->imagen = $file_name;
        $negocio->save();
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha actualizado el logo del negocio correctamente',
            'load'  =>true,
            'url'   =>route('panel_control')
        ], 200);
    }

    public function documentos()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        if($negocio->tramites)
        {
            $progreso = Seguimiento::where('negocio', Auth::user()->negocio)->get();
            $pendiente = Documentos::where('negocio', Auth::user()->negocio)->where('status', false)->get();
            $seguimiento = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->first();
            $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                ->where('negocio', Auth::user()->negocio)
                ->where('documentos.status', false)
                ->get();
            return view('negocio.documentos', [
                'progreso'      => $progreso,
                'documentos'    => $documentacion,
                'pendientes'    => $pendiente->count(),
                'tramite'       => $seguimiento->aprobado,
                'tramitando'    => $negocio->tramites
            ]);
        }else{
            return redirect()->route('panel_control');
        }
    }

    public function subir_documentacion(Request $request)
    {
        $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
            ->join('tipos_documentos', 'documentos.tipo', 'tipos_documentos.id')
            ->where('negocio', Auth::user()->negocio)
            ->where('documentos.status', false)
            ->get();

        $i=1;
        foreach($documentacion as $item)
        {
            $rules['documento_'.$i] = 'required|mimes:png,jpg,jpeg,pdf|max:10240';
            $messages['documento_'.$i.'.required'] = 'El documento '.$item->tipo.' es requerido';
            $messages['documento_'.$i.'.mimes'] = 'Solo se permiten imagenes PNG o (JPG/JPEG) o documentos PDF';
            $messages['documento_'.$i.'.max'] = 'El tamaño máximo es de 10MB';
            $i++;
        }

        $this->validate($request, $rules, $messages);
        $i=1;
        if (!file_exists($this->path_documentos)) {
            File::makeDirectory($this->path_documentos, $mode = 0777, true, true);
        }
        foreach($documentacion as $item)
        {
            $archivo = $request->file('documento_'.$i);
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path_documentos, $file_name);
            //Actualizar el registro de la BD
            $documento_actual = Documentos::find($documentacion[$i -1]->id);
            $documento_actual->nombre = $file_name;
            $documento_actual->status = true;
            $documento_actual->save();
            $i++;
        }

        $mis_documentos = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->first();;
        $mis_documentos->status = true;
        $mis_documentos->comentarios = NULL;
        $mis_documentos->save();

        return response()->json([
            'status'=> 'true',
            'type'  => 'success',
            'title' => 'Éxito',
            'text'  => 'Se ha enviado la documentación del negocio a verificación',
            'load'  => true,
            'url'   => route('panel_control')
        ], 200);
    }

    public function pago()
    {
        $ver_pago = Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->first();
        if($ver_pago->status)
        {
            $negocio = Negocios::find(Auth::user()->negocio);
            if($negocio->tramites)
            {
                setlocale(LC_ALL, 'es_ES');
                $progreso = Seguimiento::where('negocio', Auth::user()->negocio)->get();
                $detalle_pago = DetallesPagos::where('negocio', Auth::user()->negocio)->orderby('created_at','DESC')->take(1)->get();
                $id_pago = Pagos::where('OrderID', $detalle_pago[0]->pago)->first();
                if($id_pago->OrderStatus =='expired')
                {
                    return view('negocio.pago', [
                        'expiro'        => true,
                        'progreso'      => $progreso,
                        'tramitando'    => $negocio->tramites
                    ]);
                }else{
                    $pago = $this->ConektaPayment->ObtenerOrden($id_pago->OrderPaymentMethodID);
                    return view('negocio.pago', [
                        'expiro'        => false,
                        'progreso'      => $progreso,
                        'monto'         => $pago->charges[0]->amount,
                        'moneda'        => $pago->charges[0]->currency,
                        'referencia'    => $pago->charges[0]->payment_method->reference,
                        'tramitando'    => $negocio->tramites
                    ]);
                }
                return $id_pago;

            }else{
                return redirect()->route('panel_control');
            }
        }else{
            return redirect()->route('panel_control');
        }
    }
}
