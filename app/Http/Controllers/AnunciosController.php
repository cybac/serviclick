<?php

namespace App\Http\Controllers;

use File;
use App\Models\Anuncios;
use Illuminate\Http\Request;

class AnunciosController extends Controller
{
    private $path = "images/anuncio/";

    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'imagen'    =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'imagen.required'   =>'Proporcione una imagen',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);

        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }

        //Guarda la nueva imagen
        $img = $request->file('imagen');
        $file_name = uniqid().".".$img->extension();
        $img->move($this->path, $file_name);

        $anuncio = Anuncios::findorfail(1);
        File::delete($this->path.$anuncio->imagen);     //Eliminar imagen anterior
        $anuncio->imagen = $file_name;
        $anuncio->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado el anuncio',
            'load'  =>  true,
            'url'   =>  route('control_sc_anuncios')
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del anuncio',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        Anuncios::where('id', $request->id)->update(['status' => $request->status]);

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
