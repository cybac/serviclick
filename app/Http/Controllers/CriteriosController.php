<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Criterios;
use App\Models\Comentarios;
use Illuminate\Http\Request;
use App\Models\Calificaciones;
use Illuminate\Support\Facades\Auth;

class CriteriosController extends Controller
{
    public function datatable(Request $request)
    {
        $criterios = Criterios::all();
        return DataTables::of($criterios)
        ->addColumn('status', function($criterios){
            if($criterios->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_criterio($criterios->id);' class='switch' id='status_".$criterios->id."'>
                    <label for='status_".$criterios->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_criterio($criterios->id);' class='switch' id='status_".$criterios->id."'>
                    <label for='status_".$criterios->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($criterios){
            return "<button class='btn btn-success p-3 input-text w-100 mb-2' data-value='".$criterios->nombre."' data-url='".route('actualizar_criterio', $criterios->id)."' data-title='Editar Criterio de evaluación' data-subtitle='Nuevo nombre para el criterio de evaluación'>Editar</button>
            <button class='btn btn-danger delete p-3 w-100 mb-2' data-url='".route('eliminar_criterio',$criterios->id)."' style='margin-bottom: 10px;'>Eliminar</button>";
        })
        ->rawColumns(['status','btn'])
        ->toJson();
    }

    public function guardar(Request $request)
    {
        $this->validate($request,['nombre'    =>'required'],['nombre.required'   => 'Proporcione el nombre']);

        $criterio = Criterios::create([
            'nombre'    => $request->nombre
        ]);

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha guardado el criterio de evaluación',
            'dttable'   =>  "#dt_criterios",
        ], 200);
    }

    public function actualizar(Request $request, $id_criterio)
    {
        
        $this->validate($request,[
            'nombre'    =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el nombre'
        ]);
            
        $criterio = Criterios::find($id_criterio);
        $criterio->nombre = $request->nombre;
        $criterio->save();

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha actualizado el criterio de evaluación',
            'dttable'   =>  "#dt_criterios",
        ], 200);
    }

    public function eliminar($id_criterio)
    {
        $criterio = Criterios::find($id_criterio);
        $criterio->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el criterio de evaluación',
            'dttable'   =>  "#dt_criterios",
        ], 200);
    }
    
    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del criterio',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $criterio = Criterios::find($request->id);
        $criterio->status = $request->status;
        $criterio->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }

    public function comentar(Request $request)
    {
        $request->validate([
            'negocio' => 'required',
            'comentario' => 'required|string'
        ],
        [
            'negocio.required'  =>  'El Id del Negocio es requerido',
            'comentario.required'   =>  'Proporcione un comentario u observación',
        ]);
        $aux=1;
        $usuario = Auth::user();        //Contiene los datos del cliente que esta comentando
        $comentario = Comentarios::create([
            'nombre'        => $usuario->name,
            'correo'        => $usuario->email,
            'contenido'     => $request->comentario,
            'puntuacion'    => '0'
        ]);
        $calificar = Criterios::where('status', true)->orderBy('nombre')->get();
        $puntuaciones = $request->except('negocio','comentario');
        $calificacion = 0;
        foreach ($calificar as $item)
        {
            $calificacion = $calificacion + $puntuaciones['S'.$aux];
            Calificaciones::create([
                'negocio'       => $request->negocio,
                'tipo'          => $item->nombre,
                'puntuacion'    => $puntuaciones['S'.$aux],
                'comentario'    => $comentario->id
            ]);
            $aux++;
        }
        if($calificar->count() > 0)
        {
            $calificacion = $calificacion / $calificar->count();
        }
        $comentario->puntuacion = $calificacion;
        $comentario->save();
        return redirect()->route('details_id', $request->negocio)->with('resultado', 'Su reseña ha sido enviado, gracias por su comentario');
    }
}
