<?php

namespace App\Http\Controllers;

use App\Models\Alertas;
use Illuminate\Http\Request;

class AlertasController extends Controller
{
    public function index()
    {
        $anteriores_alertas = Alertas::where('leido', true)->get();
        return view('admin.alertas', [
            'anteriores_alertas'    => $anteriores_alertas
        ]);
    }

    public function alertas($id_alerta)
    {
        $alerta = ALertas::findorfail($id_alerta);
        $alerta->leido = true;
        $alerta->save();
        return redirect($alerta->url);
    }
}
