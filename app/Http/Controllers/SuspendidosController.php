<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\Pagos;
use App\Models\Negocios;
use Illuminate\Http\Request;
use App\Models\DetallesPagos;

class SuspendidosController extends Controller
{
    public function index()
    {
        return view('admin.negocios_suspendidos');
    }

    public function datatable(Request $request)
    {
        $suspendidos = Negocios::where('tramites', 0)->where('status', 0)->get();

        return DataTables::of($suspendidos)
        ->addColumn('completados', function($suspendidos){
            $pagadas = Pagos::selectRaw("COUNT(*) as pagados")
                ->whereIn('orderID', DetallesPagos::select('pago')->where('negocio', $suspendidos->id)->get())
                ->where('OrderStatus', "paid")
                ->get();
            return $pagadas[0]->pagados;
        })
        ->addColumn('expirados', function($suspendidos){
            $expirado = Pagos::selectRaw("COUNT(*) as expiro")
                ->whereIn('orderID', DetallesPagos::select('pago')->where('negocio', $suspendidos->id)->get())
                ->where('OrderStatus', "expired")
                ->get();
            return $expirado[0]->expiro;
        })
        ->addColumn('ultima', function($suspendidos){
            $ultimo = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", $suspendidos->id)->first()->id)->first();
            if(!is_null($ultimo))
            {
                switch($ultimo->OrderStatus)
                {
                    case "paid":
                        $status = "Pagada";
                        break;
                    case "expired":
                        $status = "Expiró";
                        break;
                    case "pending_payment":
                        $status = "Pendiente de pago";
                        break;
                    default: $status = "Desconocido";
                }
            }else{
                $status = "Ninguna";
            }
            return $status;
        })
        ->addColumn('fecha_ultima', function($suspendidos){
            $ultimo = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", $suspendidos->id)->first()->id)->first();
            if(!is_null($ultimo))
            {
                if(is_null($ultimo->OrderUpdateDate))
                {
                    return "Esperando pago";
                }
                return $ultimo->OrderUpdateDate;
            }else{
                return "Periodo Gratuito";
            }
        })
        ->addColumn('btn', function($suspendidos){

            $ultimo = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", $suspendidos->id)->first()->id)->first();
            if(!is_null($ultimo))
            {
                switch($ultimo->OrderStatus)
                {
                    case "paid":
                        $boton = "<span>Negocio Suspendido por un administrador<span>";
                        break;
                    case "expired":
                        $boton = "<button class='btn btn-info text-white renovar-ficha p-3 w-100 mb-2' data-url='".route('renovar_ficha_admin')."' data-id='".$suspendidos->id."'>Nueva Ficha</button>";
                        break;
                    case "pending_payment":
                        $boton = "<span>Esperando el pago</span>";
                        break;
                    default: $boton = "<span>Desconocido</span>";
                }
            }else{
                $boton = "<button class='btn btn-success text-white p-3 w-100 mb-2 quitar_suspension' data-url='".route('status_negocio')."' data-dttable='#dt_negocios_suspendidos' data-id='".$suspendidos->id."'>Quitar Suspensión</button>";
            }
            return $boton;
        })
        ->rawColumns(['completados', 'expirados', 'ultima', 'fecha_ultima', 'btn'])
        ->toJson();
    }
}
