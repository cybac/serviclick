<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Contacto;
use App\Models\Negocios;
use App\Models\Criterios;
use App\Models\Categorias;
use App\Models\Comentarios;
use App\Models\Ubicaciones;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\RedesSociales;
use App\Models\Calificaciones;
use App\Models\GaleriaTrabajos;
use App\Models\SliderFortalezas;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class NegociosController extends Controller
{
    private $path_negocios = "images/negocios/";

    public function index()
    {
        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
        $categorias = Categorias::where('id', '>', '1')->get();
        $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
            ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
            ->join('categorias', 'negocios.categoria', 'categorias.id')
            ->where('status', true)
            ->paginate(6);
        $total = Negocios::where('status', true)->get();
        $inicial = Ubicaciones::find(1);
        return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
    }
    
    public function volver()
    {
        return redirect()->route('directory');
    }


    public function busqueda(Request $request)
    {
        //determinar la url a mostrar
        if (is_numeric($request->categoria))
        {
            if(Str::length($request->ubicacion) > 0)
            {
                if(Str::length($request->keysword) > 0)
                {
                    //Buscar con los 3 parametros (Buscar con todo)
                    session(['keysword' => $request->keysword]);
                    return redirect()->route('directory_all_criterios', ['id_categoria' => $request->categoria, 'ubicacion' => $request->ubicacion]);
                }else{
                    //Buscar categoria y ubicacion
                    session()->forget('keysword');
                    return redirect()->route('directory_categoria_y_ubicacion', ['id_categoria' => $request->categoria, 'ubicacion' => $request->ubicacion]);
                }
            }else if(Str::length($request->keysword) > 0)
            {
                //Buscar categoria y keysword
                session(['keysword' => $request->keysword]);
                return redirect()->route('directory_categoria_y_keysword', $request->categoria);
            }else{
                //Buscar categoria
                session()->forget('keysword');
                return redirect()->route('directory_categoria', $request->categoria);
            }
        }else if(Str::length($request->ubicacion) > 0)
        {
            if(Str::length($request->keysword) > 0)
            {
                //Buscar ubicacion y keysword;
                session(['keysword' => $request->keysword]);
                return redirect()->route('directory_ubicacion_y_keysword', $request->ubicacion);
            }else{
                //Buscar ubicacion
                session()->forget('keysword');
                return redirect()->route('directory_ubicacion', $request->ubicacion);
            }
        }else if(Str::length($request->keysword) > 0)
        {
            //Buscar keysword
            session(['keysword' => $request->keysword]);
            return redirect()->route('directory_keysword');
        }else{
            return back();
        }
    }

    public function categorias()
    {
        $frases = SliderFortalezas::where('status', '1')->get();
        $categorias = Categorias::where('id', '>', '2')->orderby('nombre')->get();
        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
        $look_categorias = Categorias::whereIn('id', Negocios::selectRaw("DISTINCT categoria")->where("status", true)->get())->orderby('nombre')->get();
        $contacto = Contacto::all();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        return view('categorias', compact('frases', 'ubicaciones', 'categorias', 'look_categorias', 'contacto', 'sociales'));
    }

    public function categoria($id_categoria)
    {
        if(is_numeric($id_categoria))
        {
            $existe = Categorias::find($id_categoria);
            if(!empty($existe) && $id_categoria > 2)
            {
                $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                $categorias = Categorias::where('id', '>', '1')->get();
                $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                    ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                    ->join('categorias', 'negocios.categoria', '=', 'categorias.id')
                    ->where('negocios.categoria', $id_categoria)
                    ->where('status', true)
                    ->paginate(6);
                $total = $negocios;
                $inicial = Ubicaciones::find(1);
                return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
            }else{
                return redirect()->route('directory');
            }
        }else{
            return redirect()->route('directory');
        }
    }

    public function location($ubicacion)
    {
        $existe = Ubicaciones::where('nombre', $ubicacion)->first();
        if(!empty($existe))
        {
            $ubicaciones = Ubicaciones::orderBy('nombre')->get();
            $categorias = Categorias::where('id', '>', '1')->get();
            $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                ->join('categorias', 'negocios.categoria', 'categorias.id')
                ->where('negocios.ubicacion', $existe->id)
                ->where('status', true)
                ->paginate(6);
            $total = $negocios;
            $inicial = Ubicaciones::find($existe->id);
            return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
        }else{
            return redirect()->route('directory');
        }
    }

    public function keysword()
    {
        if(session()->has('keysword'))
        {
            //Construir la consulta
            $keys = explode(" ", session('keysword'));
            $query = "SELECT DISTINCT(negocio) FROM keyswords WHERE `key` = '".$keys[0]."'";
            if(count($keys) > 1)
            {
                for($i=1; $i<count($keys);$i++)
                {
                    $query .= " OR `key` = '".$keys[$i]."' ";
                }
            }
            $query .= ";";
            //Obtener los negocios que tienes dichas keys
            $resultado = DB::select($query);
            $search = [];
            for($i=0; $i < count($resultado); $i++)
            {
                array_push($search, $resultado[$i]->negocio);
            }

            $ubicaciones = Ubicaciones::orderBy('nombre')->get();
            $categorias = Categorias::where('id', '>', '1')->get();
            $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                ->join('categorias', 'negocios.categoria', 'categorias.id')
                ->whereIn('negocios.id', $search)
                ->where('status', true)
                ->paginate(6);
            $total = $negocios;
            $inicial = Ubicaciones::find(1);
            return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
        }else{
            //Sin keys previas
            return redirect()->route('directory');
        }
    }

    public function categoria_y_ubicacion($id_categoria, $ubicacion)
    {
        $existe = Ubicaciones::where('nombre', $ubicacion)->first();
        if(!empty($existe))
        {
            if(is_numeric($id_categoria)){
                $existecat = Categorias::find($id_categoria);
                if(!empty($existecat) && $id_categoria > 1){
                    $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                    $categorias = Categorias::where('id', '>', '1')->get();
                    if($id_categoria > 2)
                    {
                        $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                            ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                            ->join('categorias', 'negocios.categoria', 'categorias.id')
                            ->where('negocios.categoria', $id_categoria)
                            ->where('negocios.ubicacion', $existe->id)
                            ->where('status', true)
                            ->paginate(6);
                        $total = $negocios;
                        $inicial = Ubicaciones::find($existe->id);
                        return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
                    }else{
                        return redirect()->route('directory_ubicacion', $ubicacion);
                    }
                }else{
                    return redirect()->route('directory_ubicacion', $ubicacion);
                }
            }else{
                return redirect()->route('directory_ubicacion', $ubicacion);
            }
        }else{
            if(is_numeric($id_categoria)){
                $existecat = Categorias::find($id_categoria);
                if(!empty($existecat) && $id_categoria > 2)
                {
                    return redirect()->route('directory_categoria', $id_categoria);
                }else{
                    return redirect()->route('directory');
                }
            }else{
                return redirect()->route('directory');
            }
        }
    }

    public function categoria_y_keysword($id_categoria)
    {
        if(is_numeric($id_categoria))
        {
            $existe = Categorias::find($id_categoria);
            if(!empty($existe) && $id_categoria > 2)
            {
                if(session()->has('keysword'))
                {
                    //Tiene categoria valida y tiene keys
                    //Construir la consulta para las keys
                    $keys = explode(" ", session('keysword'));
                    $query = "SELECT DISTINCT(negocio) FROM keyswords WHERE `key` = '".$keys[0]."'";
                    if(count($keys) > 1)
                    {
                        for($i=1; $i<count($keys);$i++)
                        {
                            $query .= " OR `key` = '".$keys[$i]."' ";
                        }
                    }
                    $query .= ";";
                    //Obtener los negocios que tienes dichas keys
                    $resultado = DB::select($query);
                    $search = [];
                    for($i=0; $i < count($resultado); $i++)
                    {
                        array_push($search, $resultado[$i]->negocio);
                    }
                    $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                    $categorias = Categorias::where('id', '>', '1')->get();
                    $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                        ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                        ->join('categorias', 'negocios.categoria', '=', 'categorias.id')
                        ->where('negocios.categoria', $id_categoria)
                        ->whereIn('negocios.id', $search)
                        ->where('status', true)
                        ->paginate(6);
                    $total = $negocios;
                    $inicial = Ubicaciones::find(1);
                    return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
                }else{
                    //Sin keys previas, pero con categoria valida
                    return redirect()->route('directory_categoria', $id_categoria);
                }
            }else{
                if(session()->has('keysword'))
                {
                //Tiene keys
                    return redirect()->route('directory_keysword');
                }else{
                    //Sin keys previas, sin categoria valida
                    return redirect()->route('directory');
                }
            }
        }else{
            if(session()->has('keysword'))
            {
                //Sin categorias pero tiene keys
                return redirect()->route('directory_keysword');
            }else{
                //Sin categorias y sin keys previas
                return redirect()->route('directory');
            }
        }
    }

    public function all_criterios($id_categoria, $ubicacion)
    {
        $existe = Ubicaciones::where('nombre', $ubicacion)->first();
        if(!empty($existe))
        {
            //Existe la ubicacion
            if(is_numeric($id_categoria)){
                $existecat = Categorias::find($id_categoria);
                if(!empty($existecat) && $id_categoria > 1){
                    //Existe la categoria
                    if(session()->has('keysword')){
                        //Existen las 3 opciones
                        $keys = explode(" ", session('keysword'));
                        $query = "SELECT DISTINCT(negocio) FROM keyswords WHERE `key` = '".$keys[0]."'";
                        if(count($keys) > 1)
                        {
                            for($i=1; $i<count($keys);$i++)
                            {
                                $query .= " OR `key` = '".$keys[$i]."' ";
                            }
                        }
                        $query .= ";";
                        //Obtener los negocios que tienes dichas keys
                        $resultado = DB::select($query);
                        $search = [];
                        for($i=0; $i < count($resultado); $i++)
                        {
                            array_push($search, $resultado[$i]->negocio);
                        }
                        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                        $categorias = Categorias::where('id', '>', '1')->get();
                        if($id_categoria > 2) {
                            $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                                ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                                ->join('categorias', 'negocios.categoria', '=', 'categorias.id')
                                ->where('negocios.categoria', $id_categoria)
                                ->where('negocios.ubicacion', $existe->id)
                                ->whereIn('negocios.id', $search)
                                ->where('status', true)
                                ->paginate(6);
                            $total = $negocios;
                            $inicial = Ubicaciones::find($existe->id);
                            return view('directory', compact('categorias', 'negocios', 'total', 'ubicaciones', 'inicial'));
                        }else{
                            //Todas las categorias, buscar por ubicacion y keys
                            return redirect()->route('directory_ubicacion_y_keysword', $ubicacion);
                        }
                    }else{
                        //Existe la ubicacion y la categoria, pero no las keysword
                        return redirect()->route('directory_categoria_y_ubicacion', ['id_categoria' => $id_categoria, 'ubicacion' => $ubicacion]);
                    }
                }else{
                    //Existe la ubicacion, pero no la categoria
                    if(session()->has('keysword')){
                        //Existe la ubicacion y las keysword, pero no la categoria
                        return redirect()->route('directory_ubicacion_y_keysword', $ubicacion);
                    }else{
                        //Existe la ubicacion, pero no las categorias ni las keysword
                        return redirect()->route('directory_ubicacion', $ubicacion);
                    }
                }
            }else{
                //Existe la ubicacion, pero no la categoria
                if(session()->has('keysword')){
                    //Existe la ubicacion y las keysword, pero no la categoria
                    return redirect()->route('directory_ubicacion_y_keysword', $ubicacion);
                }else{
                    //Existe la ubicacion, pero no las categorias ni las keysword
                    return redirect()->route('directory_ubicacion', $ubicacion);
                }
            }
        }else{
            //No existe la ubicacion
            if(is_numeric($id_categoria)){
                $existecat = Categorias::find($id_categoria);
                if(!empty($existecat) && $id_categoria > 1){
                    //Existe la categoria, pero no la ubicacion
                    if(session()->has('keysword')){
                        //Existe la categoria y las keysword, pero no la ubicacion
                        return redirect()->route('directory_categoria_y_keysword', $id_categoria);
                    }else{
                        //Existe la categoria, pero no la ubicacion ni las keysword
                        return redirect()->route('directory_categoria', $id_categoria);
                    }
                }else{
                    //No existe la ubicacion ni la categoria
                    if(session()->has('keysword')){
                        //Existe las keysword, pero no la ubicacion ni las categoria
                        return redirect()->route('directory_keysword');
                    }else{
                        //No existe nada
                        return redirect()->route('directory');
                    }
                }
            }else{
                //No existe la ubicacion ni la categoria
                if(session()->has('keysword')){
                    //Existe las keysword, pero no la ubicacion ni las categoria
                    return redirect()->route('directory_keysword');
                }else{
                    //No existe nada
                    return redirect()->route('directory');
                }
            }
        }
    }

    public function loc_key($ubicacion)
    {
        $existe = Ubicaciones::where('nombre', $ubicacion)->first();
        if(!empty($existe))
        {
            if(session()->has('keysword'))
            {
                //Busqueda por ubicacion y keys
                $keys = explode(" ", session('keysword'));
                $query = "SELECT DISTINCT(negocio) FROM keyswords WHERE `key` = '".$keys[0]."'";
                if(count($keys) > 1)
                {
                    for($i=1; $i<count($keys);$i++)
                    {
                        $query .= " OR `key` = '".$keys[$i]."' ";
                    }
                }
                $query = "(".$query.");";
                $query ="SELECT negocios.id FROM negocios WHERE ubicacion = ".$existe->id." AND negocios.id in".$query;
                $resultado = DB::select($query);
                $search = [];
                for($i=0; $i < count($resultado); $i++)
                {
                    array_push($search, $resultado[$i]->id);
                }
                $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                $categorias = Categorias::where('id', '>', '1')->get();
                $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                    ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                    ->join('categorias', 'negocios.categoria', '=', 'categorias.id')
                    ->whereIn('negocios.id', $search)
                    ->where('status', true)
                    ->paginate(6);
                $total = $negocios;
                $inicial = Ubicaciones::find($existe->id);
                return view('directory', compact('categorias', 'negocios', 'total' , 'ubicaciones', 'inicial'));
            }else{
                return redirect()->route('directory_ubicacion', $ubicacion);
            }
        }else{
            if(session()->has('keysword'))
            {
                return redirect()->route('directory_keysword');
            }else{
                return redirect()->route('directory');
            }
        }
    }

    public function detalles($id)
    {
        $negocio = Negocios::findorfail($id);
        if($negocio->status) {
            $galeria = GaleriaTrabajos::where('negocio', $id)->orderby('orden')->get();
            $categoria = Categorias::findorfail($negocio->categoria);
            $puntuacion = Calificaciones::selectRaw("CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion")->where("negocio", $id)->first();
            $valoracion = $puntuacion->puntuacion;
            $relacionados = Negocios::select('negocios.id', 'negocios.nombre', 'negocios.direccion', 'negocios.web', 'negocios.telefono', 'negocios.descripcion', 'negocios.categoria as catID', 'categorias.nombre as categoria', 'negocios.mapa')
                ->selectRaw("CASE WHEN negocios.imagen != '0' THEN negocios.imagen ELSE '/0.png' END as imagen")
                ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
                ->join('categorias', 'negocios.categoria', '=', 'categorias.id')
                ->where([['negocios.id', '!=', $id], ['categoria', '=', $negocio->categoria], ['ubicacion', '=', $negocio->ubicacion], ['status', '=', true]])
                ->paginate(6);
            $comentarios = Calificaciones::select('comentario')->where('negocio', $id)->groupBy('comentario')->get();
            if(count($comentarios) > 0) {
                $comentarios = Comentarios::whereIn('id', $comentarios)->where('status', true)->get();
            }
            $criterios = Criterios::where('status', true)->orderBy('nombre')->get();
            $contacto = Contacto::all();
            $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
            return view('details', compact('negocio', 'galeria', 'comentarios', 'valoracion', 'categoria', 'relacionados', 'criterios','contacto', 'sociales'));
        }else{
            return redirect()->route('directory');
        }
    }

    public function ubicacion(Request $request)
    {
        $this->validate($request,['negocio' => 'required'],['negocio.required' => 'El ID del negocio es requerido']);
        $negocio = Negocios::findorfail($request->negocio);
        return $negocio->mapa;
    }

    public function autoacceso($key)
    {
        $usuario = User::where('token_acceso', $key)->first();
        if(!is_null($usuario)) {
            Auth::loginUsingId($usuario->id);
            $usuario->token_acceso = null;
            $usuario->save();
            return redirect()->route('panel_control');
        }else{
            $request = new Request();
            $this->validate($request,[
                'acceso'                => 'required',
                'email'                 => 'required',
                'password'              => 'required',
            ], [
                'acceso.required'       =>'El logueo automatico fallo',
                'email.required'        =>'Proporcione el correo con el que se dio de alta en la App',
                'password.required'     =>'Proporcione la contraseña con la que registro en la App',
            ]);
            return redirect()->route('home')->withErrors($validator);
        }
    }
}
