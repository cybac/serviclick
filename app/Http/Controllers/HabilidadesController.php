<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Habilidades;
use Illuminate\Http\Request;

class HabilidadesController extends Controller
{
    public function datatable(Request $request)
    {
        $habilidades = Habilidades::all();
        return DataTables::of($habilidades)
        ->addColumn('btn', function($habilidades){
            return "<a class='btn btn-success text-white p-3 w-100 mb-2' href='".route('editar_habilidad',$habilidades->id)."'>Editar</a>
            <button class='btn btn-danger delete p-3 w-100 mb-2' data-url='".route('eliminar_habilidad',$habilidades->id)."'>Eliminar</button>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.habilidades',[
            'form_edit'     =>false,
            'menu'          =>'web',
            'submenu'       =>'habilidades'
        ]);
    }

    public function editar($id_habilidad)
    {
        $habilidad = Habilidades::findorfail($id_habilidad);
        return view('forms.habilidades',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'habilidades',
            'habilidad'     =>$habilidad
        ]);
    }


    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'cantidad'  =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el nombre',
            'cantidad.required'     =>'Proporcione una cantidad'
        ]);

        

        $habilidad = Habilidades::create([
            'nombre'    => $request->nombre,
            'cantidad'    => $request->cantidad
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha agregado una nueva habilidad',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_habilidades')
        ], 200);
    }

    public function actualizar(Request $request, $id_habilidad)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'cantidad'  =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el nombre',
            'cantidad.required'     =>'Proporcione una cantidad'
        ]);

        $habilidad = Habilidades::find($id_habilidad);        
        $habilidad->nombre = $request->nombre;
        $habilidad->cantidad = $request->cantidad;
        $habilidad->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos de la habilidad',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_habilidades')
        ], 200);
    }

    public function eliminar($id_habilidad)
    {
        $habilidad = Habilidades::find($id_habilidad);
        $habilidad->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado la habilidad',
            'dttable'   =>  "#dt_habilidades",
        ], 200);
    }
}
