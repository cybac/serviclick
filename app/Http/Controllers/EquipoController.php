<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Equipo;
use Illuminate\Http\Request;

class EquipoController extends Controller
{
    private $path = "images/about/";

    public function datatable(Request $request)
    {
        $equipo = Equipo::all();
        return DataTables::of($equipo)
        ->addColumn('imagen', function($equipo){
            return "<a href='".asset($this->path.$equipo->imagen)."' target='_blank'><img src='".asset($this->path.$equipo->imagen)."' class='img-fluid' style='border-radius: 50%;'></a>";
        })
        ->addColumn('status', function($equipo){
            if($equipo->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_equipo($equipo->id);' class='switch' id='status_".$equipo->id."'>
                    <label for='status_".$equipo->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_equipo($equipo->id);' class='switch' id='status_".$equipo->id."'>
                    <label for='status_".$equipo->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($equipo){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_equipo',$equipo->id)."' style='width: 100%; margin-bottom: 10px;'>Editar</a><br>
            <button class='btn btn-danger delete p-3' data-url='".route('eliminar_equipo',$equipo->id)."' style='width: 100%; margin-bottom: 10px;'>Eliminar</button>";
        })
        ->rawColumns(['imagen', 'status' ,'btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.equipo',[
            'form_edit'     =>false,
            'menu'          =>'web',
            'submenu'       =>'equipo'
        ]);
    }

    public function editar($id_equipo)
    {
        $equipo = Equipo::findorfail($id_equipo);
        return view('forms.equipo',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'equipo',
            'equipo'        =>$equipo
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'imagen'    =>'required|mimes:png,jpg,jpeg|dimensions:ratio=1/1,min_width=600,max_width=4000|max:10240',
            'nombre'    =>'required',
            'puesto'    =>'required'
        ],
        [
            'imagen.required'       =>'Una foto es requerida',
            'imagen.mimes'          =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.dimensions'     =>'La imagen tiene que ser cuadrada resolución mínima de (600 x 600) ó una resolución máxima de (4000 x 4000)',
            'imagen.max'            =>'El tamaño máximo es de 10MB',
            'nombre.required'       =>'Proporcione el nombre',
            'puesto.required'       =>'Proporcione el puesto'
        ]);

        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
        }

        $equipo = Equipo::create([
            'imagen'    => $file_name,
            'nombre'    => $request->nombre,
            'puesto'    => $request->puesto
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha agregado el integrante al equipo',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_equipo')
        ], 200);
    }

    public function actualizar(Request $request, $id_equipo)
    {
        $rules = [
            'nombre'    =>'required',
            'puesto'    =>'required'
        ];
        if(isset($_FILES['imagen'])){
            $rules['imagen'] = 'required|mimes:png,jpg,jpeg|dimensions:ratio=1/1,min_width=600,max_width=4000|max:10240';
        }
        $messages = [
            'imagen.required'   =>'Una imagen es requerido',
            'imagen.mimes'      =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.dimensions' =>'La imagen tiene que ser cuadrada resolución mínima de (600 x 600) ó una resolución máxima de (4000 x 4000)',
            'imagen.max'        =>'El tamaño máximo es de 10MB',
            'nombre.required'   =>'Proporcione el nombre',
            'puesto.required'   =>'Proporcione el puesto'
        ];
        $this->validate($request,$rules,$messages);

        $equipo = Equipo::find($id_equipo);
        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
            File::delete($this->path.$equipo->imagen);
            $equipo->imagen = $file_name;
        }
        
        $equipo->nombre = $request->nombre;
        $equipo->puesto = $request->puesto;
        $equipo->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del integrante del equipo',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_equipo')
        ], 200);
    }

    public function eliminar($id_equipo)
    {
        $equipo = Equipo::find($id_equipo);
        File::delete($this->path.$equipo->imagen);
        $equipo->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado al integrante del equipo',
            'dttable'   =>  "#dt_equipo",
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del testimonial',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $equipo = Equipo::find($request->id);
        $equipo->status = $request->status;
        $equipo->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
