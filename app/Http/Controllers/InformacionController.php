<?php

namespace App\Http\Controllers;

use App\Models\Contacto;
use Illuminate\Http\Request;

class InformacionController extends Controller
{
    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'direccion' =>'required',
            'telefono'  =>'required',
            'correo'    =>'required'
        ],
        [
            'direccion.required'    =>'Proporcione la dirección de la sucursal',
            'telefono.required'     =>'Proporcione un número de contacto para la sucursal',
            'correo.required'       =>'Proporcione un correo de contacto para la sucursal'
        ]);
        $telefono = substr($request->telefono, 0, 3)." ".substr($request->telefono, 3, 3)." ".substr($request->telefono, 6, 2)." ".substr($request->telefono, 8, 2);
        
        $informacion = Contacto::find(1);
        $informacion->direccion = $request->direccion;
        $informacion->telefono = $telefono;
        $informacion->correo = $request->correo;
        $informacion->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la sucursal',
            'load'  =>  false,
            'url'   =>  route('control_sc_web_sucursales')
        ], 200);
    }
}
