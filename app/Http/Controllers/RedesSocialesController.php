<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RedesSociales;

class RedesSocialesController extends Controller
{
    public function ordenar(Request $request)
    {
        foreach ($request->order as $key => $value) {
            RedesSociales::where('id', $value)->update(['orden' => $key]);
		}

        return response()->json([
			'status'    => 'success',
			'text'      => 'Orden guardado'
		], 200);
    }

    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'dato'      =>'required'
        ],
        [
            'id.required'       =>'Indique el ID de la red social',
            'dato.required'     =>'Indique la URL de la red social'
        ]);

        RedesSociales::where('id', $request->id)->update(['url' => $request->dato]);
        return response()->json([
			'status'    => 'success',
			'text'      => 'URL Actualizada'
		], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID de la red social',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        RedesSociales::where('id', $request->id)->update(['status' => $request->status]);

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
