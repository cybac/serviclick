<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Fortalezas;
use Illuminate\Http\Request;

class FortalezasController extends Controller
{
    private $path = "images/fortalezas/";
    
    public function datatable(Request $request)
    {
        $beneficios = Fortalezas::all();
        return DataTables::of($beneficios)
        ->addColumn('imagen', function($beneficios){
            return "<a href='".asset($this->path.$beneficios->imagen)."' target='_blank'><img src='".asset($this->path.$beneficios->imagen)."' class='img-fluid'></a>";
        })
        ->addColumn('descripcion', function($beneficios){
            return "<p class='justificado'>".$beneficios->descripcion."</p>";
        })
        ->addColumn('btn', function($beneficios){
            return "<a class='btn btn-success text-white p-3 w-100' href='".route('editar_beneficio',$beneficios->id)."' style='margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['imagen', 'descripcion','btn'])
        ->toJson();
    }

    public function editar($id_beneficio)
    {
        $beneficio = Fortalezas::findorfail($id_beneficio);
        return view('forms.beneficios',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'beneficios',
            'beneficio'     =>$beneficio
        ]);
    }

    public function actualizar(Request $request, $id_beneficio)
    {
        $rules = [
            'titulo'        =>'required',
            'descripcion'   =>'required',
        ];
        if(isset($_FILES['imagen'])){
            $rules['imagen'] = 'required|mimes:png,jpg,jpeg|max:10240';
        }
        $messages = [
            'titulo.required'       =>'Proporcione un titulo',
            'descripcion.required'  =>'Proporcione una descripción',
            'imagen.required'       =>'Una imagen de icono es requerido',
            'imagen.mimes'          =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.max'            =>'El tamaño máximo es de 10MB'
        ];
        $this->validate($request,$rules,$messages);
        //Actualizar los datos de los beneficios
        $beneficio = Fortalezas::find($id_beneficio);
        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
            File::delete($this->path.$beneficio->imagen);
            $beneficio->imagen = $file_name;
        }
        $beneficio->titulo = $request->titulo;
        $beneficio->descripcion = $request->descripcion;
        $beneficio->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la información',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_beneficios')
        ], 200);
    }
}
