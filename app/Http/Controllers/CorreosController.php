<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Correos;
use Illuminate\Http\Request;

class CorreosController extends Controller
{
    public function datatable(Request $request)
    {
        $correos = Correos::all();
        return DataTables::of($correos)
        ->addColumn('mensaje', function($correos){
            return $correos->mensaje;
        })
        ->addColumn('btn', function($correos){
            return "<a class='btn btn-success text-white p-3 w-100' href='".route('editar_correo',$correos->id)."' style='padding-left: 30px !important; padding-right: 30px !important; margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['mensaje', 'btn'])
        ->toJson();
    }

    public function editar($id_correo)
    {
        $correo = Correos::findorfail($id_correo);
        return view('forms.correos',[
            'form_edit'     => true,
            'menu'          => 'correos',
            'submenu'       => '',
            'correo'        => $correo
        ]);
    }

    public function actualizar(Request $request, $id_correo)
    {
        $this->validate($request,[
            'titulo'    =>'required',
            'mensaje'   =>'required'
        ],
        [
            'titulo.required'       =>'Proporcione un titulo para el correo',
            'mensaje.required'      =>'Proporcione el contenido del correo'
        ]);
        Correos::where('id', $id_correo)->update(['titulo' => $request->titulo, 'mensaje' => $request->mensaje]);
        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado el correo',
            'load'  =>  true,
            'url'   =>  route('control_sc_emails')
        ], 200);
    }
}
