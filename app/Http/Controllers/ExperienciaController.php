<?php

namespace App\Http\Controllers;

use App\Models\Experiencia;
use Illuminate\Http\Request;

class ExperienciaController extends Controller
{
    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'contenido' =>'required'
        ],
        [
            'contenido.required'    =>'Proporcione una descripción'
        ]);

        $experiencia = Experiencia::find(1);
        $experiencia->contenido = $request->contenido;
        $experiencia->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la información de Acerca De',
            'load'  =>  false,
            'url'   =>  route('control_sc_web_experiencia')
        ], 200);
    }
}
