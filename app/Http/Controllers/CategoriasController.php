<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Categorias;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    private $path = "images/areas/";

    public function datatable(Request $request)
    {
        $categorias = Categorias::where('id', '>', 2)->get();
        return DataTables::of($categorias)
        ->addColumn('imagen', function($categorias){
            return "<a href='".asset($this->path.$categorias->imagen)."' target='_blank'><img src='".asset($this->path.$categorias->imagen)."' class='img-fluid' style='margin-left: 10px;'></a>";
        })
        ->addColumn('btn', function($categorias){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_categoria',$categorias->id)."' style='padding-left: 30px !important; padding-right: 30px !important; margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['imagen', 'btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.categorias',[
            'form_edit'     =>false,
            'menu'          =>'categorias'
        ]);
    }

    public function editar($id_categoria)
    {
        $categoria = Categorias::findorFail($id_categoria);
        if($id_categoria > 2)
        {
            return view('forms.categorias',[
                'form_edit'     =>true,
                'menu'          =>'categorias',
                'submenu'       =>'',
                'categoria'    =>$categoria
            ]);
        }else{
            return redirect()->route('control_sc_categorias');
        }
    }


    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'imagen'    =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'nombre.required'   =>'Proporcione el nombre del negocio',
            'imagen.required'   =>'Proporcione una imagen',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);

        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $img = $request->file('imagen');
        $file_name = uniqid().".".$img->extension();
        $img->move($this->path, $file_name);

        $categoria = Categorias::create([
            'imagen'        => $file_name,
            'nombre'        => $request->nombre
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha creado la categoria',
            'load'  =>  true,
            'url'   =>  route('control_sc_categorias')
        ], 200);
    }

    public function actualizar(Request $request, $id_categoria)
    {
        $rules = [
            'nombre'    =>'required',
        ];
        if(isset($_FILES['imagen'])){
            $rules['imagen'] = 'required|mimes:png,jpg,jpeg|max:10240';
        }
        $messages = [
            'nombre.required'   =>'Proporcione el nombre del negocio',
            'imagen.required'   =>'Proporcione una imagen',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ];
        $this->validate($request,$rules,$messages);

        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        //Guarda la nueva imagen
        if($request->hasFile('imagen')){
            $img = $request->file('imagen');
            $file_name = uniqid().".".$img->extension();
            $img->move($this->path, $file_name);
        }
        //Actualizar los datos de la categoria
        $categoria = Categorias::find($id_categoria);
        $categoria->nombre = $request->nombre;
        if($request->hasFile('imagen')){
            File::delete($this->path.$categoria->imagen);
            $categoria->imagen = $file_name;
        }
        $categoria->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la categoria',
            'load'  =>  true,
            'url'   =>  route('control_sc_categorias')
        ], 200);
    }
}
