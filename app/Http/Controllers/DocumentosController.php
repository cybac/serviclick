<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Negocios;
use App\Models\Documentos;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class DocumentosController extends Controller
{
    private $path = "documentos/";

    public function index($id_negocio)
    {
        Negocios::findorfail($id_negocio);
        return view('admin.documentos',[
            'negocio'   => $id_negocio,
            'add'       => true,
            'dttable'   =>'dt_documentos'
        ]);
    }

    public function guardar(Request $request, $id_negocio)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'documento'     =>'required|mimes:pdf,png,jpg,jpeg|max:10240'
        ],
        [
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'documento.required'     =>'Seleccione una ubicación',
            'documento.mimes'        =>'Solo se permiten imágenes PNG, JPG, JPEG o archivos PDF',
            'documento.max'          =>'El tamaño maximo del archivo es de 10MB'
        ]);
        $archivo = $request->file('documento');
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
        $archivo->move($this->path, $file_name);

        $documento = Documentos::create([
            'negocio'   => $id_negocio,
            'nombre'    => $file_name,
            'tipo'      => $request->nombre,
            'status'    => true
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha subido el documento',
            'load'  =>  true,
            'url'   =>  route('documentos_negocio', $id_negocio)
        ], 200);
    }


    public function datatable(Request $request, $id_negocio)
    {
        $documentos = Documentos::select('documentos.id', 'documentos.negocio', 'documentos.status', 'documentos.nombre','tipos_documentos.nombre as tipo')
            ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
            ->where('negocio', $id_negocio);
        return DataTables::of($documentos)
        ->addColumn('btn', function($documentos){
            if($documentos->status)
            {
                //Ver documento
                return "<button class='btn btn-primary ver-archivo text-white p-3' data-tipo='".substr($documentos->nombre, Str::length($documentos->nombre) - 3, 3)."' data-archivo='".$documentos->nombre."' style='width:50%; margin-bottom: 10px; cursor: pointer;'>Mostrar Documento</button><br>
                <button class='btn btn-danger delete p-3' data-url='".route('eliminar_documento_negocio',$documentos->id)."' style='width: 50%; margin-bottom: 10px;'>Eliminar Documento</button>";
            }else{
                //Subir documento
                return "<a class='btn btn-success text-white p-3' href='".route('editar_documento_negocio',$documentos->id)."' style='width: 50%; margin-bottom: 10px;'>Subir</a>";
            }
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function editar($id_documento)
    {
        $documento = Documentos::select('documentos.id', 'documentos.negocio', 'tipos_documentos.nombre as tipo')
            ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
            ->where('documentos.id', $id_documento)->first();
        return view('forms.documentos',[
            'form_edit'     =>true,
            'menu'          =>'negocios',
            'submenu'       =>'documentos',
            'documento'     => $documento
        ]);
    }

    public function crear($id_negocio)
    {
        return view('forms.documentos',[
            'form_edit'     =>false,
            'menu'          =>'negocios',
            'submenu'       =>'documentos',
            'negocio'     => $id_negocio
        ]);
    }

    public function actualizar(Request $request, $id_documento)
    {
        $this->validate($request,[
            'negocio'       =>'required',
            'nombre'        =>'required',
            'documento'     =>'required|mimes:pdf,png,jpg,jpeg|max:10240'
        ],
        [
            'negocio.required'       =>'El ID del negocio es requerido',
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'documento.required'     =>'Seleccione una ubicación',
            'documento.mimes'        =>'Solo se permiten imágenes PNG, JPG, JPEG o archivos PDF',
            'documento.max'          =>'El tamaño maximo del archivo es de 10MB'
        ]);
        $archivo = $request->file('documento');
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
        $archivo->move($this->path, $file_name);

        $documento = Documentos::find($id_documento);
        File::delete($this->path.$documento->nombre);       //Eliminar el archivo anterior
        $documento->nombre = $file_name;
        $documento->status = true;
        $documento->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado el documento',
            'load'  =>  true,
            'url'   =>  route('documentos_negocio', $documento->negocio)
        ], 200);
    }

    public function eliminar($id_documento)
    {
        $documento = Documentos::find($id_documento);
        File::delete($this->path.$documento->nombre);
        $documento->delete();

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el documento',
            'dttable'   =>  "#dt_documentos",
        ], 200);
    }
}
