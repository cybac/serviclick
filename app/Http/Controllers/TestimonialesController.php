<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use Illuminate\Http\Request;
use App\Models\Testimoniales;

class TestimonialesController extends Controller
{
    private $path = "images/testimoniales/";
    
    public function datatable(Request $request)
    {
        $testimoniales = Testimoniales::all();
        return DataTables::of($testimoniales)
        ->addColumn('imagen', function($testimoniales){
            return "<a href='".asset($this->path.$testimoniales->imagen)."' target='_blank'><img src='".asset($this->path.$testimoniales->imagen)."' class='img-fluid'></a>";
        })
        ->addColumn('contenido', function($testimoniales){
            return "<p class='justificado'>".$testimoniales->contenido."</p>";
        })
        ->addColumn('status', function($testimoniales){
            if($testimoniales->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_testimonial($testimoniales->id);' class='switch' id='status_".$testimoniales->id."'>
                    <label for='status_".$testimoniales->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_testimonial($testimoniales->id);' class='switch' id='status_".$testimoniales->id."'>
                    <label for='status_".$testimoniales->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($testimoniales){
            return "<a class='btn btn-success text-white p-3 w-100 mb-2' href='".route('editar_testimonial',$testimoniales->id)."'>Editar</a><br>
            <button class='btn btn-danger delete p-3 w-100 mb-2' data-url='".route('eliminar_testimonial',$testimoniales->id)."'>Eliminar</button>";
        })
        ->rawColumns(['imagen','contenido','status','btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.testimoniales',[
            'form_edit'     =>false,
            'menu'          =>'web',
            'submenu'       =>'testimoniales'
        ]);
    }

    public function editar($id_testimonial)
    {
        $testimonial = Testimoniales::findorfail($id_testimonial);
        return view('forms.testimoniales',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'testimoniales',
            'testimonial'   =>$testimonial
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'imagen'    =>'required|mimes:png,jpg,jpeg|max:10240',
            'nombre'    =>'required',
            'contenido' =>'required',
            'trabajo'   =>'required'
        ],
        [
            'imagen.required'       =>'Una imagen es requerido',
            'imagen.mimes'          =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.max'            =>'El tamaño máximo es de 10MB',
            'nombre.required'       =>'Proporcione el nombre',
            'contenido.required'    =>'Proporcione el contenido',
            'trabajo.required'      =>'Proporcione el trabajo'
        ]);

        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
        }

        $testimonial = Testimoniales::create([
            'imagen'        => $file_name,
            'nombre'        => $request->nombre,
            'contenido'     => $request->contenido,
            'trabajo'       => $request->trabajo
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha creado el testimonial',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_testimoniales')
        ], 200);
    }

    public function actualizar(Request $request, $id_testimonial)
    {
        $rules = [
            'nombre'    =>'required',
            'contenido' =>'required',
            'trabajo'   =>'required'
        ];
        if(isset($_FILES['imagen'])){
            $rules['imagen'] = 'required|mimes:png,jpg,jpeg|max:10240';
        }
        $messages = [
            'imagen.required'       =>'Una imagen es requerido',
            'imagen.mimes'          =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.max'            =>'El tamaño máximo es de 10MB',
            'nombre.required'       =>'Proporcione el nombre',
            'contenido.required'    =>'Proporcione el contenido',
            'trabajo.required'      =>'Proporcione el trabajo'

        ];
        $this->validate($request,$rules,$messages);
        //Actualizar los datos del testimonial
        $testimonial = Testimoniales::find($id_testimonial);
        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
            File::delete($this->path.$testimonial->imagen);
            $testimonial->imagen = $file_name;
        }
        
        $testimonial->nombre = $request->nombre;
        $testimonial->contenido = $request->contenido;
        $testimonial->trabajo = $request->trabajo;
        $testimonial->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado el testimonial',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_testimoniales')
        ], 200);
    }

    public function eliminar($id_testimonial)
    {
        $testimonial = Testimoniales::find($id_testimonial);
        File::delete($this->path.$testimonial->imagen);
        $testimonial->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el testimonial',
            'dttable'   =>  "#dt_testimoniales",
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del testimonial',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $testimonial = Testimoniales::find($request->id);
        $testimonial->status = $request->status;
        $testimonial->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
