<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Comentarios;
use Illuminate\Http\Request;
use App\Models\Calificaciones;

class ComentariosController extends Controller
{
    public function datatable(Request $request, $id_negocio)
    {
        $ids = Calificaciones::selectRaw("DISTINCT comentario")->where('negocio', $id_negocio)->get();
        $comentarios = Comentarios::whereIn('id', $ids);

        return DataTables::of($comentarios)
        ->addColumn('status', function($comentarios){
            if($comentarios->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_comentario($comentarios->id);' class='switch' id='status_".$comentarios->id."'>
                    <label for='status_".$comentarios->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_comentario($comentarios->id);' class='switch' id='status_".$comentarios->id."'>
                    <label for='status_".$comentarios->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($comentarios){
            return "<button class='btn btn-danger delete p-3' data-url='".route('eliminar_comentario',$comentarios->id)."' style='width: 100%; margin-bottom: 10px;'>Eliminar Comentario</button>";
        })
        ->rawColumns(['status', 'btn'])
        ->toJson();
    }

    public function datatable_all(Request $request)
    {
        $ids = Calificaciones::selectRaw("DISTINCT comentario")->get();
        $comentarios = Comentarios::select('id','nombre', 'correo', 'puntuacion', 'contenido')
            ->selectRaw("(SELECT negocios.nombre FROM calificaciones JOIN negocios on negocios.id = calificaciones.negocio WHERE comentario = comentarios.id LIMIT 1) as negocio")
            ->whereIn('id', $ids)->where('nuevo', true);

        return DataTables::of($comentarios)
        ->addColumn('btn', function($comentarios){
            return "<button class='btn btn-success accion p-3' data-url='".route('accion_comentario')."' style='width: 100%; margin-bottom: 10px;' data-id='".$comentarios->id."' data-status='1' data-title='¿Desea aprobar este comentario?' data-subtitle='La reseña sera publicada'>Aprobar Comentario</button>
            <button class='btn btn-warning accion p-3' data-url='".route('accion_comentario')."' style='width: 100%; margin-bottom: 10px;' data-id='".$comentarios->id."' data-status='0' data-title='¿Desea rechazar este comentario?' data-subtitle='La reseña seguira oculta'>Rechazar Comentario</button>
            <button class='btn btn-danger delete p-3' data-url='".route('eliminar_comentario', $comentarios->id)."' style='width: 100%; margin-bottom: 10px;'>Eliminar Comentario</button>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function eliminar($id_comentario)
    {
        $comentario = Comentarios::find($id_comentario);
        $comentario->delete();

        $calificaciones = Calificaciones::where('comentario', $id_comentario);
        $calificaciones->delete();

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el comentario junto con su calificación',
            'dttable'   =>  "#dt_comentarios",
            'dttable2'   =>  "#dt_all_comentarios",
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del comentario',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $comentario = Comentarios::find($request->id);
        $comentario->status = $request->status;
        $comentario->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }

    public function aprobar_denegar(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del comentario',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $comentario = Comentarios::find($request->id);
        $comentario->status = $request->status;
        $comentario->nuevo = false;
        $comentario->save();

        if($request->status)
        {
            return response()->json([
                'status'    =>  'true',
                'text'      =>  'Comentario Aprobado',
                'dttable'   =>  "#dt_all_comentarios",
            ], 200);
        }else{
            return response()->json([
                'status'    =>  'true',
                'text'      =>  'Comentario Rechazado',
                'dttable'   =>  "#dt_all_comentarios",
            ], 200);
        }
    }
}
