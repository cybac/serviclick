<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Equipo;
use App\Models\Legales;
use App\Models\Negocios;
use App\Models\Contacto;
use App\Models\Categorias;
use App\Models\Documentos;
use App\Models\Fortalezas;
use App\Models\Sucursales;
use App\Models\Experiencia;
use App\Models\Seguimiento;
use App\Models\Habilidades;
use App\Models\Ubicaciones;
use Illuminate\Http\Request;
use App\Models\Testimoniales;
use App\Models\RedesSociales;
use App\Models\SliderFortalezas;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $frases = SliderFortalezas::where('status', '1')->get();
        $categorias = Categorias::where('id', '>', '2')->orderby('nombre')->get();
        $look_categorias = Categorias::whereIn('id', Negocios::selectRaw("DISTINCT categoria")->where("status", true)->get())->inRandomOrder()->limit(7)->get();
        $fortalezas = Fortalezas::all();
        $testimoniales = Testimoniales::where('status', '1')->get();
        $contacto = Contacto::all();
        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        return view('home', compact('frases', 'ubicaciones', 'categorias', 'look_categorias', 'fortalezas', 'testimoniales', 'contacto', 'sociales'));
    }

    public function about()
    {
        $contacto = Contacto::all();
        $about = About::find(1);
        $equipo = Equipo::where('status', true)->get();
        $experiencia = Experiencia::find(1);
        $habilidades = Habilidades::all();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        return view('about', compact('about', 'equipo', 'experiencia', 'habilidades','contacto', 'sociales'));
    }

    public function contact()
    {
        $contacto = Contacto::all();
        $sucursales = Sucursales::all();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        return view('contact', compact('sucursales','contacto', 'sociales'));
    }

    public function terms()
    {
        $contacto = Contacto::all();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        $legal = Legales::findorfail(2);
        return view('legales', compact('legal', 'contacto', 'sociales'));
    }

    public function privacy()
    {
        $contacto = Contacto::all();
        $sociales = RedesSociales::where('status', true)->orderby('orden')->get();
        $legal = Legales::findorfail(1);
        return view('legales', compact('legal', 'contacto', 'sociales'));
    }
    
    public function negocio()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        
        if($negocio->tramites)
        {
            $progreso = Seguimiento::selectRaw("count(*) as total")->where('negocio', Auth::user()->negocio)->where('asistente', true)->get();
            if($progreso[0]->total == 0) {
                $contenido = view('partials.welcome')->render();
            } else{
                $progreso = Seguimiento::where('negocio', Auth::user()->negocio)
                    ->where('asistente', true)
                    ->get();
                if($progreso[0]->tramite == 1)
                {
                    $contenido = view('partials.logo')->render();
                }

                if($progreso[0]->tramite == 2)
                {
                    //Verificar si mandar a documentos o a informacion
                    $siguiente = Seguimiento::where('negocio', Auth::user()->negocio)
                        ->where('tramite', 3)
                        ->get();

                    if($siguiente[0]->status)
                    {
                        $negocio = Negocios::find(Auth::user()->negocio);
                        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                        $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
                        $contenido = view('partials.informacion',['negocio' => $negocio, 'ubicaciones' => $ubicaciones, 'categorias' => $categorias])->render();
                    }else{
                        $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                            ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                            ->where('negocio', Auth::user()->negocio)
                            ->where('documentos.status', false)
                            ->get();
                        $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
                        Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->update(['asistente' =>  false]);
                        Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);
                    }
                }
                if($progreso[0]->tramite == 3)
                {
                    $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                        ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                        ->where('negocio', Auth::user()->negocio)
                        ->where('documentos.status', false)
                        ->get();
                    $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
                }
                
                if($progreso[0]->tramite == 4)
                {
                    $pendientes = Seguimiento::where('negocio', Auth::user()->negocio)
                        ->where('status', false)
                        ->whereNotNull('comentarios')
                        ->get();
                    if(count($pendientes) > 0)
                    {
                        if(count($pendientes) == 2)
                        {
                            $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                                    ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                                    ->where('negocio', Auth::user()->negocio)
                                    ->where('documentos.status', false)
                                    ->get();
                                $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  false]);
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);
                        }else{
                            if($pendientes[0]->tramite == 2)
                            {
                                $negocio = Negocios::find(Auth::user()->negocio);
                                $ubicaciones = Ubicaciones::orderBy('nombre')->get();
                                $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
                                $contenido = view('partials.informacion',['negocio' => $negocio, 'ubicaciones' => $ubicaciones, 'categorias' => $categorias, 'comentarios' => $pendientes[0]->comentarios])->render();
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  false]);
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 2)->update(['asistente' =>  true]);
                            }
                            if($pendientes[0]->tramite == 3)
                            {
                                $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                                    ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                                    ->where('negocio', Auth::user()->negocio)
                                    ->where('documentos.status', false)
                                    ->get();
                                $contenido = view('partials.documentos', ['documentos' => $documentacion])->render();
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  false]);
                                //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);
                            }
                        }
                    }else{
                        //Verificar Pendientes
                        $pendientes = Seguimiento::where('negocio', Auth::user()->negocio)
                            ->where('tramite', 3)
                            ->where('status', false)
                            ->whereNull('comentarios')
                            ->get();
                        if(count($pendientes) == 1)
                        {
                            $documentacion = Documentos::select('documentos.id', 'documentos.rechazado', 'documentos.motivo','documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
                                ->join('tipos_documentos', 'documentos.tipo', '=', 'tipos_documentos.id')
                                ->where('negocio', Auth::user()->negocio)
                                ->where('documentos.status', false)
                                ->get();
                            $contenido = view('partials.documentos', ['documentos' => $documentacion, 'boton_omision' => false])->render();
                            //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 4)->update(['asistente' =>  false]);
                            //Seguimiento::where('negocio', Auth::user()->negocio)->where('tramite', 3)->update(['asistente' =>  true]);

                        }else{
                            $contenido = view('partials.bye')->render();
                        }
                    }


                }

            }

            return view('asistente',[
                'contenido'     => $contenido
            ]);
        }
        return view('negocio');
    }

    public function administrador()
    {
        return view('admin');
    }

    public function registrarse()
    {
        return view('registrarse',[
            "alta" => 1
        ]);
    }
}
