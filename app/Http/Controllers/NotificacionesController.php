<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Models\Tipos_Notificaciones;

class NotificacionesController extends Controller
{
    public function datatable(Request $request)
    {
        $notificaciones = Tipos_Notificaciones::all();
        return DataTables::of($notificaciones)
        ->addColumn('btn', function($notificaciones){
            return "<button class='btn btn-success p-3 input-text w-100 mb-2' data-value='".$notificaciones->mensaje."' data-url='".route('actualizar_notificacion', $notificaciones->id)."' data-title='Editar Notificacion' data-subtitle='Contenido de la notificación'>Editar</button>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function actualizar(Request $request, $id_notificacion)
    {
        
        $this->validate($request,[
            'nombre'    =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el contenido de la notificación'
        ]);
            
        $notificacion = Tipos_Notificaciones::find($id_notificacion);
        $notificacion->mensaje = $request->nombre;
        $notificacion->save();

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha actualizado el contenido de la notificación',
            'dttable'   =>  "#dt_notificaciones",
        ], 200);
    }
}
