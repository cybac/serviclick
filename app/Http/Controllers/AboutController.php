<?php

namespace App\Http\Controllers;

use File;
use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    private $path = "images/";

    public function actualizar(Request $request)
    {
        $refrescar = false;
        $rules = [
            'contenido'        =>'required',
        ];
        if(isset($_FILES['imagen'])){
            $rules['imagen'] = 'required|mimes:png,jpg,jpeg|dimensions:min_width=600,max_width=1280|max:10240';
        }
        $messages = [
            'contenido.required'    =>'Proporcione una descripción',
            'imagen.required'       =>'Una imagen es requerida',
            'imagen.mimes'          =>'Solo se permiten imagenes PNG o (JPG/JPEG)',
            'imagen.dimensions'     =>'La imagen tiene que tener un ratio 3:2 <br>Resolución mínima de (600 x 400)<br>Resolución máxima de (1280 x 853)',
            'imagen.max'            =>'El tamaño máximo es de 10MB'
        ];
        $this->validate($request,$rules,$messages);

        $acerca = About::find(1);
        if($request->hasFile('imagen')){
            $archivo = $request->file('imagen');
            if (!file_exists($this->path)) {
                File::makeDirectory($this->path, $mode = 0777, true, true);
            }
            $file_name = uniqid().".".$archivo->extension();
            $archivo->move($this->path,$file_name);
            File::delete($this->path.$acerca->imagen);
            $acerca->imagen = $file_name;
            $refrescar = true;
        }
        $acerca->contenido = $request->contenido;
        $acerca->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la información de Acerca De',
            'load'  =>  $refrescar,
            'url'   =>  route('control_sc_web_about')
        ], 200);
    }
}
