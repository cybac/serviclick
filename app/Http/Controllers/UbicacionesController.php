<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Ubicaciones;
use Illuminate\Http\Request;

class UbicacionesController extends Controller
{
    public function coordenadas(Request $request)
    {
        $this->validate($request,['ubicacion' => 'required'],['ubicacion.required' => 'El ID de la ubicacion es requerido']);
        $coordenadas = Ubicaciones::findorfail($request->ubicacion);
        return $coordenadas->ubicacion;
    }

    public function datatable(Request $request)
    {
        $ubicaciones = Ubicaciones::all();
        return DataTables::of($ubicaciones)
        ->addColumn('btn', function($ubicaciones){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_ubicacion',$ubicaciones->id)."' style='padding-left: 30px !important; padding-right: 30px !important; margin-bottom: 10px;'>Editar</a>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.ubicaciones',[
            'form_edit'     =>false,
            'menu'          =>'ubicaciones'
        ]);
    }

    public function editar($id_ubicacion)
    {
        $ubicacion = Ubicaciones::findorfail($id_ubicacion);
        return view('forms.ubicaciones',[
            'form_edit'     =>true,
            'menu'          =>'ubicaciones',
            'submenu'       =>'',
            'ubicacion'     =>$ubicacion
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'mapa'      =>'required'
        ],
        [
            'nombre.required'  =>'Proporcione el nombre de la ubicación',
            'mapa.required'    =>'Proporcione las coordenadas de ubicación'
        ]);

        $ubicacion = Ubicaciones::create([
            'nombre'        => $request->nombre,
            'ubicacion'     => $request->mapa
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha creado la ubicacion',
            'load'  =>  true,
            'url'   =>  route('control_sc_ubicaciones')
        ], 200);
    }

    public function actualizar(Request $request, $id_ubicacion)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'mapa'      =>'required'
        ],
        [
            'nombre.required'   =>'Proporcione el nombre de la ubicacion',
            'mapa.required'     =>'Proporcione las coordenadas de ubicacion'
        ]);
        //Actualizar los datos de ubicacion
        $ubicacion = Ubicaciones::find($id_ubicacion);
        $ubicacion->nombre = $request->nombre;
        $ubicacion->ubicacion = $request->mapa;
        $ubicacion->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la ubicacion',
            'load'  =>  true,
            'url'   =>  route('control_sc_ubicaciones')
        ], 200);
    }
}
