<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Models\SliderFortalezas;

class SliderFortalezasController extends Controller
{
    public function datatable(Request $request)
    {
        $frases = SliderFortalezas::all();
        return DataTables::of($frases)
        ->addColumn('frase', function($frases){
            return "<p>".$frases->inicio." <strong>".$frases->remarcado."</strong> ".$frases->fin."</p>";
        })
        ->addColumn('status', function($frases){
            if($frases->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_slider($frases->id);' class='switch' id='status_".$frases->id."'>
                    <label for='status_".$frases->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_slider($frases->id);' class='switch' id='status_".$frases->id."'>
                    <label for='status_".$frases->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($frases){
            return "<a class='btn btn-success text-white p-3 w-100' href='".route('editar_slider_text',$frases->id)."' style='margin-bottom: 10px;'>Editar</a>
            <button class='btn btn-danger delete p-3 w-100' data-url='".route('eliminar_slider_text',$frases->id)."' style='margin-bottom: 10px;'>Eliminar</button>";
        })
        ->rawColumns(['frase', 'status', 'btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.slider_text',[
            'form_edit'     =>false,
            'menu'          =>'web',
            'submenu'       =>'slider_text'
        ]);
    }

    public function editar($id_slider)
    {
        $slider = SliderFortalezas::findorfail($id_slider);
        return view('forms.slider_text',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'slider_text',
            'slider'        =>$slider
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'inicio'    =>'required',
            'remarcado' =>'required',
            'fin'       =>'required',
        ],
        [
            'inicio.required'       =>'Proporcione el incio del slider',
            'remarcado.required'    =>'Proporcione el remarcado del slider',
            'fin.required'          =>'Proporcione el fin del slider'
        ]);

        $slider = SliderFortalezas::create([
            'inicio'        => $request->inicio,
            'remarcado'     => $request->remarcado,
            'fin'           => $request->fin
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha creado el slider',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_slider_text')
        ], 200);
    }

    public function actualizar(Request $request, $id_slider)
    {
        $this->validate($request,[
            'inicio'    =>'required',
            'remarcado' =>'required',
            'fin'       =>'required',
        ],
        [
            'inicio.required'       =>'Proporcione el incio del slider',
            'remarcado.required'    =>'Proporcione el remarcado del slider',
            'fin.required'          =>'Proporcione el fin del slider'
        ]);
        //Actualizar los datos del slider
        $slider = SliderFortalezas::find($id_slider);
        $slider->inicio = $request->inicio;
        $slider->remarcado = $request->remarcado;
        $slider->fin = $request->fin;
        $slider->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado el slider',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_slider_text')
        ], 200);
    }

    public function eliminar($id_slider)
    {
        $slider = SliderFortalezas::find($id_slider);
        $slider->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el slider',
            'dttable'   =>  "#dt_slider_text",
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del slider',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $slider = SliderFortalezas::find($request->id);
        $slider->status = $request->status;
        $slider->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
