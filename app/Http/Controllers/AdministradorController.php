<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use Carbon\Carbon;
use App\Models\User;
use App\Models\About;
use App\Models\Legales;
use App\Models\Precios;
use App\Models\Anuncios;
use App\Models\Contacto;
use App\Models\Negocios;
use App\Models\Keysword;
use App\Models\Documentos;
use App\Mail\Notificacion;
use App\Models\Categorias;
use App\Models\Ubicaciones;
use App\Models\Experiencia;
use App\Models\Comentarios;
use Illuminate\Http\Request;
use App\Models\Recordatorios;
use App\Models\RedesSociales;
use App\Models\Calificaciones;
use App\Models\GaleriaTrabajos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdministradorController extends Controller
{
    private $path = "images/negocios/";
    private $path_galeria = "images/trabajos/";
    private $path_miniatura_movil = "images/trabajos/miniaturas/movil/";
    private $path_miniatura_web = "images/trabajos/miniaturas/web/";
    private $path_documentos = "documentos/";
    
    public function negocios()
    {
        return view('admin.negocios');
    }

    public function datatable_negocios(Request $request)
    {
        $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'categorias.nombre as categoria', 'ubicaciones.nombre as ubicacion', 'negocios.telefono', 'negocios.omision', 'negocios.pagos', 'negocios.status')
            ->selectRaw("CONCAT(negocios.precio_min, ' - ', negocios.precio_max) as precio_min")
            ->join('categorias', 'categorias.id', 'negocios.categoria')
            ->join('ubicaciones', 'ubicaciones.id', 'negocios.ubicacion')
            ->where('tramites', false)
            ->get();
        return DataTables::of($negocios)
        ->addColumn('imagen', function($negocios){
            if($negocios->imagen == "0")
            {
                return "<img src='".asset($this->path."0.png")."' class='img-fluid' style='margin-left: 10px;'>";
            }else{
                return "<img src='".asset($this->path.$negocios->imagen)."' class='img-fluid' style='margin-left: 10px;'>";
            }
        })
        ->addColumn('pagando', function($negocios){
            if($negocios->pagos)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_negocio_pago($negocios->id);' class='switch' id='status_pago_".$negocios->id."'>
                    <label for='status_pago_".$negocios->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_negocio_pago($negocios->id);' class='switch' id='status_pago_".$negocios->id."'>
                    <label for='status_pago_".$negocios->id."'></label>
                    </span>";
            }
        })
        ->addColumn('status', function($negocios){
            if($negocios->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_negocio($negocios->id);' class='switch' id='status_".$negocios->id."'>
                    <label for='status_".$negocios->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_negocio($negocios->id);' class='switch' id='status_".$negocios->id."'>
                    <label for='status_".$negocios->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn_img', function($negocios){
            if($negocios->imagen == "0")
            {
                return "<a class='btn btn-success text-white p-3 w-100 mb-2' href='".route('logo_negocio',$negocios->id)."'>Subir Logo</a>
                    <a class='btn btn-info text-white p-3 w-100 mb-2' href='".route('editar_galeria_negocio',$negocios->id)."'>Galeria</a>";
            }else{
                return "<a class='btn btn-primary text-white p-3 w-100 mb-2' href='".route('editar_logo_negocio',$negocios->id)."'>Editar Logo</a>
                    <a class='btn btn-info text-white p-3 w-100 mb-2' href='".route('editar_galeria_negocio',$negocios->id)."'>Galeria</a>";
            }
        })
        ->addColumn('btn_otro', function($negocios){
            return "<a class='btn btn btn-purple text-white p-3 w-100 mb-2' href='".route('pagos_negocio',$negocios->id)."'>Pagos</a>
            <a class='btn btn-dark text-white p-3 w-100 mb-2' href='".route('keysword_negocio',$negocios->id)."'>Keysword</a>
            <a class='btn btn-secondary text-white p-3 w-100 mb-2' href='".route('comentarios_negocio',$negocios->id)."'>Comentarios</a>";
        })
        ->addColumn('btn', function($negocios){
            return "<a class='btn btn-success text-white p-3 w-100 mb-2' href='".route('editar_negocio',$negocios->id)."'>Editar Información</a>
                <a class='btn btn-warning text-white p-3 w-100 mb-2' href='".route('documentos_negocio',$negocios->id)."'>Documentos</a>
                <a class='btn btn-lime text-white p-3 w-100 mb-2' target='_blank' href='".route('control_sc_recordatorios_negocios',$negocios->id)."'>Recordatorios</a>
                <button class='btn btn-danger delete p-3 w-100 mb-2' data-url='".route('eliminar_negocio',$negocios->id)."'>Eliminar</button>";
        })
        ->rawColumns(['imagen', 'pagando','status', 'btn_img', 'btn_otro', 'btn'])
        ->toJson();
    }

    public function editar_negocio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
        $categorias = Categorias::where('id', '>', '2')->orderBy('nombre')->get();
        return view('forms.negocios',[
            'form_edit'     =>true,
            'menu'          =>'negocios',
            'submenu'       =>'',
            'negocio'       =>$negocio,
            'ubicaciones'   =>$ubicaciones,
            'categorias'    =>$categorias
        ]);
    }

    public function guardar_negocio(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'ubicacion'     =>'required',
            'categoria'     =>'required',
            'direccion'     =>'required',
            'telefono'      =>'required',
            'descripcion'   =>'required',
            'servicios'     =>'required',
            'minimo'        =>'required',
            'maximo'        =>'required',
            'mapa'          =>'required'
        ],
        [
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'ubicacion.required'     =>'Seleccione una ubicación',
            'categoria.required'     =>'Seleccione una categoria',
            'direccion.required'     =>'Proporcione la dirección del negocio',
            'telefono.required'      =>'Proporcione un número telefonico de contacto',
            'descripcion.required'   =>'Proporcione una descripción del negocio',
            'servicios.required'     =>'Indique los servicios que proporciona su negocio',
            'minimo.required'        =>'Indique el precio minimo aproximado',
            'maximo.required'        =>'Indique el precio minimo aproximado',
            'mapa.required'          =>'Indique su negocio en el mapa'
        ]);
        if($request->has('web')){
            $negocio = Negocios::create([
                'nombre'        => $request->nombre,
                'direccion'     => $request->direccion,
                'telefono'      => $request->telefono,
                'web'           => $request->web,
                'descripcion'   => $request->descripcion,
                'servicios'     => $request->servicios,
                'precio_min'    => $request->minimo,
                'precio_max'    => $request->maximo,
                'categoria'     => $request->categoria,
                'ubicacion'     => $request->ubicacion,
                'mapa'          => $request->mapa
            ]);
        }else{
            $negocio = Negocios::create([
                'nombre'        => $request->nombre,
                'direccion'     => $request->direccion,
                'telefono'      => $request->telefono,
                'descripcion'   => $request->descripcion,
                'servicios'     => $request->servicios,
                'precio_min'    => $request->minimo,
                'precio_max'    => $request->maximo,
                'categoria'     => $request->categoria,
                'ubicacion'     => $request->ubicacion,
                'mapa'          => $request->mapa
            ]);
        }

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha guardado el negocio',
            'load'  =>  true,
            'url'   =>  route('control_sc_negocios')
        ], 200);
    }

    public function actualizar_negocio(Request $request, $id_negocio)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'ubicacion'     =>'required',
            'categoria'     =>'required',
            'direccion'     =>'required',
            'telefono'      =>'required',
            'descripcion'   =>'required',
            'servicios'     =>'required',
            'minimo'        =>'required',
            'maximo'        =>'required',
            'mapa'          =>'required'
        ],
        [
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'ubicacion.required'     =>'Seleccione una ubicación',
            'categoria.required'     =>'Seleccione una categoria',
            'direccion.required'     =>'Proporcione la dirección del negocio',
            'telefono.required'      =>'Proporcione un número telefonico de contacto',
            'descripcion.required'   =>'Proporcione una descripción del negocio',
            'servicios.required'     =>'Indique los servicios que proporciona su negocio',
            'minimo.required'        =>'Indique el precio minimo aproximado',
            'maximo.required'        =>'Indique el precio minimo aproximado',
            'mapa.required'          =>'Indique su negocio en el mapa'
        ]);

        $negocio = Negocios::find($id_negocio);
        $negocio->nombre = $request->nombre;
        $negocio->direccion = $request->direccion;
        $negocio->telefono = $request->telefono;
        $negocio->descripcion = $request->descripcion;
        $negocio->servicios = $request->servicios;
        $negocio->precio_min = $request->minimo;
        $negocio->precio_max = $request->maximo;
        $negocio->categoria = $request->categoria;
        $negocio->ubicacion = $request->ubicacion;
        $negocio->mapa = $request->mapa;

        if($request->has('web')){
            $negocio->web = $request->web;
        }else{
            $negocio->web = NULL;
        }
        $negocio->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del negocio',
            'load'  =>  true,
            'url'   =>  route('control_sc_negocios')
        ], 200);
    }

    public function eliminar_negocio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);                           //Encontrar el negocio a borrar
        File::delete($this->path.$negocio->imagen);                             //Borrar el logo del negocio
        $negocio->delete();                                                     //Borrar el negocio
        $calificaciones = Calificaciones::where('negocio', $id_negocio);        //Encontrar las calificaciones del negocio
        foreach($calificaciones->get() as $item)
        {
            Comentarios::destroy($item->comentario);      //Encontrar los comentarios sobre el negocio
        }
        $calificaciones->delete();                                              //Borrar las calificaciones
        $keysword = Keysword::where('negocio', $id_negocio)->delete();          //Encontrar las keyswords del negocio y borrarlas
        $galeria = GaleriaTrabajos::where('negocio', $id_negocio);              //Encontrar su galeria de trabajos
        foreach($galeria->get() as $item)
        {
            File::delete($this->path_galeria.$item->imagen);                    //Borra la imagen original
            File::delete($this->path_miniatura_movil.$item->imagen);            //Borrar la miniatura para dispositivos moviles
            File::delete($this->path_miniatura_web.$item->imagen);              //Borra la miniatura para web
        }
        $galeria->delete();                                                     //Borrar su galeria de trabajos
        $documento = Documentos::where('negocio', $id_negocio);                 //Encontrar sus documentos
        foreach($documento->get() as $item)
        {
            File::delete($this->path_documentos.$item->nombre);                 //Borrar el archivo
        }
        $documento->delete();                                                   //Borrar sus documentos
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el negocio',
            'dttable'   =>  "#dt_negocios",
        ], 200);
    }

    public function logo_negocio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        if($negocio->imagen == "0")
        {
            return view('forms.logo',[
                'form_edit'     =>false,
                'menu'          =>'negocios',
                'id'            =>$id_negocio
            ]);
        }else{
            return redirect()->route('editar_logo_negocio', $id_negocio);
        }
    }

    public function editar_logo_negocio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        if($negocio->imagen == "0")
        {
            return redirect()->route('logo_negocio', $id_negocio);
        }else{
            return view('forms.logo',[
                'form_edit'     =>true,
                'menu'          =>'negocios',
                'id'            =>$id_negocio,
                'negocio'       =>$negocio
            ]);
        }
    }

    public function guardar_logo_negocio(Request $request, $id_negocio)
    {
        $this->validate($request,[
            'imagen'        =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'imagen.required'   =>'Proporcione el logo del negocio',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $archivo = $request->file('imagen');
        $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
        $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital

        $negocio = Negocios::find($id_negocio);
        $negocio->imagen = $file_name;
        $negocio->save();
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha subido el logo del negocio correctamente',
            'load'  =>true,
            'url'   =>route('control_sc_negocios')
        ], 200);
    }

    public function actualizar_logo_negocio(Request $request, $id_negocio)
    {
        $this->validate($request,[
            'imagen'        =>'required|mimes:png,jpg,jpeg|max:10240'
        ],
        [
            'imagen.required'   =>'Proporcione el logo del negocio',
            'imagen.mimes'      =>'Solo se permiten imágenes PNG, JPG o JPEG',
            'imagen.max'        =>'El tamaño maximo del archivo es de 10MB',
        ]);
        if (!file_exists($this->path)) {
            File::makeDirectory($this->path, $mode = 0777, true, true);
        }
        $archivo = $request->file('imagen');
        $file_name = uniqid().".".$archivo->extension();    //Nombre del documento
        $archivo->move($this->path, $file_name);            //Se mueve el archivo a la carpeta del contenido digital

        $negocio = Negocios::find($id_negocio);
        File::delete($this->path.$negocio->imagen);         //Se borra el logo anterior
        $negocio->imagen = $file_name;
        $negocio->save();
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha actualizado el logo del negocio correctamente',
            'load'  =>true,
            'url'   =>route('control_sc_negocios')
        ], 200);
    }

    public function editar_keysword(Request $request, $id_negocio)
    {
        $keysword = Keysword::where('negocio', $id_negocio)->get();
        return view('forms.keysword',[
            'menu'      =>'negocios',
            'id'        =>$id_negocio,
            'keys'      =>$keysword
        ]);
    }

    public function guardar_keysword(Request $request, $id_negocio)
    {
        //Borrar las keys anteriores
        $keys = Keysword::where('negocio', $id_negocio)->delete();
        //Guardar las nuevas keys
        $data = implode(',', $request->except('_token', 'keysword'));   //Convierto el array en string
        $palabras = explode(',', $data);                                //Paso el string a array en una variable que pueda manejar
        $total = count($request->except('_token', 'keysword'));         //obtengo el total de datos recibidos
        if($total > 0)                              //Verifico que se enviaran etiquetas
        {
            for($i = 0 ; $i < $total; $i++)
            {
                Keysword::create([
                    'negocio'   => $id_negocio,
                    'key'       => $palabras[$i]
                ]);
            }
        }
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha guardado los cambios',
            'load'  =>true,
            'url'   =>route('control_sc_negocios')
        ], 200);
    }

    public function all_comentarios(Request $request)
    {
        return view('admin.comentarios');
    }

    public function comentarios(Request $request, $id_negocio)
    {
        return view('admin.comentarios', compact('id_negocio'));
    }

    public function status_negocio(Request $request)
    {
        $this->validate($request,[
            'id'   =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del negocio',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $negocio = Negocios::find($request->id);
        $negocio->status = $request->status;
        $negocio->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
    
    public function status_negocio_pago(Request $request)
    {
        $this->validate($request,[
            'id'   =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID del negocio',
            'status.required'   =>'Indique el status que desea asignar'
        ]);

        $negocio = Negocios::find($request->id);
        $negocio->pagos = $request->status;
        $negocio->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }

    //Usuarios
    public function usuarios()
    {
        return view('admin.usuarios');
    }

    //Categorias
    public function categorias()
    {
        return view('admin.categorias');
    }

    //Ubicaciones
    public function ubicaciones()
    {
        return view('admin.ubicaciones');
    }

    //Criterios de evaluación
    public function criterios()
    {
        return view('admin.criterios');
    }

    //Historial de pagos
    public function historial()
    {
        return view('admin.historial');
    }

    //Correos
    public function correos()
    {
        return view('admin.correos');
    }

    //Notificaciones
    public function notificaciones()
    {
        return view('admin.notificaciones');
    }

    //Precio
    public function precio()
    {
        $precio = Precios::find(1);
        return view('forms.precio', compact('precio'));
    }
    
    //Anuncios in APP
    public function anuncios()
    {
        $anuncio = Anuncios::find(1);
        return view('forms.anuncio', compact('anuncio'));
    }

    //Apartados Web
    public function slider_text()
    {
        return view('admin.slider_text');
    }

    //Testimoniales
    public function testimoniales()
    {
        return view('admin.testimoniales');
    }

    //Beneficios
    public function beneficios()
    {
        return view('admin.beneficios');
    }
    
    //Sucursales
    public function sucursales()
    {
        return view('admin.sucursales');
    }

    //Equipo
    public function equipo()
    {
        return view('admin.equipo');
    }

    //Información
    public function informacion()
    {
        $informacion = Contacto::find(1);
        return view('forms.informacion', compact('informacion'));
    }

    //Redes Sociales
    public function redes_sociales()
    {
        $redes = RedesSociales::orderby('orden')->get();
        return view('admin.redes', compact('redes'));
    }

    //Experiencia
    public function experiencia()
    {
        $experiencia = Experiencia::find(1);
        return view('forms.experiencia', compact('experiencia'));
    }

    //Habilidades
    public function habilidades()
    {
        return view('admin.habilidades');
    }

    //Acerca de
    public function about()
    {
        $acerca = About::find(1);
        return view('forms.about', compact('acerca'));
    }

    //Aviso de privacidad
    public function privacy()
    {
        $legal = Legales::find(1);
        return view('forms.legal', compact('legal'));
    }

    //Terminos y condiciones
    public function terms()
    {
        $legal = Legales::find(2);
        return view('forms.legal', compact('legal'));
    }

    public function password_reset()
    {
        return view('forms.password_reset');
    }
    
    public function actualizar_password(Request $request)
    {
        $this->validate($request,[
            'current_password'      =>'required',
            'password'              =>'required|min:8|confirmed',
            'password_confirmation' =>'required'
        ],
        [
            'current_password.required'         =>'Proporcione la contraseña actual. ',
            'password.required'                 =>'Indique la nueva contraseña. ',
            'password.min'                      =>'La contraseña debe de tener mínimo 8 caracteres. ',
            'password.confirmed'                =>'La confirmación de la contraseña no coincide. ',
            'password_confirmation.required'    =>'Confirme la nueva contraseña. ',
        ]);

        if (Hash::check($request->current_password, Auth::user()->password)){
            User::where('email', Auth::user()->email)->update(['password' => Hash::make($request->password)]);
            Auth::logout();
            return response()->json([
                'status'    => 'true',
                'type'      => 'success',
                'title'     => 'Éxito',
                'text'      => 'Se han actualizado la contraseña',
                'reload'    => true,
            ], 200);
        }else{
             //La contraseña es incorrecta
            $error['current_password']='La contraseña es incorrecta';
            return response()->json(['errors' => $error], 422);
        }
    }

    public function actualizar_precio(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'costo'     =>'required|numeric|min:1',
            'moneda'    =>'required',
            'meses'     =>'required|numeric|min:0'
        ],
        [
            'nombre.required'   =>'Proporcione el concepto de pago',
            'costo.required'    =>'Proporcione el monto a pagar',
            'costo.numeric'     =>'Proporcione un monto valido (Solo se aceptan números)',
            'costo.min'         =>'Proporcione un monto valido (La cantidad mínima es de $1)',
            'moneda.required'   =>'Proporcione el tipo de moneda',
            'meses.required'    =>'Proporcione la cantidad de meses gratuitos',
            'meses.numeric'     =>'Proporcione una cantidad valida (Solo se aceptan números)',
            'meses.min'         =>'Proporcione un cantidad valida (La cantidad mínima es de $1)',
        ]);

        Precios::where('id', 1)->update([
            'nombre'    => $request->nombre, 
            'costo'     => ($request->costo * 100), 'moneda' => $request->moneda,
            'gratis'    => $request->meses, 
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los cambios'
        ], 200);
    }

    public function recordatorios($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);

        return view('admin.recordatorios',[
            'id'        => $id_negocio,
            'negocio'   => $negocio
        ]);
    }

    public function datatable_recordatorios($id_negocio)
    {
        $recordatorios = Recordatorios::where('negocio', $id_negocio)->get();
        
        return DataTables::of($recordatorios)
        ->addColumn('contenido', function($recordatorios){
            return $recordatorios->contenido;
        })
        ->addColumn('fecha', function($recordatorios){
            $fecha = Carbon::createFromFormat('Y-m-d H:i:s', $recordatorios->created_at);
            return "<span style='display: none;'>".$recordatorios->created_at." </span><span>".$fecha->day." de ".$fecha->monthName." del ".$fecha->year." a las ".$fecha->format('g:i A')."</span>";
        })
        ->rawColumns(['contenido', 'fecha'])
        ->toJson();
    }

    public function crear_recordatorio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        return view('forms.recordatorios',[
            'id'        => $id_negocio,
            'negocio'   => $negocio
        ]);
    }

    public function enviar_recordatorio(Request $request, $id_negocio)
    {
        $this->validate($request,[
            'mensaje'    =>'required',
        ],
        [
            'mensaje.required'   =>'Indique el contenido del recordatorio',
        ]);

        //Guardar el recordatorio
        Recordatorios::create([
            'negocio'       => $id_negocio,
            'contenido'     => $request->mensaje,
        ]);
        //Enviar el recordatorio por correo
        $correos = User::select('email')->where('negocio', $id_negocio)->get();
        try{
            Mail::to($correos)->send(new Notificacion("Recordatorio", $request->mensaje));
        }catch(\Exception $ex){}
        
        return response()->json([
            'status'=>  true,
            'title' =>  'Éxito',
            'text'  =>  'Se ha enviado el recordatorio al negocio',
            'load'  =>  true,
            'url'   =>  route('control_sc_recordatorios_negocios', $id_negocio)
        ], 200);
    }

    //Volver (cuando las rutas estan mal)
    public function regresar()
    {
        return redirect()->route('control_sc');
    }

    //Volver (cuando las rutas en negocios estan mal)
    public function volver()
    {
        return redirect()->route('control_sc_negocios');
    }
}
