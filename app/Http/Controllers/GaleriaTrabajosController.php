<?php

namespace App\Http\Controllers;

use File;
use Image;
use App\Models\Negocios;
use Illuminate\Http\Request;
use App\Models\GaleriaTrabajos;

class GaleriaTrabajosController extends Controller
{
    private $path_galeria = "images/trabajos/";
    private $path_miniatura_movil = "images/trabajos/miniaturas/movil/";
    private $path_miniatura_web = "images/trabajos/miniaturas/web/";

    public function editar($id_negocio)
    {
        $galeria = GaleriaTrabajos::where('negocio', $id_negocio)->orderBy('orden')->get();
        if($galeria->count() == 0)
        {
            return redirect()->route('control_sc_negocios');
        }else{
            return view('forms.galeria',[
                'form_edit'     =>true,
                'menu'          =>'negocios',
                'id'            =>$id_negocio,
                'galeria'       =>$galeria
            ]);
        }
    }

    public function guardar(Request $request)
    {
        //$request->validate(['file' => 'image|dimensions:width=1900, height=600']);
        $request->validate(['file' => 'image|mimes:png,jpg,jpeg|max:10240']);
        if (!file_exists($this->path_galeria)) {
            File::makeDirectory($this->path_galeria, $mode = 0777, true, true);
        }
        $img = $request->file('file');
        $file_name = uniqid().".".$img->extension();    //Nombre de la imagen
        //Miniatura Movil
        if (!file_exists($this->path_miniatura_movil)) {
            File::makeDirectory($this->path_miniatura_movil, $mode = 0777, true, true);
        }
        $miniatura_movil = Image::make($img);
        $miniatura_movil->resize(440, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $miniatura_movil->save($this->path_miniatura_movil.$file_name);
        //Miniatura Web
        if (!file_exists($this->path_miniatura_web)) {
            File::makeDirectory($this->path_miniatura_web, $mode = 0777, true, true);
        }
        $miniatura_web = Image::make($img);
        $miniatura_web->resize(370, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $miniatura_web->save($this->path_miniatura_web.$file_name);
        //Guardo la imagen original
        $img->move($this->path_galeria, $file_name);
        //Crear el registro de la imagen
        $item = GaleriaTrabajos::create(['negocio' => $request->id_negocio, 'imagen' => $file_name]);
		$view = view('partials.galeria_items', ['item' => $item, 'path' => $this->path_galeria])->render();
		return response()->json([
			'status' => 'success',
			'view' => $view
		], 200);
    }

    public function ordenar(Request $request)
    {
        foreach ($request->order as $key => $value) {
            GaleriaTrabajos::where('id', $value)->update(['orden' => $key]);
		}
        return response()->json([
			'status'    => 'success',
			'text'      => 'Orden guardado'
		], 200);
    }
    
    public function eliminar(Request $request)
    {
        $galeria = GaleriaTrabajos::find($request->id_imagen);
        File::delete($this->path_galeria.$galeria->imagen);
        File::delete($this->path_miniatura_movil.$galeria->imagen);
        File::delete($this->path_miniatura_web.$galeria->imagen);
        $galeria->delete();
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha eliminado la imagen de la galeria',
            'reload'=>'true'
        ], 200);
    }
}
