<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::Cliente;

    public function authenticated()
    {
        if(Auth::user()->hasRole('administrador')){
            return redirect(RouteServiceProvider::Admin);
        }else if(Auth::user()->hasRole('negocio')){
            return redirect(RouteServiceProvider::Negocio);
        }else{
            return redirect(RouteServiceProvider::Cliente);
        }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'user'                  => 'required',
            'password'              => 'required',
        ], [
            'user.required'         => 'Proporcione un correo o número telefónico',
            'password.required'     => 'Proporcione su contraseña'
        ]);

        $tipo = filter_var($request->user, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $credentials = [
            $tipo       => $request->user,
            "password"  => $request->password
        ];

        if(Auth::attempt($credentials)){
            if(Auth::user()->hasRole('negocio')){
                return redirect(RouteServiceProvider::Negocio);
            }else{
                return redirect(RouteServiceProvider::Cliente);
            }
        }else{
            return redirect()->route('home')->withErrors(['Verifique sus datos de acceso.']);
        }
    }
}
