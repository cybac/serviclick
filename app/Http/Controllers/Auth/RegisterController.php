<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Alertas;
use App\Models\Correos;
use App\Models\Negocios;
use App\Mail\Bienvenido;
use App\Models\Documentos;
use App\Models\Seguimiento;
use App\Models\Tipos_Documentos;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        if($data['alta'] == 2){
            return Validator::make($data, [
                'empresa'                   => ['required', 'string', 'max:255'],
                'nombre'                    => ['required', 'string', 'max:255'],
                'email'                     => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone'                     => ['required', 'numeric', 'digits:10', 'unique:users'],
                'password'                  => ['required', 'string', 'min:8'],
                'captcha'                   => ['required', 'captcha'],
            ], [
                'empresa.required'		    =>'Proporcione el nombre de la empresa',
                'nombre.required'		    =>'Proporcione su nombre completo',
                'email.required'		    =>'Proporcione su correo electrónico',
                'email.email'         	    =>'El correo no tiene un formato válido',
                'email.unique'			    =>'Este correo ya está en uso, intente con otro',
                'phone.required'		    =>'Proporcione su número telefónico',
				'phone.unique'			    =>'Este número telefónico ya está en uso, intente con otro',
                'phone.min'					=>'Proporcione los 10 dígitos de su teléfono',
                'password.required'		    =>'Proporcione una contraseña para la cuenta',
                'password.min'              =>'La contraseña debe contener por lo menos 8 caracteres',
                'captcha.required'		    =>'Indique el captcha',
                'captcha.captcha'		    =>'El captcha es incorrecto'
            ]);
        } else{
            return Validator::make($data, [
                'nombre'                    => ['required', 'string', 'max:255'],
                'email'                     => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone'                     => ['required', 'numeric', 'digits:10', 'unique:users'],
                'password'                  => ['required', 'string', 'min:8'],
                'captcha'                   => ['required', 'captcha'],
            ], [
                'nombre.required'		    =>'Proporcione su nombre completo',
                'email.required'		    =>'Proporcione su correo electrónico',
                'email.email'         	    =>'El correo no tiene un formato válido',
                'email.unique'			    =>'Este correo ya está en uso, intente con otro',
                'password.required'		    =>'Proporcione una contraseña para la cuenta',
                'password.min'              =>'La contraseña debe contener por lo menos 8 caracteres',
                'captcha.required'		    =>'Indique el captcha',
                'captcha.captcha'		    =>'El captcha es incorrecto'
            ]);
        }
    }

    protected function create(array $data)
    {
        if($data['alta'] == 2)
        {
            $negocio = Negocios::create([
                'nombre'        => $data['empresa'],
                'direccion'     => 'pendiente',
                'descripcion'   => 'pendiente',
                'servicios'     => 'pendiente',
            ]);
            $user =  User::create([
                'name'          => $data['nombre'],
                'email'         => $data['email'],
                'phone'         => $data['phone'],
                'negocio'       => $negocio->id,
                'password'      => Hash::make($data['password']),
            ]);
            $user->assignRole('negocio');

            Seguimiento::create([
                'negocio'       =>$negocio->id,
                'tramite'       =>1,
                'aprobado'      =>true,
                'status'        =>true
            ]);
            Seguimiento::create([
                'negocio'       =>$negocio->id,
                'tramite'       =>2,
            ]);
            Seguimiento::create([
                'negocio'       =>$negocio->id,
                'tramite'       =>3,
            ]);
            Seguimiento::create([
                'negocio'       =>$negocio->id,
                'tramite'       =>4,
            ]);
            $documentacion = Tipos_Documentos::where('status', true)->get();
            foreach($documentacion as $item)
            {
                Documentos::create([
                    'negocio'   =>$negocio->id,
                    'tipo'      =>$item->id
                ]);
            }

            Alertas::create([
                'titulo'        =>'Registro de negocio',
                'contenido'     =>'El negocio '.$data['empresa'].' se ha registrado.',
                'importancia'   =>'bg-primary',
                'icono'         =>'mdi mdi-store',
                'url'           => route('control_sc_negocios_tramites'),
            ]);
            
            $correo = Correos::find(2);
            try{
                Mail::to($data['email'])->send(new Bienvenido($correo->titulo, $correo->mensaje));
            }catch(\Exception $ex){ }

            $this->redirectTo = RouteServiceProvider::Negocio;
            return $user;
        }else{
            $user =  User::create([
                'name'          => $data['nombre'],
                'email'         => $data['email'],
                'phone'         => $data['phone'],
                'password'      => Hash::make($data['password']),
            ]);
            $user->assignRole('cliente');

            $correo = Correos::find(1);
            try{
                Mail::to($data['email'])->send(new Bienvenido($correo->titulo, $correo->mensaje));
            }catch(\Exception $ex){ }

            $this->redirectTo = RouteServiceProvider::Cliente;
            return $user;
        }
    }
}
