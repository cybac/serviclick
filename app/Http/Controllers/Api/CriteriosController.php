<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Criterios;
use App\Http\Controllers\Controller;

class CriteriosController extends Controller
{
    public function obtener()
    {
        try {
            $criterios = Criterios::select('nombre')->where('status', true)->get();
            return response()->json([
                'result'	=> $criterios,
                'message'   => NULL,
                'count'     => $criterios->count()
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }
}
