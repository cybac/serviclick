<?php

namespace App\Http\Controllers\Api;

use DB;
use File;
use Image;
use App\Models\User;
use App\Models\Pagos;
use App\Models\Negocios;
use App\Models\Criterios;
use App\Models\Categorias;
use App\Models\Ubicaciones;
use App\Models\Comentarios;
use App\Models\Seguimiento;
use Illuminate\Http\Request;
use App\Models\DetallesPagos;
use App\Models\Calificaciones;
use App\Models\Notificaciones;
use App\Conekta\ConektaPayment;
use App\Models\GaleriaTrabajos;
use App\Models\Tipos_Notificaciones;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\RedesSocialesNegocios;
use Illuminate\Support\Facades\Validator;

use App\GeoDistancia\GeoDistancia;

class NegociosController extends Controller
{
    protected $ConektaPayment;
    private $recurso_negocio;
    private $path_negocios = "images/negocios/";
    private $path_galeria = "images/trabajos/";
    private $path_miniatura_web = "images/trabajos/miniaturas/web/";
    private $path_miniatura_movil = "images/trabajos/miniaturas/movil/";

    function __construct()
    {
        $this->ConektaPayment = new ConektaPayment();
        $this->recurso_negocio = asset($this->path_negocios);
    }

    public function mis_datos()
    {
        try {

            /*
            Old versión
            $negocio = Negocios::select('id', 
            DB::raw("CASE WHEN imagen != '0' THEN concat('".asset($this->path_negocios)."', '/', imagen) ELSE concat('".asset($this->path_negocios)."', '/0.png') END as imagen"), 
            'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 
            'categoria as categoria_id', DB::raw("(select nombre from categorias where id=categoria) as categoria"), 
            'ubicacion as ubicacion_id', DB::raw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion"),
            'mapa',
            DB::raw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN '0' ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion"),
            'status')->where('id', Auth::user()->negocio)->get(); 
            */

            //Nuevo Codigo
            $negocio = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max','categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
            ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/', imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
            ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
            ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
            ->selectRaw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN 0 ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion")
            ->where('id', Auth::user()->negocio)->get();

			return response()->json([
				'result'	=>$negocio,
				'message'   =>null,
				'count'     =>1
			]);
		} catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function index()
    {
        /* 
        OLD CODIGO
        $negocios = Negocios::select('id', 
            DB::raw("CASE WHEN imagen != '0' THEN concat('".asset($this->path_negocios)."', '/',imagen) ELSE concat('".asset($this->path_negocios)."', '/0.png') END as imagen"), 
            'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 
            DB::raw("(select nombre from categorias where id=categoria) as categoria"), 'ubicacion as ubicacion_id', 
            DB::raw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion"), 'mapa',
            DB::raw("(SELECT CASE WHEN (SUM(puntuacion)/COUNT(*)) IS NULL THEN '0' ELSE (SUM(puntuacion)/COUNT(*)) END AS puntuacion FROM calificaciones WHERE negocio = negocios.id) as puntuacion"), 'status')
            ->orderby('puntuacion','desc')->get();
        */

        //PARA LA NUEVA API
        $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
            ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
            ->selectRaw("(SELECT nombre FROM categorias WHERE id=categoria) as categoria")
            ->selectRaw("(SELECT nombre FROM ubicaciones WHERE id = negocios.ubicacion) as ubicacion")
            ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
            ->where("status", true)->orderby('puntuacion', 'desc')->get();

        return response()->json([
            'result'    => [
                'negocios' => $negocios
            ],
            'message'   => NULL,
            'count'     => $negocios->count()
        ]);
    }

    public function redes_negocio(Request $request)
    {
        $validator = Validator::make($request->all(), ['negocio' => 'required'],['negocio.required' => 'El ID del negocio es requerido']);
		if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try{
            $redes = RedesSocialesNegocios::select('redes_sociales.nombre', 'redes_sociales_negocios.url')
                ->join('redes_sociales', 'redes_sociales_negocios.red_social', 'redes_sociales.id')
                ->where('negocio', $request->negocio)->get();
            return response()->json([
                'result'        => $redes,
                'message'	    => NULL,
                'count'         => $redes->count()
            ]);
        }catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function comentarios_negocio(Request $request)
    {
        $validator = Validator::make($request->all(), ['negocio' => 'required'],['negocio.required' => 'El ID del negocio es requerido']);
		if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try{
            $comentarios = Comentarios::select("nombre", "contenido", "puntuacion", "created_at")->wherein('id', Calificaciones::select('comentario as id')->where('negocio', $request->negocio)->groupby('comentario')->get())->where('status', true)->orderby('created_at', 'desc')->get();
            return response()->json([
                'result'        => $comentarios,
                'message'	    => NULL,
                'count'         => $comentarios->count()
            ]);
        }catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function ficha(Request $request)
    {
        $pago = DetallesPagos::select('pagos.OrderID as id')
            ->join('pagos','detalles_pagos.pago','pagos.OrderID')
            ->where('negocio', Auth::user()->negocio)
            ->whereNull('pagos.OrderUpdateDate')->first();
        if(empty($pago))
        {
            return response()->json([
                'result'	=>NULL,
                'message'   =>'Sin ficha disponible por el momento',
                'count'     =>0
            ]);
        }else{
            $pago = Pagos::where('OrderID', $pago->id)->first();
            $pago = $this->ConektaPayment->ObtenerOrden($pago->OrderPaymentMethodID);

            return response()->json([
                'result'	=>[
                    'monto'         =>($pago->amount / 100),
                    'moneda'        =>$pago->currency,
                    'referencia'    =>$pago->charges[0]->payment_method->reference
                ],
                'message'   =>'Ficha de pago',
                'count'     =>1
            ]);
        }
    }
    
    public function historial_pagos(Request $request)
    {
        try{
            $historial = DetallesPagos::select('detalles_pagos.proximo', 'pagos.OrderUpdateDate as pagado')
                ->selectRaw("CONCAT('$', (pagos.OrderTotal / 100)) as total")
                ->selectRaw("CASE WHEN pagos.OrderStatus = 'paid' THEN 'Pagado' WHEN pagos.OrderStatus = 'pending_payment' THEN 'Pendiente de pago' WHEN pagos.OrderStatus = 'expired' THEN 'Expiró' ELSE 'Desconocido' END as status")
                ->join('pagos', 'detalles_pagos.pago', 'pagos.OrderID')
                ->where('negocio', Auth::user()->negocio)
                ->get();
			return response()->json([
				'result'	=>$historial,
				'message'   =>null,
				'count'     =>$historial->count()
			]);
		} catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function tramite_informacion()
    {
        try{
			$tramite = Seguimiento::select('comentarios', 'aprobado', 'status')
                ->where('negocio', Auth::user()->negocio)
                ->where('tramite', 2)->first();
			return response()->json([
				'result'	=>$tramite,
				'message'   =>null,
				'count'     =>1
			]);
		} catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function actualizar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required',
            'direccion'     => 'required',
            'telefono'      => 'required',
            'descripcion'   => 'required',
            'servicios'     => 'required',
            'precio_min'    => 'required',
            'precio_max'    => 'required',
            'categoria'     => 'required',
            'ubicacion'     => 'required',
            'mapa'          => 'required',
        ],[
            'nombre.required'       => 'Indique el nombre del negocio',
            'direccion.required'    => 'Indique la direcció del negocio',
            'telefono.required'     => 'Indique un teléfono de contacto',
            'descripcion.required'  =>'Indique la descripción del negocio',
            'servicios.required'    =>'Indique el listado de servicios del negocio',
            'precio_min.required'   =>'Indique el precio mínimo de cobro',
            'precio_max.required'   =>'Indique el precio máximo de cobro',
            'categoria.required'    =>'Indique el ID de la categoria',
            'ubicacion.required'    =>'Indique el ID de la ubicación',
            'mapa.required'         =>'Indique las coordenada del negocio en el mapa',
        ]);
		if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try{
			$negocio = Negocios::find(Auth::user()->negocio);
            $negocio->nombre        = $request->nombre;
            $negocio->direccion     = $request->direccion;
            $negocio->telefono      = $request->telefono;
            $negocio->descripcion   = $request->descripcion;
            $negocio->servicios     = $request->servicios;
            $negocio->precio_min    = $request->precio_min;
            $negocio->precio_max    = $request->precio_max;
            $negocio->categoria     = $request->categoria;
            $negocio->ubicacion     = $request->ubicacion;
            $negocio->mapa          = $request->mapa;
            if($request->filled('web')){
                $negocio->web = $request->web;
            }else{
                $negocio->web = NULL;
            }
            $negocio->save();

			return response()->json([
				'result'	=>true,
				'message'   =>'Se ha actualizado los datos del negocio',
				'count'     =>1
			]);
		}
		catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function actualizar_logo(Request $request)
    {
        $validator = Validator::make($request->all(), ['logo' => 'required'], ['logo.required' => 'Proporcione el logo del negocio']);
        if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try
        {
            if (!file_exists($this->path_negocios)) {
                File::makeDirectory($this->path_negocios, $mode = 0777, true, true);
            }
            $negocio = Negocios::find(Auth::user()->negocio);
            File::delete($this->path_negocios.$negocio->imagen);
			//inicia el guardado de imagen
			$file_name = uniqid().".png";
			$ifp = fopen($this->path_negocios.$file_name, 'wb' ); 
			$data = explode( ',', $request->logo );
			fwrite($ifp, base64_decode($data[0]));
			fclose($ifp);
			//fin del guardado de imagen
            $negocio->imagen = $file_name;
            $negocio->save();

            return response()->json([
                'result'	=>true,
                'message'   =>NULL,
                'count'     =>1
            ]);

        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function galeria_negocio(Request $request)
    {
        $validator = Validator::make($request->all(), ['negocio' => 'required'],['negocio.required' => 'El ID del negocio es requerido']);
		if ($validator->fails()) {
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}
        try{
            $recurso = asset($this->path_galeria);
            $recurso_preview = asset($this->path_miniatura_web);
            $galeria = GaleriaTrabajos::select("id")->selectraw("concat('$recurso', '/', imagen) as imagen")
                ->selectraw("concat('$recurso', '/', imagen) as imagen")
                ->selectraw("concat('$recurso_preview', '/', imagen) as preview")
                ->where('negocio', $request->negocio)->orderBy('orden')->get();
            return response()->json([
                'result'	=> $galeria,
                'message'   => NULL,
                'count'     => $galeria->count()
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function listar_galeria()
    {
        try{
            $recurso = asset($this->path_galeria);
            $recurso_preview = asset($this->path_miniatura_web);
            $galeria = GaleriaTrabajos::select("id")->selectraw("concat('$recurso', '/', imagen) as imagen")
                ->selectraw("concat('$recurso', '/', imagen) as imagen")
                ->selectraw("concat('$recurso_preview', '/', imagen) as preview")
                ->where('negocio', Auth::user()->negocio)->orderBy('orden')->get();
            return response()->json([
                'result'	=> $galeria,
                'message'   => NULL,
                'count'     => $galeria->count()
            ]);
        }catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function borrar_foto_galeria(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto'       => 'required|exists:galeria_trabajos,id',
        ], [
            'foto.required'   => 'El ID del la imagen es requerido',
            'foto.exists'     => 'Esta imagen no existe',
        ]);
        if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try {
            $foto = GaleriaTrabajos::findOrFail($request->foto);
            if($foto->negocio == Auth::user()->negocio) {
                File::delete($this->path_galeria.$foto->imagen);
                File::delete($this->path_miniatura_movil.$foto->imagen);
                File::delete($this->path_miniatura_web.$foto->imagen);
                $foto->delete();
                return response()->json([
                    'result'	=>true,
                    'message'   =>NULL,
                    'count'     =>1
                ]);
            }else{
                return response([
                    'result'    => null,
                    'message'   => ["No es dueño de la imagen que desea borrar"],
                    'count'     => 0
                ], 422);
            }
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function subir_galeria(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto'       => 'required|array|min:1',
        ], [
            'foto.required'     => 'Tiene que indicar la(s) imagen(es) a subir',
            'foto.array'        => 'Formato de subida no valido',
            'foto.min'          => 'Necesita indicar minimo 1 archivo de imagen',
        ]);
        if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try {
            foreach($request->foto as $item) {
                //inicia el guardado de imagen
                $file_name = uniqid().".png";
                $ifp = fopen($this->path_galeria.$file_name, 'wb' );        //Guardo la imagen original
                $data = explode( ',', $item);
                fwrite($ifp, base64_decode($data[0]));
                fclose($ifp);
                //fin del guardado de imagen

                $miniatura_movil = Image::make($this->path_galeria.$file_name);
                $miniatura_movil->resize(440, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $miniatura_movil->save($this->path_miniatura_movil.$file_name);
                
                //Miniatura Web
                $miniatura_web = Image::make($this->path_galeria.$file_name);
                $miniatura_web->resize(370, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $miniatura_web->save($this->path_miniatura_web.$file_name);
                
                $orden = GaleriaTrabajos::selectraw("CASE WHEN COUNT(*) > 0 THEN (max(orden) + 1) ELSE 0 END as ultimo")
                    ->where("negocio", Auth::user()->negocio)->first();

                GaleriaTrabajos::create([
                    'negocio'       => Auth::user()->negocio,
                    'imagen'        => $file_name,
                    "orden"         => $orden->ultimo
                ]);
            }
            return response()->json([
                'result'	=>true,
                'message'   =>NULL,
                'count'     =>1
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function buscar_temp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pagina'                => 'required|integer|min:1',
            'ubicacion'             => 'nullable|integer',
            'categoria'             => 'nullable|integer|min:2'
        ],[
            'pagina.required'       => 'indique la página a mostrar',
            'pagina.integer'        => 'Solo indique numeros enteros para la página a mostrar',
            'pagina.min'            => 'La página ménima es 1',
        ]);

		if ($validator->fails()){
            return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try {
            if($request->filled('categoria') && $request->filled('ubicacion')){
                //Buscar por los 2 criterios (categoria y ubicación)
                Ubicaciones::findOrFail($request->ubicacion);
                Categorias::findOrFail($request->categoria);
                if($request->categoria > 2){
                    //Usar los 2 criterios de busqueda
                    $total = Negocios::where([['negocios.categoria', $request->categoria],['negocios.ubicacion', $request->ubicacion],['negocios.status', true]])->count();
                    $paginas = ceil($total / 20);
                    if($request->pagina > 1 && $request->pagina > $paginas){
                        return response([
                            'result'    =>null,
                            'message'   =>"La página indicada no existe",
                            'count'     =>0
                        ], 422);
                    }
                    $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                        ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen") 
                        ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                        ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                        ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                        ->where([
                            ['negocios.categoria', $request->categoria],
                            ['negocios.ubicacion', $request->ubicacion],
                            ['negocios.status', true],
                        ])
                        ->skip(($request->pagina - 1) * 20)
                        ->take(20)
                        ->get();

                    return response()->json([
                        'result'	=>[
                            "negocios"      => $negocios,
                            "total"         => $paginas
                        ],
                        'message'           => NULL,
                        'count'             => $negocios->count()
                    ]);
                }else{
                    //Buscar solo por ubicación
                    $total = Negocios::where([['negocios.ubicacion', $request->ubicacion],['negocios.status', true]])->count();
                    $paginas = ceil($total / 20);
                    if($request->pagina > 1 && $request->pagina > $paginas){
                        return response([
                            'result'    =>null,
                            'message'   =>"La página indicada no existe",
                            'count'     =>0
                        ], 422);
                    }
                    $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                        ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                        ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                        ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                        ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                        ->where([
                            ['negocios.ubicacion', $request->ubicacion],
                            ['negocios.status', true]
                        ])
                        ->skip(($request->pagina - 1) * 20)
                        ->take(20)
                        ->get();
                    return response()->json([
                        'result'	=>[
                            "negocios"      => $negocios,
                            "total"         => $paginas
                        ],
                        'message'           => NULL,
                        'count'             => $negocios->count()
                    ]);
                }
                
            } else if($request->filled('categoria')){
                //Buscar por categoria
                Categorias::findOrFail($request->categoria);
                if($request->categoria > 2){
                    $total = Negocios::where([['negocios.categoria', $request->categoria],['negocios.status', true]])->count();
                    $paginas = ceil($total / 20);
                    if($request->pagina > 1 && $request->pagina > $paginas){
                        return response([
                            'result'    =>null,
                            'message'   =>"La página indicada no existe",
                            'count'     =>0
                        ], 422);
                    }
                    $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                        ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                        ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                        ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                        ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                        ->where([
                            ['negocios.categoria', $request->categoria],
                            ['negocios.status', true]
                        ])
                        ->skip(($request->pagina - 1) * 20)
                        ->take(20)
                        ->get();
                    return response()->json([
                        'result'	=>[
                            "negocios"      => $negocios,
                            "total"         => $paginas
                        ],
                        'message'           => NULL,
                        'count'             => $negocios->count()
                    ]);
                }else{
                    return response([
                        'result'    =>null,
                        'message'   =>'Proporcione una categoria valida',
                        'count'     =>0
                    ], 422);
                }
            } else if($request->filled('ubicacion')){
                //Buscar por ubicación)
                Ubicaciones::findOrFail($request->ubicacion);
                $total = Negocios::where([['negocios.ubicacion', $request->ubicacion],['negocios.status', true]])->count();
                $paginas = ceil($total / 20);
                if($request->pagina > 1 && $request->pagina > $paginas){
                    return response([
                        'result'    =>null,
                        'message'   =>"La página indicada no existe",
                        'count'     =>0
                    ], 422);
                }
                $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                    ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                    ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                    ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                    ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                    ->where([
                        ['negocios.ubicacion', $request->ubicacion],
                        ['negocios.status', true]
                    ])
                    ->skip(($request->pagina - 1) * 20)
                    ->take(20)
                    ->get();
                return response()->json([
                    'result'	=>[
                        "negocios"      => $negocios,
                        "total"         => $paginas
                    ],
                    'message'           => NULL,
                    'count'             => $negocios->count()
                ]);
            } else{
                return response([
                    'result'    =>null,
                    'message'   =>'Proporcione un criterio de busqueda',
                    'count'     =>0
                ], 422);
            }
        } catch (\Throwable $th) {
            return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
        }
    }

    public function buscar(Request $request)
    {
        if($request->filled('categoria') && $request->filled('ubicacion'))
        {
            //Buscar por los 2 criterios (categoria y ubicación)
            $existe = Ubicaciones::find($request->ubicacion);
            if(!empty($existe))
            {
                $existecat = Categorias::find($request->categoria);
                if(!empty($existecat) && $request->categoria > 1)
                {
                    if($request->categoria > 2)
                    {
                        //Usar los 2 criterios de busqueda
                        $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                            ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen") 
                            ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                            ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                            ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                            ->where([
                                ['negocios.categoria', $request->categoria],
                                ['negocios.ubicacion', $request->ubicacion],
                                ['negocios.status', true],
                            ])->get();
                        return response()->json([
                            'result'	=>$negocios,
                            'message'   =>NULL,
                            'count'     =>$negocios->count()
                        ]);
                    }else{
                        //Buscar solo por ubicación
                        $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                            ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                            ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                            ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                            ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                            ->where([
                                ['negocios.ubicacion', $request->ubicacion],
                                ['negocios.status', true]
                            ])->get();
                        return response()->json([
                            'result'	=>$negocios,
                            'message'   =>NULL,
                            'count'     =>$negocios->count()
                        ]);
                    }
                }else{
                    return response([
                        'result'    =>null,
                        'message'   =>'proporcione una categoria valida',
                        'count'     =>0
                    ], 422);
                }
            }else{
                return response([
                    'result'    =>null,
                    'message'   =>'proporcione una ubicación valida',
                    'count'     =>0
                ], 422);
            }
        }else if($request->filled('categoria')) {
            //Buscar por categoria
            $existe = Categorias::find($request->categoria);
            if(!empty($existe) && $request->categoria > 2)
            {
                $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                    ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                    ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                    ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                    ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                    ->where([
                        ['negocios.categoria', $request->categoria],
                        ['negocios.status', true]
                    ])->get();
                return response()->json([
                    'result'	=>$negocios,
                    'message'   =>NULL,
                    'count'     =>$negocios->count()
                ]);
            }else{
                return response([
                    'result'    =>null,
                    'message'   =>'proporcione una categoria valida',
                    'count'     =>0
                ], 422);
            }
        }else if($request->filled('ubicacion')){
            //Buscar por ubicación)
            $existe = Ubicaciones::find($request->ubicacion);
            if(!empty($existe)) {
                $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                    ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                    ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                    ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                    ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                    ->where([
                        ['negocios.ubicacion', $request->ubicacion],
                        ['negocios.status', true]
                    ])->get();
                return response()->json([
                    'result'	=>$negocios,
                    'message'   =>NULL,
                    'count'     =>$negocios->count()
                ]);
            }else{
                return response([
                    'result'    => null,
                    'message'   => 'proporcione una ubicación valida',
                    'count'     => 0
                ], 422);
            }
        }else{
            return response([
                'result'    =>null,
                'message'   =>'proporcione un criterio de busqueda',
                'count'     =>0
            ], 422);
        }
    }

    public function buscar_cercanos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pagina'                => 'required|integer|min:1',
            'latitud'               => 'required|numeric',
            'longitud'              => 'required|numeric',
            'ubicacion'             => 'required|array|min:1|max:2',
            'ubicacion.*'           => 'required|integer',
            'categoria'             => 'nullable|integer'
        ],[
            'pagina.required'       => 'indique la página a mostrar',
            'pagina.integer'        => 'Solo indique numeros enteros para la página a mostrar',
            'pagina.min'            => 'La página ménima es 1',
            'latitud.required'      => 'indique la latitud del usuario',
            'longitud.required'     => 'indique la longitud del usuario',
            'ubicacion.required'    => 'indique la(s) ubicacion(es) del usuario',
            'ubicacion.min'         => 'indique mínimamente 1 ubicación',
            'ubicacion.max'         => 'indique como máximo 2 ubicaciones'
        ]);

        
        
		if ($validator->fails()){
            return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try {
            //Existen las ubicaciones
            foreach ($request->ubicacion as $item) {
                Ubicaciones::findOrFail($item);
            }

            if($request->filled("categoria") && $request->categoria > 2){
                Categorias::findOrFail($request->categoria);

                $total = Negocios::whereIn('negocios.ubicacion', $request->ubicacion)->where([['negocios.categoria', $request->categoria],['negocios.status', true]])->count();
                $paginas = ceil($total / 20);
                if($request->pagina > 1 && $request->pagina > $paginas){
                    return response([
                        'result'    =>null,
                        'message'   =>"La página indicada no existe",
                        'count'     =>0
                    ], 422);
                }
                $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                    ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen") 
                    ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                    ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                    ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                    ->whereIn('negocios.ubicacion', $request->ubicacion)
                    ->where([
                        ['negocios.categoria', $request->categoria],
                        ['negocios.status', true]
                    ])
                    ->skip(($request->pagina - 1) * 20)
                    ->take(20)
                    ->get();
            }else{
                $total = Negocios::whereIn('negocios.ubicacion', $request->ubicacion)->where('negocios.status', true)->count();
                $paginas = ceil($total / 20);
                if($request->pagina > 1 && $request->pagina > $paginas){
                    return response([
                        'result'    =>null,
                        'message'   =>"La página indicada no existe",
                        'count'     =>0
                    ], 422);
                }
                $negocios = Negocios::select('id', 'nombre', 'direccion', 'telefono', 'web', 'descripcion', 'servicios', 'precio_min', 'precio_max', 'categoria as categoria_id', 'ubicacion as ubicacion_id', 'mapa','status')
                    ->selectRaw("CASE WHEN imagen != '0' THEN concat('$this->recurso_negocio', '/',imagen) ELSE concat('$this->recurso_negocio', '/0.png') END as imagen")
                    ->selectRaw("(select nombre from categorias where id=categoria) as categoria")
                    ->selectRaw("(select nombre from ubicaciones where id=negocios.ubicacion) as ubicacion")
                    ->selectRaw("(SELECT CASE WHEN SUM(puntuacion) IS NULL THEN 0 ELSE SUM(puntuacion) END FROM comentarios WHERE id IN (SELECT DISTINCT calificaciones.comentario FROM calificaciones JOIN comentarios ON comentarios.id = calificaciones.comentario WHERE negocio = negocios.id AND status = true)) as puntuacion")
                    ->wherein('negocios.ubicacion', $request->ubicacion)
                    ->where('negocios.status', true)
                    ->skip(($request->pagina - 1) * 20)
                    ->take(20)
                    ->get();
            }
            $i=0;
            $distancia = new GeoDistancia($request->latitud, $request->longitud);
            foreach ($negocios as $item) {
                $latitud = substr($item->mapa, 0, strpos($item->mapa, ","));
                $longitud = substr($item->mapa, (strpos($item->mapa, ",") + 1));
                $mi_distancia = $distancia->DistanciaKm(floatval($latitud), floatval($longitud));

                if($mi_distancia < 1){
                    $item->distancia = intval($mi_distancia * 1000)." m";
                }else{
                    $item->distancia = number_format($mi_distancia, 3)." km";
                }
                $i++;
            }
            return response()->json([
                'result'	=>[
                    "negocios"      => $negocios,
                    "total"         => $paginas
                ],
                'message'           => NULL,
                'count'             => $negocios->count()
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function comentar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'        => 'required|string|max:255',
            'correo'        => 'required|string|email|max:255',
            'negocio'       => 'required|exists:negocios,id',
            'comentario'    => 'required'
        ], [
            'nombre.required'       =>  'Proporcione el nombre de quien comenta',
            'nombre.max'            =>  'Máximo 255 caracteres',
            'correo.required'       =>  'Proporcione un correo',
            'correo.email'          =>  'Proporcione un correo válido',
            'negocio.required'      =>  'Indique el ID del negocio',
            'negocio.exists'        =>  'El negocio indicado no existe',
            'comentario.required'   =>  'Proporcione un comentario u observación',
        ]);

        if ($validator->fails()){
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try{
            $aux=1;
            $comentario = Comentarios::create([
                'nombre'        => $request->nombre,
                'correo'        => $request->correo,
                'contenido'     => $request->comentario,
                'puntuacion'    => '0'
            ]);
            $calificar = Criterios::where('status', true)->orderBy('nombre')->get();
            $puntuaciones = $request->except('negocio','comentario');
            $calificacion = 0;
            foreach ($calificar as $item)
            {
                $calificacion = $calificacion + $puntuaciones['S'.$aux];
                Calificaciones::create([
                    'negocio'       => $request->negocio,
                    'tipo'          => $item->nombre,
                    'puntuacion'    => $puntuaciones['S'.$aux],
                    'comentario'    => $comentario->id
                ]);
                $aux++;
            }
            if($calificar->count() > 0){
                $calificacion = $calificacion / $calificar->count();
            }
            $comentario->puntuacion = $calificacion;
            $comentario->save();
            return response()->json([
                'result'	=>true,
                'message'   =>NULL,
                'count'     =>1
            ]);
        }catch (\Throwable $th) {
            try{
                Calificaciones::where('comentario', $comentario->id)->delete();
                $comentario->delete();
                return response([
                    'result'    =>null,
                    'message'   =>$th->getMessage(),
                    'count'     =>0
                ], 422);
            }catch (\Throwable $th2) {
                return response([
                    'result'    =>null,
                    'message'   =>[
                        "error_principal" => $th->getMessage(),
                        "error_secundario" => $th2->getMessage(),
                    ],
                    'count'     =>0
                ], 422);
            }
		}
    }

    public function enviar_notificacion(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'negocio'   => 'required',
                'tipo'      => 'required'
            ], [
                'negocio.required'  => 'El ID del negocio es requerido',
                'tipo.required'     => 'El ID del tipo de notificación es requerido'
            ]);
        
        if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        try{
            Notificaciones::create([
                'destinatario'  => $request->negocio,           //ID del negocio
                'notificacion'  => $request->tipo               //ID del tipo de notificación
            ]);

            User::where('negocio', $request->negocio)->increment('notificaciones');

            return response()->json([
                'result'	=>true,
                'message'   =>NULL,
                'count'     =>1
            ]);
        }
        catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function leer_notificaciones(Request $request)
    {
        if(Auth::user()->notificaciones == 0)
        {
            //No tiene notificaciones nuevas
            return response()->json([
                'result'	=>[],
                'message'   =>'Sin notificaciones nuevas',
                'count'     =>0
            ]);
        }else{
            //Tiene notificaciones nuevas
            $notificaciones = Notificaciones::select('notificaciones.id', 'tipos_notificaciones.mensaje as notificacion')
                ->selectRaw("date_format(notificaciones.created_at,'%d/%m/%Y') as fecha")
                ->selectRaw("date_format(notificaciones.created_at,'%H:%i:%s') as hora")
                ->join('tipos_notificaciones', 'tipos_notificaciones.id', 'notificaciones.notificacion')
                ->where([
                    ['destinatario', Auth::user()->negocio],
                    ['nuevo', true]
                ])
                ->orderBy('fecha', 'desc')
                ->orderBy('hora', 'desc')
                ->get();

            Notificaciones::where('destinatario', Auth::user()->negocio)->where('nuevo', true)->update(['nuevo' => false]);
            User::where('id', Auth::id())->update(['notificaciones' => 0]);

            return response()->json([
                'result'	=> $notificaciones,
                'message'   => NULL,
                'count'     => $notificaciones->count()
            ]);
        }
        
    }

    public function notificacion_leida(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notificacion'          => 'required'
        ], [
            'notificacion.required' => 'El ID de la notificación es requerido'
        ]);
        
        if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

        Notificaciones::where('id', $request->notificacion)->update(['leido' => true]);

        return response()->json([
            'result'	=>true,
            'message'   =>NULL,
            'count'     =>1
        ]);
    }

    public function listar_notificaciones(Request $request)
    {
        $notificaciones = Notificaciones::select('notificaciones.id', 'tipos_notificaciones.mensaje as notificacion', 'notificaciones.nuevo', 'notificaciones.leido')
            ->selectRaw("date_format(notificaciones.created_at,'%d/%m/%Y') as fecha")
            ->selectRaw("date_format(notificaciones.created_at,'%H:%i:%s') as hora")
            ->join('tipos_notificaciones', 'tipos_notificaciones.id', 'notificaciones.notificacion')
            ->where('destinatario', Auth::user()->negocio)
            ->orderBy('fecha', 'desc')
            ->orderBy('hora', 'desc')
            ->get();

        return response()->json([
            'result'	=> $notificaciones,
            'message'   => NULL,
            'count'     => $notificaciones->count()
        ]);
    }

    public function ubicacion_cercana(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitud'               => 'required|numeric',
            'longitud'              => 'required|numeric'
        ],[
            'latitud.required'      => 'indique la latitud del usuario',
            'longitud.required'     => 'indique la longitud del usuario'
        ]);

        
        
		if ($validator->fails()){
            return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}
        
        try{
            $distancia = new GeoDistancia($request->latitud, $request->longitud);
            $ubicaciones = Ubicaciones::select('id', 'nombre', 'ubicacion as coordenadas')->get();

            $i = 0;
            $indices = [];
            $mis_distancias = [];

            foreach ($ubicaciones as $item) {
                $latitud = substr($item->coordenadas, 0, strpos($item->coordenadas, ","));
                $longitud = substr($item->coordenadas, (strpos($item->coordenadas, ",") + 1));
                $mi_distancia = $distancia->DistanciaKm(floatval($latitud), floatval($longitud));
                $item->distancia = $mi_distancia;
                $indices[$i] = $item->id;
                $mis_distancias[$i] = $mi_distancia;
                $i++;
            }

            for ($i=0; $i < count($mis_distancias); $i++) {
                for ($j= ($i + 1); $j < count($mis_distancias); $j++) {
                    if($mis_distancias[$i] > $mis_distancias[$j]){
                        //Mover ID
                        $temp = $indices[$j];
                        $indices[$j] = $indices[$i];
                        $indices[$i] = $temp;
                        //Mover Distancia
                        $temp = $mis_distancias[$j];
                        $mis_distancias[$j] = $mis_distancias[$i];
                        $mis_distancias[$i] = $temp;

                    }
                }
            }


            return response()->json([
                'result'        =>  array_slice($indices, 0, 2),
                'message'	    => NULL,
                'count'         => count(array_slice($indices, 0, 2))
            ]);
        }catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }
}
