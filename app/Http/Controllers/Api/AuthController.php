<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\RecuperacionDeCuenta;
use App\Models\Documentos;
use App\Models\FirebaseTokens;
use App\Models\Negocios;
use App\Models\Recuperaciones;
use App\Models\Seguimiento;
use App\Models\Tipos_Documentos;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required_without:phone|string|email',
            'phone' => 'required_without:email|numeric|digits:10',
            'password' => 'required|string',
            'token_notificacion' => 'required|string',
        ], [
            'email.required' => 'El correo es requerido',
            'email.email' => 'El correo debe tener un formato válido',
            'phone.required' => 'Su número telefónico es requerido',
            'phone.digits' => 'Proporcione los 10 dígitos de su teléfono',
            'password.required' => 'La contraseña es requerida',
            'token_notificacion.required' => 'EL token para las notificaciones es requerido',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        if ($request->filled("email")) {
            //Logueo con correo
            $credentials = request(['email', 'password']);
        } else {
            //Logueo con número telefónico
            $credentials = request(['phone', 'password']);
        }

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'result' => null,
                'message' => 'Unauthorized',
                'count' => 0,
            ], 401);
        } else {
            $user = Auth::user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            //Guardar el token sino se tiene

            $token = FirebaseTokens::updateOrCreate(
                [
                    'usuario' => Auth::id(),
                    'token' => $request->token_notificacion,
                ]
            );
            $token->updated_at = now();
            $token->save();

            if (is_null(Auth::user()->negocio)) {
                $name = Auth::user()->name;
            } else {
                $negocio = Negocios::find(Auth::user()->negocio);
                $name = $negocio->nombre;
            }
            return response()->json([
                'result' => [
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                    'nombre' => $name,
                    'empresa' => Auth::user()->negocio,
                ],
                'message' => null,
                'count' => 1,
            ]);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), ['tipo' => 'required'], ['tipo.required' => 'Indique el tipo de registro']);
        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        if ($request->tipo == "cliente") {
            $validator = Validator::make($request->all(), [
                'nombre' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['nullable', 'numeric', 'digits:10', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
            ], [
                'nombre.required' => 'Proporcione su nombre completo',
                'email.required' => 'Proporcione su correo electrónico',
                'email.email' => 'El correo no tiene un formato válido',
                'email.unique' => 'Este correo ya está en uso, intente con otro',
                'phone.required' => 'Proporcione su número telefónico',
                'phone.unique' => 'Este número telefónico ya está en uso, intente con otro',
                'phone.min' => 'Proporcione los 10 dígitos de su teléfono',
                'password.required' => 'Proporcione una contraseña para la cuenta',
                'password.min' => 'Se requieren 8 caracteres como minimo',
            ]);

        } else if ($request->tipo == "negocio") {
            $validator = Validator::make($request->all(), [
                'empresa' => ['required', 'string', 'max:255'],
                'nombre' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['nullable', 'numeric', 'digits:10', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
            ], [
                'empresa.required' => 'Proporcione el nombre de la empresa',
                'nombre.required' => 'Proporcione su nombre completo',
                'email.required' => 'Proporcione su correo electrónico',
                'email.email' => 'El correo no tiene un formato válido',
                'email.unique' => 'Este correo ya está en uso, intente con otro',
                'phone.required' => 'Proporcione su número telefónico',
                'phone.unique' => 'Este número telefónico ya está en uso, intente con otro',
                'phone.digits' => 'Proporcione los 10 dígitos de su teléfono',
                'password.required' => 'Proporcione una contraseña para la cuenta',
                'password.min' => 'Se requieren 8 caracteres como minimo',
            ]);
        } else {
            return response([
                'result' => null,
                'message' => ["el tipo de registro no existe"],
                'count' => 0,
            ], 422);
        }

        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        if ($request->tipo == "cliente") {
            $user = User::create([
                'name' => $request->nombre,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]);
            $user->assignRole('cliente');
        } else {
            //Registro de Negocio
            $negocio = Negocios::create([
                'nombre' => $request->empresa,
                'direccion' => 'pendiente',
                'descripcion' => 'pendiente',
                'servicios' => 'pendiente',
            ]);
            $user = User::create([
                'name' => $request->nombre,
                'email' => $request->email,
                'phone' => $request->phone,
                'negocio' => $negocio->id,
                'password' => Hash::make($request->password),
            ]);
            $user->assignRole('negocio');

            Seguimiento::create([
                'negocio' => $negocio->id,
                'tramite' => 1,
                'aprobado' => true,
                'status' => true,
            ]);
            Seguimiento::create([
                'negocio' => $negocio->id,
                'tramite' => 2,
            ]);
            Seguimiento::create([
                'negocio' => $negocio->id,
                'tramite' => 3,
            ]);
            Seguimiento::create([
                'negocio' => $negocio->id,
                'tramite' => 4,
            ]);

            $documentacion = Tipos_Documentos::where('status', true)->get();
            foreach ($documentacion as $item) {
                Documentos::create([
                    'negocio' => $negocio->id,
                    'tipo' => $item->id,
                ]);
            }
        }

        return response()->json([
            'result' => "Cuenta registrada",
            'message' => ["Cuenta registrada"],
            'count' => 1,
        ]);
    }

    public function obtener_link(Request $request) //Api para obtener un link de logueo automatico

    {
        try
        {
            $user = Auth::user();
            if (is_null($user->token_acceso)) {
                $user->token_acceso = uniqid();
                $user->save();
            }
            $link = route('login_automatico', $user->token_acceso);

            return response()->json([
                'result' => $link,
                'message' => null,
                'count' => 1,
            ]);
        } catch (\Throwable $th) {
            return response([
                'result' => null,
                'message' => $th->getMessage(),
                'count' => 0,
            ], 422);
        }
    }

    public function recuperacion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ], [
            'email.required' => 'El correo es requerido',
            'email.email' => 'El correo debe tener un formato válido',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        $existe = User::where('email', $request->email)->first();
        if ($existe) {
            $recuperar = Recuperaciones::where('usuario', $existe->id)->first();
            if (is_null($recuperar)) {
                //No existe registro de recuperación, crear uno nuevo
                $clave = Str::random(10);
                $fecha = Carbon::now()->addMinutes(30);
                $recuperar = Recuperaciones::create([
                    'usuario' => $existe->id,
                    'codigo' => strtoupper($clave),
                    'vigencia' => $fecha,
                ]);

                try {
                    Mail::to($request->email)->send(new RecuperacionDeCuenta($recuperar));
                } catch (\Exception $ex) {
                    return response()->json([
                        'result' => true,
                        'message' => 'No se pudo enviar el correo con la clave de recuperación, intente más tarde',
                        'count' => 1,
                    ]);
                }

                return response()->json([
                    'result' => true,
                    'message' => 'Se le ha enviado un correo con la clave de recuperación',
                    'count' => 1,
                ]);
            } else {
                //Existe registro de recuperacion, renovar el existente
                $clave = Str::random(10);
                $fecha = Carbon::now()->addMinutes(30);
                $recuperar->codigo = strtoupper($clave);
                $recuperar->vigencia = $fecha;
                $recuperar->save();
                try {
                    Mail::to($request->email)->send(new RecuperacionDeCuenta($recuperar));
                } catch (\Exception $ex) {
                    return response()->json([
                        'result' => true,
                        'message' => 'No se pudo enviar el correo con la nueva clave de recuperación, intente más tarde',
                        'count' => 1,
                    ]);
                }

                return response()->json([
                    'result' => true,
                    'message' => 'Se le ha enviado un nuevo correo con la nueva clave de recuperación',
                    'count' => 1,
                ]);
            }
        } else {
            return response()->json([
                'result' => false,
                'message' => 'No existe ninguna cuenta vinculada a la dirección de correo proporcionada',
                'count' => 0,
            ]);
        }
    }

    public function recuperar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clave' => 'required|string',
            'password' => 'required|string',
        ], [
            'clave.required' => 'La clave es requerido',
            'password.required' => 'La nueva contraseña es requerida',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        $recuperar = Recuperaciones::where('codigo', $request->clave)->first();
        if (!is_null($recuperar)) {
            $fecha = Carbon::now();

            if ($recuperar->vigencia > $fecha) {
                //El codigo es vigente
                $usuario = User::find($recuperar->usuario);
                $usuario->password = bcrypt($request->password);
                $usuario->save(); //Guardamos la nueva contraseña para la cuenta
                $recuperar->delete(); //Borramos el registro de recuperación

                return response()->json([
                    'result' => true,
                    'message' => 'Se ha restaurado la contraseña correctamente',
                    'count' => 1,
                ]);
            } else {
                //El codigo ha expirado
                return response()->json([
                    'result' => false,
                    'message' => 'El código ha expirado',
                    'count' => 0,
                ]);
            }
        } else {
            //El codigo no existe
            return response()->json([
                'result' => false,
                'message' => 'El código proporcionado no existe',
                'count' => 0,
            ]);
        }
    }
}
