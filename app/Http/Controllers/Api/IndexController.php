<?php

namespace App\Http\Controllers\Api;

use App\Models\Anuncios;
use App\Models\SliderFortalezas;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function slider_fortalezas()
    {
        try{
            $frases = SliderFortalezas::selectRaw("concat(inicio, ' ', remarcado, ' ', fin) as frase")->where("status", true)->get();
            return response()->json([
                'result'	=> $frases,
                'message'   => NULL,
                'count'     => $frases->count()
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }

    public function anuncios()
    {
        try{
            $recurso = asset('images/anuncio/');
            $anuncio = Anuncios::selectRaw("concat('$recurso', '/', imagen) as imagen")->where('status', true)->get();
            return response()->json([
                'result'	=> $anuncio,
                'message'   => NULL,
                'count'     => $anuncio->count()
            ]);
        } catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
    }
}
