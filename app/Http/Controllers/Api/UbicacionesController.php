<?php

namespace App\Http\Controllers\Api;

use App\Models\Ubicaciones;
use App\Http\Controllers\Controller;

class UbicacionesController extends Controller
{
    public function lista()
    {
        $ubicaciones = Ubicaciones::select('id', 'nombre', 'ubicacion as coordenadas')->get();
        return response()->json([
            'result'    =>[
                'ubicaciones' =>$ubicaciones
            ],
            'message'   =>NULL,
            'count'     =>$ubicaciones->count()
        ]);
    }
}
