<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Categorias;
use App\Models\Negocios;

class CategoriasController extends Controller
{
    public function lista()
    {
        $recurso = asset('images/areas');
        $categorias = Categorias::select('id', 'nombre')
            ->selectRaw("concat('$recurso', '/', imagen) as imagen")
            ->whereIn('id', Negocios::selectRaw("DISTINCT categoria")->where("status", true)->get())
            ->where("id", ">", 1)
            ->orderby('nombre')->get();
        return response()->json([
            'result' => [
                'categorias' => $categorias,
            ],
            'message' => null,
            'count' => $categorias->count(),
        ]);
    }
}
