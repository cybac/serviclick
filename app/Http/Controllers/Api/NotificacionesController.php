<?php

namespace App\Http\Controllers\Api;

use App\Firebase\FirebaseNotificacion;
use App\Http\Controllers\Controller;
use App\Models\Tipos_Notificaciones;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class NotificacionesController extends Controller
{
    public function notificar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'negocio' => 'required',
            'notificacion' => 'required',
        ], [
            'negocio.required' => 'El ID del negocio es requerido',
            'notificacion.required' => 'El ID de la notificación es requerida',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        try
        {
            $negocio = User::where('negocio', $request->negocio)->first();
            if (count($negocio->FirebaseTokens)) {
                $notificacion = Tipos_Notificaciones::findOrFail($request->notificacion);
                $firebase = new FirebaseNotificacion();
                $res = $firebase->enviar_ahora($notificacion->tipo, $notificacion->mensaje, $negocio->FirebaseTokens->pluck("token"), true);
                return response()->json([
                    'result' => $res,
                    'message' => 'Notificación enviada',
                    'count' => 1,
                ]);
            } else {
                return response([
                    'result' => null,
                    'message' => 'EL negocio no tiene ningun token asignado',
                    'count' => 0,
                ], 422);
            }
        } catch (\Throwable $th) {
            return response([
                'result' => null,
                'message' => $th->getMessage(),
                'count' => 0,
            ], 422);
        }
    }

    public function existe_token()
    {
        if (is_null(Auth::user()->token_notificaciones)) {
            //No tiene Token
            return response()->json([
                'result' => false,
                'message' => null,
                'count' => 1,
            ]);
        } else {
            //Tiene Token
            return response()->json([
                'result' => true,
                'message' => null,
                'count' => 1,
            ]);
        }

    }

    public function guardar_token(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token_notificacion' => 'required|string',
        ], [
            'token_notificacion.required' => 'EL token para las notificaciones es requerido',
        ]);
        if ($validator->fails()) {
            return response([
                'result' => null,
                'message' => $validator->errors()->all(),
                'count' => 0,
            ], 422);
        }

        try {
            if (is_null(Auth::user()->token_notificaciones)) {
                User::where('id', Auth::id())->update(['token_notificaciones' => $request->token_notificacion]);
            } else if (Auth::user()->token_notificaciones != $request->token_notificacion) {
                User::where('id', Auth::id())->update(['token_notificaciones' => $request->token_notificacion]);
            }

            return response()->json([
                'result' => true,
                'message' => null,
                'count' => 1,
            ]);

        } catch (\Throwable $th) {
            return response([
                'result' => null,
                'message' => $th->getMessage(),
                'count' => 0,
            ], 422);
        }
    }
}
