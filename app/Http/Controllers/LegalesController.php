<?php

namespace App\Http\Controllers;

use App\Models\Legales;
use Illuminate\Http\Request;

class LegalesController extends Controller
{
    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'id'            =>'required',
            'contenido'     =>'required',
        ],
        [
            'id.required'       =>'Una imagen es requerida',
            'contenido.required'    =>'Proporcione una descripción',
        ]);
            
        $legal = Legales::find($request->id);
        $legal->contenido = $request->contenido;
        $legal->save();

        return response()->json([
            'status'    =>  'true',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha actualizado el contenido del documento legal',
        ], 200);
    }
}
