<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Models\Tipos_Documentos;

class TiposDocumentosController extends Controller
{
    public function index()
    {
        return view('admin.documentacion');
    }

    public function datatable(Request $request)
    {
        $documentacion = Tipos_Documentos::all();
        return DataTables::of($documentacion)
        ->addColumn('status', function($documentacion){
            if($documentacion->status)
            {
                return"<span class='switch'>
                    <input type='checkbox' checked onchange='Cambio_documentacion($documentacion->id);' class='switch' id='status_".$documentacion->id."'>
                    <label for='status_".$documentacion->id."'></label>
                    </span>";
            }else{
                return"<span class='switch'>
                    <input type='checkbox' onchange='Cambio_documentacion($documentacion->id);' class='switch' id='status_".$documentacion->id."'>
                    <label for='status_".$documentacion->id."'></label>
                    </span>";
            }
        })
        ->addColumn('btn', function($documentacion){
            return "<button class='btn btn-primary p-3 input-text w-100 mb-2' data-value='".$documentacion->nombre."' data-url='".route('actualizar_documentacion', $documentacion->id)."' data-title='Editar Documentación' data-subtitle='Nuevo nombre para el documento'>Editar</button>";
        })
        ->rawColumns(['status', 'btn'])
        ->toJson();
    }

    public function guardar(Request $request)
    {
        $this->validate($request,['nombre'    =>'required'],['nombre.required'   => 'Proporcione el nombre para el documento']);

        Tipos_Documentos::create([
            'nombre'    => $request->nombre
        ]);

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha creado el nuevo tipo de documento',
            'dttable'   =>  "#dt_documentacion",
        ], 200);
    }

    public function actualizar(Request $request, $id_documentacion)
    {
        
        $this->validate($request, ['nombre'    =>'required'], ['nombre.required'       =>'Proporcione el nombre para el documento']);

        $documentacion = Tipos_Documentos::find($id_documentacion);
        $documentacion->nombre = $request->nombre;
        $documentacion->save();

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha actualizado el nombre de la documentación',
            'dttable'   =>  "#dt_documentacion",
        ], 200);
    }

    public function status(Request $request)
    {
        $this->validate($request,[
            'id'        =>'required',
            'status'    =>'required'
        ],
        [
            'id.required'       =>'Proporcione el ID de la documentacion',
            'status.required'   =>'Indique el status que desea asignar'
        ]);
        $documentacion = Tipos_Documentos::find($request->id);
        $documentacion->status = $request->status;
        $documentacion->save();

        if($request->status)
        {
            return "activo";
        }else{
            return "inactivo";
        }
    }
}
