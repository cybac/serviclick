<?php

namespace App\Http\Controllers;

use File;
use Image;
use DataTables;
use App\Models\Keysword;
use App\Models\Negocios;
use App\Models\Categorias;
use App\Models\Ubicaciones;
use Illuminate\Http\Request;
use App\Models\RedesSociales;
use App\Models\GaleriaTrabajos;
use Illuminate\Support\Facades\Auth;
use App\Models\RedesSocialesNegocios;

class MiNegocioController extends Controller
{
    private $path_galeria = "images/trabajos/";
    private $path_miniatura_movil = "images/trabajos/miniaturas/movil/";
    private $path_miniatura_web = "images/trabajos/miniaturas/web/";

    public function index()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        $ubicaciones = Ubicaciones::orderBy('nombre')->get();
        $redes = Categorias::where('id', '>', '2')->orderBy('nombre')->get();

        if($negocio->tramites)
        {
            return redirect()->route('panel_control');
        }
        return view('negocio.negocio', [
            'negocio'       => $negocio,
            'ubicaciones'   => $ubicaciones,
            'categorias'    => $redes,
        ]);
    }

    public function actualizar_negocio(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'ubicacion'     =>'required',
            'categoria'     =>'required',
            'direccion'     =>'required',
            'telefono'      =>'required',
            'descripcion'   =>'required',
            'servicios'     =>'required',
            'minimo'        =>'required',
            'maximo'        =>'required',
            'mapa'          =>'required'
        ],
        [
            'nombre.required'        =>'Proporcione el nombre del negocio',
            'ubicacion.required'     =>'Seleccione una ubicación',
            'categoria.required'     =>'Seleccione una categoria',
            'direccion.required'     =>'Proporcione la dirección del negocio',
            'telefono.required'      =>'Proporcione un número telefonico de contacto',
            'descripcion.required'   =>'Proporcione una descripción del negocio',
            'servicios.required'     =>'Indique los servicios que proporciona su negocio',
            'minimo.required'        =>'Indique el precio minimo aproximado',
            'maximo.required'        =>'Indique el precio minimo aproximado',
            'mapa.required'          =>'Indique su negocio en el mapa'
        ]);

        $negocio = Negocios::find(Auth::user()->negocio);
        $negocio->nombre = $request->nombre;
        $negocio->direccion = $request->direccion;
        $negocio->telefono = $request->telefono;
        $negocio->descripcion = $request->descripcion;
        $negocio->servicios = $request->servicios;
        $negocio->precio_min = $request->minimo;
        $negocio->precio_max = $request->maximo;
        $negocio->categoria = $request->categoria;
        $negocio->ubicacion = $request->ubicacion;
        $negocio->mapa = $request->mapa;

        if($request->has('web')){
            $negocio->web = $request->web;
        }else{
            $negocio->web = NULL;
        }
        $negocio->save();

        return response()->json([
            'status'=>  'true',
            'text'  =>  'Datos del negocio actualizados correctamente',
        ], 200);
    }

    public function galeria()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        $galeria = GaleriaTrabajos::where('negocio', Auth::user()->negocio)->orderBy('orden')->get();
        if($negocio->tramites)
        {
            return redirect()->route('panel_control');
        }
        return view('negocio.galeria',[
            'galeria'       => $galeria
        ]);
    }

    public function guardar_galeria(Request $request)
    {
        $request->validate(['file' => 'image|mimes:png,jpg,jpeg|max:10240']);
        if (!file_exists($this->path_galeria)) {
            File::makeDirectory($this->path_galeria, $mode = 0777, true, true);
        }
        if (!file_exists($this->path_miniatura_movil)) {
            File::makeDirectory($this->path_miniatura_movil, $mode = 0777, true, true);
        }
        if (!file_exists($this->path_miniatura_web)) {
            File::makeDirectory($this->path_miniatura_web, $mode = 0777, true, true);
        }
        $img = $request->file('file');
        $file_name = uniqid().".".$img->extension();    //Nombre de la imagen
        //Miniatura Movil
        $miniatura_movil = Image::make($img);
        $miniatura_movil->resize(440, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $miniatura_movil->save($this->path_miniatura_movil.$file_name);
        //Miniatura Web
        $miniatura_web = Image::make($img);
        $miniatura_web->resize(370, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $miniatura_web->save($this->path_miniatura_web.$file_name);
        //Guardo la imagen original
        $img->move($this->path_galeria, $file_name);
        //Crear el registro de la imagen

        $orden = GaleriaTrabajos::selectraw("CASE WHEN COUNT(*) > 0 THEN (max(orden) + 1) ELSE 0 END as ultimo")->where("negocio", Auth::user()->negocio)->first();

        $item = GaleriaTrabajos::create([
            "negocio"       => Auth::user()->negocio,
            "imagen"        => $file_name,
            "orden"         => $orden->ultimo
        ]);
		$view = view('partials.add_items', ['item' => $item, 'path' => $this->path_galeria])->render();
		return response()->json([
			'status'    => 'success',
			'view'      => $view
		], 200);
    }

    public function ordenar_galeria(Request $request)
    {
        foreach ($request->order as $key => $value) {
            GaleriaTrabajos::where('id', $value)->update(['orden' => $key]);
		}
        return response()->json([
			'status'    => 'success',
			'text'      => 'Orden guardado'
		], 200);
    }
    public function eliminar_galeria(Request $request)
    {
        $galeria = GaleriaTrabajos::find($request->id_imagen);
        File::delete($this->path_galeria.$galeria->imagen);
        File::delete($this->path_miniatura_movil.$galeria->imagen);
        File::delete($this->path_miniatura_web.$galeria->imagen);
        $galeria->delete();
        return response()->json([
            'status'=>true,
            'text'  =>'Se ha eliminado la imagen de la galeria',
        ], 200);
    }

    public function keywords()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        $keysword = Keysword::where('negocio', Auth::user()->negocio)->get();
        if($negocio->tramites)
        {
            return redirect()->route('panel_control');
        }
        return view('negocio.keysword',[
            'keys'          => $keysword,
        ]);
    }

    public function guardar_keysword(Request $request)
    {
        //Borrar las keys anteriores
        $keys = Keysword::where('negocio', Auth::user()->negocio);
        $keys->delete();
        //Guardar las nuevas keys
        $data = implode(',', $request->except('_token', 'keysword'));   //Convierto el array en string
        $palabras = explode(',', $data);                                //Paso el string a array en una variable que pueda manejar
        $total = count($request->except('_token', 'keysword'));         //obtengo el total de datos recibidos
        if($total > 0)                                                  //Verifico que se enviaran etiquetas
        {
            for($i = 0 ; $i < $total; $i++)
            {
                Keysword::create([
                    'negocio'   => Auth::user()->negocio,
                    'key'       => $palabras[$i]
                ]);
            }
        }
        return response()->json([
            'status'=>true,
            'type'  =>'success',
            'title' =>'Éxito',
            'text'  =>'Se ha guardado las keys',
        ], 200);

    }

    public function redes()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        $redes = RedesSociales::all();
        if($negocio->tramites)
        {
            return redirect()->route('panel_control');
        }
        return view('negocio.redes',[
            'redes'         => $redes
        ]);
    }

    public function datatable_redes(Request $request)
    {
        $redes = RedesSocialesNegocios::select('redes_sociales_negocios.id', 'redes_sociales.nombre as red_social', 'redes_sociales_negocios.url')
            ->join('redes_sociales', 'redes_sociales.id', 'redes_sociales_negocios.red_social')
            ->where("negocio", Auth::user()->negocio)->get();
        return DataTables::of($redes)
        ->addColumn('btn', function($redes){
            return "<button class='btn btn-danger delete p-3 mb-2 w-100' data-url='".route('eliminar_redes', $redes->id)."'>Eliminar</button>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function guardar_redes(Request $request)
    {
        $this->validate($request,[
            'red'   =>'required',
            'url'   =>'required'
        ],
        [
            'red.required'      =>'Indique la red social a agregar',
            'url.required'      =>'Proporcione la url de la red social indicada'
        ]);

        RedesSocialesNegocios::create([
            'negocio'       => Auth::user()->negocio,
            'red_social'    => $request->red,
            'url'           => $request->url
        ]);

        return response()->json([
            'status'    =>true,
            'type'      =>'success',
            'title'     =>'Éxito',
            'text'      =>'Se ha guardado la red social',
            'dttable'   =>'#dt_redes_sociales',
            'social'    =>true
        ], 200);
    }

    public function eliminar_redes(Request $request, $id_red)
    {
        RedesSocialesNegocios::destroy($id_red);

        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado la red social',
            'dttable'   =>  "#dt_redes_sociales"
        ], 200);

    }
}
