<?php

namespace App\Http\Controllers;

use App\Conekta\ConektaPayment;
use App\Mail\FichaDePago;
use App\Mail\Notificacion;
use App\Models\Categorias;
use App\Models\Correos;
use App\Models\DetallesPagos;
use App\Models\Documentos;
use App\Models\Negocios;
use App\Models\Pagos;
use App\Models\Precios;
use App\Models\Seguimiento;
use App\Models\Ubicaciones;
use App\Models\User;
use Carbon\Carbon;
use DataTables;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class TramitesController extends Controller
{
    private $path = "images/negocios/";
    private $path_documentos = "documentos/";
    protected $ConektaPayment;

    public function __construct()
    {
        $this->ConektaPayment = new ConektaPayment();
    }

    public function index()
    {
        return view('admin.negocios_tramite');
    }

    public function datatable(Request $request)
    {
        $negocios = Negocios::select('negocios.id', 'negocios.imagen', 'negocios.nombre', 'negocios.direccion', 'categorias.nombre as categoria', 'ubicaciones.nombre as ubicacion', 'negocios.telefono', 'negocios.status', 'negocios.created_at', 'users.email as email_contacto', 'users.phone as telefono_contacto')
            ->selectRaw("CONCAT(negocios.precio_min, ' - ', negocios.precio_max) as precio_min")
            ->selectRaw("(SELECT CASE WHEN status = 1 AND comentarios IS NULL THEN false WHEN status= 1 THEN true END FROM seguimientos WHERE negocio = negocios.id and tramite = 4) as expiro")
            ->join('categorias', 'categorias.id', 'negocios.categoria')
            ->join('ubicaciones', 'ubicaciones.id', 'negocios.ubicacion')
            ->join('users', 'users.negocio', 'negocios.id')
            ->where('tramites', true)
            ->get();
        return DataTables::of($negocios)
            ->addColumn('imagen', function ($negocios) {
                if ($negocios->imagen == "0") {
                    return "<a href='" . asset($this->path . "0.png") . "' target='_blank'><img src='" . asset($this->path . "0.png") . "' class='img-fluid' style='margin-left: 10px;'></a>";
                } else {
                    return "<a href='" . asset($this->path . $negocios->imagen) . "' target='_blank'><img src='" . asset($this->path . $negocios->imagen) . "' class='img-fluid' style='margin-left: 10px;'></a>";
                }
            })
            ->addColumn('registro', function ($negocios) {
                $fecha = Carbon::createFromFormat('Y-m-d H:i:s', $negocios->created_at);
                return "<span style='display: none;'>" . $negocios->created_at . " </span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . " a las " . $fecha->format('g:i A') . "</span>";
            })
            ->addColumn('seguimiento', function ($negocios) {
                if ($negocios->expiro) {
                    return "<button class='btn btn-warning text-white renovar-ficha p-3 w-100 mb-2' data-url='" . route('renovar_ficha_admin') . "' data-id='" . $negocios->id . "'>Renovar Ficha</button>
                <a class='btn btn-dark text-white p-3 w-100 mb-2' href='" . route('informacion_negocio', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Información</a>
                <a class='btn btn-info text-white p-3 w-100 mb-2' href='" . route('documentos_tramite', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Documentos</a>";
                } else {
                    return "<a class='btn btn-dark text-white p-3 w-100 mb-2' href='" . route('informacion_negocio', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Información</a>
                <a class='btn btn-info text-white p-3 w-100 mb-2' href='" . route('documentos_tramite', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Documentos</a>";
                }
            })
            ->addColumn('contacto', function ($negocios) {
                $contacto = "<ul>";
                if (!is_null($negocios->telefono_contacto)) {
                    $contacto .= "<li><b>Teléfono:</b> $negocios->telefono_contacto </li>";
                }
                $contacto .= "<li><b>Correo:</b> $negocios->email_contacto </li>";
                $contacto .= "</ul>";
                return $contacto;
            })
            ->addColumn('btn', function ($negocios) {
                return "<button class='btn btn-success seguimiento-tramite p-3 w-100 mb-2' data-url='" . route('seguimiento_negocio') . "' data-id='" . $negocios->id . "'>Tramites</button>
            <a class='btn btn-lime text-white p-3 w-100 mb-2' target='_blank' href='" . route('control_sc_recordatorios_negocios', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Recordatorios</a>
            <button class='btn btn-danger delete p-3 w-100 mb-2' data-url='" . route('eliminar_tramite_negocio', $negocios->id) . "' style='width: 100%; margin-bottom: 10px;'>Eliminar Negocio</button>";
            })
            ->rawColumns(['imagen', 'registro', 'seguimiento', 'contacto', 'btn'])
            ->toJson();
    }

    public function eliminar($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio); //Encontrar el negocio a borrar
        File::delete($this->path . $negocio->imagen); //Borrar el logo del negocio
        $negocio->delete(); //Borrar el negocio
        $documento = Documentos::where('negocio', $id_negocio); //Encontrar sus documentos
        foreach ($documento->get() as $item) {
            File::delete($this->path_documentos . $item->nombre); //Borrar los documentos del negocio
        }
        $documento->delete(); //Borrar sus documentos (el registro de la BD)
        Seguimiento::where('negocio', $id_negocio)->delete(); //Eliminar el seguimiento de la cuenta del negocio
        User::where('negocio', $id_negocio)->delete(); //Eliminar cuenta del usuario

        return response()->json([
            'status' => 'true',
            'type' => 'success',
            'title' => 'Éxito',
            'text' => 'Se ha eliminado el negocio',
            'dttable' => "#dt_negocios_tramites",
        ], 200);

    }

    public function renovacion_ficha_negocio()
    {
        //Verificar status de la ultima ficha
        $ficha = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", Auth::user()->negocio)->first()->id)->first();
        if ($ficha->OrderStatus == "expired") //Última ficha expirada
        {
            //Generamos una nueva orden de pago
            $this->Orden(Auth::user()->negocio);
            //Cambiar el status del proceso
            Seguimiento::where(['negocio' => Auth::user()->negocio, 'tramite' => 4])->update(['comentarios' => null]);
            return "Ficha Renovada Correctamente";
        } else if ($ficha->OrderStatus == "paid") {
            return "La ficha no se puede renovar ya que fue pagada";
        } else {
            return "Cuenta con una ficha pendiente de pago";
        }
    }

    public function ficha(Request $request)
    {
        $ficha = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", $request->id)->firstOrFail()->id)->firstOrFail();
        if ($ficha->OrderStatus == "expired") { //Última ficha expirada
            //Generamos una nueva orden de pago
            $this->Orden($request->id);
            //Cambiar el status del proceso
            Seguimiento::where(['negocio' => $request->id, 'tramite' => 4])->update(['comentarios' => null]);
            return "Ficha Renovada Correctamente";
        } else if ($ficha->OrderStatus == "paid") {
            return "La ficha no se puede renovar ya que fue pagada";
        } else {
            return "Cuenta con una ficha pendiente de pago";
        }
    }

    public function Orden($negocio_actual)
    {
        $cobrar = Precios::find(1);
        $order = $this->ConektaPayment->CrearOrden($cobrar->nombre, $cobrar->costo, 1, $cobrar->moneda, $negocio_actual);

        $InsertData["OrderPaymentMethodID"] = $order["OrderID"];
        $InsertData["OrderDate"] = date("Y-m-d H:i:s");
        $InsertData["OrderUpdateDate"] = null;
        $InsertData["OrderPaymentMethod"] = "conekta.oxxo";

        $InsertData["OrderTotal"] = 0;
        $InsertData["OrderTotalItems"] = 0;

        foreach ($order["items"] as $item) {
            $InsertData["OrderTotal"] += $item["unit_price"];
            $InsertData["OrderTotalItems"]++;
        }

        $InsertData["OrderCurrency"] = $order["Currency"];
        $InsertData["OrderStatus"] = $order["OrderStatus"];

        $order_id = Pagos::create($InsertData);

        setlocale(LC_ALL, 'es_ES');
        foreach ($order["items"] as $item) {
            $Insert["pago"] = $order_id->id;
            $Insert["negocio"] = $negocio_actual;
            $Insert["proximo"] = (new \DateTime())->add(new \DateInterval('P1M'));
            DetallesPagos::create($Insert);
        }

        $negocio = User::where('negocio', $negocio_actual)->first();
        try {
            Mail::to($negocio->email)->send(new FichaDePago($order["Total"], $order["Currency"], $order["Reference"]));
        } catch (\Exception $ex) {}
    }

    public function documentos($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        if (empty($negocio)) {
            return redirect()->route('control_sc_negocios_tramites');
        } else {
            if ($negocio->tramites) {
                $tramite = Seguimiento::where('negocio', $id_negocio)->where('tramite', 3)->first();
                return view('admin.documentos', [
                    'negocio' => $id_negocio,
                    'add' => false,
                    'dttable' => 'dt_documentos_tramite',
                    'status' => $tramite->aprobado,
                ]);
            } else {
                return redirect()->route('control_sc_negocios_tramites');
            }
        }
    }

    public function datatable_documentos($id_negocio)
    {
        $tramite = Seguimiento::where('negocio', $id_negocio)->where('tramite', 3)->first();
        $documentos = Documentos::select('documentos.id', 'documentos.negocio', 'documentos.rechazado', 'documentos.status', 'documentos.nombre', 'tipos_documentos.nombre as tipo')
            ->selectRaw("$tramite->aprobado as status_tramite")
            ->join('tipos_documentos', 'documentos.tipo', 'tipos_documentos.id')
            ->where('negocio', $id_negocio)->get();
        return DataTables::of($documentos)
            ->addColumn('btn', function ($documentos) {
                if ($documentos->status) {
                    if ($documentos->status_tramite) {
                        return "<button class='btn btn-primary ver-archivo text-white p-3 w-100 mb-2' data-tipo='" . substr($documentos->nombre, Str::length($documentos->nombre) - 3, 3) . "' data-archivo='" . $documentos->nombre . "' style='width:50%; margin-bottom: 10px; cursor: pointer;'>Mostrar Documento</button>";
                    } else {
                        return "<button class='btn btn-primary ver-archivo text-white p-3 w-100 mb-2' data-tipo='" . substr($documentos->nombre, Str::length($documentos->nombre) - 3, 3) . "' data-archivo='" . $documentos->nombre . "' style='width:50%; margin-bottom: 10px; cursor: pointer;'>Mostrar Documento</button><br>
                    <button class='btn btn-danger p-3 w-100 mb-2 rechazar-archivo' data-url='" . route('rechazar_documento') . "' data-id='" . $documentos->id . "' data-title='Solicitar Actualización de archivo' data-subtitle='Motivo del rechazo del documento' >Rechazar Documento</button>";
                    }
                } else {
                    if ($documentos->rechazado) {
                        return "<p>Esperando actualización</p>";
                    } else {
                        return "<p>Documento Pendiente</p>";
                    }
                }
            })
            ->rawColumns(['btn'])
            ->toJson();
    }

    public function informacion($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        if (empty($negocio)) {
            return redirect()->route('control_sc_negocios_tramites');
        } else {
            if ($negocio->tramites) {
                $ubicacion = Ubicaciones::findorfail($negocio->ubicacion);
                $categoria = Categorias::findorfail($negocio->categoria);
                $tramite_actual = Seguimiento::where('negocio', $id_negocio)->where('tramite', 2)->first();
                return view('admin.informacion_negocio', [
                    'id' => $id_negocio,
                    'negocio' => $negocio,
                    'ubicacion' => $ubicacion->nombre,
                    'categoria' => $categoria->nombre,
                    'tramite' => $tramite_actual->aprobado,
                ]);
            } else {
                return redirect()->route('control_sc_negocios_tramites');
            }
        }
    }

    public function aprobar_documentacion(Request $request)
    {
        $documentos = Documentos::where('negocio', $request->id)->update(['motivo' => null, 'rechazado' => false, 'status' => true]);

        $tramite = Seguimiento::where('negocio', $request->id)->where('tramite', 3)->first();
        $tramite->aprobado = true;
        $tramite->status = true;
        $tramite->save();

        //Enviar correo al aprobar la documentación
        $negocio = User::where('negocio', $request->id)->first();
        $correo = Correos::find(6);
        try {
            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
        } catch (\Exception $ex) {}

        $this->liberar_pago($request->id);

        return response()->json([
            'status' => 'true',
            'type' => 'success',
            'title' => 'Éxito',
            'text' => 'Documentacion Marcada como aprobada',
            'url' => route('control_sc_negocios_tramites'),
        ], 200);
    }

    public function rechazar_documentacion(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'dato' => 'required',
        ],
            [
                'id.required' => 'Proporcione el ID del documento',
                'dato.required' => 'Indique el mortivo del rechazo del documento',
            ]);

        //Se borra el archivo y se solicita la actualización del archivo
        $documento = Documentos::find($request->id);
        File::delete($this->path_documentos . $documento->nombre);
        $documento->rechazado = true;
        $documento->motivo = $request->dato;
        $documento->status = false;
        $documento->nombre = null;
        $documento->save();

        //Marca como en correccion la documentación
        $mis_documentos = Seguimiento::where('negocio', $documento->negocio)->where('tramite', 3)->first();
        $mis_documentos->status = false;
        $mis_documentos->comentarios = 'Se mando a corrección';
        $mis_documentos->save();

        //Enviar correo al rechazar la documentación
        $negocio = User::where('negocio', $documento->negocio)->first();
        $correo = Correos::find(4);
        try {
            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
        } catch (\Exception $ex) {}

        return response()->json([
            'status' => 'true',
            'type' => 'success',
            'title' => 'Éxito',
            'text' => 'Documento marcado como rechazado',
            'dttable' => "#dt_documentos_tramite",
        ], 200);
    }

    public function aprobar_informacion(Request $request)
    {
        $this->validate($request, ['id' => 'required'], ['id.required' => 'Proporcione el ID del negocio']);
        $tramite = Seguimiento::where('negocio', $request->id)->where('tramite', 2)->first();
        $tramite->comentarios = null;
        $tramite->aprobado = true;
        $tramite->status = true;
        $tramite->save();

        //Enviar correo al aprobar la información
        $negocio = User::where('negocio', $request->id)->first();
        $correo = Correos::find(5);
        try {
            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
        } catch (\Exception $ex) {}

        $this->liberar_pago($request->id);

        return response()->json([
            'status' => 'true',
            'type' => 'success',
            'title' => 'Éxito',
            'text' => 'Informacion Marcada como aprobada',
            'url' => route('control_sc_negocios_tramites'),
        ], 200);
    }

    public function rechazar_informacion(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'dato' => 'required',
        ],
            [
                'id.required' => 'Proporcione el ID del negocio',
                'dato.required' => 'Indique el mortivo de la corrección de los datos',
            ]);
        $tramite = Seguimiento::where('negocio', $request->id)->where('tramite', 2)->first();
        $tramite->comentarios = $request->dato;
        $tramite->aprobado = false;
        $tramite->status = false;
        $tramite->save();

        //Enviar correo al rechazar la información
        $negocio = User::where('negocio', $request->id)->first();
        $correo = Correos::find(3);
        try {
            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
        } catch (\Exception $ex) {}

        return response()->json([
            'status' => 'true',
            'type' => 'success',
            'title' => 'Éxito',
            'text' => 'Se solicito la corrección de la informacion del negocio',
            'url' => route('control_sc_negocios_tramites'),
        ], 200);
    }

    public function seguimiento(Request $request)
    {
        $this->validate($request, ['id' => 'required'], ['id.required' => 'Proporcione el ID del criterio']);

        $seguimiento = Seguimiento::select('negocio', 'tramites.tramite', 'comentarios', 'aprobado', 'status')
            ->join('tramites', 'seguimientos.tramite', '=', 'tramites.id')
            ->where("negocio", $request->id)->get();

        $data = '<div class="d-flex"><ul class="col-7 list-group p-0">';
        foreach ($seguimiento as $item) {
            $data .= '<li class="list-group-item">' . $item->tramite . '</li>';
        }
        $data .= '</ul><ul class="col-5 list-group p-0">';
        foreach ($seguimiento as $item) {
            if ($item->status) {
                if ($item->aprobado) {
                    $data .= '<li class="list-group-item text-left"><i class="mr-2 mdi mdi-check-circle-outline"></i>Completado</li>';
                } else {
                    if ($item->tramite == "Registro del primer pago al sistema") {
                        if ($item->comentarios != null) {
                            $data .= '<li class="list-group-item text-left"><i class="mr-2 mdi mdi-credit-card-off"></i>Ficha Expiró</li>';
                        } else {
                            $data .= '<li class="list-group-item text-left"><i class="mr-2 mdi mdi-credit-card"></i>Pendiente</li>';
                        }
                    } else {
                        $data .= '<li class="list-group-item text-left"><i class="mr-2 mdi mdi-timer-sand"></i>Revisar</li>';
                    }
                }
            } else {
                if ($item->comentarios != null) {
                    $data .= '<li class="list-group-item text-left"><i class="mr-2 mdi mdi-timer-sand"></i>Corrección</li>';
                } else {
                    $data .= '<li class="list-group-item text-left"><i class="mr-2 typcn typcn-warning-outline"></i>Pendiente</li>';
                }
            }
        }
        $data .= '</ul></div>';
        return $data;
    }

    public function liberar_pago($id)
    {
        $pasos = Seguimiento::selectRaw("count(*) as total")->where('negocio', $id)->where('aprobado', 1)->first();
        if ($pasos->total == 3) {
            //Comprobar meses gratis para el negocio
            $precio = Precios::find(1);
            if ($precio->gratis > 0) {
                //Darle al negocio los meses gratis
                $meses = "P" . $precio->gratis . "M";
                $fecha = (new \DateTime())->add(new \DateInterval($meses));
                //Activar negocio
                $negocio = Negocios::findOrfail($id);
                $negocio->omision = $fecha; //Indicar la fecha hasta la que se generara la ficha
                $negocio->tramites = false; //Termino el tramite
                $negocio->status = true; //Darle visibilidad al negocio
                $negocio->save();
            } else {
                //No tiene meses gratis, generar la ficha de pago
                $this->comprar($id);
                $pago = Seguimiento::where('negocio', $id)->where('tramite', 4)->first();
                $pago->status = true;
                $pago->comentarios = null;
                $pago->save();
                //Enviar correo de liberación del primer pago
                $correo = Correos::find(7);
                $negocio = User::where('negocio', $id)->first();
                try {
                    Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
                } catch (\Exception $ex) {}
            }
        }
    }

    public function comprar($negocio_actual)
    {
        $cobrar = Precios::find(1);
        $nombre_producto = $cobrar->nombre;
        $precio_producto = $cobrar->costo;
        $cantidad_producto = 1;
        $moneda_producto = $cobrar->moneda;
        $order = $this->ConektaPayment->CrearOrden($nombre_producto, $precio_producto, $cantidad_producto, $moneda_producto, $negocio_actual);
        $InsertData["OrderPaymentMethodID"] = $order["OrderID"];
        $InsertData["OrderDate"] = date("Y-m-d H:i:s");
        $InsertData["OrderUpdateDate"] = null;
        $InsertData["OrderPaymentMethod"] = "conekta.oxxo";

        $InsertData["OrderTotal"] = 0;
        $InsertData["OrderTotalItems"] = 0;

        foreach ($order["items"] as $item) {
            $InsertData["OrderTotal"] += $item["unit_price"];
            $InsertData["OrderTotalItems"]++;
        }

        $InsertData["OrderCurrency"] = $order["Currency"];
        $InsertData["OrderStatus"] = $order["OrderStatus"];

        $order_id = Pagos::create($InsertData);

        setlocale(LC_ALL, 'es_ES');
        foreach ($order["items"] as $item) {
            $Insert["pago"] = $order_id->id;
            $Insert["negocio"] = $negocio_actual;
            $Insert["proximo"] = (new \DateTime())->add(new \DateInterval('P1M'));
            DetallesPagos::create($Insert);
        }

        $code = 200;

        if (isset($order["code"])) {
            $code = 400;
        }

        //Enviar correo con la ficha de pago
        $negocio = User::where('negocio', $negocio_actual)->first();
        try {
            Mail::to($negocio->email)->send(new FichaDePago($order["Total"], $order["Currency"], $order["Reference"]));
        } catch (\Exception $ex) {}
        return response()->json($order, $code);
    }

    public function transacciones()
    {
        $order = @file_get_contents('php://input');
        $order = json_decode($order, true);
        $order_id = isset($order["data"]["object"]["order_id"]) ? $order["data"]["object"]["order_id"] : null;
        $status = isset($order["data"]["object"]["status"]) ? $order["data"]["object"]["status"] : null;

        if (!$order_id || !$status) {
            return false;
        }
        //Buscar orden en la BD
        $order_info = Pagos::where("OrderPaymentMethodID", $order_id)->first();
        if (isset($order_info->OrderID)) {
            if ($order_info->OrderStatus != $status) {
                $UpdateData["OrderStatus"] = $status;
                $UpdateData["OrderUpdateDate"] = date("Y-m-d H:i:s");
                if (Pagos::where("OrderPaymentMethodID", $order_id)->update($UpdateData)) {
                    //Actualizar registros de la BD
                    $pago = DetallesPagos::where('pago', $order_info->OrderID)->first();
                    $negocio = User::where('negocio', $pago->negocio)->first();
                    if ($status == "paid") {
                        //Cuando la ficha fue pagada
                        Seguimiento::where('negocio', $pago->negocio)->where('tramite', 4)->update(['comentarios' => null, 'aprobado' => 1]);
                        Negocios::where('id', $pago->negocio)->update(['tramites' => 0, 'status' => 1]);
                        //Enviar correo de verificacion de pagos
                        $correo = Correos::find(8);
                        try {
                            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
                        } catch (\Exception $ex) {}
                    } else if ($status == "expired") {
                        //Cuando la ficha expira
                        Seguimiento::where('negocio', $pago->negocio)->where('tramite', 4)->update(['comentarios' => 'Ficha Expiro']);
                        Negocios::where('id', $pago->negocio)->update(['status' => 0]);
                        //Enviar correo de expiración de fichas
                        $correo = Correos::find(9);
                        try {
                            Mail::to($negocio->email)->send(new Notificacion($correo->titulo, $correo->mensaje));
                        } catch (\Exception $ex) {}
                    }
                    return response()->json("Orden actualizada", 200);
                }
            } else {
                return response()->json("No hay nada que actualizar", 200);
            }
        }
    }

    //Volver (cuando las rutas en tramite de negocios esten mal)
    public function volver()
    {
        return redirect()->route('control_sc_negocios_tramites');
    }
}
