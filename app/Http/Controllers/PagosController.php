<?php

namespace App\Http\Controllers;

use App\Conekta\ConektaPayment;
use App\Firebase\FirebaseNotificacion;
use App\Mail\FichaDePago;
use App\Models\DetallesPagos;
use App\Models\Negocios;
use App\Models\Pagos;
use App\Models\Precios;
use App\Models\Tipos_Notificaciones;
use App\Models\User;
use Carbon\Carbon;
use DataTables;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PagosController extends Controller
{
    protected $ConektaPayment;
    protected $firebase;

    public function __construct()
    {
        $this->ConektaPayment = new ConektaPayment();
        $this->firebase = new FirebaseNotificacion();
    }

    public function historial()
    {
        $negocio = Negocios::find(Auth::user()->negocio);
        if ($negocio->tramites) {
            return redirect()->route('panel_control');
        }

        $ficha = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", Auth::user()->negocio)->first()->id)->first();
        if (!is_null($ficha)) {
            if ($ficha->OrderStatus == "pending_payment") {
                //Ficha nueva por pagar
                $mostrar = true;
                $renovar = false;
            } else if ($ficha->OrderStatus == "expired") {
                //Ficha expirada
                $mostrar = false;
                $renovar = true;
            } else {
                //Ficha pagada
                $mostrar = false;
                $renovar = false;
            }
        } else {
            //Sin fichas (en periodo gratuito)
            $mostrar = false;
            $renovar = false;
        }

        return view('negocio.historial', [
            'ficha' => $mostrar,
            'renovar' => $renovar,
        ]);
    }

    public function historial_negocio($id_negocio)
    {
        $negocio = Negocios::findorfail($id_negocio);
        if ($negocio->tramites) {
            return redirect()->route('control_sc_negocios');
        } else {
            //Verificar la fecha de la proxima ficha
            $pago = DetallesPagos::where('negocio', $id_negocio)->orderby('created_at', 'desc')->take(1)->get();
            if (count($pago) == 0) //Si no tiene pagos, esta en periodo de pruebas
            {
                $exonerar = true;
                $mensaje = null;
            } else {
                //Si el proximo pago es menor a la fecha actual, no se puede exonerar el pago (cuenta con ficha o se le va a generar una)
                if ($pago[0]->proximo <= now()) {
                    $exonerar = false;
                    $mensaje = '<p class="text-danger mb-0">Exoneración de pagos</p><p class="text-danger">No disponible durante el periodo de generación de fichas</p>';
                } else {
                    //Verificar el status del pago, para ver si se puede exonerar el proximo
                    $pago = Pagos::where('OrderID', $pago[0]->pago)->get();
                    if (is_null($pago[0]->OrderUpdateDate)) {
                        $exonerar = false;
                        $mensaje = '<p class="text-danger mb-0">Exoneración de pagos</p><p class="text-danger">No disponible cuando se debe pago de ficha del mes en curso</p>';
                    } else {
                        $exonerar = true;
                        $mensaje = null;
                    }
                }
            }
            return view('admin.historial_negocio', [
                'id' => $id_negocio,
                'nombre' => $negocio->nombre,
                'exonerar' => $exonerar,
                'mensaje' => $mensaje,
            ]);
        }
    }

    public function datatable(Request $request)
    {
        $historial = DetallesPagos::select('detalles_pagos.proximo', 'pagos.OrderUpdateDate as pagado')
            ->selectRaw("CONCAT('$', (pagos.OrderTotal / 100)) as total")
            ->selectRaw("CASE WHEN pagos.OrderStatus = 'paid' THEN 'Pagado' WHEN pagos.OrderStatus = 'pending_payment' THEN 'Pendiente de pago' WHEN pagos.OrderStatus = 'expired' THEN 'Expiró' ELSE 'Desconocido' END as status")
            ->join('pagos', 'detalles_pagos.pago', 'pagos.OrderID')
            ->where('negocio', Auth::user()->negocio)->get();
        return DataTables::of($historial)
            ->addColumn('proximo', function ($historial) {
                if (!is_null($historial->pagado)) {
                    if ($historial->status == "Pagado") {
                        $fecha = Carbon::parse($historial->proximo);
                        $formato_fecha = "<span style='display:none'>" . $historial->proximo . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                        return $formato_fecha;
                    } else {
                        return "";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('pagado', function ($historial) {
                if (!is_null($historial->pagado)) {
                    $fecha = Carbon::parse($historial->pagado);
                    if ($historial->status == "Pagado") {
                        return "<span style='display:none'>" . $historial->pagado . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                    } else {
                        return "<span style='display:none'>" . $historial->pagado . "</span>";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('status', function ($historial) {
                if ($historial->status == "Pagado") {
                    return "<span style='display:none'>3</span><span><p class='mb-0 text-success'>" . $historial->status . "<p>";
                } else if ($historial->status == "Expiró") {
                    return "<span style='display:none'>2</span><span><p class='mb-0 text-danger'>" . $historial->status . "<p>";
                } else if ($historial->status == "Pendiente de pago") {
                    return "<span style='display:none'>1</span><span><p class='mb-0 text-primary'>" . $historial->status . "<p>";
                } else {
                    return "<span style='display:none'>4</span><span><p class='mb-0 text-secondary'>" . $historial->status . "<p>";
                }
            })
            ->rawColumns(['proximo', 'pagado', 'status'])
            ->toJson();
    }

    public function datatable_negocio(Request $request)
    {
        $historial = DetallesPagos::select('detalles_pagos.proximo', 'pagos.OrderDate as emitido', 'pagos.OrderUpdateDate as pagado')
            ->selectRaw("CONCAT('$', (pagos.OrderTotal / 100)) as total")
            ->selectRaw("CASE WHEN pagos.OrderStatus = 'paid' THEN 'Pagado' WHEN pagos.OrderStatus = 'pending_payment' THEN 'Pendiente de pago' WHEN pagos.OrderStatus = 'expired' THEN 'Expiró' ELSE 'Desconocido' END as status")
            ->join('pagos', 'detalles_pagos.pago', 'pagos.OrderID')
            ->where('negocio', $request->negocio)->get();
        return DataTables::of($historial)
            ->addColumn('emitido', function ($historial) {
                $fecha = Carbon::parse($historial->emitido);
                $formato_fecha = "<span style='display:none'>" . $historial->pagado . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                return $formato_fecha;
            })
            ->addColumn('pagado', function ($historial) {
                if (!is_null($historial->pagado)) {
                    $fecha = Carbon::parse($historial->pagado);
                    if ($historial->status == "Pagado") {
                        return "<span style='display:none'>" . $historial->pagado . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                    } else {
                        return "<span style='display:none'>" . $historial->pagado . "</span>";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('proximo', function ($historial) {
                if (!is_null($historial->pagado)) {
                    if ($historial->status == "Pagado") {
                        $fecha = Carbon::parse($historial->proximo);
                        $formato_fecha = "<span style='display:none'>" . $historial->proximo . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                        return $formato_fecha;
                    } else {
                        return "";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('status', function ($historial) {
                if ($historial->status == "Pagado") {
                    return "<span style='display:none'>3</span><span><p class='mb-0 text-success'>" . $historial->status . "<p>";
                } else if ($historial->status == "Expiró") {
                    return "<span style='display:none'>2</span><span><p class='mb-0 text-danger'>" . $historial->status . "<p>";
                } else if ($historial->status == "Pendiente de pago") {
                    return "<span style='display:none'>1</span><span><p class='mb-0 text-primary'>" . $historial->status . "<p>";
                } else {
                    return "<span style='display:none'>4</span><span><p class='mb-0 text-secondary'>" . $historial->status . "<p>";
                }
            })
            ->rawColumns(['emitido', 'pagado', 'proximo', 'status'])
            ->toJson();
    }

    public function datatable_admin()
    {
        $historial = DetallesPagos::select('detalles_pagos.proximo', 'negocios.nombre', 'pagos.OrderDate as emitido', 'pagos.OrderUpdateDate as pagado')
            ->selectRaw("CONCAT('$', (pagos.OrderTotal / 100)) as total")
            ->selectRaw("CASE WHEN pagos.OrderStatus = 'paid' THEN 'Pagado' WHEN pagos.OrderStatus = 'pending_payment' THEN 'Pendiente de pago' WHEN pagos.OrderStatus = 'expired' THEN 'Expiró' ELSE 'Desconocido' END as status")
            ->join('pagos', 'detalles_pagos.pago', 'pagos.OrderID')
            ->join('negocios', 'detalles_pagos.negocio', 'negocios.id')
            ->get();
        return DataTables::of($historial)
            ->addColumn('emitido', function ($historial) {
                $fecha = Carbon::parse($historial->emitido);
                $formato_fecha = "<span style='display:none'>" . $historial->pagado . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                return $formato_fecha;
            })
            ->addColumn('pagado', function ($historial) {
                if (!is_null($historial->pagado)) {
                    $fecha = Carbon::parse($historial->pagado);
                    if ($historial->status == "Pagado") {
                        return "<span style='display:none'>" . $historial->pagado . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                    } else {
                        return "<span style='display:none'>" . $historial->pagado . "</span>";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('proximo', function ($historial) {
                if (!is_null($historial->pagado)) {
                    if ($historial->status == "Pagado") {
                        $fecha = Carbon::parse($historial->proximo);
                        $formato_fecha = "<span style='display:none'>" . $historial->proximo . "</span><span>" . $fecha->day . " de " . $fecha->monthName . " del " . $fecha->year . "</span>";
                        return $formato_fecha;
                    } else {
                        return "";
                    }
                } else {
                    return "<span style='display:none'>" . now() . "</span>";
                }
            })
            ->addColumn('status', function ($historial) {
                if ($historial->status == "Pagado") {
                    return "<p class='mb-0 text-success'>" . $historial->status . "<p>";
                } else if ($historial->status == "Expiró") {
                    return "<p class='mb-0 text-danger'>" . $historial->status . "<p>";
                } else if ($historial->status == "Pendiente de pago") {
                    return "<p class='mb-0 text-primary'>" . $historial->status . "<p>";
                } else {
                    return "<p class='mb-0 text-secondary'>" . $historial->status . "<p>";
                }
            })
            ->rawColumns(['emitido', 'pagado', 'proximo', 'status'])
            ->toJson();
    }

    public function pago()
    {
        $ficha = Pagos::where("OrderID", DetallesPagos::selectRaw("max(pago) as id")->where("negocio", Auth::user()->negocio)->first()->id)->first();
        if ($ficha->OrderStatus == "pending_payment") {
            $pago = $this->ConektaPayment->ObtenerOrden($ficha->OrderPaymentMethodID);
            $contenido = '
            <div class="row">
                <div class="col-12">
                    <div class="opps" style="width: 600px;">
                        <div class="opps-header">
                            <div class="opps-reminder">Ficha digital. No es necesario imprimir.</div>
                            <div class="opps-info">
                                <div class="opps-brand"><img src="' . asset('images/oxxopay_brand.png') . '" alt="OXXOPay"></div>
                                <div class="opps-ammount">
                                    <h3>Monto a pagar</h3>
                                    <h2>$ <span class="money">' . ($pago->amount / 100) . '</span> <sup>' . $pago->currency . '</sup></h2>
                                    <p>OXXO cobrar&aacute; una comisi&oacute;n adicional al momento de realizar el pago.</p>
                                </div>
                            </div>
                            <div class="opps-reference">
                                <h3>Referencia</h3>
                                <h1>' . $pago->charges[0]->payment_method->reference . '</h1>
                            </div>
                        </div>
                        <div class="opps-instructions">
                            <h3>Instrucciones</h3>
                            <ol>
                                <li>Acude a la tienda OXXO m&aacute;s cercana. <a href="https://www.google.com.mx/maps/search/oxxo/" target="_blank">Encu&eacute;ntrala aqu&iacute;</a>.</li>
                                <li>Indica en caja que quieres realizar un pago de <strong>OXXOPay</strong>.</li>
                                <li>Dicta al cajero el n&uacute;mero de referencia en esta ficha para que tecle&eacute; directamete en la pantalla de venta.</li>
                                <li>Realiza el pago correspondiente con dinero en efectivo.</li>
                                <li>Al confirmar tu pago, el cajero te entregar&aacute; un comprobante impreso. <strong>En el podr&aacute;s verificar que se haya realizado correctamente.</strong> Conserva este comprobante de pago.</li>
                            </ol>
                            <div class="opps-footnote">
                                <p class="mr-1 ml-1 mb-0">Al completar estos pasos recibir&aacute;s un correo de <strong>ServiClick</strong> confirmando tu pago.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            return $contenido;
        } else {
            return "Sin ficha disponible por el momento";
        }
    }

    public function Generar() //Usar esto solo para pruebas en local

    {
        setlocale(LC_ALL, 'es_ES');
        $negocios = Negocios::where([['pagos', true], ['status', true]])->get();
        foreach ($negocios as $item) {
            if (!is_null($item->omision)) {
                if ($item->omision <= now()) {
                    //Generarle su primer ficha
                    $this->Orden($item->id);
                    //Enviar notificacion al telefono
                    if ($item->Usuario) {
                        if (count($item->Usuario->FirebaseTokens)) {
                            $notificacion = Tipos_Notificaciones::findOrFail(9);
                            $this->firebase->enviar_ahora($notificacion->tipo, $notificacion->mensaje, $item->Usuario->FirebaseTokens->pluck("token"), true);
                        }
                    }
                    //Quitar el periodo de omisión
                    Negocios::where('id', $item->id)->update(['omision' => null]);
                }
            } else {
                $ultimo_pago = DetallesPagos::select('proximo')
                    ->where('negocio', $item->id)
                    ->orderby('detalles_pagos.id', 'desc')
                    ->take(1)->get();

                if ($ultimo_pago[0]->proximo <= now()) {
                    $this->Orden($item->id);
                    //Enviar notificacion al telefono
                    if ($item->Usuario) {
                        if (count($item->Usuario->FirebaseTokens)) {
                            $notificacion = Tipos_Notificaciones::findOrFail(9);
                            $this->firebase->enviar_ahora($notificacion->tipo, $notificacion->mensaje, $item->Usuario->FirebaseTokens->pluck("token"), true);
                        }
                    }
                }
            }
        }
        return "Ejecucion completada correctamente";
    }

    public function status_negocio(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required',
        ],
            [
                'id.required' => 'Proporcione el ID del negocio',
                'status.required' => 'Indique el status que desea asignar',
            ]);

        Negocios::where('id', $request->id)->update(['status' => $request->status]);

        if ($request->status) {
            return "activo";
        } else {
            return "inactivo";
        }
    }

    public function exonerar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'meses' => 'required',
        ],
            [
                'id.required' => 'Proporcione el ID del negocio',
                'meses.required' => 'Indique los meses de exoneración que desea asignar',
            ]);

        //Verificar donde exonerar el pago
        //(en omision para negocios que cuenten con meses gratis)
        $pago = DetallesPagos::where('negocio', $request->id)->orderby('created_at', 'desc')->take(1)->get();
        //Si no tiene pagos, esta en periodo gratuito
        if (count($pago) == 0) {
            //Aumentar los meses gratis
            $negocio = Negocios::find($request->id);
            $exoneracion = Carbon::parse($negocio->omision)->addMonths($request->meses);
            $negocio->omision = $exoneracion;
            $negocio->save();
            $mensaje = "Exoneración aplicada con exito";
        } else {
            //Si el proximo pago es menor a la fecha actual.
            //no se puede exonerar el pago (cuenta con ficha o se le va a generar una)
            if ($pago[0]->proximo <= now()) {
                $mensaje = "No disponible durante el periodo de generación de fichas";
            } else {
                //Verificar el status del pago.
                //para ver si se puede exonerar el proximo
                $pago = Pagos::where('OrderID', $pago[0]->pago)->get();
                if (is_null($pago[0]->OrderUpdateDate)) {
                    $mensaje = "No disponible cuando se debe pago de ficha del mes en curso";
                } else {
                    $detalles = DetallesPagos::where('pago', $pago[0]->OrderID)->first();
                    $exoneracion = Carbon::parse($detalles->proximo)->addMonths($request->meses);
                    $detalles->proximo = $exoneracion;
                    $detalles->save();
                    $mensaje = "Exoneración aplicada con exito";
                }
            }
        }

        return response()->json([
            'status' => true,
            'text' => $mensaje,
            'load' => true,
            'url' => route('control_sc_negocios'),
        ], 200);
    }

    public function Orden($negocio_actual)
    {
        $cobrar = Precios::find(1);
        $order = $this->ConektaPayment->CrearOrden($cobrar->nombre, $cobrar->costo, 1, $cobrar->moneda, $negocio_actual);

        $InsertData["OrderPaymentMethodID"] = $order["OrderID"];
        $InsertData["OrderDate"] = date("Y-m-d H:i:s");
        $InsertData["OrderUpdateDate"] = null;
        $InsertData["OrderPaymentMethod"] = "conekta.oxxo";

        $InsertData["OrderTotal"] = 0;
        $InsertData["OrderTotalItems"] = 0;

        foreach ($order["items"] as $item) {
            $InsertData["OrderTotal"] += $item["unit_price"];
            $InsertData["OrderTotalItems"]++;
        }

        $InsertData["OrderCurrency"] = $order["Currency"];
        $InsertData["OrderStatus"] = $order["OrderStatus"];

        $order_id = Pagos::create($InsertData);

        setlocale(LC_ALL, 'es_ES');
        foreach ($order["items"] as $item) {
            $Insert["pago"] = $order_id->id;
            $Insert["negocio"] = $negocio_actual;
            $Insert["proximo"] = (new \DateTime())->add(new \DateInterval('P1M'));
            DetallesPagos::create($Insert);
        }

        $code = 200;

        if (isset($order["code"])) {
            $code = 400;
        }

        $negocio = User::where('negocio', $negocio_actual)->first();
        try {
            Mail::to($negocio->email)->send(new FichaDePago($order["Total"], $order["Currency"], $order["Reference"]));
        } catch (\Exception $ex) {}
        return response()->json($order, $code);
    }
}
