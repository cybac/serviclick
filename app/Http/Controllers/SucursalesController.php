<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use Illuminate\Http\Request;
use App\Models\Sucursales;

class SucursalesController extends Controller
{
    public function datatable(Request $request)
    {
        $sucursales = Sucursales::all();
        return DataTables::of($sucursales)
        ->addColumn('btn', function($sucursales){
            return "<a class='btn btn-success text-white p-3' href='".route('editar_sucursal',$sucursales->id)."' style='width: 100%; margin-bottom: 10px;'>Editar</a><br>
            <button class='btn btn-danger delete p-3' data-url='".route('eliminar_sucursal',$sucursales->id)."' style='width: 100%; margin-bottom: 10px;'>Eliminar</button>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('forms.sucursales',[
            'form_edit'     =>false,
            'menu'          =>'web',
            'submenu'       =>'sucursales'
        ]);
    }

    public function editar($id_sucursal)
    {
        $sucursales = Sucursales::findorfail($id_sucursal);
        return view('forms.sucursales',[
            'form_edit'     =>true,
            'menu'          =>'web',
            'submenu'       =>'sucursales',
            'sucursal'      =>$sucursales
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'direccion' =>'required',
            'telefono'  =>'required',
            'correo'    =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el nombre de la sucursal',
            'direccion.required'    =>'Proporcione la dirección de la sucursal',
            'telefono.required'     =>'Proporcione un número de contacto para la sucursal',
            'correo.required'       =>'Proporcione un correo de contacto para la sucursal'
        ]);
        $telefono = substr($request->telefono, 0, 3)." ".substr($request->telefono, 3, 3)." ".substr($request->telefono, 6, 2)." ".substr($request->telefono, 8, 2);
        $sucursal = Sucursales::create([
            'nombre'        => $request->nombre,
            'direccion'     => $request->direccion,
            'telefono'      => $telefono,
            'correo'        => $request->correo
        ]);

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha dado de alta una nueva sucursal',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_sucursales')
        ], 200);
    }

    public function actualizar(Request $request, $id_sucursal)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'direccion' =>'required',
            'telefono'  =>'required',
            'correo'    =>'required'
        ],
        [
            'nombre.required'       =>'Proporcione el nombre de la sucursal',
            'direccion.required'    =>'Proporcione la dirección de la sucursal',
            'telefono.required'     =>'Proporcione un número de contacto para la sucursal',
            'correo.required'       =>'Proporcione un correo de contacto para la sucursal'
        ]);
        $telefono = substr($request->telefono, 0, 3)." ".substr($request->telefono, 3, 3)." ".substr($request->telefono, 6, 2)." ".substr($request->telefono, 8, 2);
        $sucursal = Sucursales::find($id_sucursal);
        $sucursal->nombre = $request->nombre;
        $sucursal->direccion = $request->direccion;
        $sucursal->telefono = $telefono;
        $sucursal->correo = $request->correo;
        $sucursal->save();

        return response()->json([
            'status'=>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado la sucursal',
            'load'  =>  true,
            'url'   =>  route('control_sc_web_sucursales')
        ], 200);
    }

    public function eliminar($id_sucursal)
    {
        $sucursal = Sucursales::find($id_sucursal);
        $sucursal->delete();
        
        return response()->json([
            'status'    =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado la sucursal',
            'dttable'   =>  "#dt_sucursales",
        ], 200);
    }
}
