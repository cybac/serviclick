<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FichaDePago extends Mailable
{
    use Queueable, SerializesModels;

    public $monto, $moneda, $referencia;

    public function __construct($importe, $moneda, $referencia)
    {
        $this->monto = $importe;
        $this->moneda = $moneda;
        $this->referencia = $referencia;
    }

    public function build()
    {
        return $this->view('mails.pago');
    }
}
