<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notificacion extends Mailable
{
    use Queueable, SerializesModels;

    public $titulo, $mensaje;

    public function __construct($titulo, $contenido)
    {
        $this->titulo = $titulo;
        $this->mensaje = $contenido;
    }

    public function build()
    {
        return $this->view('mails.notificacion');
    }
}
