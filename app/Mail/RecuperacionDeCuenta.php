<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperacionDeCuenta extends Mailable
{
    use Queueable, SerializesModels;

    public $recuperacion;

    public function __construct($datos)
    {
        $this->recuperacion = $datos;
    }

    public function build()
    {
        return $this->view('mails.recuperacion');
    }
}
