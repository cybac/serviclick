<?php

namespace App\Conekta;

use Conekta\Conekta;
use App\Models\User;

class ConektaPayment
{
    function __construct()
    {
        Conekta::setApiKey(env('CONEKTA_SECRET'));
        Conekta::setApiVersion(env('CONEKTA_API_VERSION'));
    }

    function CrearOrden($producto, $precio, $cantidad, $moneda, $negocio)
    {
        $usuario = User::select('users.name', 'users.email', 'negocios.telefono')
            ->join('negocios','users.negocio', '=', 'negocios.id')
            ->where('users.negocio', $negocio)
            ->first();
        
        $expires_at = (new \DateTime())->add(new \DateInterval('P5D'))->getTimestamp();
        
        $orden["line_items"][0]["name"]                         = $producto;
        $orden["line_items"][0]["unit_price"]                   = $precio;
        $orden["line_items"][0]["quantity"]                     = $cantidad;
        $orden["shipping_lines"][0]["amount"]                   = 0;                        //Agregar para compras físicas
        $orden["shipping_lines"][0]["carrier"]                  = "FEDEX";                  //Agregar para compras físicas
        $orden["currency"]                                      = $moneda;
        $orden["customer_info"]["name"]                         = $usuario->name;
        $orden["customer_info"]["email"]                        = $usuario->email;
        $orden["customer_info"]["phone"]                        = $usuario->telefono;
        $orden["shipping_contact"]["address"]["street1"]        = "Av Central Calle Central";   //Agregar para compras físicas
        $orden["shipping_contact"]["address"]["postal_code"]    = "29000";                      //Agregar para compras físicas
        $orden["shipping_contact"]["address"]["country"]        = "MX";                         //Agregar para compras físicas
        $orden["charges"][0]["payment_method"]["type"]          = "oxxo_cash";
        $orden["charges"][0]["payment_method"]["expires_at"]    = $expires_at;

        try {
            $conekta_orden = \Conekta\Order::create($orden);
            if(isset($conekta_orden->id))
            {
                return $this->Detalles($conekta_orden);
            }
        }
        catch (\Conekta\ParameterValidationError $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
        catch (\Conekta\Handler $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
    }

    function ObtenerOrden($id_orden)
    {
        return  \Conekta\Order::find($id_orden);
    }

    function Detalles($orden)
    {
        //Guardar detalle de la orden en la base de datos
        $stub["OrderID"]        = $orden->id;
        $stub["OrderStatus"]    = $orden->payment_status;
        $stub["items"]          = $orden->line_items;
        $stub["Total"]          = $orden->amount;
        $stub["Currency"]       = $orden->currency;
        $stub["Reference"]      = $orden->charges[0]->payment_method->reference;
        return $stub;
    }

    public function CrearPlan($ID, $nombre, $monto, $moneda, $periodo, $frecuencia, $dias_prueba, $expira)
    {
        try{
            $plan = \Conekta\Plan::create([
                'id'                => $ID,
                'name'              => $nombre,
                'amount'            => $monto,
                'currency'          => $moneda,
                'interval'          => $periodo,
                'frequency'         => $frecuencia,
                'trial_period_days' => $dias_prueba,
                'expiry_count'      => $expira
            ]);
            return $plan;
        }
        catch (\Conekta\ParameterValidationError $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
        catch (\Conekta\Handler $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
    }

    public function CrearCliente($nombre, $email, $tel)
    {
        try{
            $customer = \Conekta\Customer::create([
                'name'              => $nombre,
                'email'             => $email,
                'phone'             => $tel,
                'payment_sources'   => [['type' => "oxxo_recurrent"]]
            ]);
            return $customer;
        }
        catch (\Conekta\ParameterValidationError $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
        catch (\Conekta\Handler $error)
        {
            $err["code"]    = $error->getCode();
            $err["message"] = $error->getMessage();
            return $err;
        }
    }

    public function ObtenerCliente($id_cliente)
    {
        return  \Conekta\Customer::find($id_cliente);
    }
}
