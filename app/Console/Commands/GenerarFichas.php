<?php

namespace App\Console\Commands;

use App\Conekta\ConektaPayment;
use App\Firebase\FirebaseNotificacion;
use App\Mail\FichaDePago;
use App\Models\DetallesPagos;
use App\Models\Negocios;
use App\Models\Pagos;
use App\Models\Precios;
use App\Models\Tipos_Notificaciones;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class GenerarFichas extends Command
{
    protected $signature = 'fichas:generar';
    protected $description = 'Generamos fichas nuevas para los negocios, si son requeridas';
    protected $ConektaPayment;
    protected $firebase;

    public function __construct()
    {
        $this->ConektaPayment = new ConektaPayment();
        $this->firebase = new FirebaseNotificacion();
        parent::__construct();
    }

    public function handle()
    {
        setlocale(LC_ALL, 'es_ES');
        $negocios = Negocios::where([['pagos', true], ['status', true]])->get();
        foreach ($negocios as $item) {
            try {
                if (!is_null($item->omision)) {
                    if ($item->omision < now()) {
                        //Generarle su primer ficha y quitarle el periodo de omisión
                        $this->Orden($item->id);
                        //Enviar notificacion al telefono
                        if ($item->Usuario) {
                            if (count($item->Usuario->FirebaseTokens)) {
                                $notificacion = Tipos_Notificaciones::findOrFail(9);
                                $this->firebase->enviar_ahora($notificacion->tipo, $notificacion->mensaje, $item->Usuario->FirebaseTokens->pluck("token"), true);
                            }
                        }
                        Negocios::where('id', $item->id)->update(['omision' => null]);
                    }
                } else {
                    $ultimo_pago = DetallesPagos::select('proximo')
                        ->where('negocio', $item->id)
                        ->orderby('detalles_pagos.id', 'desc')
                        ->firstOrFail();
                    if ($ultimo_pago->proximo <= now()) {
                        $this->Orden($item->id);
                        //Enviar notificacion al telefono
                        if ($item->Usuario) {
                            if (count($item->Usuario->FirebaseTokens)) {
                                $notificacion = Tipos_Notificaciones::findOrFail(9);
                                $this->firebase->enviar_ahora($notificacion->tipo, $notificacion->mensaje, $item->Usuario->FirebaseTokens->pluck("token"), true);
                            }
                        }
                    }
                }
            } catch (\Exception $ex) {}
        }
        return "Termino Ejecucion";
    }

    public function Orden($negocio_actual)
    {
        $cobrar = Precios::findOrFail(1);
        $order = $this->ConektaPayment->CrearOrden($cobrar->nombre, $cobrar->costo, 1, $cobrar->moneda, $negocio_actual);

        $InsertData["OrderPaymentMethodID"] = $order["OrderID"];
        $InsertData["OrderDate"] = date("Y-m-d H:i:s");
        $InsertData["OrderUpdateDate"] = null;
        $InsertData["OrderPaymentMethod"] = "conekta.oxxo";

        $InsertData["OrderTotal"] = 0;
        $InsertData["OrderTotalItems"] = 0;

        foreach ($order["items"] as $item) {
            $InsertData["OrderTotal"] += $item["unit_price"];
            $InsertData["OrderTotalItems"]++;
        }

        $InsertData["OrderCurrency"] = $order["Currency"];
        $InsertData["OrderStatus"] = $order["OrderStatus"];

        $order_id = Pagos::create($InsertData);

        setlocale(LC_ALL, 'es_ES');
        foreach ($order["items"] as $item) {
            $Insert["pago"] = $order_id->id;
            $Insert["negocio"] = $negocio_actual;
            $Insert["proximo"] = (new \DateTime())->add(new \DateInterval('P1M'));
            DetallesPagos::create($Insert);
        }

        $negocio = User::where('negocio', $negocio_actual)->first();
        try {
            Mail::to($negocio->email)->send(new FichaDePago($order["Total"], $order["Currency"], $order["Reference"]));
        } catch (\Exception $ex) {}
    }
}
