<?php

namespace App\Firebase;

use Illuminate\Support\Facades\Http;

class FirebaseNotificacion
{
    private $url;

    public function __construct()
    {
        $this->url = env("FIREBASE_URL", null);
    }

    public function enviar_ahora($titulo, $mensaje, $destinatario, $broadcast = false)
    {
        try {
            if ($broadcast) { //Mensaje para varios usuarios
                Http::withHeaders([
                    "Authorization" => "key=" . env("FIREBASE_KEY", null),
                    "Content-Type" => "application/json",
                ])->post($this->url, [
                    "registration_ids" => $destinatario,
                    "notification" => [
                        "title" => $titulo,
                        "body" => $mensaje,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                    "data" => [
                        "scheduled" => false,
                        "title" => $titulo,
                        "body" => $mensaje,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                ]);
                return true;
            } else { //Mensaje para un usuario
                Http::withHeaders([
                    "Authorization" => "key=" . env("FIREBASE_KEY", null),
                    "Content-Type" => "application/json",
                ])->post($this->url, [
                    "to" => $destinatario,
                    "notification" => [
                        "title" => $titulo,
                        "body" => $mensaje,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                    "data" => [
                        "scheduled" => false,
                        "title" => $titulo,
                        "body" => $mensaje,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                ]);
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function programar_envio($titulo, $mensaje, $destinatario, $fecha, $id)
    {
        try { //Mensaje programado para varios usuarios
            Http::withHeaders([
                "Authorization" => "key=" . env("FIREBASE_KEY", null),
                "Content-Type" => "application/json",
            ])->post($this->url, [
                "registration_ids" => $destinatario,
                "data" => [
                    "schedule" => true,
                    "id" => $id,
                    "title" => $titulo,
                    "body" => $mensaje,
                    "content_available" => true,
                    "priority" => "high",
                    "date" => $fecha,
                ],
            ]);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function eliminar($destinatario, $id)
    {
        try { //Mensaje programado para varios usuarios
            Http::withHeaders([
                "Authorization" => "key=" . env("FIREBASE_KEY", null),
                "Content-Type" => "application/json",
            ])->post($this->url, [
                "registration_ids" => $destinatario,
                "data" => [
                    "schedule" => false,
                    "id" => $id,
                ],
            ]);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function enviar_web($titulo, $mensaje, $id, $seccion, $destinatario, $broadcast = false)
    {
        try {
            if ($broadcast) { //Mensaje para varios usuarios (web)
                Http::withHeaders([
                    "Authorization" => "key=" . env("FIREBASE_KEY", null),
                    "Content-Type" => "application/json",
                ])->post($this->url, [
                    "registration_ids" => $destinatario,
                    "data" => [
                        "title" => $titulo,
                        "body" => $mensaje,
                        "id" => $id,
                        "seccion" => $seccion,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                ]);
                return true;
            } else { //Mensaje para un usuario (web)
                Http::withHeaders([
                    "Authorization" => "key=" . env("FIREBASE_KEY", null),
                    "Content-Type" => "application/json",
                ])->post($this->url, [
                    "to" => $destinatario,
                    "data" => [
                        "title" => $titulo,
                        "body" => $mensaje,
                        "id" => $id,
                        "seccion" => $seccion,
                        "content_available" => true,
                        "priority" => "high",
                    ],
                ]);
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }
}
