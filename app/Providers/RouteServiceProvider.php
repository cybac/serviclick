<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Symfony\Component\Finder\Finder;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';
    
    public const Cliente = '/';
    public const Negocio = '/panel';
    public const Admin = '/control_sc';

    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(function(){
                    $this->requireRoutes('routes/api');
                });

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(function(){
                    $this->requireRoutes('routes/web');
                });
        });
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }

    public function requireRoutes($path)
    {
        return collect(Finder::create()->in(base_path($path))->name('*.php'))->each(function($file){
            require $file->getRealPath();
        });
    }
}
