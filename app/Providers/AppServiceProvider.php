<?php

namespace App\Providers;

use App\Models\Alertas;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        if (! $this->app->runningInConsole()) {
            $alertas = Alertas::where('leido', false)->orderby('created_at', 'asc')->get();
            view()->share('alertas', $alertas);
        }
    }
}
