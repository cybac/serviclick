<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Restablecer contraseña')
                    ->greeting('Hola')
                    ->line('Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.')
                    ->action('Restablecer contraseña', url(config('app.url').route('password.reset', ['token' => $this->token, 'email' => $notifiable->getEmailForPasswordReset()], false)))
                    ->line('Este enlace solo es válido dentro de los proximos '.config('auth.passwords.users.expire').' minutos.')
                    ->line('Si no realizaste esta solicitud, ignora este correo.')
                    ->salutation('Saludos, '. config('app.name'));
    }
}
